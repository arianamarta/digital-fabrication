# My Fab Doc Website

You can see my repository [here](https://gitlab.com/arianamarta/digital-fabrication/).

You can see my website [here](https://arianamarta.gitlab.io/digital-fabrication).

My website and final project is licensed under the CERN Open Hardware License v2.

You can find the full license text [here](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2) or [here](https://choosealicense.com/licenses/cern-ohl-p-2.0/).
