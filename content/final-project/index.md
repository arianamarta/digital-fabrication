---
title: Final Project
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
featured_image: '/images/brainstorming.jpg'
weight: 20
menu:
  main:
    weight: 1
---

Wellcome to my final project webpage where you can follow all the steps and challenges i faced. Hope you enjoy the ride and have a lovely reading.

You can see the video of the project working [Here](https://www.youtube.com/watch?v=mC_3RLsOids).

Table of content:

- **Ideas:**
* [First Ideas](#first-ideas)
* [More Ideas and Final Decision](#more-ideas-and-final-decision)

- **Start the project:**
* [Wifi Communication Between 2 XIAO Boards](#wifi-communication-between-2-xiao-boards)
* [Design and Do Input Box](#design-and-do-input-box)
* [Design and Do Output Brain](#design-and-do-output-brain)
* [Combine Everthing](#combine-everthing)

# First Ideas

In the very beginning of the corse we were asked to start thinking about the final project and what we would like to do. 

We had to create this final project page and describe our ideas.

I took myself a night to brainstorm some ideas for the final project and i end up having more ideas than what i was expecting to.

Here's a list of all of them:

1. [**Cup scaler for baking**](#cup-scaler-for-baking)
2. [**Interactive board with voice recognition**](#interactive-board-with-voice-recognition)
3. [**Analogue speaker with lights**](#analogue-speaker-with-lights)
4. [**Device that records voice into text**](#device-that-records-voice-into-text)

-----

## Cup scaler for baking

A couple of years ago, when I turned vegan, I started becoming very interested for cooking, baking and nutrition in general. 

As a Portuguese person we tend to use the minimal resources to spend the minimal amount of money and thus i never bought those fancy measuring cups for baking. 

Every time was a challenge to convert cups to grams or litters. 

Baking is science and thus all the mesurmants have to be presice otherwise the cake doesnt get as flufly, taller or stratchy as it should.

Having this in mind I come up with this idea of building a cup (with standard measurments) which is equivelant to 120 grams.

The cup would have a scale to also measure the grams and/or litters of the content inside.

The cup would have some interface to choose to convert to grams or litters and also a display where we can read the quantity in grams or litters of the content inside the cup.

Following is a sketch how it would look like:

{{<image src="images/cup.jpg" alt="Cup sketch." >}}

-----

## Interactive board with voice recognition

I'm a ART lover. I've always wanted to proceed Arts but, unfortunatly, in Potugal is not an option. 

I took a look to some old projects and they all seam to have some artistic parts to it. Therefore i new i had to come up with something similiar.

I always struggled to decorate my room so i thought it would be nice to have a paint there.

With that in mind I though it would be so cool to have an interactive painting board to decorate the room.

The board would be a display to show images. People would press a botton from the side and say something they wanted to have painted for example "Dog in the woods" and the board wpould have a voice recognition sensor to convert the voice into words and afterwards it would be connected to those AI websites that convert sentances into images which would later happear on the display.

Following is a sketch how it would look like:

{{<image src="images/canvas.jpg" alt="Cup sketch." >}}

-----

## Analogue speaker with lights

Once again as a portuguese person we dont have to much power to afford expensive stuff and thus i could never have my own speaker. 

I do lots of hang out at my place and having a speaker to put some background music is essential. 

With that in mind i though to build a well designed analogue speaker with some dynamic lights accordigly to the bit of the song.

Following there's two sketchs of how the speaker can look like.

I'm still deciding if i put the led outside or if i just pu them inside and make nice pattern on the outside for the light to come to the outside. I will most likely choose this last option.

First image has a round design and the second a square design. 

I didn't study yet what is be the best design for a better quality of sound but im more inclined for the round option.

{{<image src="images/speaker1.jpg" alt="Round shape." >}}

{{<image src="images/speaker2.jpg" alt="Square shape." >}}

----

## Device that records voice into text

I normally have ideas in the most uncomfortable situations!

Or eaither im ouside and its supper cold to type huge amount of text to my note book, or either im in bed with the lights and eyes closed just brainstorming with myself.

In all those uncomfortable situations i dont feel like grabbing my phone or pen and papper to write everything down. Doing a record message its also not ideal because then i have to hear a really long voice message so i can get the idea back again.

With this in mind I thought it woul be nice to build a really light and cool portable device where i could just record my ideas and they would be converted into text directly to my cloud for example.

For this one i didn't make a sketch since i don't plan to proceed with this idea. 

-----

# More Ideas and Final Decision

Guess what! I had another night of brainstorming instead of sleeping. I come up once again with more ideas that im defiantly more excited about.

My main inspiration for this final project came from our professor. Once he shown us an exhibition he made where there were fake trees in the middle of the room where some visuals were being projected on them. On top of the visuals there were also some audio. The way piece of art worked was, the more people were in the room the visuals were more reddish and the sound more dark and heavy. But when there were barely people or no people in the room the visuals would be bright and green and we cold hear sound of birds and nature. 

My interpretation from this exhibition/ the massage the author transmitted to me was about climate change and the bad influence the human presence has on the nature. 

When I saw this I immediately realise that I wanted my final project to be an exhibition about an important topic to me and to the society. I want my final project to have a meaning to me and possibly touch the others. Therefore im very much motivated now to conclude this project.

At this stage I know I want to do an exhibition where I can share an important message to the visitors. But now the problem is what? 

I start thinking about important topics to me and the ideas start appearing afterwards.

Important topics worth spreading the word are:

* [**Climate change or human influence on the nature and ecosystem**](#climate-change-or-human-influence-on-the-nature-and-ecosystem)
* [**Mental health**](#mental-health)
* [**Sustainability**](#sustainability)

And much more…

* [**Conclusion**](#conclusion)

I come up with ideas for all those three topics. All the ideas have the same structure. There’s some kind of human interaction input that will trigger an output

The hard part now will be picking one of them to focus on.

## Climate change or human influence on the nature and ecosystem

In the exhibition there would be as human input a pot with a plant. Then the human would have to water the plant and I would have a sensor measuring the humidity, or water level or something like this. Accordingly to that act of watering the plant (with means the human is showing care for the nature) an output will be trigger. In this case the output is an heart made with dragon skin material that will start beeping for a certain time. It would be nice if I could put some light on the inside of the heart.

The moral of story is that by taking care of the nature we all can breathe on this nature. Not just the plant but also all the living beings living on earth.

(As output I could either do the heart or a bird getting out of a box or something as if he was hidden because the nature outside is dangerous and unbreathable.)

{{<image src="images/climate.jpg" alt="Square shape." >}}

## Mental health

In the exhibition there would be, as human input, a box with good and bad words/fellings/moods where the human would have to press the word. Then accordingly to what the human choose, if bad or good word, a different output would be triggered. 

The output would be once again dragon skin material with a shape of a brain. If the selected word was bad, the brain would deflate and bad sounds could be played (like a thunder strake). When we are bad mentally we feel tired, heavy, empty, sad, dark, tense, etc. 

But, if a good word would be selected, the brain would be with the normal shape, and with some yellow/white LEDs light on (the LEDs could be on a flexible circuit board that would go around the shape of the brain or something)  and there could also be some happy, ambiance, comfortable sound touch has the nature sound with birds etc. The we are good mentally we feel full, motivated, bright, hopeful, excited, happy, calm and relax, etc.

The box and the brain could be connected through wifi. The box could send a POST requests about what the desire output the brain should have.

{{<image src="images/mental.jpg" alt="Square shape." >}}

## Sustainability

In the exhibition there would be, as human input, several trash bins for recycling (or just one, have to think about this). Then the human, when recycling (throwing something to the trash bin), it would output something. 

There’s to options for this:

1. I could 3D print a big egg (kinda like Harry Potter movie) where inside there would be a plant. So, when the sensor on the bin would trigger, the egg would open mysteriously. There should be yellow LEDs to look like something magical/gold and then inside there would be the plant. (For this I would have to have some kind of motors or just some magnets or something.)

2. Or, the other option, is inspired on the movie “Wally”. Where I would make a box (Wally) and then when the input triggers I would open the box (same way as wally) and inside there would be a boot with a plant inside (just like the movie) or just a plant. There should also be white LEDs inside for the mystery. The mechanics of this door would also be a motor . 

The purpose of this exhibition is to incentive people to be more sustainable by showing a leaving being (plant) as consequence of the sustainable act. By having this acts on our day live we help the nature.

{{<image src="images/sust.jpg" alt="Square shape." >}}

## Conclusion

I was always more towards the first two ideas but after some reflection i decided to go with the second idea about mental health.

I choose this idea because I will be using almost all the machines I learn along the course which is so cool. I will use, 3D design and printing, lazer cutting (and maybe CNC if the machine is working properly), molding and casting, electronics design and board milling, coding in Arduino where I will even use wifi connection between two boards instead of just using jumper cables.

Its gonna be so much fun and a very exciting journey for sure. Can’t wait to see the finish prototype.

# Lets start the PROJECT !!!

I first started the project with the wifi connection since i was doing the week 15 (network and communication) and week 16 (Interface and Application Programming) at the same time.

# Wifi Communication Between 2 XIAO Boards

I want to communicate the input box with the brain using two XIAO ESP32 boards. One of them will be the hotspot and the other one will be a device that will be connected to that access point. I found this [information](https://randomnerdtutorials.com/esp32-access-point-ap-web-server/) where I understood more about the communication I was looking for.

In the tutorial they say the following:

“In this situation the router acts as an access point and the ESP32 is set as a station. In this scenario, you need to be connected to your 
router (local network) to control the ESP32.”
“But if you set the ESP32 as an access point (hotspot), you can be connected to the ESP32 using any device with Wi-Fi capabilities without 
the need to connect to your router.”

Conclusion:
“In simple words, when you set the ESP32 as an access point you create its own Wi-Fi network and nearby Wi-Fi devices (stations) can connect to it (like your smartphone or your computer).” 

And this is exactly what I want.

Therefore, what I did was, combine what I learn from this tutorial with what I learn from the professor on the “Interface and Application Programming” week.

I first created an Access Point and I tested if it worked. This will be done on the server side and will be placed on the brain. Following is the code and a video showing it working:


{{< highlight go-html-template >}}
#include "WiFi.h"

// To create an access point connection
const char* ssid     = "Mentally";
const char* password = "confused";

// Set web server port number to 80
WiFiServer server(80);

// Setting up our webserver
//AsyncWebServer server(webServerPort);

void setup()
{
  Serial.begin(115200);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Setting AP (Access Point)…");
  WiFi.softAP(ssid, password);

  Serial.print("Host IP:");
  Serial.println(WiFi.softAPIP());

  server.begin();
}

void loop()
{
  WiFiClient client = server.available();   // Listen for incoming clients

  if(client){
    Serial.println("New Cliente");
    Serial.print("Cliente local IP address: ");
    Serial.println(client.localIP());
    Serial.print("Access point IP address: ");
    Serial.println(WiFi.softAPIP());
    Serial.print("Wifi local IP address: ");
    Serial.println(WiFi.softAPIP());
    Serial.print("Number of connections:");
    Serial.println(WiFi.softAPgetStationNum());
  }

  client.stop();
  delay(1000);
}
{{< /highlight >}}

{{<video src="images/test_ap_connection_v.mp4">}} Test creation of AP.{{</video>}}

Then I made the client side which will be placed on the input box. I first started by making sure the client would easily be connected with the Access point I just created. Everything worked fine after some time learning on the internet. It turn out not to be as hard as I thought. 

Following is a video showing the client running in my laptop and the server running on the desktop computer. In the video you can see the client successfully connecting to the server through the Access Point I created. By succeeding on this I reach an important part to make this communication working. 

{{< youtube w6uEYGvueQ4  >}} 

Then, I tried to see if I could send GET requests to the server using the Access Point. I was working on this at the same time I was doing the week 15 “Networking and Communication” so I just used the simple code to access the rootpath. I used the “ESPAsyncWebServer” library to deal with the requests.

I first join the “Mentally” AP I created and I sent a GET request to the main rootpath and to the /trying rootpath to see how it worked and if it worked.

Following is two videos showing that

{{<video src="images/main_v.mp4">}} GET request to rootpath.{{</video>}}

{{<video src="images/trying_v.mp4">}} GET request to /trying rootpath.{{</video>}}

Then I wasn’t sure how the communication would work! If I had to do POST requestes or if GET request would be enough. 

I made some research on the internet and I understood how GET and POST request works and I found an example code I use as a guide and therefore I manage to make it work.

The code works as follow. Every time a button is pressed on the input box, the client will send a GET request to the server with the updated value for the “btn” parameter under /button rootpath.

The “btn” parameter will be the number of the button that was pressed. Accordingly to the number of the button that was pressed the server(brain) will read the value and turn on one of the LEDs accordingly.

Following is the code of the server:

{{< highlight go-html-template >}}

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char* ssid     = "Mentally";
const char* password = "confused"; 

int webServerPort = 80;
int GREEN = D8;
int RED = D9;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {
  pinMode(GREEN, OUTPUT);
  pinMode(RED, OUTPUT);

  Serial.begin(115200);

  // Setting the XIAO 1 as an access point
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    sendResponse(request,"Hello World!");
    Serial.print("Number of connections:" );
    Serial.println(WiFi.softAPgetStationNum());
  });

  server.on("/trying", HTTP_GET, [](AsyncWebServerRequest *request){
    sendResponse(request,"Accessing the /trying root path");
  });

  // check the buttom state on the FIXE board
  // FIXE board should make a GET request to the path /buttom with the following parameters:
  // btn=1
  // all path is: /buttom?btn=1 for example.
  server.on("/buttom", HTTP_GET, [](AsyncWebServerRequest *request){

    String msg;
    if (request->hasParam("btn")) {
      // The incoming params are Strings
      String param = request->getParam("btn")->value();
      // .. so we have to interpret or cast them
      if (param == "1"){ // good word
        Serial.println("Turning green LED on.");
        msg = "Good word turn green LED on sucessfuly.";
        digitalWrite(GREEN, HIGH);
        digitalWrite(RED, LOW);

        // Now i would control the LEDs and AIR pump
      }else if(param == "2"){ // bad word
        Serial.println("Turning red LED on.");
        msg = "Bad word turn red LED on sucessfuly.";
        digitalWrite(GREEN, LOW);
        digitalWrite(RED, HIGH);

      } else {
        msg = "No button provided.";
      }
    } else {
      msg = "No parameter provided.";
    }


    // in this case the response could be for the website i want to make notifying what happened and what people could do
    // I could also display on the board a smile or a message.
    sendResponse(request,msg);
  });

  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){ 
    
    String msg = "";
    if (request->hasParam("state") && request->hasParam("color")) {
      // The incoming params are Strings
      String state_param = request->getParam("state")->value();
      String color_param = request->getParam("color")->value();
      // .. so we have to interpret or cast them
      if (state_param == "on"){
        if (color_param == "green"){
          Serial.print("Turning green LED on.");
          msg += "Turning green LED on.";
          digitalWrite(GREEN, HIGH);
        } else if (color_param == "red"){
          Serial.print("Turning red LED on.");
          msg += "Turning red LED on.";
          digitalWrite(RED, HIGH);
        } else {
          Serial.print("No valid color.");
          msg += "No valid color.";
        }
      }else if(state_param == "off"){
        if (color_param == "green"){
          Serial.print("Turning green LED off.");
          msg += "Turning green LED off.";
          digitalWrite(GREEN, LOW);
        } else if (color_param == "red"){
          Serial.print("Turning red LED off.");
          msg += "Turning red LED off.";
          digitalWrite(RED, LOW);
        } else {
          Serial.print("No valid color.");
          msg += "No valid color.";
        }
      }else {
        Serial.print("No valid state.");
        msg += "No valid state.";
      }
    } else if (request->hasParam("state")){
      String state_param = request->getParam("state")->value();
      
      if (state_param == "on"){
        Serial.print("Turning LEDs on.");
        msg += "Turning LEDs on.";
        digitalWrite(GREEN, HIGH);
        digitalWrite(RED, HIGH);
      }else if(state_param == "off"){
        Serial.print("Turning LEDs off.");
        msg += "Turning LEDs off.";
        digitalWrite(GREEN, LOW);
        digitalWrite(RED, LOW);
      }else {
        Serial.print("No valid state.");
        msg += "No valid state.";
      }
    } else {
      Serial.print("No valid parameters.");
      msg += "No valid parameters.";
    }
    
    sendResponse(request, msg);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  // Start server
  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();

  //Serial.print("Number of connections:" );
  //Serial.println(WiFi.softAPgetStationNum());
} 

void loop() {
  //  Serial.print("Number of connections:" );
  //  Serial.println(WiFi.softAPgetStationNum());
  // I leave the Arduino loop function empty because the server handles the requests asynchronously.
}

{{< /highlight >}}

And this is the code for the client:

{{< highlight go-html-template >}}

#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "Mentally";
const char* password = "confused";

//Your IP address or domain name with URL path
String mainRootpath = "http://192.168.4.1/";
String buttomRootpath = "http://192.168.4.1/buttom";
String ledRootpath = "http://192.168.4.1/led";
String tryingRootpath = "http://192.168.4.1/trying";

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// define the Buttons and LEDs on the FIXE board
int buttonPin1 = D3;
int buttonPin2 = D4;
int red = D9;
int blue = D8;

String resp;

void setup() {
  Serial.begin(115200);
  
  // setup the input buttons and output LEDs
  pinMode(buttonPin1,INPUT_PULLUP);
  pinMode(buttonPin2,INPUT_PULLUP);
  pinMode(red,OUTPUT);  
  pinMode(blue,OUTPUT);
  digitalWrite(red, LOW);
  digitalWrite(blue, LOW);


  // starting Wifi connection
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");
}

void loop() {
  
  int btnState1 = digitalRead(buttonPin1);
  int btnState2 = digitalRead(buttonPin2);

  String btnNumber;
  String buttomRootpathAll;

   // Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED ){ 
      
      // define more numbers accordingly to how many will i do!!
      if (btnState1 == LOW) {

        btnNumber = "1";

        buttomRootpathAll += buttomRootpath + "?btn=" + btnNumber;
        resp = httpGETRequest(buttomRootpathAll);

        Serial.print("Response from the GET request: ");
        Serial.println(resp);
        Serial.println("");
        digitalWrite(red, HIGH);
        digitalWrite(blue, LOW);
        delay(200);
/*      
    I dont think this is needed !!!
        // wait for the server to process the new values
        // later send 
        delay(2000);
        buttomRootpathAll += buttomRootpath + "?btn=" + btnNumber + "&state=" + "off";
        httpGETRequest(buttomRootpathAll);
*/
      } else if (btnState2 == LOW) {

        btnNumber = "2";

        buttomRootpathAll += buttomRootpath + "?btn=" + btnNumber;
        resp = httpGETRequest(buttomRootpathAll);

        Serial.print("Response from the GET request: ");
        Serial.println(resp);
        Serial.println("");
        digitalWrite(blue, HIGH);
        digitalWrite(red, LOW);
        delay(200);
      }
    }
    else {
      Serial.println("WiFi Disconnected");
    }

  delay(100);
}

// instead of making a post reqest i should make a get request where i just send the new value like so : /buttom?btn=1&state=on
String httpGETRequest(String serverName) {
  WiFiClient client;
  HTTPClient http;
  String payload;

  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName.c_str());
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
 
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
    //Serial.println(payload);
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

{{< /highlight >}}

What I will do at the end is making 4 bad words and 5 good words and the server will just have two options accordingly to the buttons pressed. One option being deflat if its a bad word and turn the leds on if its a good word.

For the GET requests I use “HTTPClient” and “ESPAsyncWebServer” library.

Im so happy everything worked as expected!

{{<video src="images/final_client_server_v.mp4">}} Communication between client and server.{{</video>}}

# Design and Do Input Box

My idea for the input box is that i will have some buttons where every time we press them a light should be turn on. For that I will make use of what I learn from the molding and casting week and I will design small keys with transparent material such as dragon skin.

1. ### Design individual keys

First I will design an individual key in different ways. One with the numbers going in the material and another with the numbers going of the material. I’ll then put light and see which one looks the better. Following is the procedure:

I made the design of the individual keys in Fusion 360 and it looks like this:

{{<image src="images/key_1_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/key_1_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/key_2_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/key_2_2.jpg" alt="Square shape." size="40%" >}}

Then I 3D printed the keys

{{<image src="images/print_key_1.jpg" alt="Square shape." size="40%" >}}

And I pour the material inside them and I waited until it’s ready.

For the material I decided to use dragon skin material since it’s transparent. The final goal is to have light going through the material and therefore it has to be transparent.

I never used dragon skin before so I start by reading the datasheet:

{{<image src="images/datasheet_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/datasheet_2.jpg" alt="Square shape." size="40%" >}}

Where I took the following important information:

* Mixing 1A:1B 
* Pot life is 45 min (so we have time to go make coffee and drink it)
* Cure time is 7 hours at room temperature
* Mix well and thoroughly both part A and B individually and when combined 
* Vacuum degas the material for 2-3 minutes. 
* Pour the mixture in a single spot at the lowest point of the containment field

This procedure is basically the same as I did with the “Mold Star 15 Slow” material. Because this was already explained fully on the week 14 I didn’t took pictures form the whole procedure but just some parts. 

{{<image src="images/procedure_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/procedure_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/procedure_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/procedure_4.jpg" alt="Square shape." size="40%" >}}

Then I waited 7 hours and this is how they look. 

{{<image src="images/mold_key_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/mold_key_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/mold_key_3.jpg" alt="Square shape." size="40%" >}}

As you can see from the images above i made a beginners mistake of forgetting to mirror the numbers. Very dumb of my side, but once again this was a test piece and its the perfect time to make mistakes and learn with them. 

Another conclusion is that i will have to increase the size of the letters! They are very small and barely readable. I will print again with the number mirrored and bigger and then ill make a final decision.

{{<image src="images/key_again_1.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/key_again_2.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/key_again_3.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/key_again_4.jpg" alt="Square shape." size="40%" >}}

After milling the board I will test both keys and see which one behaves better with the light.

Meanwhile i have this board that has the same LEDs as the one I will be putting so i decided to see how thick the silicone keys have to be in order for the light to be visible through it.

Following are to video showing that I cant make the keys too high. I should make them less than 10mm high. Because of that I will have to think I will I insert the display in order to not be too tall. Because if I insert the display with the headers it will be too tall and therefore the keys would have to be too big. At the end of the second video you can see what I mean with the hight of the components. I found a why to insert the display with twoo bend headers that you can see later on the design of the box session.

{{< youtube TbimLsbyyOY >}}

{{< youtube bXUmurOtvog >}}

2. ### Do MUX library and KiCad

Next i tried to visualize how would i put all the electronic on the input box. The first idea is to make it in a wood box cut in the lazer cutter. But im still debating if I should just 3D printing it to make sure I fit all the components tightly in the box without they moving around the box.

At the same time I decided to start making the board and mill it because the design of the box will be depended on that anyway. 

I first starting by adding the XIAO board and all the LEDs and resistors I wanted until I realised I want more input and output pins than the capacite of the XIAO. I want to have 2x9 LEDs and 9 buttons, plus 4 pins for the OLED display I’m gonna add in the box. 

{{<image src="images/started.jpg" alt="Square shape." size="40%" >}}

When summing them all I readlized i need 9+9+4 = 22 pins and the XIAO only has 11 pins available discarding the GRND, VCC and 3V3 pins. Which gives a total of 14 pins on the XIAO board. 

When asking the professor what I could do he recommended to use a multiplexer. I’m disappointed I completely forgot about those! Of course that’s the way to go make so much sense. 

But of course I would have to face challenges along the way and this was the first one of many I will have.

I first search for the symbol and footprint of this MUX we have at the lab. I look it up on the internet with no success so then I ask the professor. He also couldn’t find it on the official KiCad library.

Therefore the only solution left is making my own library where I would include the symbol and footprint of this MUX. For one reason it’s nice I have to do this because I will be learning something new, which I love, but on the other hand this is just making me slow down the evolution of the project. If I keep having this unpredicted small problems I might not finish everything I want on time.

At the lab there were available two types of MUX, one 1:8 and one 1:16. I picked the 16 pin since I’ll be needing a lot of input/output pins.

I started by looking for the [datasheet](https://www.ti.com/lit/ds/symlink/mux36s16.pdf?ts=1684916170888&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FMUX36S16%252Fpart-details%252FMUX36S16IPWR) where I will take all the important information to make the Kicad Library. The professor show me very quick how to work with both the symbol library and the footprint one. Following is all the procedure to build the symbol library.

We first have to click the “Create Symbol Library” button 

{{<image src="images/symbol_library.jpg" alt="Square shape." size="40%" >}}

Then we decided to create a new library called “Ariana” where I will be including the MUX im gonna do. 

{{<image src="images/new_library_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/new_library_2.jpg" alt="Square shape." size="40%" >}}

Next, I look at the datasheet and I looked to the layout of the pins and which name, number and kind where they so I could start drawing the symbol.

{{<image src="images/data_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/data_2.jpg" alt="Square shape." size="40%" >}}

This Is how we add pins and the info we have to fill. Both the VCC and GROUND are input power. NC is unconnected.

{{<image src="images/add_pin.jpg" alt="Square shape." size="40%" >}}

I then ran the rules checker and everything looked good.

{{<image src="images/rules_1.jpg" alt="Square shape." size="40%" >}}

This is how it looks after adding all the pins. I have to make sure its centered.

{{<image src="images/symbol_done.jpg" alt="Square shape." size="40%" >}}

Next I create the footprint library.

{{<image src="images/footprint_library.jpg" alt="Square shape." size="40%" >}}

The initial part is the same as before. I create a new library called Ariana and I named the new element TSSOP_28. 

{{<image src="images/lib_foot.jpg" alt="Square shape." size="40%" >}}

I followed the professors instructions and I started adding pads. I added 28 which is the amount of pins the MUX has.

{{<image src="images/add_pad_foot.jpg" alt="Square shape." size="40%" >}}

Then I miss position them accordingly to the datasheet and making sure it’s centred.

I had troubles understand which one of the following were the correct measurements. 

{{<image src="images/which_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/which_2.jpg" alt="Square shape." size="40%" >}}

Turns out that there’s different sizes for the MUX so I grab the clipper and I measure the MUX I have at the lab. 

The correct is the second image. The important information is the distance between pads along the x-axis (5.60/2=2.80mm) and along y-axis (0.65mm)

Another thing I had to consider is that each pad is 1.55x0.25mm and we want to make pads as bigger as possible to be easier to solder. But we cant make it too thick because they have to be 0.65mm distante from each other. A safe distance to maintain between pads its at least 0.25mm so I just decided to make the pads 0.4 thick. Which its bigger than 0.25 but not too big.

On the image bellow you can see this safe distance im telling about

{{<image src="images/safe_dist.jpg" alt="Square shape." size="40%" >}}

Following is an example of x position, y position and the pad size respectively.

For the y-position I would always sum or subtract 0.65.

{{<image src="images/x_pos_foot.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/y_pos_foot.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/y_size_foot.jpg" alt="Square shape." size="40%" >}}

To finalize, it’s good practice to include a circular pad on the first pad so we can easily identify it. As well as this 2 lines that should be on the “Silkscreen” layer

{{<image src="images/circular_pad_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/circular_pad_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/circular_pad_3.jpg" alt="Square shape." size="40%" >}}

Then we should also move the “REF**” tag to the “Silkscreen” layer and make it visible. The rest of the tags can be left hidden as they are not important.

{{<image src="images/ref.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/no_ref.jpg" alt="Square shape." size="40%" >}}

Finally we have to create some kind of border where we allow other components to be places . Meaning that we cant add anything inside those borders. Which makes sense because it will be the place for MUX itself. Those borders have to be draw in the “Courtyard” layer.

{{<image src="images/border.jpg" alt="Square shape." size="40%" >}}

This is how the footprint looks like.

{{<image src="images/foot_final.jpg" alt="Square shape." size="40%" >}}

Next, I run the rule checker to confirm everything is ok.

{{<image src="images/foot_check.jpg" alt="Square shape." size="40%" >}}

Then I went back to the symbol library and I connected both of them together.

I move on to design the PCB finally. I inserted the MUX the same way I would insert all the components. It’s under Ariana library.

{{<image src="images/add_mux_canvas.jpg" alt="Square shape." size="40%" >}}

 I reach this far when I realize another problem! It feels like I cant move on from this simple PCB I have to design for the input box.

As you can see from the image below, this type of MUX called “MUX36D08” only has 3 pins for the address and 2 drain pins to select the A or B option. In this case we need 5 pins to address 16 pins rather than just 4 pins. Since that is the only 1:16 MUX we have at the lab I thought that I could just learn more about this mix and see how could I program it. 

{{<image src="images/problem_mux.jpg" alt="Square shape." size="40%" >}}

I first started by looking if I could find any example code how to program an 1:16 pin MUX where instead of 4 addressable pins we just have 3 pins plus 2 drain pins.

I couldn’t find anything with no surprise because im probably the worst person googling. And know that im writing this I just reminded that I could have just asked ChatGPT. I keep forgetting about it. But anyway, since I couldn’t find anything I just decided to learn more about drain pins and how do they work so I could just code it myself.

I found out that those DA and DB were drain pins when looking at the datasheet. In this picture we can also see the description of the EN and because it looks important I decided to connect one of the XIAO pins (D7 on the image above) on it. 

{{<image src="images/drain_info_1.jpg" alt="Square shape." size="40%" >}}

Then in the datasheet I also discover this:

{{<image src="images/drain_info_2.jpg" alt="Square shape." size="40%" >}}

After reading that info I got even more confused. There is so many strange words that I don’t know what to do with it. I felt that I was already loosing to much time on this and because I have a limit amount of question I have to make to the professor before he gets tired of my I just decide to go back and look for another MUX. At that time I wouldn’t mind to just use two 1:8 MUX and have a less trouble coding rather than maintain this 1:16 MUX and figure out how to deal with the drain pins.

So I go and I found that on the box where was written “1:8 MUX” there was instead another kind of 1:16 MUX. Well, I’m happy that its not 1:8 and its 1:16, which is what I want, but on the other hand im pissed I just discover that now after losing so much time making the libraries. 

Never the less this MUX I just found is exactly the second MUX (MUX36S16) they mention in the datasheet of the other MUX (MUX36D08) I was using. Which is nice because they will have exactly the same footprint so I don’t need to repeat that one at least. This MUX its what I was looking for. It works as I was thought with the 4 addressable pins. 

I go then, back again, do another symbol for this MUX, but thank good I don’t have to the footprint since I can just use the same. The only difference is just having one drain pin and having 4 addressable pins.

{{<image src="images/another_MUX_name.jpg" alt="Square shape." size="40%" >}}

I then replace the older MUX with the new one and start by inserting the LEDs and respective resistors followed by the buttons, display and the terminal for the battery.

This is how it looks like.

{{<image src="images/final_white.jpg" alt="Square shape." size="40%" >}}

Then I run the rule checker and I encounter 50 errors. Im so surprise cause the errors are in the LEDs, in the input power, in the buttons, etc, which are places I’ve done before where I did exactly the same and did not encounter those errors.

The errors i normally face are because of the flags or because of a bug on kicad where we just ignore them.

Then I moved to the PCB editor and I start by placing the components as I want them to be.

{{<image src="images/beggining.jpg" alt="Square shape." size="40%" >}}

I first thought how far apart I wanted the buttons to be and then I used this lines to guide the measurements. It might not be the best solution but was the first one I thought about.
It did took so much time!!

{{<image src="images/lines_to_measure_location.jpg" alt="Square shape." size="40%" >}}

This is their final position that I’m happy with.

{{<image src="images/finally_done_leds.jpg" alt="Square shape." size="40%" >}}

Then I continue inserting all the elements. When I start drawing the paths I realised I should change the pins position for both the XIAO and MUX since it was hard to find a good trace for all the paths. This design was defiantly a challenge since there’s so much of this traces to think and design.

{{<image src="images/change_pins_position.jpg" alt="Square shape." size="40%" >}}

Then I add a ground Cooper layer all over the board when I realise I should have take into account the distance between the header for the display and the XIAO. The header has to be a certain distance from the other elements.

{{<image src="images/display_away.jpg" alt="Square shape." size="40%" >}}

Then I made some holes in the corners of the board just to give me more options in the future. Im planing to use them to fix the board properly to the box.

{{<image src="images/3mm_hole.jpg" alt="Square shape." size="40%" >}}

Finally I decided to change the diameter of the holes that will have rivets already on KiCad since I just learn it with the professor. However I will start doing it always on CopperCam since in there we can “edit all identical shapes” which makes things quicker.  

{{<image src="images/change_diameter_header_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/change_diameter_header_2.jpg" alt="Square shape." size="40%" >}}

Finally I run the rules checker and I had some issues with a missing connection. 

{{<image src="images/problem_with_EN_pin.jpg" alt="Square shape." size="40%" >}}

Everything for me looked connected so I couldn’t understand what was wrong. So I ask the professor and he says that I’m cutting the ground in that area. Im closing it. Therefore I had to find a different path trace and I made it work.

{{<image src="images/rule_check_black.jpg" alt="Square shape." size="40%" >}}

This is how the end result looks like and the geber file respectively.

{{<image src="images/final_black_input.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/geber_file_2.jpg" alt="Square shape." size="40%" >}}

3. ### Milling

Finally I move on to milling. I do the same procedure as always where I plot the geber files of the front copper layer and drill holes. I go to CopperCam to generate the tool path. 

For the tools I select the 1-3mm tool as it’s more precise. Since I her the max and the “track width” its very small I find useful to use this more precise tool.

{{<image src="images/using_small_tool.jpg" alt="Square shape." size="40%" >}}

In the begging this message appear. I have no idea what is it about but I just move on with it and seams everything is fine.

{{<image src="images/possible_problem.jpg" alt="Square shape." size="40%" >}}

Then I miss changing the diameter of some holes since I’ll be inserting rivets. I could have done this in KiCad but I forgot.

{{<image src="images/how_to_change_diameter.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/or_just_using_ground.jpg" alt="Square shape." size="40%" >}}

Then I use for number of successive contours and for extra contour around pads I just put 1 and that turn out to be a very stupid mistake. But its with mistakes we learn cause honestly I never understood what where those about and now, after seeing the board milled, I can see what are they for. 

It hard to tell if it’s good or not because no matter I put 1,2,3 or 4 the board will always have some red lines. Which at the end won’t matter because it will work anyway but it does causes confusion. Specially now that was my forth time milling and clearly I miss some experience.

{{<image src="images/always_lots_off_red.jpg" alt="Square shape." size="40%" >}}

First I finally got what the professor meant with the “sand the board first to remove the first layer so its easier to mill” I was so confused with which kind of sander I was suppose to use but now I know and I’ve been using it every time I miil.

{{<image src="images/sand_before_mill.jpg" alt="Square shape." size="40%" >}}

I started to mill on the SRM-20 machine and once again it felt that the bed wasn’t level because it wasn’t milling in some areas. I hate when this happens. 

{{<image src="images/no_good_mill.jpg" alt="Square shape." size="40%" >}}

Because I didn’t want to bother kris everytime I decided to just try and do like he did last time. It took me some timer to remember but then it came to me. What he did last time was go to the position where it didn’t mill and zero the z-axis there. In case that doesn’t work what he did was put the tool up to 1mm and then lower the tool all the way down. Which means that the zero of the z-axis will be 1mm below the board. 

{{<image src="images/one_mm_more_deep.jpg" alt="Square shape." size="40%" >}}

I did that and still didn’t work. Because the professor was occupied I decided to ask help from a colleagues that suggested to just push the tool down while tight it. I did that and guess what?? The tool broke. Later my colleague came saying that he didn’t know I was using the more sensitive tool and that was maybe the reason why it broke. We should force things with this small and sensitive tool.

Never the less I finally got to ask the professor and he just recommended me using the MDX-40. I must confess I was a bit scared to use it cause I never used it for milling. But it turns out to be the exact same and even better. Thank god I change machine! This one works so much better.

We just have to make sure the settings are correct. More people use this machine for carving and the settings are different to when we mill.

Following are the settings we have to make sure are correct.

{{<image src="images/correct_settings_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/correct_settings_2.jpg" alt="Square shape." size="40%" >}}

I change the collet to the PCB one and I tight it to the machine. Later we realise I tight it too much and we had to brake it so we could remove it from the machine. Upps!

{{<image src="images/collet_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/collet_2.jpg" alt="Square shape." size="40%" >}}

Then I insert the tool and I zero the X,Y-axis. I used the same board since I barely had anything milled and therefore I wouldn’t waste a new board. For that I marked more less where was the x and y origin. Of course it wasn’t complete correct but almost. 

{{<image src="images/insert_and_zero_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_and_zero_1.jpg" alt="Square shape." size="40%" >}}

Then I zero the z-axis using the sensor. So convenient.

{{<image src="images/insert_and_zero_3.jpg" alt="Square shape." size="40%" >}}

It milled perfectly as you can see by the images bellow.

{{<image src="images/first_mill_of_many_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/first_mill_of_many_2.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/first_mill_of_many_3.jpg" alt="Square shape." size="40%" >}}

Then I change the tool to the cutting and drilling.

{{<image src="images/change_to_orange_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/change_to_orange_2.jpg" alt="Square shape." size="40%" >}}

As you can see from the image bellow the file only had the cutting and not the drilling.

{{<image src="images/no_drill_why.jpg" alt="Square shape." size="40%" >}}

 It strange since I tick both the cutting and drilling options before saving the file. Anyway I had to go back again to CopperCam to export the drilling file only. Bellow is how the board looks like at the end of the milling.

{{<image src="images/shit_board.jpg" alt="Square shape." size="40%" >}}

I found the board a bit strange! I found that the path were thinner than normal so I decided to ask the professor if it’s okay like this and if it will work.

The professor said that I should mill again and put more clearance on the pads. AKA more “number of successive contours” in CopperCam”. Once again I cant tell the difference between which one is the best option since all the options have red lines.

Bellow are two images where the one on the left is using 3 as number of successive contours and the other image is when using 4. I decided to go with 4 with no specific reason honestly.

{{<image src="images/with_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/with_4.jpg" alt="Square shape." size="40%" >}}

This is how the board looks like. Much better and with paths more clear.

{{<image src="images/final_2nd_board.jpg" alt="Square shape." size="40%" >}}

Following you can see how much better the new board is from the old one.

{{<image src="images/what_a_diff_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/what_a_diff_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/what_a_diff_3.jpg" alt="Square shape." size="40%" >}}

Finally I check with the multimeter if all the connections are working and all seams good. Spoiler Alert, it won’t be. This is just the begging of to many tries.

{{<image src="images/check_mul.jpg" alt="Square shape." size="40%" >}}

4. ### Soldering

I started by soldering the MUX since it seams the hardest one so I wanna make sure I get rid of it in the beginning. I’ve never done it before and it sound kinda impossible so I asked the professor how should I do it. 

He showed me all the procedures on the board. Because I wanna learn how to do it I grab the previous board I made and I solder a mix on it just to see if I was capplable of doing it. On thing is theory another is practice and we learn by doing. 

Following are the steps and images on how to solder a MUX.

We have to go to the microscope to do this. So we have to use this portable solder pens where most of them don’t even work.

{{<image src="images/microscope.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/solder_pen.jpg" alt="Square shape." size="40%" >}}

First we apply flux as usual. Next we put a bit of solder in the first and opposite last tabs . 

{{<image src="images/flux_pen.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_bit_solder.jpg" alt="Square shape." size="40%" >}}

Then we place the MUX in the position we want. There is small circle indicating where is the pin 1 and we should use it to guide the position of the MUX.

{{<image src="images/correct_mux_position.jpg" alt="Square shape." size="40%" >}}

Then we apply a bit of heat gently to melt the solder and connect the first pin to the board. We do the same with the opposite last tab. 

{{<image src="images/first_touch.jpg" alt="Square shape." size="40%" >}}

When doing this the MUX is fix so we can proceed to finish solder the rest of the pins. I first apply a bit more of flux and then I solder the pins gently by going up and down with the solder pen. Sometimes the pins will be connected and it’s very easy to see that through the microscope. What we have to do is just pass the pen though them and the connection will be removed.

{{<image src="images/final_mux_solder_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mux_solder_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mux_solder_3.jpg" alt="Square shape." size="40%" >}}

This is how it looks like. Pretty good for the first time.

{{<image src="images/final_mux_solder_4.jpg" alt="Square shape." size="40%" >}}

Next I check with the MUX if everything is well connected and I realise that too of the pins weren’t well connected so I just went back and apply more solder to it. Checked again and everything seams connected.

{{<image src="images/final_mux_solder_5.jpg" alt="Square shape." size="40%" >}}

Next I will solder the buttons. There was nothing signalling where was ground or not so I just went to the dataset and what it seams is that both the pins in line are connected so doesn’t matter how do I place the buttons as long I do it horizontally. 

Here is me confirming with a multimeter the pins are connected.

{{<image src="images/confirm_pins_connected.jpg" alt="Square shape." size="40%" >}}

I love soldering and I’ve been doing it for quite some time now so I have my strategies by now. One of them being applying flux on the tabs first.

{{<image src="images/apply_flux_tabs.jpg" alt="Square shape." size="40%" >}}

Then I put the buttons, LEDs and resistors.

{{<image src="images/put_btns_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_btns_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_btns_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_LEDs_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_LEDs_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_LEDs_3.jpg" alt="Square shape." size="40%" >}}

Later I found how that the LEDs are all mixed up and I end up putting red where I wanted to put green. There must have been a blue around also. Before inserting the LEDs I had to think about the configuration of where I wanted the good or bad words to be placed.

Finally, as usual, I check if everything is well connected with the multimeter. 

{{<image src="images/mul_leds_resi.jpg" alt="Square shape." size="40%" >}}

Next I insert the rest of the components and I check with the multimeter the connections.

{{<image src="images/rest_comp_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/rest_comp_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/rest_comp_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/rest_comp_4.jpg" alt="Square shape." size="40%" >}}

Unfortunately the display comes with the headers already solder on it. And as already explained above I really need a different attachment to the display and therefore I need to remove the headers from the display. Another thing I’ve never done and it’s a challenge. I ask the professor how to do it and he explained me. It’s so hard that I couldn’t do it and I was afraid to join it that I ask the professor to do it for me cause honestly I thought I was doing something wrong. Turns out the secret is just to be patient.

Never the less, after trying with one display and burn it with the hot gun, we finally manage to remove the headers.

The procedure was as following. 

We first tried to remove as much as solder as we cold with the “solder removal” thing that doesn’t work to well. Or at least not as good as this japonês brand the professor mentioned.

{{<image src="images/header_remove_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/header_remove_1.jpg" alt="Square shape." size="40%" >}}

Then we tried to remove more solder using the strip instead of the heat gun this time.

{{<image src="images/header_remove_3.jpg" alt="Square shape." size="40%" >}}

We want back to the “solder removal” thing to remove more. 

{{<image src="images/header_remove_4.jpg" alt="Square shape." size="40%" >}}

After we’ve remove all the solder we pulled the pins out with the clipper.

{{<image src="images/header_remove_5.jpg" alt="Square shape." size="40%" >}}

Very exhausting and unclear procedure but at least my display is free of the header.

I confirm once again the with the multimeter if everything Is good.

{{<image src="images/mul_good_again.jpg" alt="Square shape." size="40%" >}}

Finally, this is how the board looks like after all the soldering done. Looks pretty beautiful.

{{<image src="images/final_all_2nd_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_all_2nd_2.jpg" alt="Square shape." size="40%" >}}

Next it’s time to code the board. For my understanding shouldn’t be too hard since it’s just turning the LEDs on when pressing a button and I’ve done that before. Plot twist, I was sooooo wrong.

5. ### Coding the input box

Since we were having a class at this point I just started by coding the display alone. It should be easy and trivial.

At this [link](https://randomnerdtutorials.com/guide-for-oled-display-with-arduino/)  I learned the basics of the display and a lot of interesting options. I’ve also installed the required libraries needed such as the “adafruit_SSD1306.h” and the “adafruit_GFX.h”. 

I first started with displaying a simple dot just to see if it works. I used the example code that’s is provided on the website mention above. Later I move on to learn how to diplay images and text.

{{<image src="images/dot_example.jpg" alt="Square shape." size="40%" >}}

Later I found another [website](https://www.instructables.com/How-to-Display-Images-on-OLED-Using-Arduino/) where they would suggest using this [website](https://javl.github.io/image2cpp/) to convert images to bitmap. The website is so easy to use and it works so well.

For the sake of testing I choose an happy, sad and wobbly faces. The file has to be jpg.

{{<image src="images/choose_image_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/choose_image_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/choose_image_3.jpg" alt="Square shape." size="40%" >}}

Following are the settings I used to generate the bitmap.

{{<image src="images/settings.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/generate_code.jpg" alt="Square shape." size="40%" >}}

Here is how the images look like.

{{<image src="images/happy_face.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/wobbly_face.jpg" alt="Square shape." size="40%" >}}

If the image is not in *.jpg it won’t work as good.

{{<image src="images/no_work_if_png.jpg" alt="Square shape." size="40%" >}}

Finally I tried displaying some text based on the code provided on those websites mentioned above. On the first image you can see that the text is not properly adjust so then I had to change the code by splitting the text into lines and now works perfect.

{{<image src="images/bad_display.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/text_code.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/good_display.jpg" alt="Square shape." size="40%" >}}

Following is the code I made just to test the display. It is missing the dot example since i won’t insert it to my final project.

{{< highlight go-html-template >}}

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


// bunch of 0s and 1s , I didn’t copy them otherwise it would have been very big the code
const unsigned char epd_bitmap_happy_face [] PROGMEM = { };

const unsigned char epd_bitmap_wobbly [] PROGMEM = { };


void setup() {
  Serial.begin(115200);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  // Clear the buffer
  display.clearDisplay();

  showHappyFace();

  showWobblyFace();

  showText();

}

void loop() {
}

void showHappyFace(){
  display.drawBitmap(0, 0, epd_bitmap_happy_face, 128, 64, WHITE);
  display.display();
  delay(2000);
  display.clearDisplay();
}

void showWobblyFace(){
  display.drawBitmap(0, 0, epd_bitmap_wobbly, 128, 64, WHITE);
  display.display();
  delay(2000);
  display.clearDisplay();
}

void showText(){
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(F("Eat some "));
  display.println(F("chocolat"));
  display.println(F("to chear"));
  display.println(F("your day!"));
  display.display();      // Show initial text
  delay(100);
}

{{< /highlight >}}


Then I move on to coding the buttons and LEDs. I first had to learn how the MUX works. I look up some tutorials and example code and all of them had this extra pin where they would use it to read or write data from the channel selected. This [site](https://electropeak.com/learn/interfacing-cd74hc4067-16-channel-analog-digital-multiplexer-with-arduino/) is one of those example code I found. 

I was so confused because I thought I didn’t have that pin in my MUX. My first thought was, since I’m following an example code from another brand of MUX maybe that’s why they have this extra pin. So I should just try to find an example code from the same brand as my MUX. I couldn’t find so I just tried to do the coding by myself. 

I reached a certain point where I would select the channel but them I would be like “now what?”. Well, the answer for that its exactly that extra pin the example codes are using. In my MUX case that pin is called “Drain” and guess what ?? I didn’t include it in my board. 

Well, of course I just reach that conclusion with the help of the professor. He suggested me to use a jumper cable from a pin that has not been used from the XIAO to the Drain pin on the MUX. It will be a hard job to do this since the tabs for the MUX are very small. Luckily, the drain pin on the MUX is the up right corner which makes it easier to solder a jumper cable on it.

Following is an image of this step and me confirming with a multimeter that I didn’t made any short circuit.

{{<image src="images/jumper_cable_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/jumper_cable_2.jpg" alt="Square shape." size="40%" >}}

Then I made some code inspired by the example codes I saw. However those codes are very compact and therefore harder to read and more prune for error. I tried to print every time I pressed the button to see if it was working but it wasn’t working. It would print that i was pressing all of them but i wasn't.

{{<image src="images/code_doesnt_work.jpg" alt="Square shape." size="40%" >}}

So I just ask help to the professor where he made a simple code just focus on reading the MUX channels for the buttons and save their state in a boolean array. So later we could loop through the array and print the values. When doing like this it worked. 

Following is the code and the a video showing it working.

{{< highlight go-html-template >}}

// Define Drain Pin
int D = D0;
// Enable pin, When low all address pins are switch off, When High we can read the address pins.
int enable = D1;

// Define MUX channels ---> (A3,A2,A1,A0)
int controlPin[] = {Add3, Add2, Add1, Add0};

int muxChannel[9][4]={
    {0,0,0,0}, //channel 0 ----> Btn3 --> bad ; i=1 
    {0,0,0,1}, //channel 1 ----> Btn6 --> bad ; i=2
    {0,0,1,0}, //channel 2 ----> Btn7 --> bad ; i=6
    {0,0,1,1}, //channel 3 ----> Btn8 --> good ; i=0
    {0,1,0,0}, //channel 4 ----> Btn9 --> good ; i=3
    {1,0,0,0}, //channel 8 ----> Btn5 --> good ; i=4
    {1,1,0,1}, //channel 13 ----> Btn2 --> good ; i=5
    {1,1,1,0}, //channel 14 ----> Btn4 --> good ; i=7
    {1,1,1,1}  //channel 15 ----> Btn1 --> bad ; 
  };

bool btnStates[9]={
  LOW, 
  LOW, 
  LOW, 
  LOW,
  LOW, 
  LOW, 
  LOW, 
  LOW,
  LOW
};

void setup() {
  Serial.begin(115200);

  // Define pin mode for the Address Pins
  pinMode(Add0,OUTPUT);
  pinMode(Add1,OUTPUT);
  pinMode(Add2,OUTPUT);
  pinMode(Add3,OUTPUT);
  pinMode(enable,OUTPUT);

  // Dont know if i should initialize them to 0 Lets see !!
  digitalWrite(Add0, LOW);
  digitalWrite(Add1, LOW);
  digitalWrite(Add2, LOW);
  digitalWrite(Add3, LOW);
  digitalWrite(enable, LOW);
  digitalWrite(enable, HIGH);

}

void selectChannel(int i){
  digitalWrite(Add0, muxChannel[i][3]);
  digitalWrite(Add1, muxChannel[i][2]);
  digitalWrite(Add2, muxChannel[i][1]);
  digitalWrite(Add3, muxChannel[i][0]);
} 

void loop() {
  // loop through the channels that have a button!
  // we want to read the values of the buttons and in case they have been pressed (LOW) we want to turn the LEDs on
  for (int i=0; i<9; i++){
    selectChannel(i);
    pinMode(D, INPUT_PULLUP);
    btnStates[i] = digitalRead(D);
    Serial.print(btnStates[i]);
    Serial.print(", ");
    
  }
  Serial.println();
  delay(200);
}

{{< /highlight >}}

{{<video src="images/MUX_working_btn_v.mp4">}} Showing the MUX working on reading the buttons state.{{</video>}}

Now that we know the MUX is working on reading the button state let’s try to turn on the LEDs. When I first design this board I put two LEDs and their respective resistors in series. I knew this could give me a hard time in the future. I didn’t made any calculations or ask the professor about this, I just went for it. Was stupid from my side. 

My background is electronics and something was telling me that put two LEDs in series wouldn’t be a good idea. I quite don’t know why but it was a feeling. Maybe because there wouldn’t be enough voltage or current or we would have to use more or less resistors. Or I should have just put them in parallel. Never the less, this is theory I’ve study a while ago and I didn’t bother to search for or to ask the professor. And now I’m paying from my mistakes. 

However I must say that good things can come up after bad things and this was the case. 

Conclusion is that I asked the professor about if the LEDs would work and he said that most likely not. Right now I don’t remember what was the reason why. However we tried to see if the MUX would be able to LED up at least one LED. So I removed one LED and resistor and I used jumper cables to replace them as you can see on th image bellow.

{{<image src="images/replace_LED_with_jumper.jpg" alt="Square shape." size="40%" >}}

Then we made a simple code where we tried to just light up that LED and see if it would work. And it didn’t. 

So we decided to see if there was any kind of current going though the LED or if the problem was with the MUX not selecting the channel correctly or if the code was wrong. 

In order to look for voltage picks we can use the logic analyser along with the software called logic. 

{{<image src="images/logic_analyser_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/logic_analyser_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/software_logic.jpg" alt="Square shape." size="40%" >}}

At this state I was more focus on understanding the software and taking conclusions that I didn’t document this part, but basically, whenever we would press the button we would see a very small pick on the voltage. Which means that the MUX is constantly changing the channel and they don’t keep the values. So whenever the MUX would change the channel there wouldn’t be any enough voltage to light the LED. 

We tried putting some delays and we manage to see the LED light up for a fraction of second (and it was when I realises all the LEDs are mixed up because it light green instead of red which was the colour that was suppose to be).

Never the less, we conclude that the MUX couldn’t be the one lighting the LEDs so the professor suggested me to use neon LEDs and control them with the XIAO board. A good thing about this LEDs is that we just need one pin to control all of them since we are talking about addressable LEDs. 

This was a surprisingly very easy and useful solution. And I must had that thank good I used NEON LED since I can control the brightness and make sure they are visible through the silicon. The other LEDs where too week to be visible. Also I can change the colours very easily so I don’t have to think ahead where I wanna place the good and bad words. This was a life changing solution that I which I could have asked the professor earlier. But I don’t wanna bother him every time with pointless things. I rather safe is precious time for more important situations. 

Okay, time to repeat everything again uhuhuhuh. I have to change the KiCad, mill and solder again. 

6. ### KiCad again

What I’ve learn from the previous board is that I have to include the drain pin and the NEON LEDs. 

I search for the [datasheet](https://cdn-shop.adafruit.com/product-files/2757/p2757_SK6812RGBW_REV01.pdf) of the LEDs we have at the lab so I know how would I connect them on KiCad. The LEDs need a voltage input of 5V which means I will have to use at least two lithium batteries and a voltage converter from the battery output voltage to 5V. Then I can power both the NEON LEDs and the XIAO board from the converter output since the XIAO has an internal 5V to 3.3V converter.  I also search for the [datasheet](https://www.tracopower.com/int/tsr1e-datasheet) of the converter we have at the lab so I know how to work with it.

{{<image src="images/i_need_drain_pin.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/include_NEON_led.jpg" alt="Square shape." size="40%" >}}

This is how the final sketch looks like.

{{<image src="images/final_white_neon_led.jpg" alt="Square shape." size="40%" >}}

Then I move on to the PCB editor where I replace the old LEDs and resistors with the NEON LEDs. 

{{<image src="images/replace_with_neon_led.jpg" alt="Square shape." size="40%" >}}

Once again it took me a lot of time making sure all the buttons and LEDs were well aligned. I also had to keep changing the layout of the pis in order to be easier to make the path traces.  At some point I finally found a good placement and I started connecting the paths when I realise the LEDs have the tabs offset.

{{<image src="images/bad_offset_of_led_2.jpg" alt="Square shape." size="40%" >}}

I then asked the professor how to fix it and now I know how. Ice first go to the footprint editor and we fix the offset by aligning it with the origin (I don’t have a picture unfortunately). Then we have to do the following.

{{<image src="images/bad_offset_of_led_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/bad_offset_of_led_4.jpg" alt="Square shape." size="40%" >}}

Finally I run the rule checker when I realise I had closed the board and there were missing connections because of the ground. 

{{<image src="images/problem_closing_ground.jpg" alt="Square shape." size="40%" >}}

I couldn’t find a way to connect the ground so I asked the professor how could I do it. He suggested to make a double sided board where the other side of the board would be just ground. Then I would have to insert this holes with rivets so I could connect that with the other side of the board that is the ground.

{{<image src="images/use_rivets_other_side_ground.jpg" alt="Square shape." size="40%" >}}

When doing that I have no errors.

{{<image src="images/no_errors.jpg" alt="Square shape." size="40%" >}}

This is how the board looks like at the end.

{{<image src="images/not_final_black_neon_led.jpg" alt="Square shape." size="40%" >}}

Thank god at this stage I still had some brain cells and I realised a problem if we do the board like this. My display need rivets so I couldn’t make all the other side of the board ground otherwise I would have short circuit in all the display pins. We talked with the professor to just make one sided board and on the back put this copper tape on the places I need and make the holes with the rivets anyway. I didn’t like too much the idea so I just went back to the editor and I tried to find a way to connect all the grounds together without any closed session. And I manage to do it uhuhuhuh.

Following is the image with no more holes. 

{{<image src="images/actually_manage_no_holes.jpg" alt="Square shape." size="40%" >}}

I changed the copper layer from the back layer to the front layer again and I run the rule checker to confirm everything was good.

{{<image src="images/no_error_finally.jpg" alt="Square shape." size="40%"  >}}

This is how the end result looks like along with the respective geber file.

{{<image src="images/final_black_neon_LED.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/geber_file_final.jpg" alt="Square shape." size="40%" >}}

Im finally done with the PCB designing now I have to generate the tool path again and mill the baord.

7. ### Mill and Solder again

I milled the board again and everything went fine (with my surprise). So then I measure with the multimeter if all paths are well connected.

{{<image src="images/mul_in_board_2.jpg" alt="Square shape." size="40%" >}}

The professor often says that sometimes we shouldn’t try to be sustainable for several reasons. Well, for this case, I took the challenge of removing all the buttons and every other element I could use on the new board. It took me a lot of time that now it’s making me not enjoy more the good summer, but at least I tried making a good action. 

{{<image src="images/replace_things_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/replace_things_1.jpg" alt="Square shape." size="40%" >}}

Then I insert the buttons back in the new board and I measure if everything was good.

{{<image src="images/replace_things_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/replace_things_4.jpg" alt="Square shape." size="40%" >}}

Then I insert the LEDs. For the LEDs I had to go back to the datasheet to confirm the orientation. This is the relevant info I took. There’s a little triangle at the end indication where is the ground.

{{<image src="images/there_is_little_triangul.jpg" alt="Square shape." size="40%" >}}

Then I apply some solder both on the LED pads and the pads on the board and I insert it with the help of tweezers and the solder pen.

{{<image src="images/insert_neon_led_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_neon_led_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_neon_led_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_neon_led_4.jpg" alt="Square shape." size="40%" >}}

Then I insert all other components such as MUX and headers. Finally I insert the converter. I wasn’t sure if I would insert I with an header or directly on the holes. So I ask the professor and he says that those elements are THT components which means Through-Hole-Technology and therefore they are suppose to be inserted directly with no headers.

I then go to the datasheet to see its orientation and there’s a little dot indicating the VIN. 

{{<image src="images/dot_is_vin.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/converter_tape.jpg" alt="Square shape." size="40%" >}}

Now, all the components are on the board so I just decided to take care of the battery. I always forget the orientation of them, which one is minus or plus so I just used the multimeter to measure it.

{{< youtube JYrSKDKlc0Q  >}} 

Then I wanted to insert a switch. I wasn’t sure how would they work so after some discussion with the professor I understood how to include it in the circuit.

{{<image src="images/how_switch_works_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/how_switch_works_2.jpg" alt="Square shape." size="40%" >}}

{{<video src="images/test_switch_v.mp4">}} See Orientation of switch.{{</video>}}

8. ### Code board again

Then I finally move on to code the board. I first try the previous code just about the MUX reading the buttons and everything was fine. Then I move on to code the LEDs, I must confess I was afraid to do that cause I was afraid if I would explode one of them or something. 

So, I try the code and nothing happens! Non of the LEDs light up. At this stage I’m still very new when it comes to debugging this kind of problems, but I promise that after so many tries you get the hang of it. Nevertheless, so many things can be the reason why it’s not working. It can be the code or a short circuit somewhere in the board. I ask the professor for an opinion and the first think he mentions is that I should make the path track wither for the battery (Vin). Another thing he said is that I must likely have a short circuit somewhere. 

I grab the multimeter and I realize that the Vout of the converter and the ground were connected. That’s why all the LEDs were in short-circuit. The professor said that it’s my job to try to figure out where is the problem and fix it. Once again, I’m still very new to this so hearing that I have to try to figure out where is the short circuit in my huge board I got very unmotivated and lazy. Mostly because I don’t know what to look for and how to start. 

Since I had to fix my problem with the fact that the battery trace is not wide enough I just decided to fix that on kicad and mill the board again. 

Another note is that I’ve milled two boards before this and they always worked with no problem. So I thought, “why is everything not working now?”, but them, I happen to talk with a colleague that said that its very normal that most of the boards we solder end up having short circuits. He said all his four board had it and he was fixing them. I took the change to ask how does he do it and why he just doesn’t mill again. He said its a waste of time milling again. I asked what does he do to figure out the problem and he said that he just measures the voltage sometimes and other things. 

After our conversation I got inspired and motivated to try to fix my board so I when to the soldering station and I start measuring the voltage in all the LEDs. I used a machine to supply power to the board. I found out that three of the LEDs had way little voltage than the other so I tough on removing them and solder them again.

As you can see on the video bellow the first LED light up for some reason, so at least that one wasnt in short circuit.

{{<video src="images/LED_on_no_SC_v.mp4">}}{{</video>}}

On the image bellow i show how i measure the voltage on each LED and how I discover that 3 of them had less voltage and I should just resolder it.

{{<image src="images/problem_led_resolder.jpg" alt="Square shape." size="40%" >}}

However on my way to do resolder the LEDs I don’t know what I did but I manage to put a lot of current on the power supply that explode the LEDs. At least I saw some smoke and it smelled bad. It was so stupid what I did. I had just found out the problem. But now I can risk from the list that I burn a component.

Moving on, like I said I end up changing the KiCad and I milled again. 

9. ### Hopefully last time I mill and solder the board

This are the changes I made on KiCad. I made the battery traces bigger.

{{<image src="images/for_battery_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/for_battery_2.jpg" alt="Square shape." size="40%" >}}

Then I move on to mill the board. 

When It start I realise that some parts weren’t being milled. 

{{<image src="images/parts_not_miiled.jpg" alt="Square shape." size="40%" >}}

My first intuition was that the bed wasn’t levelled. I asked the professor and since its the MDX-40 that shouldn’t be the problem. He then throw me some possibilities of what could be the problem. Being one of them the tape being overlapping with each other. As soon as he mentions that I remembered seeing that indeed the tape was overlapping as you can see from the video below. It’s so interesting how this so small detail can influence. It’s just a fraction of millimetres.

{{<video src="images/tape_overlapping_v.mp4">}} Tape overlapped.{{</video>}}

This is me putting the tape better.

{{<image src="images/tape_fixed.jpg" alt="Square shape." size="40%" >}}

Another thing was that I used to use the sensor to zero the z-axis and the professor told me I shouldn’t do that but do like we do with the SRM-20 where we zero the z-axis manually. Which ever was the solution it doesn’t matter because now the machine milled well in all areas.

However for my surprise, when the job finished, the board wasn’t fully milled. There were clearly some spots left. Once again I was so confused with what could have been the error. For a moment I though it could have been some problem with CopperCam that didn’t generated all the paths. But that’s strange cause I did everything as I normally do. I asked the professor and he said that it could be that the tool broke at that point and that’s why, even tho it continue doing the job it wasn’t engraving. 

{{<image src="images/board_not_fully_eng.jpg" alt="Square shape." size="40%" >}}

I then checked the tool and yes, that did happen. The tool was broken. I had to find a solution to prevent from it to broken again. My board is so big that it takes so much time to mill and I don’t have all the time in the world to keep milling. Just not to mention the fact I’m wasting boards.

Since it’s a 3 hour milling job it’s natural that the tool can brake. The solution I found was reducing the cutting speed up to 70% to 80%.

I change the tool to a new one, I made sure I put the tape properly and I zero the z-axis manually and I milled again. Was so afraid that the tool could brake again or that the CopperCam didn’t generate all the paths. 

This is a video showing the machine properly.

{{<video src="images/milling_working_v.mp4">}} Machine Working.{{</video>}}

This is how the board looks like after engraving. Here you can see that it worked and that the problem was indeed the fact that the tool broke at some point.

{{<image src="images/mill_work_5th_1.jpg" alt="Square shape." size="40%"  >}}

This is the board after cutting and drilling. Looks promising. Now I just have to make sure I don’t make any short circuit will soldering.

{{<image src="images/mill_work_5th_2.jpg" alt="Square shape." size="40%" >}}

My strategy this time for soldering is to check with the multimeter after each component I insert. It takes longer but wayyyy easier to debug. 

Once again, right after the board was milled, i check if all the connections are working.

{{<image src="images/con_working_mul.jpg" alt="Square shape." size="40%" >}}

I then started by soldering the MUX. I was having some troubles soldering the pin when I realised that the MUX was upside down. So off course the tabs werent touching the surface and therefore impossible to solder them togther.

Then I move on to solder the LEDs since I think will be the ones causing more possible short circuits.

{{<image src="images/LEDs_first.jpg" alt="Square shape." size="40%" >}}

This video shows me testing that all the connections on the LED are not causing short circuit.

{{<video src="images/check_LED_ok_v.mp4">}} LED not in short circuit.{{</video>}}

I did end up having two LEDs causing short circuit. I had to remove the LED again and resolver it.

{{<image src="images/remove_led_resolder.jpg" alt="Square shape." size="40%" >}}

Here’s a video showing me understanding that the ground and the Data_Out pin were connected.

{{<video src="images/check_LED_bad_v.mp4">}} LED in short circuit.{{</video>}}

I even recorded me confirming if the tabs were still causing short circuit after removing the LED and I also checked if the LED itself was causing the short circuit. Non of them happened so I just solder the LED again more carefully.

Following are the videos of the me checking the tabs and LED respectively.

{{<video src="images/check_tabs_short_circuit_v.mp4">}} Check if tabs are in short circuit.{{</video>}}

{{<video src="images/check_LED_short_circuit_v.mp4">}} Check if LED is in short circuit.{{</video>}}

I also made videos of the procedure I do to solder the LEDs but I manage to put my had in front of the camera all the time. It’s fine. The video was too long anyway.

{{<image src="images/bad_recordings.jpg" alt="Square shape." size="40%" >}}

Then I finish soldering everything. I also made a video filming me solder the headers. I found that my technic is pretty good and it’s so satisfying to see how the solder goes so well around the header pin. It mostly because of the sold pen. I love it.

{{<video src="images/solde_pins_smothly_v.mp4">}} Check if LED is in short circuit.{{</video>}}

Finally, I realise I have to power the XIAO board from the VOUT of the converter. I thought on releasing a bit the converter so I could put a jumper cable between the converter and the board. Just to have everything compact and clean. I also didn’t want to have a jumper cable right in front of the board. However I must say I didn’t really succeed on removing it. I end up inserting a huge ball of solder and bend a bit the converter’s pins.

{{<image src="images/try_remove_converter_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/try_remove_converter_2.jpg" alt="Square shape." size="40%" >}}

I then give up and I just solder the jumper cable in front of the board.

{{<image src="images/try_remove_converter_3.jpg" alt="Square shape." >}}

Finally this is how the board looks like after all elements added.

{{<video src="images/final_input_board_v.mp4">}} Final input board.{{</video>}}

Then I insert the battery and I move on to code the board and hope everything works good. At this stage I already have a base code to test the MUX and then I just have to had the code for the LEDs.

{{<image src="images/insert_bat_inp_box.jpg" alt="Square shape." size="40%" >}}

10. ### Coding the new board

I first started by making some changes to the previous simple code for reading the button stated with the MUX. The reason being that I had to make some changes on the location of the pins and also because the MUX channel addresses for the button also changed.

We made this work with the previous board already so it’s with my surprise that it doesn’t work. I was so confused. We’ve tested the code before and it worked and I measure all the connections between the MUX and the buttons and they seam connected.

As you can see on the video bellow, some buttons work and other don’t. It’s so strange. 

{{<video src="images/mux_upside_down_no_work_v.mp4">}} Board responding weirdly.{{</video>}}

Never the less, I don’t know what I did, it might have been an angel illuminating me or something, but I manage to spot that the small circle of the MUX was on the other way around. 

{{<image src="images/mux_up_side_down.jpg" alt="Square shape." size="40%" >}}

So then I had no other option than removing the MUX. This one will be a very interesting challenging. I didn’t wanna bother the professor so I tried by myself with no idea what I was doing. Sometimes that’s not a good idea.
My strategy was trying to melt all the pins from one side at the same time using the pen and trying to pull that side using a tweezers. It’s with no surprise that I say it didn’t work and I just end up destroying some solder paths and scratching the MUX. 

I decided to just go ask the professor how to do it in a good and safe way. He told me to use the softer heat gun to melt the solder and remove the MUX with the tweezers. I did so and it worked.

{{<image src="images/remove_mux_1.jpg" alt="Square shape." size="40%" >}}
 
After working with so many boards by now and facing so many challenges I start squaring some trouble shooting skills. I decided to use the solder wire to remove excess solder from the MUX tabs.

{{<image src="images/remove_mux_2.jpg" alt="Square shape." size="40%" >}}

Then, because of my previous brute force attempt, I notice that some tabs were a bit lifted and/or tilted. 

{{<image src="images/remove_mux_3.jpg" alt="Square shape." size="40%" >}}

Is gonna be an interesting challenge to make sure all the MUX pins are touching the tabs sufficiently and successfully.

I measure with the multimeter and I realised a couple of the pins weren’t fully touching. I had to put a little bit more solder but unfortunately I end up putting too much. I tried to remove it just by passing the solder pen on top of it just like I would do before. But because it was so much solder it wasn’t working. And, this is another example of how much better I am at finding solutions for problems because of all the experience I’ve gone through. I remember I could just use the solder wire again to remove the extra solder. I did so and it worked uhuhuh.

{{<image src="images/remove_solder_mux.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/almost_fucked_pin.jpg" alt="Square shape." size="40%" >}}

So, I go immediately try out if now it works and still the button 1 wasn’t working as you can see on the video bellow. 

{{<video src="images/loop_of_just_8_problem_v.mp4">}} Button 1 still not working.{{</video>}}

It’s so hard to debug this kind of problems because the error can be in literally everything. It can be the soldering, the code itself or the button being broken. Thank good I was with energy to debug all this problem. And I must say with some good luck on me.
I start excluding possibilities. I grab the mix and I see if the button is well solder(connected) I then see if there voltage around it. Everything was proper. I then go check if I code well the channel address for that button and yes I also did. 

I was with no more ideas so I ask the professors opinion. He does the same debugging as me and nothing. Until, when we start looking to the code and I spot the for loop. The loop only looped for 8 times and not 9. Remember I told I used the previous code. Well that one only had eight buttons being selected by the MUX instead of nine like now. 

The funniest part is that I had to change the size of the arrays “buttonState” etc from 8 to 9, but I still manage to forgot about the loop. 

Nevertheless, now I know what is the problem and it makes so much more sense now why the number one wasn’t being selected. The loop only looped through the first 8 channels on an array of 9 being the number 1 on the last position of that array.

{{<image src="images/loop_just_8.jpg" alt="Square shape." size="40%" >}}

Following is a video showing the buttons working properly. Finally done with this step. I can finally move on to the next one.

{{<video src="images/mux_btns_finally_working_v.mp4">}} Buttons now working.{{</video>}}

Next I start by separating the good words from the bad words and depending on the category the display will display something different. For this I just simply used a variable where I assign “1” if good word or “2” if bad word.

It’s a very simple code but guess what? It also didn’t work. I swear, this day was full of debugging but I must confuse I really liked it. I think because I always succeeded. Otherwise I’m pretty sure I would have hated it. Here’s a video showing It not working as expected. I would press a good word and it would print “n=2” as if it was a bad word.

{{<video src="images/problem_with_equal_v.mp4">}}{{</video>}}

Anyway, the solution for this problem was in the if statement. I had “if( i=1 )” instead of the “if( i==1 ) ”.

I proceeded then to display a face and a text accordingly if its a good word or bad word. On the video you can see it working. We are finally closer to finish doing the input box. I just need to code the LEDs afterwards.

{{<video src="images/btns_display_working_v.mp4">}} Buttons and display working.{{</video>}}

Then I just miss coding the LEDs. I first try the classic “Adafruit_NeoPixel” library but it didn’t work. The following video shows nothing lighting up when I press the button.

 {{<video src="images/LEDs_not_working_v.mp4">}} LEDs not working.{{</video>}}

So then I thought that maybe because the LEDs are not from Adafruit and from a different brand I just decided to find a library that would work on this specific LEDs. The LEDs we have are the SK6812B so I found this library on this [GitHub page](https://github.com/Electry/Arduino_SK6812) that seams likely to work. 

Well, it didnt work! This error happered. 

{{<image src="images/error_with_library.jpg" alt="Square shape." size="40%" >}}

I didnt tryed to hard to hard to try to fix that problem. I just tried again another library that accordingly to the internet would work with this SK6812 model. The library is called LiteLed. It also didn’t work.

At this point I just start doubting that the problem is about the libraries or the code but the LEDs or something else.

Once again, time for debugging.  I started by measuring the voltage on the LEDs and on the power output of the converter (VOUT) and the voltage was around 1V which is strange since it should be around 5V. 

Or either the converter is broken or the battery is turned OFF. I look at it and its with the switch ON so it should work. 

{{<image src="images/was_on.jpg" alt="Square shape." size="40%" >}}

So then I thought maybe the batteries are uncharged. When I was changing them I turn the switch off and some LEDs light up. I was so confused!! Then I realised that one of the battery was bad connected with the plus and minus. I was so afraid if I broke some component or not! But makes more sense now that I just had 1V and not 5V and that’s why the LEDs didn’t LED up. 

{{<image src="images/wrong_bat_connection.jpg" alt="Square shape." size="40%" >}}

Then I insert the batteries properly.

{{<image src="images/good_bat_connection.jpg" alt="Square shape." size="40%" >}}

Now with the proper battery connection I measure 5V on the LEDs and on the Vout of the converter.

{{<image src="images/proper_voltage.jpg" alt="Square shape." size="40%" >}}

**NOTE**:  I have to have the battery OFF to upload the code to the board because the battery might cut the USB connection and thus no power will come from the USB. They’re an internal MOSFET as I’ve been told by the professor.

{{<video src="images/XIAO_has_mosfet_inside_v.mp4">}}{{</video>}}

Then I decided to try again the “Adafruit_NeoPixel” library and it worked uhuhhuuh.

{{<video src="images/LEDs_first_working_v.mp4">}}LEDs finally working.{{</video>}}

As you can see on the video and on the image bellow the LEDs are very bright and thus I decided to change the brightness. I feel blind right now.

{{<image src="images/finally_LED_on.jpg" alt="Square shape." size="40%" >}}

To turn the LEDs off we can either change the brightness to zero or just use the .clear() function. Important note about this function and about the .brightness() function is that in order for them to work we have to call for the .show() function right afterwards. Another problem I had to find the solution for.

{{<image src="images/we_need_the_show.jpg" alt="Square shape." size="40%" >}}

Another interesting thing about this board is that the LEDs actually work without the battery. It’s a bit strange because I thought that they had to be powered with 5V but as you can see from the video bellow, the LEDs are lighting up even with the battery off. I would say that it works because I’m just lighting on LED at at time and not a multiple of them. 

{{< youtube IDO1Pegtv-c  >}} 

Then I decided to change the colour of the LEDs to be red if it’s a bad word being selected or green otherwise. I then realise that the colours were swapped. Was so strange. But then I read more about LEDs on the internet and I would say that the problem is that I’m considering my board to be RGBW but it might be GRBW. That can explain why the red and green are swapped.

{{<video src="images/GRB_not_RGB_v.mp4">}} Colours swapped.{{</video>}}

I also learned from the professor that we should be careful when touching the board while being ON because my fingers can conduct electrons and destroy the components. 

To finish I mount all the components on the box I design and 3D printed to have a final look of how the input box will look like. I test it just for the sake of documentation when I notice that the number 8 wasn’t working properly. As you can see on the first video bellow, the number 8 did work at some point, but starting from the second video it stoped working.

{{<video src="images/8_working_not_working_v.mp4">}} 8 working.{{</video>}}

{{<video src="images/box_and_8_not_work_v.mp4">}} 8 not working.{{</video>}}

Once again I had to debug the problem. By now I already now which routine to have. I first measure the voltage and it was what it was supposed to be. The connections were also good so I just decided to switch the LED cause it might have broke or something.

To switch the LED I used the heat gun for that. I didn’t know to well how to do it and I was so afraid to destroy something since I REALLY didn’t want to mill and solder another board again or lose time debugging an error. But I did with the heat gun with 250 temperature and 51 pressure and everything was fine !!! Uffff. 

{{<image src="images/remove_led_heat_gun.jpg" alt="Square shape." size="40%" >}}

Now with the new LED everything is working perfectly.

{{<video src="images/8_fixed_v.mp4">}} 8 now working.{{</video>}}

Finally I try to make the connection between two XIAO. I want to finish the code on the client side (inout_box). 
If the the number pressed corresponds to a good word I will light the green LED ON on the server board. If a bad word is pressed the red LED will be turned on instead.

When succeeding with this I will have the final code for the input box(client) and most of the structure for the output brain (server).

I did succeed. On the first video the green LED didn’t light up. For a moment I thought it could be the code but them I grounded myself and I realise that it could only be the fact the LED broke in the meantime. To confirm this theory I just change my server board to the FIXE board and I made it so that every time a good word is pressed the blue LED light up and every time is the bad word selected the red LED is turned ON (second video).

{{<video src="images/cummunication_1_v.mp4">}}{{</video>}}

{{<video src="images/cummunication_2_v.mp4">}}{{</video>}}

To finalise this session I just wanna add one more thing. As you could tell by now this day was my debugging day. And I can happily say that I very much succeeded with no need to bother the professor for help.

It was also this day I found a recipe for when the XIAO board might not work. I can say its on of the recipes other people might find useful in the future. This is the error.

{{<image src="images/always_same_error.jpg" alt="Square shape." size="40%" >}}

And following is a video choowing how to fix that error.

{{<video src="images/how_to_fix_problem_v.mp4">}}{{</video>}}

11. ### Design keys and box in FUSION 360

To design the box I used Fusion 360. I’ve heard from a friend that we could import the PCB as *.step file into fusion 360 and work around it. I decided to check some YouTube tutorials to get the overall overview of what I should do.

I first insert the step file as follow:

{{<image src="images/insert_PCB.jpg" alt="Square shape." size="40%" >}}

As you can see on the image bellow there’s a problem, the imported PCB doesn’t come with all the componentes inserted neither with the path tracks. That’s a problem because in order to design the case to cast the keys I need to know the location of the bottoms.  

I decide to look up on the internet a solution for this problem and I see this [video](https://www.youtube.com/watch?v=L25B5WaZ2EI) where I follow more less the same steps.

Basically the solution is insert an image of the PCB on top of it. The process is as follow. 

First we have to “Apply Apperance” on the object, select which ever material and click in advance and image.

{{<image src="images/apply_apperance.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/click_green_and_advance.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/then_image.jpg" alt="Square shape." size="40%" >}}

Accordingly to the video they suggest to hit “plot” and export as *.jpg. However when doing that the image turn out being black and impossible to see any path.

{{<image src="images/image_black.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/impossible_to_offset.jpg" alt="Square shape." size="40%" >}}

So, I just decided to go to the “3D View” and export the view as *.jpg and this is what I get from that. Much better result.

{{<image src="images/good_image.jpg" alt="Square shape." size="40%" >}}

Then I repeat the same process where I select image.

{{<image src="images/select_image.jpg" alt="Square shape." size="40%" >}}

And I doble click on it so I can edit it and align it. 

{{<image src="images/double_click_on_image.jpg" alt="Square shape." size="40%" >}}

This method is clearly not the best but the only one that work with me. It takes so much time trying to align it.

{{<image src="images/try_align_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/try_align_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/try_align_3.jpg" alt="Square shape." size="40%" >}}

This is the final result. Lot of work but it’s definitely needed. I’m also proud of what I had to learn and research just to reach to this stage.

{{<image src="images/final_image.jpg" alt="Square shape." size="40%" >}}

Next I start adding all the components on the board such as the display, XIAO board, terminal and headers. For that I went to [GrabCad](https://grabcad.com) website where I found all the step files for those componentes. I found it was important to have them on the board so I can design the board around them. 

I started by inserting the display and its done as shown on the image bellow.

{{<image src="images/insert_into_current_design.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/into_design.jpg" alt="Square shape." size="40%" >}}

I will place the display with two headers so it can be very low. Therefore I had to first remove the headers that come with the display step file.

{{<image src="images/removing_pins_from_display.jpg" alt="Square shape." size="40%" >}}

Then I haded two headers, a female and a male and I join them together. 

{{<image src="images/align_headers_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/align_headers_2.jpg" alt="Square shape." size="40%" >}}

Here you can see that the pieces actually touch so should be okay.

{{<image src="images/they_are_in_contact.jpg" alt="Square shape." size="40%" >}}

Finally I joint the headers together.

{{<image src="images/combine_headers.jpg" alt="Square shape." size="40%" >}}

I snap them to the board along with the display.

{{<image src="images/align_to_board.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/snap_display.jpg" alt="Square shape." size="40%" >}}

I did the same with the XIAO board. I fist inserted the headers followed by the XIAO.

{{<image src="images/put_headers_xiao.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/then_XIAO.jpg" alt="Square shape." size="40%" >}}

Finally I move on to insert the terminal.

{{<image src="images/insert_terminal.jpg" alt="Square shape." size="40%" >}}

I made a lot of testing and measurements to make sure everything would work at the end.

While I was doing the box I was also milling the board at the same time. And this is the part where I realise that my board wont work if I use two LEDs in series. The professor suggested to use neon LEDs instead. I was a bit sad cause I made all this work already and I will have to change the board and repeat everything again. I decided to decrease the size of the holes I made on the board from 5mm to 2.5mm.

I go to the Kicad and I change the board. I go again to the 3D view and I export the front view as *jpg. On top of that I export the new board as a step file so I can had it to fusion.

Let’s repeat everything again uhuhuhuh.

I export the new PCB again.

{{<image src="images/new_pcb.jpg" alt="Square shape." size="40%" >}}

And I had the image as an “Appearance” to the board.

{{<image src="images/new_image_jpg.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_this_shit_again.jpg" alt="Square shape." size="40%"  >}}

I place once again all the elements on the board such us the terminal, XIAO board, display and headers.

{{<image src="images/place_final_header.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/after_inserting_everything.jpg" alt="Square shape." size="40%" >}}

Because this design is full of components and it’s a mess I decided to create a new design where I insert the PCB as a component.

{{<image src="images/create_another_design_for_case.jpg" alt="Square shape." size="40%" >}}

Now, it’s finally the time I start designing the actual box.

Inside the box I will have the batteries. I will be using 2 batteries so I will use the battery cases that exist in the lab. In order for the battery case to not move around the box I had to make some walls around the battery case. I thought on inserting a step file of the battery case but I wasn’t sure if the one I found on “GrabCad” was the same as the one we have at the lab. I decided to just take the measurements and make a simple rectangular box to simulate the actually battery case.

So I create a new component for the battery case 

{{<image src="images/have_to_use_2_battery.jpg" alt="Square shape." size="40%" >}}

And I make a simple rectangular box.

{{<image src="images/improvise_battery_box.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/very_simple_cause_no_time.jpg" alt="Square shape." size="40%" >}}

Then I project all the circles I have in the board so I can make some holes and threads to fix the board to the box latter.

{{<image src="images/do_circle_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_circle_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_circle_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_circle_4.jpg" alt="Square shape." size="40%" >}}

Then I placed the battery case where I want it to be at the end and I design walls around it.

{{<image src="images/drawing_base_support.jpg" alt="Square shape." size="40%" >}}

I extrude all the walls and the circles all the way to the PCB.

{{<image src="images/extrude_them_all_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_them_all_2.jpg" alt="Square shape." size="40%" >}}

Now I will simulate how do I want the silicone keys to be and I will design a mold for them so I can cast the board with silicone keys.

I first design the squares around the bottoms and LEDs for the keys and I’ll extrude them 9mm. It’s 9mm because of the measurements I made of the hight of the display and because it cant be more thicker otherwise its very hard to press the buttons.

{{<image src="images/design_keys.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_9mm.jpg" alt="Square shape." size="40%"  >}}

Then I made a 2mm “floor” for the keys so they all would be united. I decided to do this so I could have some kind of protection between the up part of the box and the PCB.

{{<image src="images/extrude_silicon_keys_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_silicon_keys_2.jpg" alt="Square shape." size="40%" >}}

Then a create the molding box for the silicone keys.

{{<image src="images/molding_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/molding_box_2.jpg" alt="Square shape." size="40%" >}}

As you can see on the image bellow, some pins of the headers are in contact with the molding box. When we extrude the box holes are created In those spots. To make sure the pins fit in those holes I had to make them a bit bigger so I sed this tool I just heard about from a tutorial called “offset faces”.

{{<image src="images/offset_holes_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/offset_holes_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/offset_holes_3.jpg" alt="Square shape."size="40%"  >}}

{{<image src="images/offset_holes_4.jpg" alt="Square shape." size="40%" >}}

I do the same with the MUX. I design a small rectangular around the MUX and I extrude some mm. 

{{<image src="images/taking_mux_into_account_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/taking_mux_into_account_2.jpg" alt="Square shape." size="40%" >}}

Finally I just miss doing the numbers and the molding box its done. Because I made the some tests before I know how do I want the numbers to be like. Which size and how to extrude them.

{{<image src="images/do_numbers_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_numbers_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_numbers_3.jpg" alt="Square shape." size="40%"  >}}

Next I continue with designing the box itself. I first started by finishing the bottom part of the box. 

I will extrude the outside walls and make the bottom 3mm thick.

{{<image src="images/extrude_walls_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_walls_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_bottom_3mm.jpg" alt="Square shape." size="40%" >}}

And this is how the bottom case looks like.

{{<image src="images/bottom_case.jpg" alt="Square shape." size="40%" >}}

Next I do the upper part of the box. At this point is just a rectangular box with holes at the keys locations.

{{<image src="images/up_case.jpg" alt="Square shape." size="40%" >}}

Then I wanna consider the display so I have to make a small hole around it. I use again the “offset” tool for this purpose.

{{<image src="images/offset_display_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/offset_display_2.jpg" alt="Square shape." size="40%" >}}

Some with the headers. As you can see on the image bellow, the headers touch the up case. In the beginning I thought on just making holes for them like I did before with the mold case but them I thought I could just me the up box higher so it doesn’t touch the headers and then make 2 circle tubs connecting the unbox with the PCB.

{{<image src="images/consider_holes.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/had_support_up_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/had_support_up_2.jpg" alt="Square shape." size="40%" >}}

In order to be able to insert the display I have to make a separe piece. At list this was the first idea I come up with so I just stick with it.

I make a hole a bit wither than the header and I cut it from the up case.

{{<image src="images/make_hole_here_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_hole_here_2.jpg" alt="Square shape." size="40%" >}}

Next, I will apply fillets around the case so everything looks more smooth and less sharp.

I will also make some offset on the holes to make sure the keys fit perfectly. I’ve learned with my colleagues that we should always make this small offsets because of the printing. 

{{<image src="images/offset_keys_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/offset_keys_2.jpg" alt="Square shape." size="40%" >}}

Then I insert fillets on the bottom and up case.

{{<image src="images/insert_fillet_on_edges_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_same_up_box.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/also_on_top.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/and_bottom.jpg" alt="Square shape." size="40%" >}}

Finally it’s time to find a mechanism to snap the up part with the bottom part. 

I’ve never done design or used fusion before and this is the first time I design a box. So I don’t know which kind of snap to use and the worst part is that im so bad at googling that I couldn’t find any tutorial where I could get some inspiration or idea for the shaping. Therefore I just decided to dream about it and try to imagine a good shaping. Following is the what I come up with. 

On the image bellow we have the bottom part in grey and the up part in light/transperent grey. I will make extrude the triangular and ill add it to the bottom part. 

{{<image src="images/snap.jpg" alt="Square shape." size="40%" >}}

Then I apply some “chambers” and “fillets” to make it more smooth and less sharp. And I mirror this component to the other side of the box.

{{<image src="images/mirror_snap.jpg" alt="Square shape." size="40%" >}}

Then I look to the snapping through a session analises and I decided to make some changes to make the snapping more smooth. Once again I’ve never done this so I wanna make sure it works on the first try.

{{<image src="images/snaping.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/some_changes_to_the_snaping.jpg" alt="Square shape." size="40%"  >}}

This is how the final snapping looks like.

{{<image src="images/happy_with_final_snapping.jpg" alt="Square shape." size="40%" >}}

Next, I make the smaller piece that will be the “door” to add and remove the display from the box. Once again I’ve never done this before and I didn’t had any tutorial that I could base myself on so I just dreamed how could I make the attachment. 

{{<image src="images/making_attachment_piece.jpg" alt="Square shape." size="40%" >}}

I think it makes sense to be a sliding piece. So I design this rounded extrusions that are the result of applying fillets to a rectangular.

{{<image src="images/attach_side.jpg" alt="Square shape." size="40%" >}}

Then I use the “combine” tool where I select the piece and the up case as the target body and I select the option cut so then the up case will have this rounded extrusions cut off.

{{<image src="images/side_on_up_box.jpg" alt="Square shape." size="40%" >}}

Then I also made an attachment bellow so I could snap the piece to the up case. But this is actually pointless because that piece turns out to be supper tiny so it actually doesn’t make any difference and also because the sliding piece its very tight to the up case that it holds itself very tightly to the up case.

{{<image src="images/attach_bellow.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/bellow_on_down_box.jpg" alt="Square shape." size="40%" >}}

Finally I decided to make some holes to attach better the display just in case my design wouldn’t make it tight enough. I just did this to have all the options available just in case.

{{<image src="images/2mm_hole_just_in_case.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/thread_in_hole.jpg" alt="Square shape." size="40%" >}}

Finally I just miss making a hole for the switch. 

I search for the switch on the “GrabCad” website and I import the respective step file. I couldn’t find the exact switch but I thought it would have the same measurements so I went for it. It was very dumb of my side to assume that they would have the same measurements because at the end, after printing it, I notice that the hole was to small for the switch we have at the lab.

{{<image src="images/add_switch.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_hole_switch.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pocket_for_switch.jpg" alt="Square shape." size="40%" >}}

Then I’ve also had some kind of hole so I could easily remove the up part from the bottom part.

{{<image src="images/to_remove_up_part.jpg" alt="Square shape." size="40%" >}}
 
This is how the bottom and up part look like respectively.

{{<image src="images/final_bottom.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_up.jpg" alt="Square shape." size="40%" >}}

This is how the box will look like at the end. I put different colours for the different pieces I will print.

{{<image src="images/final_box.jpg" alt="Square shape." size="40%" >}}

I then 3D printed both the box and the key molde.

{{<image src="images/printing_keys_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/printing_keys_box_2.jpg" alt="Square shape." size="40%" >}}

{{<video src="images/printing_keys_box_v.mp4">}}{{</video>}}

{{<image src="images/printing_keys_box_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/printing_keys_box_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/printing_keys_box_5.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/printing_keys_box_6.jpg" alt="Square shape." size="40%" >}}

You can see from the images bellow how the box turned out. I’m honestly very happy with it because I’ve never design a box before and the attachments worked pretty well in my first try.

The only observation I have is that I tough that the red piece was a bit bigger. And therefore the attachment I made shown by the green circle on the image bellow it won’t even work and it’s pointless. However, the piece is very tight anyway so it won’t get loose and be lost.

{{<image src="images/attatchment_small.jpg" alt="Square shape." size="40%" >}}

On the following video you can see the mechanism I created to be able to insert the display and close it with the extra red piece.

{{<video src="images/showing_how_to_make_in_v.mp4">}}{{</video>}}

Another comment about the input box is that the hole for the switch is too small. The reason why is that I didn’t took the measurements myself from the one we have at the lab but instead I grabbed an *.stl file I found on GrabCad of a switch that clearly is not the same we have at the lab.

{{<image src="images/hole_too_small.jpg" alt="Square shape." size="40%" >}}

About the key molde, I tried it out on the board and it almost fit perfectly.

{{<image src="images/almost_perfect_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/almost_perfect_2.jpg" alt="Square shape." size="40%" >}}

The problem is that the MUX its a bit taller than what I predicted (I shouldn’t have been lazy and I should have just measure it with the calliper). But also one of the holes should be a bit deep.

{{<image src="images/almost_perfect_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/almost_perfect_4.jpg" alt="Square shape." size="40%" >}}

To fix it I just use this sanding tool for the MUX and the drilling machine to drill the hole a bit more.

{{<video src="images/sanding_tool_v.mp4">}}{{</video>}}

{{<image src="images/drill_hole_keys_mold.jpg" alt="Square shape." size="40%" >}}

Now, it fits much better.

{{<image src="images/fits_much_better_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/fits_much_better_2.jpg" alt="Square shape." size="40%" >}}

Then I pored the silicone on the key molde and I put the board on top of it. I made a video of me showing the whole procedure. I just have to submit it meanwhile because it’s about 20 min long.

After the silicone was ready I tried to remove the molde from the boar and it was very hard for two reasons. First I dint know about the magical spray that prevents from the silicone to be stake on the mold and second I could have improved my design a bit better. And that’s what I will do in the future. 

But meanwhile hers some videos of me struggling to open it. After the first video I just grabbed a spatula (same kind we use to remove the PCB from the milling machine) and I did my best to remove it. Im sure I heard some cracks so thank god I used my old board that doesn’t work. This is a positive view for milling so many times, is that I can use the older boards for testing.

{{<video src="images/struggle_removing_v.mp4">}}{{</video>}}

{{<video src="images/remove_first_molde_v.mp4">}}{{</video>}}

As you can see from the above video, the silicone keys weren’t fully covered by the silicone making some holes. I did have more silicone to poor but I thought that when inserting the board it would make volume and the mount of silicone I inserted would have been enough. But clearly it wasn’t so I will repeat again this with the updated key mold box and with considering all this issues. 

Although I just wanna mention that “bad things happen for a reason” (it’s a saying we have on Portugal). And what I mean by this is that now I know about the spray which will be crucial for the brain and finally, because the silicone didn’t cover too well some buttons I could realise that the buttons fully covered by the silicone where too heavy and hard to press whereas the buttons where the silicone was just posing on top where much easier to press. 

Conclusion I will first poor the silicone fully all over the keys but then I will reap it off. When doing that, I will still have the holes for the keys and they will be more loose and easier to press. 

  - #### Update on keys mold and box

As previously mentioned I made the switch hole too small. I head back again to Fusion to fix this issue. This time I grabbed a calliper and I measure the sizes from the switch i’m gonna use. Once again I learned from colleagues to consider the measurement a bit more than the one you see in the calliper. I did exactly that and at the end it fit perfectly. First try again. What a lucky beginner I am.

{{<image src="images/had_to_make_it_bigger.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/switch_in_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/switch_in_2.jpg" alt="Square shape." size="40%" >}}

I also made some changes to the key molde box to make it easier to remove. I then added this extrusions to the box and I must say it worked really well. The design was really well thought in my opinion.

{{<image src="images/easy_to_take_out.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/also_this_to_help_remove.jpg" alt="Square shape." size="40%" >}}

Then I 3D printed it again. Following is the new box on the right and the old one on the left. As you can see, the 3D print is not the best cause we can see the lines on the keys. Unfortunately the max quality from the Pursas are 0.20mm and therefore this things can happen. Although the old mold got a bit better. It might be the filament.

{{<image src="images/final_key_box_print_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_key_box_print_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_key_box_print_3.jpg" alt="Square shape." size="40%" >}}

Then the removal was so easy. This was the time I should have actually recorded.

{{<image src="images/easy_removal.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/second_mold.jpg" alt="Square shape." size="40%" >}}

# Design and Do Output Brain

For the brain I had the idea of just take an .*stl file of a brain design by someone else and move one from that. However the problem is that those files when open in Fusion 360 are converted to "mesh" which makes it impossible to edit or do anything but just directly 3D print it. There must be a way to make it work for sure. I did try it very fast by seeing a YouTube video but it didn’t work out so I just decided to ask a colleague how did he do a similar thing and he said he used blender for that. He also said he could help me on this. He said he could use Rhino to change it from mesh to something I could edit on Fusion 360. 

I found some but they were very detailed and therefore the computer couldn’t open or run it for being very heavy.

He then suggested me to just do the brain myself on blender. He said blender is very easy to learn and that there are a lot of videos explaining.

As always, I like to put myself into more challenges so I just decided to sculpt myself the brain in blender. I try to look up for YouTube tutorial on how to make a brain in blender and all the videos or either where to fast, or simple or with no voice explaining but just music. For a person that doesn’t know anything about blender or any short key, seeing videos with no explanation is impossible.

Since at this stage I was stuck with the MUX and I could ask for help for the professor I just decided to spend my entire day learning blender on YouTube with the classic “blender for beginners” donut tutorial. 

1. ### Learn Blender

To learn blender I started with the [classic tutorial](https://www.youtube.com/watch?v=nIoXOplUvAw&list=PLjEaoINr3zgFX8ZsChQVQsuDSjEqdWMAD), that for sure everyone did, made by Blender Guru YouTube channel. 

I took some pictures of the procedure but I won’t extend myself to much on this one.

I started by inserting the Torus shape on the Object Mode. 

{{<image src="images/add_torus_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/add_torus_2.jpg" alt="Square shape." size="40%" >}}

Then he mentions we have to make sure the scale is 1 in all the axis. I don’t quite remember why at the moment.

{{<image src="images/make_sure_scale_one.jpg" alt="Square shape." size="40%" >}}

Then we had to apply subdivisions so we can have this dots where we can pull them around. We will use them to shape the donut a bit. We all know the donuts don’t have a perfect round and smooth surface. They are wobbly.

{{<image src="images/shape_doughnut.jpg" alt="Square shape." size="40%" >}}

Next we do the icing. We also apply the subdivision to manipulate it. We also have to use the snap tool so we can make sure the icing goes down following the line of the donut. Otherwise the icing will be kinda floating frozen in the air.

{{<image src="images/do_icing_shape.jpg" alt="Square shape." size="40%" >}}

We also added a foto as reference to make sure we make a realistic icing. 

{{<image src="images/use_image_referance_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/use_image_referance_1.jpg" alt="Square shape." size="40%" >}}

This is how it looks. It’s still a bit rough. So then we move to the sculpting tab so we can sculpt the donut. This is one of my favourite parts.

{{<image src="images/sculpt_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/sculpt_2.jpg" alt="Square shape." size="40%" >}}

At this stage I just missed putting the appearance and making the environment around the doughnut. It was also almost the end of the day so I didn’t went farther cause that doesn’t matter for the brain. 

2. ### Do my own brain

Now it’s time to do my own brain. I found this [tutorial](https://www.youtube.com/watch?v=e82KKJTrGCw) on YouTube but they don’t talk, there’s only music on the background so I actually don’t know too much whats going on. The fact that I made the doughnut helped a lot to make this possible I must say.

Never the less, as you can see the begging of the video is an sphere at the sculpturing mode. However after seeing and searching a lot about blender I know that blender doesn’t provide us with the best sphere and therefore we should make our own from the square. There’s other options accordingly to this [tutorial](https://www.youtube.com/watch?v=aRSVYFrRWeU) I followed to make the sphere.

I then start with the square and I make it into a sphere.

{{<image src="images/start_with_square.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/turn_it_to_ball.jpg" alt="Square shape." size="40%" >}}

I finally have the sphere and I go to the sculpting mode and I start sculpting the sphere as I follow the tutorial.

During the whole sculpture of the brain we mainly just use two different tools. One where we actually shape the brain and the other one where we carve the brain to make the waves.

{{<image src="images/start_sculpting_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/start_sculpting_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/start_sculpting_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/start_sculpting_4.jpg" alt="Square shape." size="40%" >}}

It’s hard to see this going anywhere but we just have to continue. 

Once again ,we can also find images on the internet so we get inspirations from real models. I honestly didn’t guide myself too much with it but it was definitely helpful to have an idea.

{{<image src="images/image_as_reference.jpg" alt="Square shape." size="40%" >}}

On the image bellow I will use the second tool I mentioned for example.

{{<image src="images/start_sculpting_5.jpg" alt="Square shape." size="40%" >}}

It’s at this stage I start realising that the tutorial is using the “mirror” option and im not. I realised because he started carving the brain in one side and the same effect would happen magically on the other side. I was like “of course we have to have the mirror option so the brain can get symmetrical and also save double the time”. 

At this point I was using the old desktop computer we have at the lab and I was seeing it crashing. It was at least legging a lot. So since I had to repeat the brain again I just decided to start again in my computer that has a better CPU to run heavy programs.

I repeated the exact same thing as already mentioned and I arrive to this final shape of the brain that I’m happy with.

{{<image src="images/proccess_2.jpg" alt="Square shape." size="40%" >}}

This time I also made sure to apply the mirror tool.

{{<image src="images/mirror.jpg" alt="Square shape." size="40%" >}}

Then I was having some problems with the second tool for carving. As much I would make the tool smaller it would carve a very big hole. I don’t know what I did but at some pint I made it work. I don’t know if I went back to the Model mode and I made sure the scale was set to 1 or if I just turned the blender off and on back again. But anyway the following image shows the desire outcome. 

{{<image src="images/when_working_good.jpg" alt="Square shape." size="40%" >}}

Then I just continue craving the brain following the tutorial. That’s why I didn’t follow too much an existing image because I could just follow the tutorial.

I would also change between tools depending on what I was doing.

{{<image src="images/proccess_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/proccess_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/proccess_5.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/proccess_6.jpg" alt="Square shape." size="40%" >}}

At some point I did this but dont know how. I decided to show because it’s so funny the fact it looks like evil horns. 

{{<image src="images/did_shit_but_funny.jpg" alt="Square shape." size="40%" >}}

Then I fix it and continue sculpting the brain. It’s starting getting shape. Exciting!!!

{{<image src="images/proccess_7.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/proccess_8.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/proccess_9.jpg" alt="Square shape." size="40%" >}}

When it comes to the waves of the brain itself I just invented myself the patterns. This is how it looks after sculpting the brain. Pretty good!

{{<image src="images/final_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_4.jpg" alt="Square shape." size="40%" >}}

Then I decided to just do the molding box in blender. I saw this [tutorial](https://www.youtube.com/watch?v=57CmROIJP6w) about making molding boxes on blender where I took some ideas.

I first started by making a copy of the brain just in case I mess up I don’t lose it. Smart.

{{<image src="images/made_copy_in_case_i_mess_up.jpg" alt="Square shape." size="40%" >}}

Then I insert a cube which will be my box.

{{<image src="images/add_cube.jpg" alt="Square shape." size="40%" >}}

Then I start designing the box around the brain. I thought how would I make the casting of the brain and I design the box accordingly.

{{<image src="images/cube_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cube_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cube_3.jpg" alt="Square shape." size="40%" >}}

Then I apply “Boolean” so I can intersect the brain with the box and have the shape of the brain left in the box.

{{<image src="images/intersect.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/module_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/module_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/module_3.jpg" alt="Square shape." size="40%" >}}

Next I wanna make a small object to insert inside the brain so my brain has a certain thickness.

I first split the brain. I saw how to do this in another tutorial off course.

I have to use the bisset tool.

{{<image src="images/cutting_brain_0.jpg" alt="Square shape." size="40%" >}}

Then we have to select what we wanna divide.

{{<image src="images/cutting_brain_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cutting_brain_3.jpg" alt="Square shape." size="40%" >}}

And we are then left with two pieces.

{{<image src="images/cutting_brain_2.jpg" alt="Square shape." size="40%" >}}

Then I do the same idea as the doughnut and I start pulling some dots until I reach a certain shape.

{{<image src="images/making_boal_inside_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/making_boal_inside_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/making_boal_inside_3.jpg" alt="Square shape." size="40%" >}}

Honestly iim not happy with the result and by now it feels like I’m wasting too much time learning blender to make things I could just do in Fusion. 

{{<image src="images/not_good_solution.jpg" alt="Square shape." size="40%" >}}

I comment this out with my colleague that told me about blender for the first time. He agreed with me that I should move the brain to Fusion and just work with it there from now on.

I told him that it might not work because fusion doesn’t work too well with meshes. He said that he could help me on converting mesh to vectors using rhino. I really much appreciated the help because I couldn’t afford wasting my time learning another CAD software such as the beast Rhino. 

He then show me the final result and he send me the step file so I could upload in fusion. 

{{<image src="images/from_rhino.jpg" alt="Square shape." size="40%" >}}

This is the brain in fusion. Amazing. 

{{<image src="images/moved_to_fusion.jpg" alt="Square shape." size="40%" >}}

3. ### Design Mold Box

I first had to scale my brain. It was 3 m long !!

{{<image src="images/scale_brain.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/scale_factor.jpg" alt="Square shape." size="40%" >}}

I’m happy with the final size.

{{<image src="images/good_size.jpg" alt="Square shape." size="40%" >}}

Then I start doing the box. At this stage my knowledge of fusion was still a bit limit. It still is but back then was even more. So to do the boxes I was first doing 4 individual boxes.

{{<image src="images/do_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/do_box_2.jpg" alt="Square shape." size="40%" >}}

But then I learned I could just do a big one and split it using planes. I learned that strategy in some tutorial. I would see loots of tutorials because I was making the input box at the same time. And its when I learned a lot about tools I barely used. Again I’m still very new in fusion and any CAD software really.

{{<image src="images/box_sketch.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/box_new_body.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/box_opacity.jpg" alt="Square shape." size="40%" >}}

I also learned in those tutorial how to reduce the opacity of a body so it’s easier to see the other ones inside. 

Then I also learned how to subtract the brain on the box. We have to use the tool “Combine” and then select the box as the target body and the brain as the tool body. I’ve also learned by experience what does “New Component” and “Keep tools” mean. New component will create a new component of the result. And keep tools will maintain the body we are using as cutting. In this case if we don’t keep the tool tick we will lose the brain. 

{{<image src="images/to_substract.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/substract.jpg" alt="Square shape." size="40%" >}}

Then, like already mentioned, I’ll create the offset planes that I will use to cut the box into 4 parts. At this stage I still didn’t learned about the option “MidPlane” otherwise I would have done that. In this case I had to count where was the middle. 

{{<image src="images/offset_plane_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/offset_plane_2.jpg" alt="Square shape." size="40%" >}}

Then we use the split body tool to split the box. I select the box as target and the planes as tools.

{{<image src="images/split_body_1.jpg" alt="Square shape." size="40%" >}}

And this is how the box looks like.

{{<image src="images/box_split_left.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/box_split_right.jpg" alt="Square shape." size="40%" >}}

I was following a tutorial on how to make molding boxes in Fusion and he used this spheres to help with the alignment of the boxes when we want to combine them together. I do the same.

{{<image src="images/registration_pins_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/registration_pins_2.jpg" alt="Square shape." size="40%" >}}

Then I use “rectangular pattern” to repeat the sphere. That’s another tool I’m glad I learned cause before I would do “Move/copy” tool and have to count the distance where to put. Now I know better.

{{<image src="images/retangular_pattern.jpg" alt="Square shape." size="40%" >}}

This is how it looks like.

{{<image src="images/registration_pins_3.jpg" alt="Square shape." size="40%" >}}

Then I use the same strategy as before to subtract the spheres on the boxes bellow. I use the “Combine” tool again for the subtraction.

{{<image src="images/substract_pins.jpg" alt="Square shape." size="40%" >}}

This is how it looks like.

{{<image src="images/registration_pins_4.jpg" alt="Square shape." size="40%"  >}}

I have a note about this, at the time I’m writing this I already printed the box and I must say I should have made a face offset on the hole for the sphere since it was a bit to tight. But in my defence I was following a tutorial of a person that should have more experience them me. Never the less I found a solution which I will mention latter in the documentation.

I then repeat the same procedure of the rest of the faces I have to combine. Such as the box on the right with the box on the left.

{{<image src="images/pins_also_on_side_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pins_also_on_side_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pins_also_on_side_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pins_also_on_side_4.jpg" alt="Square shape." size="40%" >}}

After all pins added this is how it looks.

{{<image src="images/after_all_pins.jpg" alt="Square shape." size="40%" >}}

Now that I have the box done I will make an interior object so I can insert in the middle to make the brain without interior.

For that I decided to just copy paste the brain and scale down a bit the copied brain.

{{<image src="images/scale_for_interior_body.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/method_not_best.jpg" alt="Square shape." size="40%" >}}

But as you can see from the image above, scaling it down like this is not the best.

So I just decided to change the “scaling point”. Luckily there’s that option in fusion. 

{{<image src="images/change_point_of_scale.jpg" alt="Square shape." size="40%" >}}

However I had to eyeball it anyway. It took so long to find a reasonable place. I had to use “Section Analises” to make it easier to see if at any point the interior brain was touching the box. I finally reach a point I was happy with. I was a bit concerned if the brain would deflate because a colleague mentioned that for him he had to make the walls 1mm thick because 2mm was too much. And I have more than 2mm thickness in some areas for sure. 

However I couldn’t do batter and I was already losing to much time with it. Also im gonna deflate not inflate so should be easier and I also have a bigger pump. So I just went for it and I risked it.

{{<image src="images/section_analizes_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/section_analizes_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/section_analizes_3.jpg" alt="Square shape." size="40%" >}}

Then I have to make an attachment to the interior piece so I can maneuver it. My first idea is to have the two up parts of the boxes together, then pour the liquid inside and then stuff the interior pieces inside the brain , the put the two left bottom pieces and pour the rest of the liquid. So for that I first start with the attachment.

{{<image src="images/scketch_for_hold_piece.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/thickness.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/same_for_front.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pipe_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pipe_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pipe_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pipe_4.jpg" alt="Square shape." size="40%" >}}

Then ill extrude the cylinder and join it with the small interior brain.

{{<image src="images/join_piece_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/join_piece_2.jpg" alt="Square shape." size="40%" >}}

Then I will cut that same piece on the bottom box.

{{<image src="images/cut_piece_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cut_piece_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cut_piece_3.jpg" alt="Square shape." size="40%" >}}

Then I make a cone so I can pour the liquid from that hole.

{{<image src="images/make_cone.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/axis_to_revolve.jpg" alt="Square shape." size="40%">}}

{{<image src="images/revolve_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/revolve_2.jpg" alt="Square shape." size="40%" >}}

I also had some fillet.

{{<image src="images/fillet_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/fillet_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/fillet_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/fillet_4.jpg" alt="Square shape." size="40%" >}}

When I reach this far I start realising that this method won’t work because I cant join the two up pieces and stuff the brain in. It won’t fit. So I add to change my approach. I was trying my best to avoid doing the brain in two pieces and join them together afterwards but that’s what I’m left to do. I was talking with a colleague and he suggested me to hold the interior piece with clamps. He’s doing the same and it works I found that a good idea so I changed my design for that.

I first start by splitting the brain, because now I will have to have two individual moods for the right and left side. 

{{<image src="images/split_brain.jpg" alt="Square shape." size="40%" >}}

Then I make this attachment on the box so I can clamp the interior brain with it.

{{<image src="images/make_suport_on_box.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_support_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_support_2.jpg" alt="Square shape." size="40%" >}}

Then as a final touch I will put this attachments all over the box to attach them together with crews. Spoiler alert I end up just clamp it to make sure they were really well tight. Instead of using the screws. They wouldn’t be strong enough to press the two pieces together.

{{<image src="images/for_the_threads.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/M6_but_put_7.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/thread_it_self.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/pattern_for_thread.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/all_four_threads.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_threads_every_where.jpg" alt="Square shape." size="40%" >}}

Following is how the mold looks inside and outside respectively.

{{<image src="images/mold_inside.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/finish_mold.jpg" alt="Square shape." size="40%" >}}

Then I move on to 3D print the moles. My friends recommended me to go to care since their “ultimakers” are better maintained and thus print better. I really wanted to print this as soon as possible that I end up printing one of the inside brains in FabLab. 

{{<image src="images/print_fablab_1.jpg" alt="Square shape." size="40%" >}}

But I add immediately a problem which was not enough filament for the size of the brain and therefore the printer was printing in the air. 

{{<image src="images/print_fablab_2.jpg" alt="Square shape." size="40%" >}}

But then I change the filament and the print was successful. I can see a bit the difference of quality between the other half printed in vare. The print in care looks as follow.

{{<image src="images/print_vare_1.jpg" alt="Square shape." size="40%" >}}

In vare I also printed the up and bottom mold boxes. Everything looks so nice. And also a bit smaller than what i was thinking it would be but thats fine.

{{<image src="images/print_vare_2.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/print_vare_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/print_vare_4.jpg" alt="Square shape." size="40%" >}}

Then as you can see on the video bellow, the sphere tabs are a bit too big for the holes so the connection doesn’t fit perfectly. And I don’t want that. I wanna make sure that its as tight and sealed as possible do no silicone can go through it.

{{<video src="images/make_it_sealer_v.mp4">}}{{</video>}}

I wasn’t gonna print everything again off course. I couldn’t afford losing that time and waste more PLA. So I just go with the engineering way of solving or as I like to say in Portuguese “Desemerdar”. I go to the wood workshop and I found this sanding tools and I just start sanding the spheres until they get shorter and fit the other half perfectly.

{{<image src="images/make_it_shorter_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_it_shorter_2.jpg" alt="Square shape." size="40%" >}}

And as you can see on the image above they now fit perfectly. I do the same with the rest of the spheres.

{{<image src="images/make_it_shorter_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_it_shorter_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_it_shorter_5.jpg" alt="Square shape." size="40%" >}}

I even drop one of them and it broke a bit. I put like 5% infill for printing so it would take less time. And that’s why those pieces are easily breakable. But that’s fine. I already decided I will just use clamps to push them together instead of the screws. It’s much faster and efficient.

{{<image src="images/make_it_shorter_6.jpg" alt="Square shape." size="40%" >}}

Finally, I see if my idea with the small clamps works on holding the brain and it does.

{{<image src="images/it_holdes_good.jpg" alt="Square shape." size="40%"  >}}

4. ### Cast the Brain

For the casting I decided to do a video of me doing it. This turned out to be a mess so I don’t feel proud of showing it. 

But if I have to put the video into words. I started by pouring water into the mold with the small brain inside to see more less how much grams I will need of silicone. It was already around hundred grams! Then I had to double the amount for the other half plus a bit extra to repeat the molde for the keys for the input box. I remember it end up being around 2 hundred something. 

So you can imagine how bad this will turn out. 

I start pouring the parts and at the end the cup was almost full. I had to mix it well before putting in the pressure machine. Mixing was already so hard and I had a voice inside of me saying that I should do the 3 parts separate instead of all of them together. But I was stubborn cause I just wanted to have it done as soon as possible before lunch. 

Well turns out that the liquid expands when inside the pressure machine so I might have spoiled a lot of material after finishing with the machine. I look inside and I see the mess. I just wanted to cry. I had to clean everything but I barely had paper left. 

Nevertheless after the cleaning I only had enough for one half of the brain and the keys mold. So I decide to do it. Pouring everything was fine and this is how they look like. In the corner surrounded in green is the key box.

{{<image src="images/final_after_mess.jpg" alt="Square shape." size="40%" >}}

Then I made again more material for the other half of the brain. This time I even put too little so I had to prepare more material again afterwards.

{{<image src="images/too_little_material_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/too_little_material_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/too_little_material_3.jpg" alt="Square shape." size="40%" >}}

As you might have notice I put a stick and a height on top to make sure the brain stays evenly with the box surface and doesn’t lift up with the liquid.

Then after some hours I come back to remove the interior part and to join the two half together. To remove the interior pieces was a challenge. I even broke a tool and a bit one of the interior brains.

{{<image src="images/after_dry.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/interior_brain_broke.jpg" alt="Square shape." size="40%" >}}

But them I found some strategies and the other half got out more smoothly. Thank good I used the spray otherwise it would have been impossible to remove the pieces from the silicone.

{{<image src="images/out_smooth_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/out_smooth_2.jpg" alt="Square shape." size="40%" >}}

Then I prepare more material and I join the pieces together. I use a lot of clamps to tight the box as much as possible. 

{{<image src="images/join_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/join_2.jpg" alt="Square shape." size="40%" >}}

Finally, after it’s done I go there and I remove the brain from the box. This time was way more smooth and easy.

{{<image src="images/remove_from_box_1.jpg" alt="Square shape." size="40%" >}}

And this is how the brain looks like. Looks so niceeeee !!! So happy !!

{{<image src="images/remove_from_box_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/remove_from_box_3.jpg" alt="Square shape." size="40%" >}}

{{<video src="images/how_brain_looks_like_v.mp4">}}{{</video>}}

I arrive to the classroom and my colleague got so excited about it so we decided to try it out immediately to see if it works. We we so happy to see the result. Honestly, thank god it worked. I really didn’t wanna do another design, 3D print for days and cast it again. 

{{<video src="images/pump_brain_succ_v.mp4">}}{{</video>}}

Finally, just a note. I didn’t know how to use the power supply machine before and that’s why I exploded the led previously. But now I learned. 

On this [website](https://www.distrelec.de/en/vacuum-pump-12v-sparkfun-electronics-d2028b/p/30145494) I found the [datasheet](https://www.distrelec.de/Web/Downloads/_t/ds/ROB-10398_eng_tds.pdf) of the pump.

In the datasheet they don’t say specifically the current but we know this mathematical equation P=VI and since P=12 and V=12 then MAX current I=1A but ill setup lower first just in case.

Basically we first have to join the two clamps together and set the max current we want to have. I'll set it to 0.7A to not be the exact max value.

{{<image src="images/set_current.jpg" alt="Square shape." size="40%" >}}

Then we take the clamps apart and we set the max voltage we want. Once again my pump has a max voltage of 12V.

{{<image src="images/set_voltage.jpg" alt="Square shape." size="40%" >}}

And this is how the setup for the machine is done. Then we just put the clamps on the circuit we wanna test and turn the power supply machine on.

Now that I think about, it actually makes sense. I remember back when I studies Electronic theory, when we wanted to measure the current we would do the calculations with the circuit in short circuit and when we wanted to measure the voltage we would calculate it in open circuit. 

5. ### KiCad Output Brain

I’m gonna keep this short cause by now I started being repetitive with the explanations. 

The PCB for the brain its a bit less complex. I will have as components a pump, a valve, a pressure sensor, a voltage converter, LED strip, a XIAO board and a battery.

So I start by adding the XIAO board, headers for the pressure sensor and voltage converter. As for the pump, valve, batteries and LED strip I will use terminals.

Then, in order to control the pump and valve I’ve been told by a colleague I could use relays. I’ve never heard of it so I had to so some research on how to connect them.

I add them to the canvas.

{{<image src="images/add_relay_1.jpg" alt="Square shape." size="40%" >}}

And this is how they should be connected.

{{<image src="images/add_relay_2.jpg" alt="Square shape." size="40%" >}}

I also read about the micro pressure sensor to know how to connect it. 

{{<image src="images/how_to_press_sensor_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/how_to_press_sensor_2.jpg" alt="Square shape." size="40%" >}}

I also checked the LED strip i have and its no suprise that they have to be powered with 5V.

{{<image src="images/power_LED_strip.jpg" alt="Square shape." size="40%" >}}

At this stage I was talking with the professor and he told me to use a MOSFET along with a DIODE instead of the relay. He also explained me how it worked and the reason why of inserting it. A MOSFET worker like a gate. I will have the XIAO board controlling the MOSFET (Controlling the Gate pin on the MOSFET). Depending if it’s HIGH or LOW there will be a change on the polarity of the electrons closing or opening the “gate”.

So, in my case, I have a terminal connected to the MOSFET. Which means that when the “gate” its open on of the pins one the terminal will be connected to ground. However if it’s closed, it will be floating. This terminals will be powering the pump and the valve. Meaning that when the gate its open we supply them with voltage and they are ON. And when the gate its closed the ground is floating and no voltage its provided making them OFF. 

The other pin of the terminal is the positive power. We also have to insert a diode between the MOSFET and the power supply to prevent from current to go to the MOSFET and destroy it.

And finally this is how the canvas looks like after inserting all the elements.

{{<image src="images/final_white_out.jpg" alt="Square shape." size="40%" >}}

**NOTE:** As you can see on the image above I have the SDA and SCL correctly placed at this stage. But them I move to the PCB editor and I decided to change them just to be easier to connect the paths. Off course by that time I didn’t even realised what I was doing because the XIAO has specific and unique pins just for SDA and SCL and therefore they cant be changed to other locations on the XIAO board.

Moving on to the PCB editor. I place all the elements thinking how would I place the pump and batteries later in the box.

After making the input box I know by now I should make the battery paths thicker. I thouth it has to do with the voltage but it has more to do with the current flowing. In my case I will use around 0.7A for the pump and no more than 1A for sure. The professor says that a safe value is 0,7mm or 0,8mm. I went for the 0,8mm since I have all the space I want and since I wanna be safe cause I don’t feel like milling 5 times again. I also asked if I could use the same thickness for both 5V and 12V as the current might be different but he said I could use the same. 

I then create a net class for the battery.

{{<image src="images/create_net_class_bat.jpg" alt="Square shape." size="40%" >}}

Then I have to assign the battery paths to that one I created. 

First we have to right click on the path and click “Assign Netclass…”

{{<image src="images/assign_1.jpg" alt="Square shape." size="40%" >}}

Then we choose the one we created called “BAT”. By doing this, all the paths that are “PWR_12V” will change.

{{<image src="images/assign_2.jpg" alt="Square shape." size="40%" >}}

Then we right click the path again and we go to properties and we tick the “Use net class widths”.

{{<image src="images/assign_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/assign_4.jpg" alt="Square shape." size="40%" >}}

By pressing “ok” all the “PWR_12V” paths will be changed to the net class I created called “BAT”.  As you can see on the image bellow the path is thicker.

{{<image src="images/assign_5.jpg" alt="Square shape." size="40%" >}}

I did the same for the 5V paths.

This is how it looks like at the end. By this time I had already change the SDA and SCL like mentioned above. Ups, sounds like me from the future will have to mill again.

{{<image src="images/final_black_out.jpg" alt="Square shape." size="40%" >}}

I run the rule checker and everything looks good.

{{<image src="images/good_rule_checker.jpg" alt="Square shape." size="40%" >}}

Following is the 3D viewer and the Geber view.

{{<image src="images/3D_out_brain.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/geber_out.jpg" alt="Square shape." size="40%" >}}

  - #### Update Kicad

As mentioned above I changed the SDA and SCL pins. So I went back again to the XIAO board to confirm their location.

{{<image src="images/confirm_location_pins.jpg" alt="Square shape." size="40%" >}}

And then I changed on the canvas and on the PCB editor. Now, everything should work.

{{<image src="images/changes_to_white_out_box.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/new_out_box.jpg" alt="Square shape." size="40%"  >}}

6. ### Milling and Soldering

  - #### Milling
I first start by generating the tool paths in CopperCam. Now I know and I always put at least 4 for the “contour around paths” just to make sure. This is how it looks like. For once there’s not that many red spots.

{{<image src="images/milling_out_box.jpg" alt="Square shape." size="40%"  >}}

Then I milled the board again on the MDX-40. At this point we don’t have the collet on the other machine. The professor also mentioned we should use the MDX-40 if we are using the smaller bit. 

**Note:** When I export the rootpaths there’s two files being generated. One for engraving and one for cutting and drilling. But there were some times that the drilling paths weren’t on the same file as the cutting. It would only cut and I would have to go back to CopperCam and save just the drilling tool paths again.

  - #### Soldering

Like always I start by seeing with the multimeter if all the paths are well connected. As usual they were.

{{<image src="images/all_good_out.jpg" alt="Square shape." size="40%" >}}

Then I placed all the rivets. I realise that when using the MDX-40 there’s no need to put the holes for the rivets 1.55mm I think 1.50 is enough. I feel like it’s because MDX-40 is better and more precise than SRM-20. So the problem now is that its a bit hard to insert the rivets cause the hole its too loose. I made a video to show how I do it.

{{<video src="images/place_rivets_v.mp4">}}{{</video>}}

Then I test with the multimeters if the rivets are touching each other and making a short circuit. And as you can see on the video below everything is fine.

{{<video src="images/test_rivets_v.mp4">}}{{</video>}}

After the rivets I solder the diodes. I had to go check the datasheet to see the orientation. As you can see on the image bellow, the black bar as to be on the side of the power supply. And the other side facing the MOSFET.

{{<image src="images/how_to_solder_diod.jpg" alt="Square shape." size="40%" >}}

I test once again if I’m making any short circuit.

{{<video src="images/mul_diode_v.mp4">}}{{</video>}}

Then I solder the MOSFETs and I do the same. On the video bellow you can see that there is two short circuits. One in both the MOSFETs.

{{<video src="images/short_circuit_mosfet_v.mp4">}}{{</video>}}

Then I remove the MOSFET to solder back again. I do it with the help of the Heat gun. Latter I found out is not the best solution cause that can brake the MOSFET. But It actually didn’t so that’s good.

{{<video src="images/remove_short_circuit_gun_v.mp4">}}{{</video>}}

Then I check that I removed the short-circuit on the 1st MOSFET but there’s still one more missing.

{{<video src="images/checking_sc_on_mosfet_v.mp4">}}{{</video>}}

I found out that I had a bit of solder on the tabs where the XIAO board will be inserted. I’m so surprised I spotted that small piece of solder but I’m very proud. 

The following video is me simulating the discovery and how did I fix it.

{{<video src="images/fixed_sc_mosfet_v.mp4">}}{{</video>}}

Then I confirm I removed all the short-circuits.

{{<video src="images/confirm_no_sc_on_mosfet_v.mp4">}}{{</video>}}

Then I check if all the connections that go to the MOSFET are working properly and its when I realised that there’s is no connection between the diode and the MOSFET in one of them.

{{<video src="images/no_path_diode_mosfet_v.mp4">}}{{</video>}}

I look closely and I realises that I might have destroyed the path when using the heat gun. I can see the path a bit lifted. I tryed to fix it with solder by soldering the path to the MOSFET but it didn’t work. I didn’t wanna mill again so I just thought on put a bit of wire connecting the misfit to the path again. It worked surprisingly well and you can barely see the wire. It was also so well solder, I’m proud.

{{< youtube X_1e_B8fp-o  >}} 

When I started inserting the terminals and headers I realised that my design was made a bit in rush since I dint consider too much the distances and sized of the elements. For example the 3-terminal and the sensor almost interfere with each-the. 

{{<image src="images/all_good_out.jpg" alt="Square shape." size="40%" >}}

But what is even worse is the 2-terminal for the battery and the converter. They don’t fit if I put them both on the same side of the board.  But luckily it works if one of them is on the other side of the board. If the converter didn’t have any specific orientation it could fit. However the small dot indicating Vin has to be placed correctly.

I was trying to avoid putting components on the other side of the board so the board can be as much flat as possible. But it’s okay I will just make some pols to lift up the board. I made 4 holes for screws already on purpose in case I messed up something.

{{<video src="images/components_dont_fit_v.mp4">}}{{</video>}}

{{<image src="images/term_conv_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/term_conv_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/term_conv_3.jpg" alt="Square shape." size="40%" >}}

7. ### Arduino Code 

Now that the board is ready I will first check the micro pressure sensor. I go the same [website] (https://learn.sparkfun.com/tutorials/sparkfun-qwiic-micropressure-hookup-guide/all) I used to check the pins connections and I install the specific Arduino library.

{{<image src="images/install_arduino_pressure.jpg" alt="Square shape." size="40%"  >}}

Then I test if the sensor works using the example code provided. And the Serial Monitor wasn’t working. 

{{<image src="images/monitor_not_working.jpg" alt="Square shape." size="40%" >}}

I tryed everything. I tryed booting the board and flashing the software again to the board and still nothing. I asked help for the professor. He said that it makes easier to debug if we do a simpler code like just a print to serial monitor. He does that and still not working.

{{<image src="images/board_not_working_proprely.jpg" alt="Square shape." size="40%" >}}

Then he went to his computer and he tryed the same and again it didn’t work. Then at some point it started working and I think its because he pressed the restart Botton on the board. Now the simple code its working.

{{<image src="images/finally_working_simple_code.jpg" alt="Square shape." size="40%" >}}

Then I upload once again the example code and it wasn’t working. It was printing that the sensor wasn’t connected. 

I then tried the code with jumper cables to see if the problem was with my board or with the sensor. And it worked. Which means that my board its not working. NOOOOO !!! I thought I was finally gonna do this at the first try.

{{<image src="images/working_sensor_with_cables.jpg" alt="Square shape." size="40%" >}}

I grabbed the multimeter to see if there was any short circuit or any path that wasn’t properly connected to the sensor. But everything was fine. 

I then go to Kicad and I realise that I changed the pin layout at some point. And now the SDA and SCL pins are not properly connected to the XIAO ones. I don’t know why I thought on looking at this but I’m proud I manage to find the error very quickly. 

{{<image src="images/confirm_location_pins.jpg" alt="Square shape." size="40%" >}}

I had to change the Kicad again but I already mention the updates I did on the “kicad” session above.

8. ### Mill and Solder again

Now that I fixed the Kicad I milled the board again. This time I didn’t succeed on the first try. 

As you con see on the board bellow there are some paths that are too thin. I checked with the multimeter and of course some paths weren’t connected all the way between tabs. 

{{<image src="images/first_try_shit.jpg" alt="Square shape." size="40%" >}}

At this point I wasn’t sure what could it be. I first thought that I had chosen too much contour around the tabs. But I actually did the exact same as last time so it shouldn’t be that. 

I had some ideas from the professor of what could be the problem.

The problem here was that I used a tool that was a bit broken and thus wasn’t too sharp on the tip. So when cutting the width of the path will be bigger because the tool, since it’s broken, has a bigger cutting area. The professor also recommended to put a very thin piece of paper between the tool and the board. I end up not doing it because I think the problem was the tool and I wanted to try that hypothesis. 
I was also wondering if the bed wasn’t leveled and if I should change places but he said that there’s no need for that. I end up changing the place because of the leftover board I pick only allow me to mill in a certain area. 

At the end the milling worked so I can conclude the problem was the tool not be sharp enough! I’m kinda disappointed because I did checked the first time if the tool was broken and I notice was less sharp, but honestly I didn’t though it would influence. But it actually does influence a lot. Now I know and I won’t commit the same error again. I also have a problem of assuming the things will also work.

Nevertheless you can see on the image bellow that the paths are way more clear.

Then I move on to soldering and I do pretty much the exact same procedure as last time.

In order to not use the last converter from the lab and since I’ve used a lot of them by now I was encourage to try to remove the previous one from the old board and reutilise it. We all know how hard and long it is to remove solder but I accepted the challenge. I did like I was taught by the professor. I first use the solder sucker followed by the wire strip and I manage to remove it uhuhuh. The converter end up having the pins a bit curved but I manage to straight them with a clipper. Let’s hope it works at the end.

{{<image src="images/remove_conv_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/remove_conv_2.jpg" alt="Square shape." size="40%" >}}

At the end I measure with the multimeter if everything was well connected and I discover that one of the terminal wasn’t in contact with the rivets as I didn’t get any response from the multimeter. I didn’t know what to do. After some thoughts I decided to just try or either to remove it or press it more in. I did the same procedure as we do to remove solder. I tried and nothing seams to work. I then used the wire solder to try to remove more solder. I try to pull it off but it wouldn’t get out so I just decided to press it more in. I measure with the multimeter and that actually worked. I’m so happy cause I was about to run out of more ideas to do. Then I solder the pins back again. 

Following is a video simulating what I did to find the solution.

{{< youtube -ZtTreNGkos  >}}

This is how the board looks like at the end.

{{<image src="images/end_board_out_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/end_board_out_2.jpg" alt="Square shape." size="40%" >}}

9. ### Arduino Code again

1. **Pressure Sensor**
I first test the sensor pressor if it’s working this time. It seams to be working since now I actually see values being printed.

The following videos is me trying to understand how does the sensor work and how big is the range of the pressure when I put my finger on it. I even try to see different kinds of units. On the last video you can see that sometimes if I put my finger a long time it feels like it gets suck on the 360 value. Judging by my experiments this sensor is not giving me good vibes. It feels very unstable with the changes of values. 

{{<video src="images/read_sensor_shit_1_v.mp4">}}{{</video>}}

{{<video src="images/read_sensor_shit_2_v.mp4">}}{{</video>}}

{{<video src="images/read_sensor_shit_3_v.mp4">}}{{</video>}}

Nevertheless, I know now that the sensor works so I will continue with coding the board. I will now code the LEDs that will go inside the brain. I do a simple code just for the sake of testing and later I will integrate everything in the server code. 

2. **LEDs**

As you can see, by this time I already have the LEDs inside the brain even though I didn’t mention them yet here in the documentation. You can read about it later but for now let’s focus on the code itself.

I did a simple code just for turning the LEDs on. I used once again the Adafruit_NeoPixel library. However this time the LEDs I have are just RGB and not RGBW like the ones I use on the input box. 

Therefore, when I first coded and I tried to output white colour something different happened. I was so afraid I broke something. Following is a video of what happened.

{{<video src="images/not_exaclty_white_1_v.mp4">}}{{</video>}}

So then I went to the [Adafruit specific webpage](https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library-use) about NEON pixels when I found the following important information.

So when declaring the strip I changed from NEON_RGBW to NEON_RGB. The strip I have is WS2812B so I maintained the NEO_KHZ800.

{{<image src="images/important_info_declare_RGB.jpg" alt="Square shape." size="40%" >}}

Then I had to search how to make white out of RGB colours and this is what I found.

{{<image src="images/how_to_make_white_with_RGB.jpg" alt="Square shape." size="40%" >}}

It works although the colour is not exactly white but a mix between violet-blue-whitish colour.

{{<video src="images/not_exaclty_white_2_v.mp4">}}{{</video>}}

By this time I was using a 2- battery energy supply to power the LEDs. But now it’s time to change to a 3-battery supply so I can have enough voltage to power the pump. 

3. **Pump**

I kept asking the professor if I should use the batteries or just plug the brain to the buildings electricity. He said for me to just test and see. I was skeptical that the batteries would be powerful enough for the pump and also I doubt that they would last all day. But nevertheless I did what he said and I tried it myself. Thats the best way to learn. It’s by doing and not by always being told what to do.

So I start by soldering the battery to the switch.

{{<image src="images/switch_battery.jpg" alt="Square shape." size="40%" >}}

However I found the battery cables very thin so I asked the professor if I should change them or if those default ones would work. He told me to change and he told about this new ones that appear at the lab. But then I saw the wire inside it looked as thin as the previous ones

{{<image src="images/new_random_cables.jpg" alt="Square shape." size="40%" >}}

I found those still thiner than the ones we have at the top shelf on the soldering station. So I just decided to go with those. Following is the comparative of the cables. The one I decided to go with is the one in the middle.

{{<image src="images/cables_order.jpg" alt="Square shape." size="40%" >}}

Replacing the default cables with the new ones was a challenge. I first started by removing the old one.

{{<image src="images/replace_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/replace_2.jpg" alt="Square shape." size="40%" >}}

Then I insert the new ones and I solder them. Always with the help of my best friend flux pen.

{{<image src="images/replace_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/replace_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/replace_5.jpg" alt="Square shape." size="40%" >}}

Then I tested if the connections were good and I showed how does the switch work.  I put a switch cause last time I tried to unplug the battery I saw a sparkle and I’m afraid to brake the board or something worser.

{{<video src="images/replace_6_v.mp4">}}{{</video>}}

I then did once again a simple code just to turn on the pump. The pump and the valve are controlled by the XIAO board so I just defined those pins as OUTPUT pins and every time I want to turn the pump ON I set the PUMP pin to HIGH. And if I wanna turn the pump OFF I set it too LOW. That simple. I’m proud I figure that out by myself without seeing anything on the internet or ask the professor. If not that hard but still not obvious.

I then upload the code and nothing happened. The pump didn’t turn on. Instead of first doubting the code I decided to measure the voltage that was being supplied by the battery and it turns out to be just 8.3V which indicates they are not fully charge! You can see this on the video bellow as well as me showing the voltage coming from the 5V converter. That means the converter works and I didn’t broke it when I change it from board to board.

{{<video src="images/not_enough_voltage_on_pump_v.mp4">}}{{</video>}}

Then I change the batteries to charged ones and still, even though the pump turns on, it’s not in its full power. So, the brain doesn’t deflate anyway. 

{{<video src="images/changed_batteries_still_weak_v.mp4">}}{{</video>}}

As I was experience this I was talking with my colleagues when they suggested me to just plug the brain instead of using batteries. The brain will be anyway stoped in the exhibition and not moving around so it’s fine. 

A colleagues show me this one that can provide 12V and a max of 1.3A which is perfect for my case. He also showed me how does the order of the cables go.

{{<image src="images/how_to_connect_cables.jpg" alt="Square shape." size="40%" >}}

**Note:** We have to be very careful to not touch it cause it runs 230V so we can die from it. Very scary!!

Then I tried the pump with the new power supply and the pump works perfectly and in its full power.

{{<video src="images/works_at_full_power_pump_v.mp4">}}{{</video>}}

Then I tried with the brain support I made to see if the air would go through it and if it would still push air strong enough. Once again I still didn’t mention it here in the documentation but I will later. 

{{<video src="images/pushing_air_v.mp4">}}{{</video>}}

Then I try to see if the pump is strong enough to deflate the brain. I’ve tested already with the power supply machine but this time I just wanna make sure this new power supply will be enough. This is me basically facilitating the debugging process in case it doesn’t work with the brain support. As expected it works.

{{<video src="images/works_with_power_supply_v.mp4">}}{{</video>}}

Then I try with the brain support to see if it works. I used the first one I made. It didn’t work though. I still didn’t lose the hope because I have the new one I made so I’ll wait to try with that one and I’ll take conclusions from it later. 

{{<video src="images/doesnt_work_with_1st_support_v.mp4">}}{{</video>}}

Finally to conclude the day I decided to integrate both the pump and the LEDs. At this stage I have the pressure sensor figured, I have the LEDs working and I can turn the pump ON and OFF.

So I decided to just create this simple code that turn the pump ON and OFF as long as the LEDs from 2s in 2s. 

Following is this starting code and a video.

{{< highlight go-html-template >}}

#include<Wire.h>
#include <SparkFun_MicroPressure.h>

//define Pressure Sensor
SparkFun_MicroPressure mpr; // Use default values with reset and EOC pins unused

//define pins
int pump_pin = D9;
int valve_pin = D8;

//LEDs
#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define LED_PIN D7
#define LED_COUNT 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_RGB + NEO_KHZ800);

void setup() {
  Serial.begin(115200);
  Wire.begin();

  pinMode(pump_pin,OUTPUT);
  pinMode(valve_pin,OUTPUT);
  digitalWrite(pump_pin,LOW);
  digitalWrite(valve_pin,LOW);

  // NEON pixels
  strip.begin();
  strip.setBrightness(64);
  strip.show();

  while(!mpr.begin())
  {
    Serial.println("Cannot connect to MicroPressure sensor.");
    delay(100);
  }
}


void LEDsOn(){
  uint32_t white = strip.Color(250,250,200);

  for (int i=0; i<6; i++){
    strip.setPixelColor(i, white);
  }
  strip.show();
}

void LEDsOff(){

  strip.clear();
  strip.show();
}

void loop() {

  Serial.print(mpr.readPressure(),4);
  Serial.println(" PSI");
  Serial.print(mpr.readPressure(KPA),4);
  Serial.println(" kPa");
  Serial.print(mpr.readPressure(ATM),6);
  Serial.println(" atm");
  Serial.println();
  delay(500);

  // turn on LEDs
  LEDsOn(); 

  int n=1;

  if (n=1){
    digitalWrite(pump_pin,HIGH);
    digitalWrite(valve_pin,HIGH);
    n=2;
  }
  delay(2000);
  digitalWrite(pump_pin,LOW);
  digitalWrite(valve_pin,LOW);
  // turn LEDs off
  LEDsOff(); 
  
/*
  if ((mpr.readPressure(ATM),6) > 1){
    // start pump and open valve
    digitalWrite(pump_pin,HIGH);
    digitalWrite(valve_pin,HIGH);
  }

  if ((mpr.readPressure(ATM),6) < 1){
    // stop pump and turn off valve
    digitalWrite(pump_pin,LOW);
    digitalWrite(valve_pin,LOW);
    
    
  }
  delay(200);
*/
/*
  LEDsOn();
  delay(2000);
  LEDsOff();
  delay(2000);
*/
}

{{< /highlight >}}


{{<video src="images/first_testing_pump_leds_v.mp4">}}{{</video>}}

10. ### Brain support

I think is finally time to keep you updated of the work I’ve shown already but didn’t talked about it yet. 

I’ll start with the design of the support for the brain. I did it in Fusion 360.

I create a new design and I insert the brain and mold on that design so I can use the lines as reference of the sizes. And thats what I do. I project the lines I find important for the design of the support.

{{<image src="images/sketch_project_line.jpg" alt="Square shape." size="40%" >}}

Then I design a shape I think it will work. I made the diameter of the support a bit thicker than the diameter of the silicone on purpose so it gets supper tight. Plot twist it end up just reaping the silicone in two. But it’s fine I can always glue them back together with more silicone.

{{<image src="images/first_scketch_idea.jpg" alt="Square shape."size="40%"  >}}

I also measured the width of the LEDs which is 10mm. As you can see on the image above I do a straight line of about 10mm and a curved line of 7mm thinking it would be enough to have to lines of 10mm LED strip. I was very much wrong. After printing it I see it doesn’t work. But I’ll keep the gossip for later. 

Then I revolve the sketch I made to make it circular.

{{<image src="images/revolve_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/revolve_2.jpg" alt="Square shape." size="40%" >}}

Then I thought that I would have to do this extra piece on top of the air pipe to protect the hole from being completely covered by the brain when deflate and potentially explode something. I was clearly wrong. First the brain has lot of irregularities to make that happen and it’s also not as thin and light as a paper to create that suction I was afraid of. But anyway, before using my brain cells I just continue with it. The worst part is that I lost time on this.

So I create this extra piece and I revolve it.

{{<image src="images/some_adjustments.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/another_revolve.jpg" alt="Square shape." size="40%" >}}

Then I make an offset plane to make some holes for the air to flow.

{{<image src="images/offset_plane_for_holes.jpg" alt="Square shape." size="40%" >}}

I’ve learned about this “circular pattern” that is a life changer and ill use it as much as I can and need.

{{<image src="images/circular_pattern.jpg" alt="Square shape." size="40%" >}}

Then I extrude the sketch.

{{<image src="images/extrude_body_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_body_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/for_pump_air.jpg" alt="Square shape." size="40%" >}}

Then I make this extrusion to reenforce the structure.

{{<image src="images/reforce_structure_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/reforce_structure_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/reforce_structure_3.jpg" alt="Square shape." size="40%" >}}

Then I had some fillets and some adjustments to make it easier to print without supports. The printers can go as far as 6mm horizontally without any supports.

{{<image src="images/fillet_and_adjstments.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/actually_better_for_printing.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/for_better_printing.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/2mm_good_printing_max_6mm.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/should_be_good.jpg" alt="Square shape." size="40%" >}}

This is how it looks.

{{<image src="images/final_support_1.jpg" alt="Square shape." size="40%" >}}

As you can see there will be no need for supports.

{{<image src="images/no_need_support_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/no_need_support_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/no_need_support_3.jpg" alt="Square shape." size="40%" >}}

Then I printed the support and as you can see it actually didn’t work ahahah.

{{<image src="images/up_support_fail.jpg" alt="Square shape." size="40%" >}}

This is the print overall.

{{<image src="images/first_print_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/first_print_2.jpg" alt="Square shape." size="40%" >}}

I also tried putting it inside the brain to see if it fits. It’s kinda tight like I mentioned which made me reap the brain a bit but overall it works and I’m happy with the thickness. Also you can see form the image that the Brian tilts and therefore I must make a support for it.

{{<image src="images/worked_reaped.jpg" alt="Square shape." size="40%" >}}

Then I test if the air tube enters the support or if the hole is too thin, I also test if the LEDs can go around it and if the wires fit the hole I made. As you can see on the video bellow the LEDs cant go around that well so I gotta increment that a little bit. Im also afraid to make it to big that’s why. The tube actually happens to have a perfect size so not everything was bad out of this first prototype. The wires also have enough space inside the hole I made for them.

{{<video src="images/test_tube_leds_v.mp4">}}{{</video>}}

- #### New design of the brain support

I do again another design where I try to not increment that much the hight of the support but just enough. I also change the little thing I made on top of the air tube so it will print correctly for sure.

{{<image src="images/had_to_do_changes.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/update_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/maybe_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/maybe_2.jpg" alt="Square shape." size="40%" >}}

I decide to also make a small attachment on the side so it prevents the brain from falling down.

{{<image src="images/do_the_side_attach.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/side_attachment.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/revolve_new_body.jpg" alt="Square shape." size="40%" >}}

I then make this rings around the support so I can hold that extra side piece. I’m not too confident about those but at this point I just go for it and I fix it later in case doesn’t work.

I make a hole to the ring to enter. 

{{<image src="images/make_hole.jpg" alt="Square shape." size="40%" >}}

And I also make a hole for the screw to enter the ring and thigh it.

{{<image src="images/make_screw_hole.jpg" alt="Square shape." size="40%" >}}

This is how the piece looks like.

{{<image src="images/make_ring.jpg" alt="Square shape." size="40%" >}}

I then split it into two pieces and I make sure I cut the support from those in case they were overlapping. Which is the case.

{{<image src="images/split_into_two.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/adjust_rimb_to_big.jpg" alt="Square shape." size="40%" >}}

This is how it looks at the end.

{{<image src="images/final_again_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_again_2.jpg" alt="Square shape." size="40%" >}}

I don’t know if I already mentioned this by now but I also learned how can I save the *.stl file from individual bodies. 

I have to go to file -> 3D Print -> “STL” -> “high” -> untick “select to 3D print…” -> select the object/body -> hit “ok” -> save in a place of preference.

{{<image src="images/how_print_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/how_print_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/how_print_3.jpg" alt="Square shape." size="40%" >}}

Following is how the print looks like.

{{<image src="images/second_support_print.jpg" alt="Square shape." size="40%" >}}

As for the attachment pieces it didn’t exactly work since the bigger pieced was a bit lifted and I didn’t even realised when in Prusa Slicer.

{{<image src="images/first_attachment_piece.jpg" alt="Square shape." size="40%" >}}

Now I know better so I will be more careful with the way I print the pieces. I will just make the bigger piece vertically and hope it works well with no supports. I do believe it will.

{{<image src="images/printing_lift_change.jpg" alt="Square shape." size="40%" >}}

I reprint it and its when I properly look at the pieces and I realise that they are very thin and easily breakable.

{{<image src="images/soooo_thin.jpg" alt="Square shape." size="40%" >}}

I go back to fusion and I make them a bit thicker by basically offset the faces.

{{<image src="images/made_it_bigger.jpg" alt="Square shape." size="40%" >}}

I reprint and this is how they look like. Why bigger and stronger it seams.

{{<image src="images/second_more_strong_print_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/second_more_strong_print_2.jpg" alt="Square shape." size="40%" >}}

Now, I will make the LED strip to put around the support. I measure and I will have two strips connected with 3 LEDs each of them. This is an old and used LED strip so I was a bit afraid the LEDs would be working. 

I had to remove some old solder using the solder strip.

{{<image src="images/remove_solder_LED_strip.jpg" alt="Square shape." size="40%" >}}

Then I cut 3 long wires that will be my Vin, GRND and Data. I solder them and I put this heat strips to hide the wires. I use this drier for that purpose since it’s less powerful than the heat gun but it does the job as well.

{{<image src="images/drier_heat_strip.jpg" alt="Square shape." size="40%" >}}

I also made a video simulating the procedure I do to solder the wires. It just misses me applying the flux pen in the beginning.

{{<video src="images/showing_how_done_simulation_v.mp4">}}{{</video>}}

{{<image src="images/final_tough_gun_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_tough_gun_2.jpg" alt="Square shape." size="40%" >}}

Then I test how I want to place them in the support and I see if it works. I like it, it’s not that bad. 

{{<image src="images/rotate_around_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/rotate_around_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/rotate_around_3.jpg" alt="Square shape." size="40%" >}}

I was just a bit concerned that the LEDs would produce too much heat and damage the strip or even burn something so I asked the professor about that. He told me that normally we should have some kind of metal bellow the LEDs so it absorbs the heat. He told me I could find those at K-rounta. We also look around in the metal workshop and we found some possible solutions.

I really didn’t wanna have to so this one extra thing. It looks confusing and also it would make the support heavy and with a diameter very think. I would have to change the design of the support again so the support with the metal piece could enter inside the brain. 

I didn’t like the idea so I kinda push him towards another solution by saying that I won’t even use the LEDs that often during the exhibition. He agreed on that but he still emphasised  about the fact that in a bigger application we should use this metal piece. 

The professor also gave his opinion about how he would put the LEDs. I tried it but I didn’t like it so I stick with the original plan. This was his idea.

{{<image src="images/professors_idea_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/professors_idea_2.jpg" alt="Square shape." size="40%" >}}

I like more the first idea since it seams to be better attach to the support.

{{<video src="images/first_idea_led_strip_1_v.mp4">}}{{</video>}}

Nevertheless, he then suggested me to by this Epoxy in k-rounta to glue the LEDs on the support. This glue is more resistance to the heat than normal double sided tape.

I could go to k-rounta at that point and I really wanted desperately to finish the project as soon as possible so I just tried the double sided tape and it worked. Was enough for me at that point. At least to test the LEDs. I also put this tape at the end to isolate those conductive ends.

{{<image src="images/put_tape_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_tape_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/put_tape_3.jpg" alt="Square shape." size="40%" >}}

This is how it looks like at the end.

{{<video src="images/first_idea_led_strip_2_v.mp4">}}{{</video>}}

Finally I insert the support with the LEDs inside the brain. I reap it a little bit more but that’s fine. I also attach the side pieces and I put a screw to tight it together. Although it was hard to do it all with just two hands do I just decided to put a Zip tie just to see the result. It works nicely and it’s so small and sophisticated that it goes well with the brain and it doesn’t feel like an outsider piece. 

{{<image src="images/attack_piece_sup_brain_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/attack_piece_sup_brain_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/attack_piece_sup_brain_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/attack_piece_sup_brain_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/attack_piece_sup_brain_5.jpg" alt="Square shape." size="40%" >}}

11. ### Design pump connecter 

At this stage I want to test if the pump could deflate the brain. However, the pump entrance is too big for the air tube so I cant connected. That was annoying me having to always hold it with an hand so I just decided to make this connecter. I had to make it at some point off course.

I started by taking the measurements of the pump entrance and off the tube. I also judge a bit how high should the connecter be and how thick. By my experience by now I know I have to make the measurements in fusion at least 0.5mm bigger than what the calliper says. I did so and it happened to fit perfectly after I printed. We can call this a lucky beginner. But it feels nice not having to keep redesign until we get the perfect size. 

{{<image src="images/scketch_for_pump.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/all_sketches_pump_con.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/not_confident_it_will_work.jpg" alt="Square shape." size="40%" >}}

This is how the print turn out.

{{<image src="images/print_1st_connect_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/print_1st_connect_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/print_1st_connect_3.jpg" alt="Square shape." size="40%" >}}

Later I also made a connecter for the bigger tube just because I thought I warn gonna need the valve and therefore I could just use the bigger tube rather than the small one. At the end I figured I needed the valve and I changed the connecter back again to the original. 

Nevertheless I used the same design but I just made the last end a bit bigger for the bigger tube.

{{<image src="images/pump_connecter_big_pipe.jpg" alt="Square shape." size="40%" >}}

I had to make it a bit thicker judging by the first print I made of it. Unfortunately i didn’t took a picture of it but it was so thin that the air could go throw it which is not ideal. So I just redesign it to make it thicker.

{{<image src="images/make_it_a_bit_thicker.jpg" alt="Square shape." size="40%" >}}

This is the print.

{{<image src="images/thicker_connecter_print.jpg" alt="Square shape." size="40%" >}}

### Join all the code onto Server

At this stage i have the LEDs attach on the support and they are inside the brain. I also have the tube inside the support and connected to the pump. I have all the things I need to be able to create the animation for the LEDs when a good word is selected and to turn on the pump when a bad word is selected.

I will first start with the LEDs since seam the easiest ones and later I’ll do the pump.

1. **Do LED animation for when good word selected**

I was already able to make a simple light ON and OFF with the white colour but now I wanna make some nice animations to kinda try to recreate or give a sense of happiness and good/healthy mental health. 

I liked when there were some colours inside the brain. So I don’t wanna make just white LEDs. 

Since we are in the pride month I thought to use the pride colours. I have 6 LEDs and the flag has 6 colour which is perfect. I thought on making a spiral effect going from the first LED to the last.  

I had to go to the internet and search for the RGB numbers for the colours I wanted.

{{<image src="images/finding_RGB_values.jpg" alt="Square shape." size="40%" >}}

I then created an array with all the colours.

{{<image src="images/array_with_all_colours.jpg" alt="Square shape." size="40%" >}}

And then I set each individual LED with one of the colours through a for loop to make that spiral effect.

{{<image src="images/the_colors_also_work.jpg" alt="Square shape." size="40%"  >}}

I’m planning to have two animations right after each other.

The next animation I wanna do it’s make a fade in/ fade out effect in white colour. Like what it’s normally made every time you wanna recreate this spiritual light going through your brain. 

For that I just have to first set the colour white to all the LEDs and then turn on and off the LEDs. It sounds simple but it’s not that easy. 

To make this kind of loop I do a for loop that will repeat four times. I just wanna have this fade in/fade out animation four time.

But them I also have to have a four loop that will increment and decrement the brightness of the LEDs to create the fading of the light.

I then combined this two animations so they happen one after the other and I uploaded the code to see if it works. It off course didn’t since there’s still some changes to do.

{{<video src="images/nothing_happens_with_LEDs_v.mp4">}}{{</video>}}

I couldn’t understand why wasn’t working so I just decided to start debugging with prints to know if it would enter the animations loops. As you can see on the video bellow, it does enter. So the problem has to be somewhere else.

{{<video src="images/debugging_with_prints_v.mp4">}}{{</video>}}

I then decide to do all the animations individually and in the most simple way. I start with the spiral effect with just the white colour and it worked. Perfect!

{{<image src="images/simple_code_works.jpg" alt="Square shape." size="40%" >}}

{{<video src="images/just_white_color_v.mp4">}}{{</video>}}
 
Then I tried with the colours to see if the problem was with the way I created the colour array but it also worked. 

{{<video src="images/also_works_with_colours_v.mp4">}}{{</video>}}

Then I test the second animation individually. The fade in/fade out. And I discover this one is the one with the problems. 

Once again, I start with a simpler code where I just focused on making the fade effect with one of the LEDs and I made it working. The reason why it wasn’t working is because I have to set the colour again after setting the brightness. 

So, before, I was doing a for loop just to change the brightness and I would set the colour before starting the loop. But it turns out that it doesn’t work like this. I have to first change the brightness and then set the colour for the LED. So I will basically have to change the brightness and set the colour all those 16 times.

Following is a video chowing the one LED fading.

{{<video src="images/just_one_works_v.mp4">}}{{</video>}}

And then I finally make them all fade.

{{<video src="images/all_fading_v.mp4">}}{{</video>}}

Now that I have all the animations working properly individually I will try to combine them together again and see if it works. As you can see on the video bellow the spiral effect didn’t work even though I had it there in the code.

{{<video src="images/spiral_still_not_working_v.mp4">}}{{</video>}}

I later realise the problem. The spiral effect wasn’t working I have to set the brightness back again to 64. The the last time the brightness was set was to 0 after the fade out animation. That’s why the spiral wasn’t being seen even though it was happening.

Now both the animations work as expected. Exciting!!!

{{<video src="images/finally_works_all_animations_v.mp4">}}{{</video>}}

**NOTE:** The light is so bright even if the max of brightness is just 64 omgggg my eyes are complaining.

This is the simple code i made for this animations.

{{< highlight go-html-template >}}
#include<Wire.h>

//LEDs
#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define LED_PIN D7
#define LED_COUNT 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_RGB + NEO_KHZ800);

uint32_t white = strip.Color(250,250,200);
uint32_t red = strip.Color(250,0,0);
uint32_t orange = strip.Color(255,165,0);
uint32_t yellow = strip.Color(250,250,0);
uint32_t blue = strip.Color(0,0,250);
uint32_t green = strip.Color(0,250,0);
uint32_t purple = strip.Color(128,0,128);

// array of colors
uint32_t color[6] = {red, orange, yellow, blue, green, purple};

void setup() {
  Serial.begin(115200);
  Wire.begin();

  // NEON pixels
  strip.begin();
  strip.setBrightness(64);
  strip.show();
}

void LEDsOn(){
  
// set Brightness again to 64 because the last time the brightness was set was to 0
  strip.setBrightness(64);
  strip.show();

  //hopefully this is a spiral efect
  for (int i=0; i<6; i++){
    strip.setPixelColor(i, color[i]);
    strip.show();
    delay(100);
  }

  delay(1000);
  // lighting up and down leds in a faded way
  // i have to simulate a loop. I want this to happen 4 times 
  // fadeAmount should be 4 up to 64. We have to loop 16 times.
  int brightness = 0;
  int fadeAmount = 4;

  strip.setBrightness(brightness);
  strip.show();

  // set the first time the LEDs to zero
  for (int i=0; i<6; i++){
    strip.setPixelColor(i, white);
  }
  strip.show();
  
  delay(200);
  
  // do fade in fade out 4 times
  for (int i=0; i<4; i++){
    // fade in
    for (int n=0; n<16; n++){
      // change the brightness
      brightness = brightness + fadeAmount;
      strip.setBrightness(brightness);
      strip.show();
      for (int i=0; i<6; i++){
        strip.setPixelColor(i, white);
      }
      strip.show();
      delay(30);
    }

    // fade out
    for (int n=0; n<16; n++){
      // change the brightness
      brightness = brightness - fadeAmount;
      strip.setBrightness(brightness);
      strip.show();
      for (int i=0; i<6; i++){
        strip.setPixelColor(i, white);
      }
      strip.show();
      delay(30);
    }
    delay(80);
  }
}

void loop() {

  // turn on LEDs
  LEDsOn(); 

  delay(1000);

}
{{< /highlight >}}

2. **Do pump deflate the brain when bad word selected**

With the LEDs done I move to code the pump. It shouldn’t take long.

I first started with a simple code where I turn the pump on and off.

{{<video src="images/turn_pump_on_off_v.mp4">}}{{</video>}}

Then I tested how the pump, valve and the XIAO work together by once again making a simple code but now integrating the valve. It’s when I discovered that the valve as to be in the opposite state as the pump. So when the pump is on (HIGH) the valve has to open(LOW). The valve opens when we write LOW apparently. 

{{<image src="images/valve_open_when_low.jpg" alt="Square shape." size="40%" >}}

{{<video src="images/pump_valve_working_simple_v.mp4">}}{{</video>}}

It feels kinda pointless to have the valve tho. I can just turn the pump on and off. Well, plot twist, I will need the valve to let the air get in to the brain so it can get to its natural shape again. But I don’t know about this at this state.

This is the simple code for the pump and valve testing.

{{< highlight go-html-template >}}

#include<Wire.h>
#include <SparkFun_MicroPressure.h>

//define Pressure Sensor
SparkFun_MicroPressure mpr; // Use default values with reset and EOC pins unused

//define pins
int pump_pin = D9;
int valve_pin = D8;

void setup() {
  Serial.begin(115200);
  Wire.begin();

  pinMode(pump_pin,OUTPUT);
  pinMode(valve_pin,OUTPUT);
  digitalWrite(pump_pin,LOW);
  digitalWrite(valve_pin,HIGH);

  while(!mpr.begin())
  {
    Serial.println("Cannot connect to MicroPressure sensor.");
    delay(100);
  }
}

void pumpOn(){
  digitalWrite(valve_pin,LOW);
  digitalWrite(pump_pin,HIGH);
}

void pumpOff(){
  digitalWrite(pump_pin,LOW);
  digitalWrite(valve_pin,HIGH);
}

void loop() {

  Serial.print(mpr.readPressure(),4);
  Serial.println(" PSI");
  Serial.print(mpr.readPressure(KPA),4);
  Serial.println(" kPa");
  Serial.print(mpr.readPressure(ATM),6);
  Serial.println(" atm");
  Serial.println();
  delay(500);

  // turn on pump
  pumpOn(); 
  delay(3000);
  pumpOff();

}

{{< /highlight >}}

3. **Join both codes on the Server code**

Now that I have both the LEDs and the pump working I wanna combine them on the server code.

I copy paste both the LEDs animation and the pump code to specific places on the server code. I basically have this rootpath called “/button” where I will receive from the client (input box) if its a good word (parameter btn=1) or a bad word (parameter btn=2). I then have a if statement where I will either turn the LEDs ON if “btn=1” or turn the pump ON if a “btn=2”.

I new I had uploaded on the XIAO board inside the input box the lates code so I just turn the box on very confident it would work. But it didn’t and the reason why it was because the code I had on the other XIAO (the server) was just the simple pump test which means I didn’t create the access point. That’s why the input box wasn’t working because it was waiting for connecting with the Access point that the server creates.  Even tho the code was already on the board off course it didn’t work cause its waiting to be connected to the mentally confused Access Point.

On the video bellow I show the input box not working and me realising that there is no “Mentally” network available.

{{<video src="images/no_mentally_network_v.mp4">}}{{</video>}}

So, then I tried to upload the output_brain code through my computer and it didn’t work. It gave me an error. 

{{<image src="images/error_with_server_code.jpg" alt="Square shape." size="40%" >}}

I didn’t lose too much time trying to fix it so I just decided to upload the code from the desktop computer to the board has I new that it would work. And it did. Strange though. 

On the video bellow you can see that the “Mentally” Access Point is now created by the server and that the input box sends a request successfully to the server. However, at the end of the video you can see that after the LED animation the pump turn ON for a fraction of seconds and an error is thrown on the server side that will them be sent to the client where I print on the serial monitor. 

Clearly something is wrong since the natural behaviour should be just the animation LEDs and nothing more.

{{< youtube N_P2rsuIjos  >}} 

I also tried pressing a bad word and the pump didn’t even started.

{{<video src="images/pump_doesnt_even_start_v.mp4">}}{{</video>}}

I had the printing of the errors so I just tried to find a solution on the internet. 

This is the error being thrown on the server side.

{{<image src="images/error_being_thrown_on_server_side.jpg" alt="Square shape." size="40%" >}} 

Once again I’m very bad at googling and finding a solution for my problems, although I do generally understand the problem itself. For example, in this case, I understood that my goodWord() and badWord() call back functions are taking too much time and therefore the watchdog timer is trigger. 

Some places on the internet even suggested to split the code so the clock doesnt take too long inside one task and therefore trigger the watch dog. It kinda makes sense and I did try to split the code but still it didnt work.

{{<image src="images/split_code_didnt_work.jpg" alt="Square shape." size="40%" >}} 

I was already taking soooooo long to try to fix this error. 

I first tried to understand the error on the internet. It was were I understood the problem was with the watchdog. I saw some possible solutions but they were hard to understand how to implement them. I hate when the solutions are very generic. Cause then I don’t know how to incorporate them in my code. 

For example, this [website](https://stackoverflow.com/questions/66278271/task-watchdog-got-triggered-the-tasks-did-not-reset-the-watchdog-in-time) had really good suggestion, explanations and solutions but I end up not succeeding. But then, when I was almost to quite debugging this and ask the professor for help, I just reminded of chatGPT. I actually never tried it but my colleagues use it very often and it seams to help a lot. 

Since I knew what and where the problem was I guided chatGPT to a working solution. I’m so happy now it’s working after soooo many attempts. 

Following is some parts of my conversation with ChatGPT.

{{<image src="images/chat_1.jpg" alt="Square shape." size="40%"  >}} 

{{<image src="images/chat_2.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/chat_3.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/chat_4.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/chat_5.jpg" alt="Square shape." size="40%" >}} 

{{<image src="images/chat_6.jpg" alt="Square shape." size="40%" >}} 

Video showing the server code working properly.

{{<video src="images/finally_everything_works_together_v.mp4">}}{{</video>}}

Later I realise that the exact same solution was on that website mentioned but I just didn’t know how to implement it, for example I would have never guess that the “void* args” would be replaced with “void* pvParameters”. 

{{<image src="images/also_solution_int.jpg" alt="Square shape." size="40%" >}} 

There still a problem left that I don’t think it will influence but I might try to fix it later.  The error is: “E (153) esp_core_dump_flash: Core dump data check failed:”. I tried chatGPT but it was very general solutions and the internet was the same.

{{<image src="images/it_still_says_this_eventhough_works.jpg" alt="Square shape." size="40%" >}} 

At this stage I finally have all the code pretty much done. 

I have the input box (client) sending the request to the output brain (server) that reacts differently accordingly if it’s a good or a bad word.

# Combine Everthing

1. ### Fix brain support 

At this point I made two designs for the brain support. The grey one and the white one. The white one had all the measurements correctly but still it had the little thing on top of the air tube. And when I tried using that support with the brain it didn’t work. I thought that the problem was once again on the design and the fact I had that little thing. So I went back again to fusion and I just remove it and print the same design but without the thing. Turns out later that the problem was something else and so that support could have actually worked.

By removing the thing I’m just trying to make the debugging of the problem easier.

I printed again with the Ultimakers instead of the Prusa just because they were unavailable. Our Ultimakers have way poor quality than the Prusa and as you can see it didn’t even work. 

{{<video src="images/bad_print_ultimakers_v.mp4">}}{{</video>}}

So then I print again in the Prusa with a more light colour so it’s not that visible when inside the brain. And this one worked better.

Then I decided to glue the Brian better again with silicone because it was already opening a lot every time I would insert the support.

Also I wanted to test if the support works on the brain so I have to make it as tight and sealed as possible. 

For that I pour the silicone on the parts I wanna glue them together and I put the Brian inside the box to make sure it has the correct shape at the end. I also put the interior brain at the end so when glueing the pieces they don’t make a smaller hole. Here’s a picture to explain better what I mean.

{{<image src="images/glue_together_end_1.jpg" alt="Square shape." size="40%" >}}

Then after some time I open the box and I notice there’s still some holes to cover.

{{<image src="images/glue_together_end_2.jpg" alt="Square shape." size="40%" >}}

I then did more silicone and I was so surprised it didn’t had bubbles so I took a picture. Lately was hard to be completely rid of the bubbles after doing vacuum. I think it’s because the less liquid you put the easier to get rid of the bubbles.

{{<image src="images/glue_together_end_3.jpg" alt="Square shape." size="40%" >}}

Then I pour again on the hole and wait until it’s ready. I had to repeat this another time just to Mae sure its completely closed with no holes.

{{<image src="images/glue_together_end_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/glue_together_end_5.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/glue_together_end_6.jpg" alt="Square shape." size="40%" >}}

This is how it looks at the end.

{{<image src="images/glue_together_end_7.jpg" alt="Square shape." size="40%" >}}

Now I’m gonna try to see if I can deflate the brain using just the pipe and my hands to seal it. As you can see it works.

{{<video src="images/seals_hand_pipe_works_v.mp4">}}{{</video>}}

Then I try doing the same with the support inside the brain. 

I first put the LEDs around the new brain support I made.

{{<image src="images/LEDS_around_new_supp.jpg" alt="Square shape." size="40%" >}}

And this is how it looks. 

{{<image src="images/cant_hold_itself.jpg" alt="Square shape." size="40%" >}}

As you can see the brain can hold itself so I must attach the extra piece I made for that purpose. I’ll put a elastic to hold it just because I don’t feel like using again a zip tie. It’s just a temporary fast solution. 

{{<image src="images/with_elastic_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/with_elastic_2.jpg" alt="Square shape." size="40%" >}}

Then I test with the pump to see if it also deflates as goos as when I seal it with my hand and nothing happens.

{{<video src="images/test_supp_doesnt_work_v.mp4">}}{{</video>}}

I was kinda disappointed cause I thought it was very sealed since I could feel it when inserting the support inside the brain. It was very hard to do it successfully without reaping the brain again.

Sounds like time for another debugging. 

I decided to go to the soldering station to debug the problem with the voltage supply machine since its faster. I try it again and it doesn’t work. The exact same thing happened! 
I try to blow on it and also nothing (off course ahahah) I was just staring at it trying to understand what could be the problem. 

I even ask the professor for some possible solutions. He told me to try the zip tie and dip the brain in water to see possible holes. I first try the zip tie and it didn’t work. Off course it didn’t because like I already mention the brain it’s well tight to the support.

{{<image src="images/see_if_zip_tie_works.jpg" alt="Square shape." size="40%"  >}}

I really didn’t wanna do the water suggestion. First because it’s a mess and second because I’ve deflate the brain already with the pipe and my hand so the problem must be on the support. The professor also mention that the PLA is not that good and it can have some holes. 

I then thought that the hole I have for the cables for the LEDs inside the brain could be the one causing air leakage. I knew about it when I design it but I wasn’t expecting to influence that much. I also wanted that some air could get in to inflate the brain back again.

Never the less I decided to just use the old good friend tape. We even have a meme in Portugal about fixing everything with [black tape](https://www.youtube.com/watch?v=5yZ7PwepIQw). I go to the wood workshop and I grab the best tape I can find. It’s not the black tape as the video but it’s duck tape and it does the job as good. 

{{<image src="images/duck_tape_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/duck_tape_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/duck_tape_3.jpg" alt="Square shape." size="40%" >}}

I try blowing on it and it worked. I couldn’t believe it! I run to the soldering station and I deflate the brain with the pump and it also worked.

{{<video src="images/it_works_with_tape_v.mp4">}}{{</video>}}

I was trying to avoid using the valve but now I really have to use it so the air can come in again and inflate the brain to its normal shape. And it’s fine. I have all the things I need to incorporate the valve and I also know how it works.

Of course I wont put the tape for the final project so I thought I can put silicone around it and hope it works.

I joined the valve on the circuit and I code it so I integrate it. I first turn the pump on and off so its a bit hard to see the brain deflate. 

{{<video src="images/pump_brain_1_v.mp4">}}{{</video>}}

So then I had a delay between stoping the pump and opening the valve so there’s some time to see the brain deflated. 

{{<video src="images/pump_brain_2_v.mp4">}}{{</video>}}

I also tested the amount of time I should maintain the pump on until it has a nice shape. 3s is a good amount of time. I’m happy with this final result.

{{<video src="images/pump_brain_3_v.mp4">}}{{</video>}}

2. ### Incorporate MicroPressure Sensor

In my opinion having the brain deflating during a certain time its good enough. I don’t feel the need of having the micro sensor. First off all I didnt like the fact it was very unstable when I tested and second I don’t see the pint on using it since the purpose is just to deflate the brain with no other conditions. If the brain deflates properly in 3 seconds that’s fine by me.

Nevertheless, I asked the professor his opinion about this matter. He said that I should try to use the sensor. He also mentioned that it’s a micro sensor so it’s normal that the values change very easily and that I should just check a range of values rather than just a limit value.

Well, I tried doing that. The code starts by turning the pump ON and turning it OFF if the pressure goes bellow a certain value. I also made prints to know in which state the pump was. 

The first value I choose didn’t quit work as you can see by the video bellow.

{{<video src="images/sensor_pump_try_1_v.mp4">}}{{</video>}}

Then, judging by the prints I made I tried different values and it worked relatively good.

{{<video src="images/sensor_pump_try_2_v.mp4">}}{{</video>}}

{{<video src="images/sensor_pump_try_3_v.mp4">}}{{</video>}}

{{< youtube Cfh_oMP8S_0 >}} 

I wanted to be more sure of the exact limit because when testing the brain sometimes it would inflate longer than the previous times and that was scaring me. Sometimes it felt like it wasn’t gonna stop pumping and that’s really not ideal. I don’t like this unstableness. But honestly I know deep down that the code for it might not be the best and the one causing this unstableness. The fact I’m using a while loop and not some other loop statement might be causing this behaviour. Nevertheless I try a new approach.

The while loop can be seen here.

{{<image src="images/tryed_with_while_still_not_that_stale.jpg" alt="Square shape." size="40%" >}}

What I did was just pump ON and OFF the brain for 3s in a loop and print the values of the sensor in all different states of the pump. By doing that I could more less see the range off values I should stop the pump. As you can see by the image bellow its very unstable. Sometimes the values would change but other times it wouldn’t change. The professor said to just use one value to be more stable. I’m gonna go with Kpa.

I was printing in all the states of the pump.

{{<image src="images/printing_everywhere.jpg" alt="Square shape." size="40%" >}}

And sometimes it would change.

{{<image src="images/other_times_changes.jpg" alt="Square shape." size="40%"  >}}

But other times no.

{{<image src="images/sometimes_dont_change.jpg" alt="Square shape." size="40%" >}}

I even made more ovious prints so I can clearly see the changes. As you can see the values mainly change on the "Pump ON begginig" and sometimes the value would be very different than the overall average.

{{<image src="images/try_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/try_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/try_3.jpg" alt="Square shape." size="40%" >}}

I was really not in favour of this sensor but I even thought decided one more strategy. Since it was hard to see the range of values I should stop the pump I just decided to pump the pump continuously and stop it with the switch when I would feel that the pump was enough deflated. By doing that several times I realised I should define the limit bellow around 75KPa to stop the pump.

Those where the prints and still you can see it not being that stable.

{{<image src="images/continuously_prints.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/not_stable_still.jpg" alt="Square shape." size="40%" >}}

I then start by stopping the pump when the pressure goes bellow 75KPa. But it didn’t quit work.

{{< youtube huL8c8FxNVM  >}} 

Then I kept trying lowering the values. I tried 72KPa and 71KPa and also didnt work.

{{<video src="images/bigger_than_72_v.mp4">}}{{</video>}}

{{<video src="images/bigger_than_71_v.mp4">}}{{</video>}}

Finally I tried 70 and it worked. 

{{<video src="images/bigger_than_70_v.mp4">}}{{</video>}}

I reach this state where I can use the sensor to deflate and inflate the brain but like I mentioned I don’t find it extremely needed for my project. Pumping ON and OFF the brain using a define time works as well and it gives me more sense of stability. 

To prove that I code again the pump to be ON and OFF after 4s. And it works so well. So I just decided to go with the time instead with the sensor. I did make the sensor work but again I don’t feel the need of using it.

{{<video src="images/without_sensor_much_better_v.mp4">}}{{</video>}}

3. ### Test everything together again

Now that I have the brain support on the brain and the pump working successfully on deflating the brain and also I have the LEDs working I decide to do the same test I did before where I incorporate the input box with the brain. 

If you remember I’ve already made the client and the server working together. So now I wanna film the code working when the support is inside the brain. Because, after succeeding with this step, I just miss substituting the tape with silicone to seal all the holes and create the box to hide all the components of the brain. 

So, when I try to connect the input box with the output brain it didn’t work. I thought it was because my computer wasn’t connected with the Mentally network, even though I don’t remember ever doing that. I tried that just in case and still it didn’t work.

{{<video src="images/not_working_miss_antena_v.mp4">}}{{</video>}}

I then remember that it could be the fact I forgot to put the antenna on the output brain. I also don’t remember having that but anyway I tried it and it worked. At least I can proudly say I keep learning with my mistakes. It was not the first time this happened and before I would have to ask the professor what could be the solution and now I can solve it by myself uhuhuhh.

{{<video src="images/antena_and_leds_not_working_v.mp4">}}{{</video>}}

Then I pressed a bad word so I could see if the brain would deflate and the pump didn’t even turned ON. I was so confused but I immediately realised I forgot to turn the switch ON. As you can see on the video bellow the pump its working everytime we press a bad word.

{{< youtube qb9UAJLlJYU >}} 

Now, I will test if the LEDs also work. I’ve test them already so it should work. The only think I did was insert them inside the brain. 

However, as you can see on the video bellow, the LEDs are not working as they should. The normal behaviour should be the spiral effect in several colours followed by the fade in/fade out effect in white colour. 

{{<video src="images/leds_not_working_on_final_v.mp4">}}{{</video>}}

I was so annoyed this was happening with me right now. I just want to finish the project and this small thinks keep happening with me. They were working so well before. I might have broken something when inserting the support with the LEDs inside the brain. I must confess it was a ruff procedure. 

Once again I have to debug this problem. I really didn’t wanna remove the support from the brain so I first try to see if the problem is with the code or if I really just broke something.

The idea is to upload the simple code I made to turn the LEDs on and see if it works. If it doesn’t it means the LEDs are really broken. 

When I tried to upload the code to the XIAO board it didn’t work again!!!! I swear, if I could receive money for every time the board didn’t work properly I would be rich by now. 

Nevertheless I tried everything, boot, restart, upload the flash memory again. And nothing. I then decided to do the technique of the professor where we just do a simple code. I did it and only after a lot of tries it finally worked. I mainly restart and boot again. 

{{<image src="images/works_with_simple.jpg" alt="Square shape." size="40%" >}}

Then I upload the LED code and it also worked now.

{{<image src="images/now_other_code_worked.jpg" alt="Square shape." size="40%" >}}

As you can see on the video bellow the LEDs stop working. They stooped working since I put them inside the brain. Inserting them inside the brain was an hard task so I might have disconnected something or I don’t know what happened. I also tried a simpler code where I just turn the LEDs ON and OFF in white colour and still some result. This confirms my theory that I might have broken them when inserting them inside the brain.

{{<video src="images/LEDs_not_working_broke_them_v.mp4">}}{{</video>}}

I remove the support from the brain and I try to understand what could have happened for it to stop working. I measured the voltage around each LED and everything seamed fine. They do have 5V so I don’t know what could be the issue.

{{<video src="images/debug_LEDs_mul_v.mp4">}}{{</video>}}

I didn’t lose to much time on that I just decided to make a new strip. Thank god I have a big strip where I can remove parts from it. I still have room to keep braking LEDs and replace them. 

When I was gonna replace I realise that one of the cables got loose. It might have been that the problem who knows.

{{<image src="images/maybe_this_the_problem.jpg" alt="Square shape." size="40%" >}}

I have two strips of 3 LEDs. I start by just replacing one of them to see if the other one was still good. 

{{<image src="images/replace_just_one.jpg" alt="Square shape." size="40%" >}}

As you can see on the video bellow the strip doesn’t work so I really gotta replace it also.

{{<video src="images/only_one_strip_works_v.mp4">}}{{</video>}}

After replacing them all it works. However, the worst part is that I don’t know what I did to brake them so I’m a bit afraid I’ll brake this ones again. 

{{<video src="images/replace_two_works_v.mp4">}}{{</video>}}

Now that the LED strip works again I will be more careful this time to insert it inside the brain. 

The first thing to do is to use double sided tape to glue the LEDs to the support. But, with my surprise, the tape didn’t work this time. So strange! I tried all double sided tapes at the lab but no success.

{{<image src="images/tape_not_working.jpg" alt="Square shape." size="40%" >}}

I asked the professor what to do and he mentioned again the use of Epoxy. I was so frustrated that I had to go buy the glue and lose time on it when I really just wanna finish the project. But since that’s the only option I just gotta accept it. 

I went buy the Epoxy right in the morning. I read the instructions on how to use it. 

{{<image src="images/instructions_epoxy.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/epoxy_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/epoxy_2.jpg" alt="Square shape." size="40%" >}}

I found it strange they don’t mention the use of gloves or a mask. Thank god I was in the silicone room and I used the gloves because the professor told me later that we really should use them. He also mention to not bread it to much. Well, I dint know that so I gotta say that it smelled soooo bad. It smelled like dead fish in a swamp. Very bad. I could still smell it the next day. 

Nevertheless here’s a video of me applying the Epoxy on the support and them tight it with an elastic so it gets pressure. We are suppose to press or clamp it for 20 minutes and of course I’m not gonna hold it myself all that time because I have more things to do. 

{{<video src="images/how_epoxy_works_v.mp4">}}{{</video>}}

{{<image src="images/after_epoxy_1.jpg" alt="Square shape." size="40%" >}}

After the 1 hour curing time I go there to check it and it looked good.

{{<image src="images/after_epoxy_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/after_epoxy_3.jpg" alt="Square shape." size="40%" >}}

Then it’s time to insert it on the brain. This time I’m being more careful so I use the scissors to open the entry a bit more so it’s easier to insert it inside the brain.

{{<image src="images/after_epoxy_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/after_epoxy_5.jpg" alt="Square shape." size="40%" >}}

Then this time I decided to do a new strategy. Instead of putting the brain on the mold box I just use zip ties to press the two ends together and I apply silicone around it. 

I forgot to put the easy removal spray in both the support and the zip ties. I wanted to put on the support to make it more movable and I wanted to put on the zip ties so it’s easier to remove. I hope it will be easy at the end.

{{<image src="images/after_epoxy_6.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/after_epoxy_7.jpg" alt="Square shape." size="40%" >}}

After the curing time I remove the zip ties and I notice there’s still just one more spot missing so I apply a bit more silicone to cover that hole. After that the support safely inside the brain.

{{<image src="images/after_epoxy_8.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/after_epoxy_9.jpg" alt="Square shape." size="40%" >}}

It this point I have the code working as well as the brain reacting as expected. 

Now to finish the project I just miss three more things:

* Make the website 
* Make a mold to pour silicone around the support for the brain
* Make the box for all the components of the brain

4. ### Make the Website

I decided to have a website that would be connected to the Access Point the server is creating. 

The website will be showing the overall mood of the participants. I have two graphs. One showing how many people are feeling what and the percentage of the feelings selected. I will also display how many users have been participating along the day as well as a description of the concept of the project.

I made this website as part of the assignment for the week 16: Interface and Application Programming. I will have all the explanation there so please feel free to have a look. Meanwhile I will show the final result bellow. 

{{< youtube Zz5b_h_MYkk >}}

5. ### Make a mold to pour silicone around the support for the brain

- **Make the design**

Now I will make a mold where I will pour the silicone there so I can cover all the support and make sure it’s perfectly sealed from any possible holes for air leakage.

I start with a new design where I insert the support there.

Then I create a sketch and I draw a rectangular around it. I didn’t thought that much about which size to make it. I just looked at it and thought would be a good size to also hold the weight of the brain. 

{{<image src="images/create_mold_box_support.jpg" alt="Square shape." size="40%" >}}

Then I thought it would be nice to insert screws and pour the silicone around them as well. Later I would use those screws to attach the brain to the output box. By doing this the screws would be attached and fixed by the silicone which is perfect.

I then make holes with a diameter of 5.5mm so I can later insert an M5 screw. By now we all know we have to increment the hole size by at least 0.5mm so the screw can enter perfectly. 

{{<image src="images/M5_holes.jpg" alt="Square shape." size="40%" >}}

Then I will extrude the walls and the floor. I extrude the walls enough to connect this silicone base with the silicone already around the brain.

{{<image src="images/extrude_the_walls.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/extrude_walls_and_floor.jpg" alt="Square shape." size="40%" >}}

Then because I will be inserting the screws and I want them to have a certain hight I have to create this bottom base with some supports holding the up part. I’ll show some pictures so you can see better what I’m saying. It’s hard to explain.

But basically I started by making an offset plane with a distance of 13mm. I measure the size of the screw and how high I wanted those to be on the silicone and 13mm was the result. 

{{<image src="images/offset_plane_because_off_screws.jpg" alt="Square shape." size="40%" >}}

Then I make the supports to hold the up part.

{{<image src="images/make_support_bellow.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/bellow_base_1.jpg" alt="Square shape." size="40%" >}}

Then I used the “Combine” tool to cut a bit of those supports on the up part so then its easier to connect them together.

{{<image src="images/bellow_base_2.jpg" alt="Square shape." size="40%" >}}

Once again I offset the faces 0.5mm so I’m sure the supports enter the holes. 

{{<image src="images/offset_faces_to_make_sure_enters.jpg" alt="Square shape." size="40%" >}}

Finally I just miss making the hole for the air tube and for the LED cables. For the air tube hole I just projected the circle I have from the brain support. Cause that size of hole I’m sure by now that it works with the air tube.

{{<image src="images/extrude_hole_for_pipe_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_hole_for_pipe_2.jpg" alt="Square shape." size="40%" >}}

Then I do the same for the LED cables. I measure the cables and it gave me a value of 1.5mm. This time I forgot to make that 0.5mm offset and at the end it won’t work. But il go to that later. I’m disappointed I forgot about that. I might have distracted myself.

{{<image src="images/1,5_size_off_cables.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/space_for_cables.jpg" alt="Square shape." size="40%" >}}

This is how the those two parts looks like at the end.

{{<image src="images/final_up_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_up_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_off_all.jpg" alt="Square shape." size="40%" >}}

I then create some fillets so it’s easier to remove the silicone. Although I don’t know if it actually made any difference. But it’s good to have it consider.

{{<image src="images/easy_removal_add_fillet_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/easy_removal_add_fillet_2.jpg" alt="Square shape." size="40%" >}}

- **3D printing**

Recently we have been having lots of problems with the Prusas printers. I tried to print the bottom part and this was what was happening. 

{{<image src="images/bad_prusa_printing_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/bad_prusa_printing_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/bad_prusa_printing_3.jpg" alt="Square shape." size="40%" >}}

As you can see it feels like the machine is not well calibrated. But actually, we manage to discover later that the problem was the fact the filament was already 3 years hold and it had already too much humidity inside and it was also braking very easily. After this I can conclude that those filaments are the ones causing the destruction of our printers. 

Nevertheless there was no Prusa printer available so I had to once again use the Ultimaker machines. I really don’t like them as they seam to never work properly. 

I first tried with the Extended 2+ and it wasn’t working. It was printing in the air.

{{<image src="images/priting_air_ultimaker.jpg" alt="Square shape." size="40%" >}}

So then I print on the Connected+ and it didn’t print that well. It was also so weak that one of the supports broke. That’s on me cause I wanted to be quicker and I put like 5% infill. 

{{<image src="images/connect_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/connect_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/connect_3.jpg" alt="Square shape." size="40%" >}}

By that time I had printed the up part with the Prusa printer and it worked amazing.

{{<image src="images/with_prusa_amazing.jpg" alt="Square shape." size="40%" >}}

It was nice to see that the attachment of the supports work so well. Im proud.

{{<image src="images/up_bottom_with_support_1.jpg" alt="Square shape." size="40%" >}}

I also put the screw inside and I realised I wanted it to be a bit higher so I change the design on fusion. I put the supports a bit lower and I printed it again in the Prusa printer.

{{<image src="images/make_smaller_pipe.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/up_bottom_with_support_2.jpg" alt="Square shape." size="40%" >}}

Then I tested if the holes for the air tube was big enough as well the holes for the LED cables. I end up realising that the holes for the cables weren’t big enough! I wasn’t gonna print again. It’s a waste of time and material. 

{{<image src="images/hole_cables_too_small.jpg" alt="Square shape." size="40%" >}}

So I decided to use the vertical drilling machine I’ve mentioned already in my website in previous assignments.  I will use the 1.5mm drilling tool and ill make the holes bigger.

{{<image src="images/155mm_tool.jpg" alt="Square shape." size="40%" >}}

As you can see now the cables fit perfectly and still I manage to make the holes small enough so there’s less possibility for the silicone to go down through them.

{{<image src="images/cables_fit_perfectly.jpg" alt="Square shape." size="40%" >}}

Now I have the mold ready I just miss pour the silicone. 

I’ve also notice something I will keep in mind next time I design. I should have made a small hole for the screw to enter so I’m sure they lay down perfectly perpendicular on the bottom part. Cause, as you can see from the image bellow, the screws can easily bend. I won’t reprint it because it doesn’t matter too much for my case. I will though try my best to make them perpendicular when casting.

{{<image src="images/screws_not_exactly_perp.jpg" alt="Square shape." size="40%" >}}

- **Casting**

I first start by inserting the cables and the tube.

{{<image src="images/insert_cables_tube_mold_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_cables_tube_mold_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_cables_tube_mold_3.jpg" alt="Square shape." size="40%" >}}

Then I realised when inserting the tube that it’s hard to tell if the tube is being pushed or if it’s all the way in. So I made a mark using the previous support I 3D printed.

{{<image src="images/make_mark_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_mark_2.jpg" alt="Square shape." size="40%" >}}

Then I insert the screws and I’m good to start casting. I also made sure I put the spray on the mold before putting the screw. I want to take the silicone from the mold easily but I don’t want to take the screws from the silicone so it’s important that they don’t have the spray.

{{<image src="images/insert_cables_tube_mold_4.jpg" alt="Square shape." size="40%" >}}

Then I have to lift the brain a little bit because I want to make sure I also have silicone underneath the support closing all the holes. For that I come up with this mechanism. I feel so engineer after this. 

{{<image src="images/what_an_engineer_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/what_an_engineer_2.jpg" alt="Square shape." size="40%" >}}

I had to make the silicone 3 times because it was never enough amount. The 2nd time I did the vacuum didn’t work to well since there was bubbles but I didn’t care at this point, its a prototype anyway and it will work as equal. I also learned with my mistakes when casting the brain, so I didn’t want to make too much material at once and risk spoiling material when creating vacuum. 

{{<image src="images/final_mold_sup_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mold_sup_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mold_sup_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mold_sup_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/final_mold_sup_5.jpg" alt="Square shape." size="40%" >}}

Then I just miss waiting for it to be ready. I must confess I’m afraid that the silicone base will be too big and not look good. I already have plans on cutting the silicone shorter if that’s the case. But lets see at the end.

6. ### Make the box for all the components of the brain.

I have to do two types of boxes. One 3D printed where I make sure all the components are well tight and secure inside it and another box in wood to hid them all. For the wood one I will use laser cutter.

- **Design 3D printed box**

I first thought how was I gonna attach and secure all the elements. At this stage I don’t have to much time to be very precise as I really just wanna finish it. I then thought I could just secure the elements with zip ties. It’s actually a very smart idea. For the pump I will have to do it anyway. So mind as well just do it for all of them. This is also just a prototype that can be improve later in the future if needed or required.

Moving on, I start by placing all the elements on the table so I can figure out the measurements of the box. 

I decided to go with those measurements for now.

{{<image src="images/simulate_wood_box.jpg" alt="Square shape." size="40%" >}}

Then I simulate the battery. I took the measurements and I just made a rectangular with the same size so then I can build walls around it. 

{{<image src="images/place_battery.jpg" alt="Square shape." size="40%" >}}

I do the same for the pump.

{{<image src="images/add_pump.jpg" alt="Square shape." size="40%" >}}

Then for the PCB i was thinking to just take the measurements ruffly but then I thought it would be important to have the exact placement of the holes so I just export the *.step file from KiCad. Exactly like I did last time with the PCB of the input box.

I go to Kicad and I export the *.step file.

{{<image src="images/export_step_file_out_box.jpg" alt="Square shape." size="40%" >}}

Then I upload the PCB to Fusion and I insert it on the design im doing the box.

{{<image src="images/insert_PCB_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_PCB_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/insert_pcb_on_design.jpg" alt="Square shape." size="40%" >}}

I also offset the holes to make sure they are big enough later to insert the screws.

{{<image src="images/offset_holes_from_board.jpg" alt="Square shape." size="40%" >}}

Then I finally had the sizes of the valve.

{{<image src="images/add_valve.jpg" alt="Square shape." size="40%" >}}

I start building walls and holes for the zip ties around the objects. I had to measure the zip ties and off course I offset them to make sure they are big enough.

 I first did it for the Pump.

{{<image src="images/hole_for_zip_ties.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/build_walls_around_pump.jpg" alt="Square shape." size="40%" >}}

I end up adding the air valve that a colleagues provide us the step file. It’s easier to work with it. 

{{<image src="images/insert_air_valve.jpg" alt="Square shape." size="40%" >}}

Then I make the walls around the battery. I made them with 1mm offset just in case. My measurements of the objects weren’t the most precise.

{{<image src="images/making_walls_on_battery_with_one_mm_offset.jpg" alt="Square shape." size="40%" >}}

Then I extrude the floor and the walls with a reasonable high.

{{<image src="images/extrude_floor.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/extrude_walls.jpg" alt="Square shape." size="40%" >}}

Then I snap the valve so then I can add walls around it. 

{{<image src="images/snap_valve.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/create_house_for_valve.jpg" alt="Square shape." size="40%" >}}

I will secure the valve also with a zip tie. So I make a hole for that purpose.  

{{<image src="images/make_hole_zip_tie.jpg" alt="Square shape." size="40%" >}}

Next I made this little pocket because the pump has this screw piping out making it irregular and not touching the floor perfectly horizontally.

{{<image src="images/make_hole_for_circle_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/make_hole_for_circle_2.jpg" alt="Square shape." size="40%" >}}

Finally I made some holes for screws so I can attach it to the wooden box later. I end up increasing a bit the size of the box for this purpose.

{{<image src="images/make_holes_possible_attachment_to_other_box.jpg" alt="Square shape." size="40%" >}}

Then I thought that I should make some kind of support for the bottom that would not just hide the zip ties but also make the bottom flat. When the zip ties go around they have this curvature that makes the surface uneven. You can see it later when I will mount everything. 

So, I make this bottom piece that will be fixed to the up one with screws and therefore I made the exact same holes as the ones from the up box.

{{<image src="images/make_box_so_zip_under_1.jpg" alt="Square shape." size="40%" >}}

This is how they look together.

{{<image src="images/make_box_so_zip_under_2.jpg" alt="Square shape." size="40%" >}}

I then 3D printed the pieces and this is how they look like.

{{<image src="images/btm_part_out_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/btm_part_out_box_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/btm_part_out_box_3.jpg" alt="Square shape." size="40%" >}}


- **Design the outside wood box**

For this box I used the module I already had when I made the tea box on the Week 5: Computer-Controlled Cutting assignment.

At the end my PLA box end up having a size of 16cm by 16.5cm so for the wooden box I will increment 10mm more and ill make it 17cm by 17.5 cm.

I start by changing the design I already have. As you can see by the image bellow I put wrong the measurements and I end up making a box double the size. But I did notice before cutting.

{{<image src="images/changing_tea_box_sizes.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/changing_tea_box_sizes.jpg" alt="Square shape." size="40%" >}}

Then I realise the sizes. 

{{<image src="images/wrong_double_size.jpg" alt="Square shape." size="40%" >}}

And I correct them.

{{<image src="images/now_final_bottom.jpg" alt="Square shape." size="40%" >}}

I then do the same readjust of the sizes to all faces. 

{{<image src="images/face_26_final.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/face_14_final.jpg" alt="Square shape." size="40%" >}}

I must have been very tired when doing this cause I kept making mistakes. 

This time the box is completely closed so I must also had the tabs on the top face. So I changed the two previous faces.

{{<image src="images/forgot_up_connection_f26.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/forgot_up_connection_f14.jpg" alt="Square shape." size="40%" >}}

Then I must add the specific things for my box such as holes for the switch, battery cable and screws.

I first start with the back face of the box where I create the hole for the battery cable. I measured and it gave me 7. Then I do 7-kerf and I obtain the 6.75 you can see on the image bellow. But you can already expect that this won’t work because once again I forgot to had the extra 0.5mm offset after evey time we measure something.
 
{{<image src="images/create_smaller_hole_for_cable.jpg" alt="Square shape." size="40%" >}}

Then I make the hole for the switch also in the back face. For that I took the measurements from the input box I made.

{{<image src="images/make_hole_for_switch.jpg" alt="Square shape." size="40%" >}}

Then I just miss adding the holes for the screws so I can attach the silicone base of the brain with the box. For that I import the mold to this design and I project the holes for the screws and the holes of the air tube and LED cables to the sketch of the up face. I will offset the holes with the size of the kerf. They have to become smaller.

{{<image src="images/insert_mold_to_know_where_holes.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/offset_holes_with_size_of_kerf.jpg" alt="Square shape." size="40%" >}}

This is how the up face looks like at the end.

{{<image src="images/up_face.jpg" alt="Square shape." size="40%" >}}

Now, I’m good to start laser cutting. I thought I still remember all the steps but I had to review some settings to send the print to the Epilog printer.

I start by measuring the kerf of this 4mm playwood piece. I do the classic 20mm*20mm cube and I measure with the calliper.

{{<image src="images/cube_laser_cut_1.jpg" alt="Square shape." size="40%" >}}

Accordingly to the following 3 images the kerf can either be 0.3mm, 0.4mm or 0.25mm respectively.

{{<image src="images/cube_laser_cut_3.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cube_laser_cut_4.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/cube_laser_cut_5.jpg" alt="Square shape." size="40%" >}}

I decided to go with the 0.3mm of them all. I also tested if the hole for the air tube would work and it looks like it will.

{{<image src="images/cube_laser_cut_2.jpg" alt="Square shape." size="40%" >}}

I must have been really tired that day or just in an hurry or distracted that I wasn’t considering more things. For example I should have tested more if the kerf was correct by making smaller pieces and see if they would fit well together. But I didnt do that and I went straight to cut all the 6 pieces of the box and I guess hoping it would fit.

{{<image src="images/cut_all_pieces_without_checking.jpg" alt="Square shape." size="40%" >}}

Of course you can guess by now that it didnt work. The holes were too tight. Also both the switch and the battery cable didnt fit in. 

{{<image src="images/cable_didnt_fit.jpg" alt="Square shape." size="40%" >}}

Another error I notice was that I made the connections 10mm big and I don’t know why. It doesn’t make any sense. If the thickness of the box is 4mm I have to make those also 4mm so at the end when we join ask the pieces together they don’t pop out.

With all this being said a lot of things had to be changed before cutting again.

A good thing I did this time was creating a parameter called kerf. So then I can make the measurements with that parameter and just change the kerf for testing.

{{<image src="images/use_parameters.jpg" alt="Square shape." size="40%" >}}

So then I start changing all the measurements so they depend on the kerf, for example.

{{<image src="images/do_like_this_instead.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/parameter_there_too.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/change_distance.jpg" alt="Square shape." size="40%" >}}

As you can see on the image bellow all of the measurements depend on the parameter kerf.

{{<image src="images/make_them_all_smaller.jpg" alt="Square shape." size="40%" >}}

Now for this second time I will cut I will make this two small test pieces where I will test different kerfs. I will not just see which kerf is the best for the joint connection as well as to see the sizes of the holes for the battery and switch.

As I’m not sure if the hole for the battery and switch didn’t work because of the wrong kerf or if its because I have to consider the classic 0.5mm offset I just decided to make those both options. One in each piece. Following are the both test pieces.

{{<image src="images/test_piece_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/test_piece_2.jpg" alt="Square shape." size="40%" >}}

I’m more hopeful with the 0,25mm for the kerf so I start with this one.

{{<image src="images/more_hopeful_for_this.jpg" alt="Square shape." size="40%" >}}

I first cut the test piece and it didn’t fit. Was still too tight. 

{{<image src="images/test_piece_too_tight.jpg" alt="Square shape." size="40%" >}}

Then I remember that when I made the tea box the best kerf option was the one I picked based on just measuring one individual square piece. Which in this case gives a kerf of 0.4. I don’t wanna go with that value already cause im afraid it will be too loose so I decided to go with 0.35mm for the kerf instead.

{{<image src="images/chnaging_again.jpg" alt="Square shape." size="40%" >}}

I cut again the same test pieces and now it fits perfectly. And it’s a tight fit like I want. So I decide to go with the 0.35mm kerf.

{{<image src="images/now_it_fits_test_piece_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/now_it_fits_test_piece_2.jpg" alt="Square shape." size="40%" >}}

Then I test which one of the pieces has the correct sizes for the switch and the battery cable. I discover that the one with the 0.5mm offset is the one that works. The other one doesn’t work. Its to tight.

{{<image src="images/size_cable_test_pieces_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/size_cable_test_pieces_2.jpg" alt="Square shape." size="40%" >}}

Then I cut all the pieces. It will be supper quick because all the measurements are depending on the value of the kerf. So I know I just have to put the correct kerf that all the measurements will be actualised.

After cutting all the pieces I realised I didn’t checked if the hole for the air tube will work with this new kerf value. 

I tried it and it didnt work. 

{{<image src="images/hole_air_tube_no_works.jpg" alt="Square shape." size="40%" >}}

Which means I gotta cut again. I don’t have more space left on the playwood I was given so I better not mess up again. To make sure I cut the up face again with the correct size I did once again a small test piece. And this one seams to work.

{{<image src="images/test_piece_air_tube.jpg" alt="Square shape." size="40%" >}}

I also tested if he hole for the screws were good enough and they were. So I just gotta change the size of the hole for the air tube and recut again.

{{<image src="images/check_hole_screw_wood.jpg" alt="Square shape." size="40%" >}}

I cut the piece again and not it works perfectly.

{{<image src="images/good_size_hole_now.jpg" alt="Square shape." size="40%" >}}

I also engraved my signature and an image I found on the internet on the front face so the box doesn’t look plane. 

Now I that I have all the pieces I confirm if they fir together and they do uhuuhh.

{{<image src="images/pieces_fit_together.jpg" alt="Square shape." size="40%" >}}

Then I also check if the PLA box fits inside this box. And off course it does.

{{<image src="images/PLA_fits_wood.jpg" alt="Square shape." size="40%" >}}

Then I start thinking how am I gonna attach the PLA box with the wooden box. I have the screws on the PLA box which creates some hight making it impossible to glue the PLA box with the wooden box.

{{<image src="images/theres_hight.jpg" alt="Square shape." size="40%" >}}

So I just decided to make holes on the bottom part of the box big enough for the screws to go through. By doing this I could later glue the box. Another nice thing is that the hight of the cap of the screw is not taler than the thickness of the wood which means that the box will be touching the table with its all surface. 

Before cutting the bottom part of the box I decided to make a test piece first. I only have one opportunity so I cant mess up.

I measure the diameter of the cap of the screw and I made an offset of 0.5mm.

{{<image src="images/add_0.5.jpg" alt="Square shape." size="40%" >}}

It worked really well.

{{<image src="images/test_piece_screw_bottom.jpg" alt="Square shape." size="40%" >}}

Next, I insert that box into the current design so I can project the holes on the bottom piece.

{{<image src="images/insert_box_to_make_hole.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/adjust_location_make_holes.jpg" alt="Square shape." size="40%" >}}

Then I make a bigger circle with the size of the cap of the screw plus the offset.

{{<image src="images/size_off_hole.jpg" alt="Square shape." size="40%" >}}

Now I just have to cut those holes on the bottom piece. I was hard to place the holes perfectly in the piece. I had to make this engraving tabs so I could see the distances of the holes to the outlines of the bottom piece. And then I just didn’t do the engraving by setting it to “off”.

I think it worked pretty well at the end. The holes were tight enough to secure the PLA box without glue which is perfect.

{{<image src="images/no_need_glue_tight.jpg" alt="Square shape." size="40%" >}}

And you can see that the PLA box now sits perfect on the wood box and the wood box doesn’t have the screw piping out and lift it. The wood box is also perfectly toughing the table. It work out perfect at the end.

{{<image src="images/perfect_1.jpg" alt="Square shape." size="40%" >}}

This I how the boxes will be attach to each other.

{{<image src="images/perfect_2.jpg" alt="Square shape." size="40%" >}}

7. ### Put all the components on the box

- **Finalise the output brain** 

I start by putting all the components on the PLA box and see if they fit.

I realise that the pump fits almost perfectly but still I cant slide it all the way down. 

{{<image src="images/cant_slide.jpg" alt="Square shape." size="40%" >}}

To fix this I thought about my hold friend sander that already helped me with the mold box for the brain. I start sandind the sides of the most front piece and I see if it will work. Otherwise I can always reap that piece off. I don’t feel like reprinting at this point. I don’t have time.

{{<image src="images/sand_piece_box.jpg" alt="Square shape." size="40%" >}}

Then I try to insert the pump and it worked. Now the pump is tightly pressed between walls.

{{<image src="images/pump_fits.jpg" alt="Square shape." size="40%" >}}

Then I move on to put the pipe ties when I notice that the zip tie is not big enough to go all the way around the pump. 

{{<image src="images/not_big_enough.jpg" alt="Square shape." size="40%" >}}

But that’s not a problem I can just put two zip ties. Easy fix.

{{<image src="images/two_zip_ties.jpg" alt="Square shape." size="40%" >}}

As you can see the zip ties create this curvatures on the other side that lifts the plate up. And that’s why I made the bottom piece so I can hide the zip ties but also make a bottom flat surface.

{{<image src="images/not_flate.jpg" alt="Square shape." size="40%" >}}

Then I put zip ties around the battery. I also needed two of them.

{{<image src="images/around_battery.jpg" alt="Square shape." size="40%" >}}

Then I also insert the PCB with screws and I add the valve with the zip tie. This is how it looks for now.

{{<image src="images/almost_done_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/almost_done_box_2.jpg" alt="Square shape." size="40%" >}}

Then I realised I still had to solder a wire from the 5V converter to the Battery IN on the XIAO board to supply energy to the it. Otherwise it doesn’t work.

Because this is already an old board and I’ve solder already wires there I think it got to weak and the tab got removed. Meaning that I cant solder anymore any wire to the Bat_IN tab on the XIAO board.

{{<image src="images/no_more_bye_XIAO.jpg" alt="Square shape." size="40%" >}}

I had to grab a new XIAO and upload the server code again there.

I solder the wire and now all went well.

{{<image src="images/new_xiao.jpg" alt="Square shape." size="40%" >}}

Then I’m gonna attach the board with this screws to the surface.

{{<image src="images/attach_board_screws_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/attach_board_screws_2.jpg" alt="Square shape." size="40%" >}}

Then I finally attach the two pieces together with the screws.

{{<image src="images/attach_pieces_box.jpg" alt="Square shape." size="40%" >}}

Then I start by building the box around it.

{{<image src="images/build_around_1.jpg" alt="Square shape." size="40%" >}}

I had the power cable and the switch on the back side.

{{<image src="images/build_around_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/build_around_3.jpg" alt="Square shape." size="40%" >}}

I had to cut some wires to make them smaller and fit better inside the box. I also had to solder smaller cables on the switch accordingly to the new placement of the objects.

The switch almost goes against the walls of the battery. What a lucky beginner cause I was defiantly not considering the switch when I made the design.

{{<image src="images/switch_almost.jpg" alt="Square shape." size="40%" >}}

Finally I tight really well all the zip ties.

{{<image src="images/tight_zip_ties_1.jpg" alt="Square shape." size="40%"  >}}

{{<image src="images/tight_zip_ties_2.jpg" alt="Square shape." size="40%" >}}

I cut all the zip ties and I also connected all the cables and this is how it looks. It looks really good at the end

{{<image src="images/finall_PLA_setup_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/finall_PLA_setup_2.jpg" alt="Square shape." size="40%" >}}

Just to make sure the power cable doest go away I also put this zip tie. Although the entrance its very tight on the cable so it should get out that easily but better be safe than sorry.

{{<image src="images/zip_power_cable.jpg" alt="Square shape." size="40%" >}}

At this stage the silicone is ready. 

{{<image src="images/silicone_base_ready.jpg" alt="Square shape." size="40%" >}}

I remove the mold and I’m happy to see that the silicone also covered bellow the support like I intended to. 

{{<image src="images/silicone_bellow_nice.jpg" alt="Square shape." size="40%" >}}

Looks so niceeeee.

{{<image src="images/silicone_Mzinf.jpg" alt="Square shape." size="40%" >}}

I put the silicone base on the up part of the box and im happy to see everything fits perfectly in the holes. I notice the silicone is a bit lift it.

{{<image src="images/a_bit_lift_sil_box.jpg" alt="Square shape." size="40%" >}}

So then I realise there’s a bit of silicone that drop down the screw with the gravity. I will cut it with the help of a x-ato and that fixed the problem.

{{<image src="images/problem_sil_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/problem_sil_box_2.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/problem_sil_box_3.jpg" alt="Square shape." size="40%" >}}

After doing all the designs I got a bit concerned if the screws would touch the pump. I did some measurements and I flip the direction of the up part of the box on purpose but still I wasn’t sure if it would work. Never the less I took the risk and im so thrilled to see that it works and it doesn’t work.

{{<image src="images/doesnt_touch_uhuhu.jpg" alt="Square shape." size="40%" >}}

Then I had some walls and the up part but I leave two walls open so I can still attach the pip going from the valve to the brain as well as the LED cables.

{{<image src="images/less_two_walls.jpg" alt="Square shape." size="40%" >}}

It was a bit challenging to tight the LED cables as the screw driver couldn’t exactly reach the screws. I had to lift the up part a bit to have more angle.

{{<image src="images/more_angle.jpg" alt="Square shape." size="40%" >}}

Next I had to cut the air tube so it fits perfectly and it doesn’t bend. Its not ideal if it bends cause that would cut the flow of the air and nothing would happen

{{<image src="images/cut_tube.jpg" alt="Square shape." size="40%" >}}

This is how everything looks like inside the box. Pretty nice.

{{<image src="images/inside_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/inside_2.jpg" alt="Square shape." size="40%" >}}

Then before closing everything I cant forget to put the antenna. Otherwise it won’t work as we all now by now.

{{<image src="images/antena.jpg" alt="Square shape." size="40%" >}}

Finally I had the small support to hold the brain. At this stage I use a zip tie for that purpose. Honestly it works so well and it’s barely visible. 

{{<image src="images/zip_to_support_1.jpg" alt="Square shape." size="40%" >}}

Later I will attach the rings I design but now I have to make them bigger since the diameter changed with all the silicone I put to make it stronger.

{{<image src="images/change_to_rings_later.jpg" alt="Square shape." size="40%" >}}

Finally this is how the box looks like. Beautiful!!!

{{<image src="images/final_box_out.jpg" alt="Square shape." size="40%" >}}

- **Finalise the input box**

By this point you might not remember but I casted again the keys for the input box. Because the first time I did it it didnt work to well. 

So now I will first reap it off from the old board where I cast them. 

{{<image src="images/rip_off_old_board.jpg" alt="Square shape." size="40%" >}}

Then I will put them on top of the new board. While reaping it off, it leaves some holes for the buttons and LEDs so it’s fine. It fits and works really well. I do this because I want the silicone to not be completely attach to the keys otherwise to gets supper hard to press them. So if I do this strategy where I just put the silicone one top, they buttons get a bit lose and easier to press them.

{{<image src="images/put_on_top.jpg" alt="Square shape." size="40%"  >}}

Then I still miss including the switch. So I still have to so that. 

I insert the switch on the board and I solder from there the cables.

{{<image src="images/solder_bat_inp.jpg" alt="Square shape." size="40%" >}}

Then I have to make sure I leave the battery case ON and then I just turn it OFF and ON through the outside switch.

{{<image src="images/turn_bat_on_inp.jpg" alt="Square shape." size="40%" >}}

Finally I screw the the PCB on the board so it gets fixed there.

{{<image src="images/screw_inp.jpg" alt="Square shape." size="40%" >}}

This is how it looks like at this stage.

{{<image src="images/this_stage_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/this_stage_2.jpg" alt="Square shape." size="40%" >}}

Then I put the silicone on top and I close the box with the up part.

{{<image src="images/close_up.jpg" alt="Square shape." size="40%" >}}

Then I print again the small piece to be the same colour because before was red which is very visible.

{{<image src="images/print_small_again_white.jpg" alt="Square shape." size="40%" >}}

Then I insert the display followed by the small piece. And this is the final result of the input box

{{<image src="images/display_box_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/display_box_2.jpg" alt="Square shape." size="40%" >}}

8. ### Final show

Now that I have everything set I just miss test if all works together. 

Before shoeing the video I would like to make a small discretion of the project and its purpose.

We will have an exhibition in the fall so I thought my project could be made for an exhibition.

I thought about important topics that I would like to transmit to the society. Those being things like climate change, sustainability, mental health, etc.

I ended up to have an idea for the mental health topic so I went for it.

So the idea of the project is that I will have an input box with 9 buttons. Those buttons correspond to a single word/mode/feeling. For example the number one is “Happy”, number two “tired”, etc. Then I will have an output brain that will react differently accordingly with the button (feeling) pressed.

If the participant is feeling good, and thus presses a good button, the brain will light up some LEDs inside of it with an happy and insightful animations.

However if the person is feeling bad, and thus presses a bad button, the brain will deflate. Exactly the same as we feel when we are sad or tired. It feels like our brain is heavy and smooshed.  

Finally I will display the overall mood of the participants of Aalto. There will be two graphs. One showing the number of people that picked the feeling and another one showing the percentage of how many times a felling was selected by the participants.

I find this website very interesting and useful to understand how the overall students are feeling. This could be good as an interactive study about mental health for schools and universities. 

Finally it’s time to see this baby working! Everything works perfectly. I’m sooo happy! Finally !!!!!! UHUHUHUH

{{< youtube mC_3RLsOids >}} 

As you can see form the video the video above, when the brain deflates, it tilts to the side. So i gotta make the small support a bit bigger so it has a bigger surface touching the brain and hopefully prevent it from tilting to the side.

We were also asked to do a video to present our project. By that time I didnt have anything ready to show how my project would look like so I just made a funny video showing what would it be at the end. Feel free to give a look at the [video](https://arianamarta.gitlab.io/digital-fabrication/presentation.mp4) and [poster](https://arianamarta.gitlab.io/digital-fabrication/presentation.png) we had to make.

Also, I miss showing the code for the server and the client. So here it goes.

First the server code:

{{< highlight go-html-template >}}

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>
#include <ArduinoJson.h>

#include <freertos/task.h>

#include <SparkFun_MicroPressure.h>
//define Pressure Sensor
SparkFun_MicroPressure mpr; // Use default values with reset and EOC pins unused

// for the Access point
// Some variables we will need along the way
const char* ssid     = "Mentally";
const char* password = "confused"; 

int webServerPort = 80;

// Setting up our webserver
AsyncWebServer server(webServerPort);

//define pins
int pump_pin = D9;
int valve_pin = D8;

//LEDs
#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define LED_PIN D7
#define LED_COUNT 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_RGB + NEO_KHZ800);

uint32_t white = strip.Color(250,250,200);
uint32_t red = strip.Color(250,0,0);
uint32_t orange = strip.Color(255,165,0);
uint32_t yellow = strip.Color(250,250,0);
uint32_t blue = strip.Color(0,0,250);
uint32_t green = strip.Color(0,250,0);
uint32_t purple = strip.Color(128,0,128);

// array of colors
uint32_t color[6] = {red, orange, yellow, blue, green, purple};

// array for the word count
// the lables are: ['Happy', 'Stressed', 'Motivated', 'Proud', 'Sad', 'Confident', 'Anxious', 'Thrilled', 'Scared'],
int wordCount[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
// total count of participants
int totalAmountUsers = 0;

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void sendResponse(AsyncWebServerRequest *request, String message) {
  AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void goodWordTask(void* pvParameters) {
  // set Brightness again to 64 because the last time the brightness was set was to 0
  strip.setBrightness(64);
  strip.show();

  // hopefully this is a spiral effect
  for (int i = 0; i < 6; i++) {
    strip.setPixelColor(i, color[i]);
    strip.show();
    vTaskDelay(pdMS_TO_TICKS(100));
  }

  vTaskDelay(pdMS_TO_TICKS(1000));

  // lighting up and down LEDs in a faded way
  // I have to simulate a loop. I want this to happen 4 times
  // fadeAmount should be 4 up to 64. We have to loop 16 times.
  int brightness = 0;
  int fadeAmount = 4;

  strip.setBrightness(brightness);
  strip.show();

  // set the first time the LEDs to zero
  for (int i = 0; i < 6; i++) {
    strip.setPixelColor(i, white);
  }
  strip.show();

  vTaskDelay(pdMS_TO_TICKS(200));

  // do fade in fade out 4 times
  for (int i = 0; i < 4; i++) {
    // fade in
    for (int n = 0; n < 16; n++) {
      // change the brightness
      brightness = brightness + fadeAmount;
      strip.setBrightness(brightness);
      strip.show();
      for (int i = 0; i < 6; i++) {
        strip.setPixelColor(i, white);
      }
      strip.show();
      vTaskDelay(pdMS_TO_TICKS(30));
    }

    // fade out
    for (int n = 0; n < 16; n++) {
      // change the brightness
      brightness = brightness - fadeAmount;
      strip.setBrightness(brightness);
      strip.show();
      for (int i = 0; i < 6; i++) {
        strip.setPixelColor(i, white);
      }
      strip.show();
      vTaskDelay(pdMS_TO_TICKS(30));
    }
    vTaskDelay(pdMS_TO_TICKS(80));
  }

  vTaskDelete(NULL);
}

void badWordTask(void* pvParameters) {
  // Deflate the brain
  // PUMP ON & VALVE OFF
  digitalWrite(valve_pin, LOW);
  digitalWrite(pump_pin, HIGH);

  vTaskDelay(pdMS_TO_TICKS(3000)); // Change this with the pressure sensor later

  // PUMP OFF & VALVE OFF
  digitalWrite(pump_pin, LOW);
  vTaskDelay(pdMS_TO_TICKS(3000));

  // PUMP OFF & VALVE ON
  // let the air get in again
  digitalWrite(valve_pin, HIGH);

  // Blinking/fading LEDs in black or red
  // You can modify this part to achieve the desired effect

  vTaskDelete(NULL);
}

void setup() {

  Serial.begin(115200);

  // Setting the XIAO 1 as an access point
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  Wire.begin();

  pinMode(pump_pin,OUTPUT);
  pinMode(valve_pin,OUTPUT);
  digitalWrite(pump_pin,LOW);
  digitalWrite(valve_pin,HIGH);

  // NEON pixels
  strip.begin();
  strip.setBrightness(64);
  strip.show();


  while(!mpr.begin())
  {
    Serial.println("Cannot connect to MicroPressure sensor.");
    delay(100);
  }

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    sendResponse(request,"Hello World!");
    //Serial.print("Number of connections:" );
    //Serial.println(WiFi.softAPgetStationNum());
  });

  // input box makes a request saying if btn pressed is good (1) or bad (2) word
  // all path is: /buttom?btn=1 for example.
  server.on("/buttom", HTTP_GET, [](AsyncWebServerRequest *request){

    String msg;
    if (request->hasParam("btn")) {
      // The incoming params are Strings
      String param = request->getParam("btn")->value();
      Serial.print("value: ");
      Serial.println(request->getParam("btn")->value());
      // .. so we have to interpret or cast them
      if (param == "1"){ // good word
        Serial.println("Good word was pressed.");
        msg = "Good word turn the LEDs on.";
        //goodWord();
        // Create the task
        xTaskCreate(goodWordTask, "goodWordTask", 2048, NULL, 1, NULL);


        // Now i would control the LEDs and AIR pump
      } else if(param == "2") { // bad word
        Serial.println("Bad word was pressed.");
        msg = "Bad word deflate the brain.";
        //badWord();
        // Create the task
        xTaskCreate(badWordTask, "badWordTask", 2048, NULL, 1, NULL);

      } else {
        msg = "No button provided.";
      }
    } else {
      msg = "No parameter provided.";
    }

    // in this case the response could be for the website i want to make notifying what happened and what people could do
    sendResponse(request,msg);
  });

  server.on("/wordCount", HTTP_GET, [](AsyncWebServerRequest *request){

    String msg;
    int position;
    if (request->hasParam("position")) {
      // The incoming params are Strings
      String param = request->getParam("position")->value();
      Serial.print("position: ");
      Serial.println(request->getParam("position")->value());

      position = param.toInt();
      Serial.println(position);

      wordCount[position] +=1;
      totalAmountUsers +=1;

      msg = "Position reseived successfuly.";
    } else {
      msg = "No parameter provided.";
    }

    StaticJsonDocument<200> doc;
    JsonArray nameCount = doc.createNestedArray("nameCount");
    for (int i = 0; i < 9; i++) {
      nameCount.add(wordCount[i]);
    }
    
    doc["totalAmountUsers"] = totalAmountUsers;

    char responseJSON[200];
    serializeJson(doc, responseJSON);

    sendResponse(request, responseJSON);

    // in this case the response could be for the website i want to make notifying what happened and what people could do
    sendResponse(request,msg);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  // Start server
  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
} 

void loop() {
  // I leave the Arduino loop function empty because the server handles the requests asynchronously.
}


{{< /highlight >}}


Now the client code:

{{< highlight go-html-template >}}

#include <WiFi.h>
#include <HTTPClient.h>

const char* ssid = "Mentally";
const char* password = "confused";

//Your IP address or domain name with URL path
String mainRootpath = "http://192.168.4.1/";
String buttomRootpath = "http://192.168.4.1/buttom";
String wordRootpath = "http://192.168.4.1/wordCount";

String resp;

//LEDs
#include <Adafruit_NeoPixel.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define LED_PIN D8
#define LED_COUNT 9
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_RGBW + NEO_KHZ800);

// Display
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// happy_face
const unsigned char epd_bitmap_happy_face [] PROGMEM = {
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0xe0, 0x07, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03, 0xf0, 0x0f, 0xc0, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x03, 0xf0, 0x0f, 0xc0, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x03, 0xf0, 0x0f, 0xc0, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x03, 0xe0, 0x0f, 0x80, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0xc0, 0x03, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x10, 0x00, 0x00, 0x08, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x10, 0x00, 0x00, 0x08, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00, 0x00, 0x10, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x08, 0x00, 0x00, 0x10, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x02, 0x00, 0x00, 0x40, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x03, 0x00, 0x00, 0xc0, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x01, 0x80, 0x01, 0x80, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x40, 0x02, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00,
 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x38, 0x1c, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x07, 0xe0, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x01, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};
// wobbly_face
const unsigned char epd_bitmap_wobbly [] PROGMEM = {
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x3f, 0xf0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc1, 0xff, 0xfe, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0f, 0xff, 0xff, 0xc3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xf1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0xff, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xf1, 0xff, 0xff, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xe3, 0xff, 0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xff, 0xff, 0x8f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x8f, 0xff, 0xff, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0xff, 0xff, 0xff, 0xff, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xfe, 0xff, 0xf3, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 0xfd, 0xff, 0xfe, 0x3f, 0xf3, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xfc, 0xff, 0xff, 0x8f, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xf1, 0xff, 0xff, 0xc7, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xe3, 0xff, 0xff, 0xf3, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0x0f, 0xff, 0xff, 0xf9, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xfe, 0x3f, 0xff, 0xf0, 0xfb, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0xfc, 0x7f, 0xf0, 0x7f, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xf0, 0x7f, 0xe0, 0x7f, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xe0, 0x7f, 0xe0, 0x7f, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xe0, 0x7f, 0xe0, 0x7f, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xe0, 0x7f, 0xe0, 0x7f, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xf0, 0xff, 0xf0, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc,
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0xfe, 0x0f, 0xff, 0xff, 0xef, 0xfc, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfc, 0x78, 0x1f, 0xff, 0xff, 0xe1, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfe, 0x79, 0x3f, 0xff, 0xff, 0xf0, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xfe, 0x7f, 0x3f, 0xff, 0xff, 0xf0, 0xf9, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 0x3f, 0x01, 0xff, 0xe2, 0xf3, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 0x1c, 0x00, 0x7f, 0xc7, 0xf3, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x9f, 0x80, 0xfc, 0x1f, 0x8f, 0xe7, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0x8f, 0xc1, 0xff, 0x00, 0x1f, 0xc7, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xc7, 0xff, 0xff, 0xe0, 0x7f, 0x8f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xe3, 0xff, 0xff, 0xff, 0xff, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xf1, 0xff, 0xff, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xf8, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x7f, 0xff, 0xff, 0xf8, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0x3f, 0xff, 0xff, 0xf1, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x0f, 0xff, 0xff, 0xc3, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc1, 0xff, 0xfe, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x3f, 0xf0, 0x3f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, 0x0f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc, 
 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfc
};

// Define the Address pins
int Add0 = D2;
int Add1 = D3;
int Add2 = D7;
int Add3 = D6;
// Define Drain Pin
int D = D0;
// Enable pin, When low all address pins are switch off, When High we can read the address pins.
int enable = D1;

// Define MUX channels ---> (A3,A2,A1,A0)
int controlPin[] = {Add3, Add2, Add1, Add0};


int btnAddresses[9][4]={
  {0,0,0,0}, //channel 0 ----> Btn3 --> good
  {0,0,0,1}, //channel 1 ----> Btn6 --> good 
  {0,0,1,0}, //channel 2 ----> Btn7 --> bad 
  {0,0,1,1}, //channel 3 ----> Btn8 --> good 
  {0,1,0,0}, //channel 4 ----> Btn9 --> bad 
  {1,0,0,0}, //channel 9 ----> Btn5 --> bad 
  {1,1,0,1}, //channel 14 ----> Btn2 --> bad 
  {1,1,1,0}, //channel 15 ----> Btn4 --> good 
  {1,1,1,1} //channel 16 ----> Btn1 --> good 
};

bool btnStates[9]={
  LOW, 
  LOW, 
  LOW, 
  LOW,
  LOW, 
  LOW, 
  LOW, 
  LOW,
  LOW
};

void setup() {
  Serial.begin(115200);

  // Define pin mode for the Address Pins
  pinMode(Add0,OUTPUT);
  pinMode(Add1,OUTPUT);
  pinMode(Add2,OUTPUT);
  pinMode(Add3,OUTPUT);
  pinMode(enable,OUTPUT);

  // Dont know if i should initialize them to 0 Lets see !!
  digitalWrite(Add0, LOW);
  digitalWrite(Add1, LOW);
  digitalWrite(Add2, LOW);
  digitalWrite(Add3, LOW);
  digitalWrite(enable, LOW);
  digitalWrite(enable, HIGH);

  // NEON pixels
  strip.begin();
  strip.setBrightness(64);
  // .show() pushes data out to the pixels.
  // Since no color has been set yet, this initializes all NeoPixels to an initial 'off'
  strip.show();

  // Setup for the Display
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { 
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();

  // starting Wifi connection
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");
}

void selectBtnChannel(int i){
  digitalWrite(Add0, btnAddresses[i][3]);
  digitalWrite(Add1, btnAddresses[i][2]);
  digitalWrite(Add2, btnAddresses[i][1]);
  digitalWrite(Add3, btnAddresses[i][0]);
}

void sendWordPosition (String position){

  String wordRootpathAll;

  wordRootpathAll += wordRootpath + "?position=" + position;
  
  resp = httpGETRequest(wordRootpathAll);
  Serial.print("Response from the GET request: ");
  Serial.println(resp);
  Serial.println("");
  delay(200);
}

void turnLEDOn(int i){
  /*
  My LEDs addressing are: | My LEDs Layout:
    0 3 6    |  1 2 3
    1 4 7    |  4 5 6
    2 5 8    |  7 8 9
  */

  // red, green, ... are numbers from 0 (off) to 255 (max brightness)
  //strip.setPixelColor(n, red, green, blue, white);
  // I think the color order is GRB instead of RGB
  uint32_t white = strip.Color(0,0,0,250);
  uint32_t green = strip.Color(250,0,0,0);
  uint32_t red = strip.Color(0,250,0,0);

  // also 
  // array for the word count
  // the lables are: ['Happy', 'Stressed', 'Motivated', 'Proud', 'Sad', 'Confident', 'Anxious', 'Thrilled', 'Scared'],
  // which means :   ['Good', 'Bad', 'Good', 'Good', 'Bad', 'Good', 'Bad', 'Good', 'Bad'],
  // position in array: [0, 1, 2, 3, 4, 5, 6, 7, 8]
  // buttons are:       [1, 2, 3, 4, 5, 6, 7, 8, 9]
  String position;

  switch (i) {
  case 0:
    // its button 3 ---> GOOD
    // btn=3 => n=6
    strip.setPixelColor(6, green);
    position = "2";
    break;
  case 1:
    // its button 6 ---> GOOD
    // btn=6 => n=7
    strip.setPixelColor(7, green);
    position = "5";
    break;
  case 2:
    // its button 7 ---> BAD
    // btn=7 => n=2
    strip.setPixelColor(2, red);
    position = "6";
    break;
  case 3:
    // its button 8 ---> GOOD
    // btn=8 => n=5
    strip.setPixelColor(5, green);
    position = "7";
    break;
  case 4:
    // its button 9 ---> BAD
    // btn=9 => n=8
    strip.setPixelColor(8, red);
    position = "8";
    break;
  case 5:
    // its button 5 ---> BAD
    // btn=5 => n=4
    strip.setPixelColor(4, red);
    position = "4";
    break;
  case 6:
    // its button 2 ---> BAD
    // btn=2 => n=3
    strip.setPixelColor(3, red);
    position = "1";
    break;
  case 7:
    // its button 4 ---> GOOD
    // btn=4 => n=1
    strip.setPixelColor(1, green);
    position = "3";
    break;
  case 8:
    // its button 1 ---> GOOD
    // btn=1 => n=0
    strip.setPixelColor(0, green);
    position = "0";
    break;
  default:
    strip.clear();
    break;
  }
  // push the color data to the strip
  strip.show();
  sendWordPosition(position);
}

void showHappyFace(){
  display.clearDisplay();
  display.display();
  display.drawBitmap(0, 0, epd_bitmap_happy_face, 128, 64, WHITE);
  display.display();
  delay(2000);
}

void showHappyText(){
  display.clearDisplay();
  display.display();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(F("Hey, look"));
  display.println(F("at the"));
  display.println(F("brain !!"));
  display.display();      // Show initial text
  delay(3000);
}

void showWobblyFace(){
  display.clearDisplay();
  display.display();
  display.drawBitmap(0, 0, epd_bitmap_wobbly, 128, 64, WHITE);
  display.display();
  delay(2000);
}

void showSadText(){
  display.clearDisplay();
  display.display();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(F("Eat some "));
  display.println(F("chocolat"));
  display.println(F("to chear"));
  display.println(F("your day!"));
  display.display();      // Show initial text
  delay(3000);
}

void showStatusText(){
  display.clearDisplay();
  display.display();
  display.setTextSize(2); 
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(F("WiFi "));
  display.println(F("Disconnected!"));
  display.display();      
  delay(2000);
}

void turnDisplayOn(int n, bool status){
  if(n==1){
    // good word
    Serial.println("I'm good word");
    showHappyFace();
    showHappyText();
    if(!status){
      showStatusText();
    }
  } else {
    // bad word
    Serial.println("I'm bad word");
    showWobblyFace();
    showSadText();
    if(!status){
      showStatusText();
    }
  }
}

// instead of making a post reqest i should make a get request where i just send the new value like so : /buttom?btn=1&state=on
String httpGETRequest(String serverName) {
  WiFiClient client;
  HTTPClient http;
  String payload;

  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName.c_str());
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
 
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
    //Serial.println(payload);
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void sendRequest (int n){

  String btnNumber;
  String buttomRootpathAll;

  if(n==1){ // good word
    btnNumber = "1";
    buttomRootpathAll += buttomRootpath + "?btn=" + btnNumber;
    resp = httpGETRequest(buttomRootpathAll);

    Serial.print("Response from the GET request: ");
    Serial.println(resp);
    Serial.println("");
    delay(200);

  } else { // bad word
    btnNumber = "2";
    buttomRootpathAll += buttomRootpath + "?btn=" + btnNumber;
    resp = httpGETRequest(buttomRootpathAll);

    Serial.print("Response from the GET request: ");
    Serial.println(resp);
    Serial.println("");
    delay(200);
  }
}
void loop() {
  // Read all button states
  int n;
  bool status;
  for (int i=0; i<9; i++) {
    selectBtnChannel(i);
    pinMode(D, INPUT_PULLUP);
    btnStates[i] = digitalRead(D);
    //delay(200);
    if (!btnStates[i]){
      turnLEDOn(i);
      // good word i = 0,1,2,8
      if ( i==0 || i==1|| i==3 || i==7 || i==8 ){
        n = 1;
        Serial.println();
        Serial.print("n: ");
        Serial.println(n);
        Serial.println();
      } else {
        // bad word
        n = 2;
        Serial.println();
        Serial.print("n: ");
        Serial.println(n);
        Serial.println();
      }
      if(WiFi.status()== WL_CONNECTED ){ 
        status = 1;
        turnDisplayOn(n,status);
        display.clearDisplay();
        display.display();
        delay(1000);
        sendRequest(n);
      } else {
        status = 0;
        turnDisplayOn(n,status);
        Serial.println("WiFi Disconnected");
      }
      delay(5000);
      strip.clear();
      strip.show();
    }
  }
}

{{< /highlight >}}

9. ### Fix some imperfections

Because I’m to perfectionist I had to make some changes on the code. 

I first have to match the labels I have in the website with the code I made. Cause at the moment they are not matching. So I had to look up the order of the labels I have on the website and change the code on the client side. Bellow are images of some of the changes I had to make.

{{<image src="images/adjust_the_lables.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/change_here.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/and_there.jpg" alt="Square shape." size="40%" >}}

Then I also realised that the brain would react to the input almost right after clicking the button not giving time for the user to read what is on the display, or even worse, not realising what the brain was doing.

So I had to fix the delays between clearing the display and sending the request to the brain.

Some of this changes required me to code the XIAO board on the output box. 

It was hard for me to reach it with the small cable so I had to go get my phone charger so I could program it. 

{{<image src="images/hard_to_reach_1.jpg" alt="Square shape." size="40%" >}}

{{<image src="images/hard_to_reach_2.jpg" alt="Square shape." size="40%" >}}

This is a good lesson for my future designs. The same has happened with the input box. Every time I wanna change the code I have to open the box which is not ideal. So next time I will consider that and I’ll make make the XIAO reach the end of the box and make an entrance for connecting the cables on the box. Of course that in this case it’s not a problem. I don’t have to constantly change the code. This was just a very punctual thing. And it’s actually better that the boxes look clean from the outside. This is anyway a single purpose project so no need to keep changing the program/code.

Nevertheless following is the video where I made this final adjustments of the delays and labels.

{{<video src="images/final_final_video_v.mp4">}} It works!!! {{</video>}}

10. ### Still left to do

Theres still some minor adjustments left to do.

1. One of them is making a hole on the output brain so I can let air get in to inflate the brain again. 

Actually, now that I’m thinking about it I might not need to make a hole for that purpose because the pump has two end. One that sucks air which I’m using to deflate the Brian but also another end that pump air out. So there will be air inside the box pumped by the pump that can be used to inflate the brain to its normal shape.

Nevertheless I still miss making a cut on the back side of the box so it gets easy to open the box after closing it completely. 

{{<image src="images/make_opening.jpg" alt="Square shape." size="40%" >}}

Because right now I left the front side open just in case I have to do some adjustment inside the brain box. Right now I’m afraid to close it and then being very hard or impossible to open it again. So I really gotta make a cut in the corner for easier opening. By doing this opening I will also let air get in anyway so no need for the extra hole I mentioned above.

2. Then, as already mentioned after showing the video, I gotta make the small support of the brain a bit bigger so it has bigger surface touching the brain and hopefully prevent it from tilting to the side.

However, ive heard feedback from people I’ve shown the video and they actually like the fact it’s kinda falling. Because, if you think about it, when we are tired or sad, everything feels heavy and the world seams to fall down. So it’s actually a nice perspective and view of it. I gotta think what to do about it.

3. Make a paper showing what number correspondes to what word/mode/felling. For example the number one means "Happy", number two "tired", and so on and so foward.

4. Finally I miss writing on the website about the project description as well as display the amount of participants along with the charts.

Conclusion,  I’m very proud of what I achieved and how much I’ve learned throwout this experience.