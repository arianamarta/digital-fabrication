---
title: "About Me"
description: "A short description of myself"
featured_image: '/images/landscape.jpg'
weight: 10
menu:
  main:
    weight: 1
---

{{<image src="me.jpg" alt="A photo of me." size="50%" >}}

Hi! my name is Ariana Marta and I'm from Portugal.

I have a bachelors in Electrical and Computers Engineering and currently I’m a full masters degree student here In Aalto University studying Control Robotics and Autonomous Systems.

I’m taking minors in Computer Science and Arts which has always been a passion.

I’m a creative and dedicated person with the anger to learn new things. Therefore I consider this course perfect for me since I can finally work with tools to build and design amazing electronic devices.

Thank you. 
