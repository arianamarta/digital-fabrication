---
title: "My FabLab Academic Website"

description: "All my progress explain here"
# 1. To ensure Netlify triggers a build on our exampleSite instance, we need to change a file in the exampleSite directory.
theme_version: '2.8.2'
cascade:
  featured_image: '/images/home_img.jpg'
---

Hi this is my first website !

My website and final project is licensed under the CERN Open Hardware License v2.

You can find the full license text [here](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2) or [here](https://choosealicense.com/licenses/cern-ohl-p-2.0/).
