---
date: 2023-02-21T10:58:08-04:00
description: ""
featured_image: "/images/arduino.jpg"
tags: []
title: "Week 6: Embedded Programming"
weight: 50
---

This week we learned a bit about embedded programing. 

We all received a kit consisting on a board, wires, buttons and one XIAO-RP2040. 

Table of content:

* [Solder the Pins](#solder-the-pins)
* [SetUp](#setup)
* [LEDs](#leds)
* [Neon LEDs](#neon-leds)
* [Buttons](#buttons)
* [Serial Class](#serial-class)

Option:

* [CircuitPython](#circuitpython)

### Solder the Pins

We first had to solder the pins on the XIAO-RP2040. In order to do that I saw the video provided by the professor. 

I took some photos of the procedure:


{{<image src="../images/6week/fan.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/6week/heat.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/6week/sponge.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/6week/needle.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/6week/string.jpg" alt="Size of png extantion." size="40%" >}}

After solder the pins I grabbed a multimeter and I tested if adjacent pins were connected or not. We don’t want that to happen but because they are very close to each other there could be a bridge of flux uniting two pins.

When touching the pins with the needles of the multimeters and there’s no sound produced it means they are not connected. That’s the expected outcome.

{{<image src="../images/6week/multimeter.jpg" alt="Size of png extantion." size="40%" >}}

### SetUp

Next, it's time to test the XIAO-RP2040 and all it's functionalities such as LEDs, neon LEDs as well as using buttons and Serial class.

I first started by intalling "Arduino" software, following by adding Seeed Studio XIAO RP2040 board package to the Arduino IDE.

Go to preferences:

{{<image src="../images/6week/preferences.jpg" alt="Size of png extantion." size="40%" >}}

Then add the url provided on this [page](https://wiki.seeedstudio.com/XIAO-RP2040-with-Arduino/):

{{<image src="../images/6week/url.jpg" alt="Size of png extantion." size="70%" >}}

### LEDs

Next, I started to test the board with a simple blink code provided by the arduino software by going to File -> Examples -> Basics -> Blink.

{{<image src="../images/6week/blink.jpg" alt="Size of png extantion." size="60%" >}}

Before uploading the code to the board we have to select the correct board and port.

We can do that as shown by the images bellow:

{{<image src="../images/6week/board.jpg" alt="Size of png extantion." size="50%" >}}

{{<image src="../images/6week/port.jpg" alt="Size of png extantion." size="45%" >}}

After that we can finally upload the code and see magic happening.

I also made some changes to the code to test it's functionalities.

I first started with this simple blink code by changing the LED color from RED to GREEN and the delay from 1000 to 500 making it blink with double the frequency.

{{<image src="../images/6week/Led_G_and_delay.jpg" alt="Size of png extantion." size="40%" >}}

Next, I decided to play around more with the LEDs so i decided that i want it to blink but changing the colors every time. Meaning that, instead of blinking just one color, it would blink every color alternatively.

The code is as follows:

{{<image src="../images/6week/blink_all.jpg" alt="Size of png extantion." size="40%" >}}

### Neon LEDs

Following, I tested the NEONPIXEL LED. 

For that I inspired myself on the professors example code.

Once again, I first started simple just by making the LED blink random colors. 

{{<image src="../images/6week/neon_blink.jpg" alt="Size of png extantion." size="50%" >}}

Improving, then, to a more complex code where I decided to test how does the button works.

The idea is to make the button turn on and off the NEONPIXEL LED. However, when on, it should blinking random colors.

Here's the following code:

{{<image src="../images/6week/neon_blink_1.jpg" alt="Size of png extantion." size="55%" >}}

{{<image src="../images/6week/neon_blink_2.jpg" alt="Size of png extantion." size="45%" >}}

Here's also a video I made to better understand what the code does:

{{<video src="../images/6week/neon_blink_button1.mp4">}} This is the neon blinking video.{{</video>}}

When I first tested the code it worked when pressing the button to turn on the led, but it didnt work when pressing the button to turn it off. 

The way i fix it was by adding a delay of 200ms so i give some time for it to read the status of the button. 

Next, to make sure it was working i made some Serial prints so i could see in the Serial Monitor, if the button status was correctly read and if it enter a specific area of the code.

As you can see by the image bellow, When the button is pressed ( == LOW ) and the LED is not blinking ( blinking == 0 ) I print "Start". Meaning it will start blinking. Same when the LED is blinking ( blinking == 1 ) I print "Stop". Meaning it will stop blinking. 

{{<image src="../images/6week/print.jpg" alt="Size of png extantion." size="40%" >}}

Besides that I also print the status of the button ("Serial.println(btnState)") to see if it actually read when i press it.

{{<image src="../images/6week/print_status.jpg" alt="Size of png extantion." size="40%" >}}

Heres a short video of that happening:

{{<video src="../images/6week/neon_blink_button2.mp4">}} This is the Serial Monitor pannel video.{{</video>}}

### Buttons

Although I already tested one button on the previous example, I decided to test both button provided. I did it more to understand the wiring and if i knew the connections, rather than make a fancy program with the LEDs.

Therefore, I wrote a simple program where one button will turn on the Red LED and the other button will turn on the Green button. Very simple, but again, the purpose of this experience was seeing if i knew how to wire two buttons.

Here is the code:

{{<image src="../images/6week/2button.jpg" alt="Size of png extantion." size="40%" >}}

I must confess the biggest challange was finding space for the board, cables and two buttons in such a tiny space. Here is how the wiring looks like:

{{<image src="../images/6week/wiring.jpg" alt="Size of png extantion." size="40%" >}}

Here is a video to show how it works:

{{<video src="../images/6week/2_button.mp4">}} This is 2 buttons video.{{</video>}}

### Serial class

Finally, I tested the Serial class a bit more in deth. Although I already used it on the Neon LED example to print to the console the values of the Button status as well as some strings, i decided to anyway test this class a bit more. In this [page](https://www.arduino.cc/reference/en/language/functions/communication/serial/) you can find all the functions and their explanations.

I first started by running the professores example code to see how it works.

{{<image src="../images/6week/serial.jpg" alt="Size of png extantion." size="50%" >}}

When sending the message the LED is also turn on and off. Later on I also change the delay for smaller frequency so i can see that phenomena happen better. Changed from 100 to 1000. The LED_BUILTIN is the RED one.

{{<image src="../images/6week/test1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/6week/test2.jpg" alt="Size of png extantion." size="40%" >}}

Note that, the last line saying "recived: " means that the last character of every input message is the newline character "\n" and thus its also printed. 

{{<image src="../images/6week/new_line.jpg" alt="Size of png extantion." size="40%" >}}

Finally, I decided to test the Find() function provided by the Serial class. First I read what does the function does and then i implemented some code to see it working. In the website i mentioned above, it's written that, "Serial.find() reads data from the serial buffer until the target is found. The function returns true if target is found, false if it times out."

I went to the Serial.setTimeout() function and it's written that the default value is 1000 milliseconds which is more than enough so no need to set this parameter.

I come up with a simple code where i wrote "hello" as my target word and if it finds the correct word i turn on the Green LED for 1000 ms and if the word typed is incorrect i turn on the Red LED for 1000 ms as well.

Here's the code:

{{<image src="../images/6week/serial_find.jpg" alt="Size of png extantion." size="40%" >}}

And this is a video showing how it works:

{{<video src="2_button.mp4">}} This is 2 buttons video.{{</video>}}


## CircuitPython

Optionally, we were asked to test other softwares to code the XIAO-RP2040, using either CircuitPython and/or MicroPython. 

I decided to go for CircuitPython since it's built on MicroPython and thus simpler. 
