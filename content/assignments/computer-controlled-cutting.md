---
date: 2023-02-15T10:58:08-04:00
description: ""
featured_image: "/images/laser.jpg"
tags: []
title: "Week 5: Computer-Controlled Cutting"
weight: 40
---

This week was about laser cutting.

Unfortunatly I wasn't able to finish laser cutting on the first week since my schedule was very tight with other courses and thus was hard to manage a time that didn't overlaped with the other students turns.

Never the less i manage to test and cut some stuff to prove i knew about kerf and the design.

Following i will explain my evolution allong the weeks to have the final and desired project.

Evolution through the weeks:

* [Week 1: Design and Testing](#design-and-testing)
* [Week 2: CardBox Prototype](#cardbox-prototype)
* [Week 3: Wood Prototype](#wood-prototype)
* [Design Explanation](#design-explanation)

### Design and Testing

We had to sketch something in a CAD sotware of our choice. I choose Fusion 360. I'm still super new in this CAD software or any CAD software so i took some time to get used to it and to it's constrains. 

I decided to do as my project a Tea Box where i made some separations in the interior for the different kinds of tea i'll be putting. 

I will also engrave the sides of the box with some finnish and portuguese words as well as some phrases from portugues poems in the interior of the box.

{{<image src="../images/5week/side.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/name.jpg" alt="Size of png extantion." size="50%" >}}

{{<image src="../images/5week/poem.jpg" alt="Size of png extantion." size="70%" >}}


After testing for the first time i realized I had to change how my 2D sketch was made. I change it in a way of making it easy to change all the dimensions considering the thickness of the material i'll use and considering the kerf. The kerf changes all the time both for the same material but also between materials and thus i must have a sketch easy to adjust the dimensions.

This is how i made it so its easy to change the parameters considering kerf and thickness:

As you can see, for the indent (piece coming in) i must decrease the value with the kerf, for example if i want it to be 20mm i do 20-0.28(kerf) = 19,72.

As for the piece coming out I have to increase the kerf thus if I want 20mm i'll make it 20.28.

{{<image src="../images/5week/closer.jpg" alt="Size of png extantion." size="60%" >}}

{{<image src="../images/5week/buttom_sketch.jpg" alt="Size of png extantion." size="60%" >}}

This following image is on of the separations in the interior of the box. I made the indent fillet at the edges so it gets easier and smoother to combine all the separation pieces together.

{{<image src="../images/5week/interior.jpg" alt="Size of png extantion." size="60%" >}}


To calculate the kerf I cut 5 pieces as explained by the professor. I measured the total amount of all of them together and it was 98.6mm. To calculate the kerf I do the total expected amount of the five pieces all align together (100mm) subtracted with the value read from the caliper (98.6mm) giving 1.4mm. Next, I devided that value by the total amount of pieces (5 pieces) which will give me the kerf (the diameter of the laser beam). In this case the kerf is 100-98.6 = 0.28.

After finding the value of the kerf I made a simple design where i could test both joints, the interior and the engraving to see if i did the calculus correct for the kerf. Turns out everything ok and it fit perfectly as you can see by the images bellow.

{{<image src="../images/5week/1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/8.jpg" alt="Size of png extantion." size="40%" >}}

Along the next week, I'll come back to the lab to finalize the box in carton and move forward to the wood.

### CardBox Prototype

This week I went to the lab ones in the morning for around 3 hours. My goal was prototype really fast in cardboard and move forward to the wood. 

Unfortunatly my goal was not meet. I come up with a minor problem that, because I took too long to give up and ask for help, it took me two hours to move on. The problem I faced was suprisingly the calculation of the kerf. I don't know why but it was giving me 1.14mm. I stick to it and clearly didnt work. Therefore i kept trying and changing the kerf values untill I finally stopped a moment to think about what was happening. 

So, what I did was measuring each individual square (both sides) and do the average of the measuments to pick the kerf. 

Since the material is cardboard and the lazer goes through the material it makes sense that the measurments are not that precised since the inside is not completly filled and its irregular. Fothermore, the ruler is also never precised for every measurment.

This to say that, when I finally choose a better kerf (one that actually makes sense such as 0.23 instead of 1.14) I manage to perfectly fit the parts of the box. 

I made my design in order to be able to adjust the parameters very fast and easy accordingly to the value of the kerf and width of the material. Therefore, when I finally got the kerf correct I cut all the pieces for the box very fast and they fit really well.

It was good to test my design first with cardboard because i realized that i dont want to do the holes in the bottom as shown in the image bellow. There is no need for that since the interior pieces fit perfect without the holes.

{{<image src="../images/5week/cartbox_int.jpg" alt="Size of png extantion." size="40%" >}}

I'm so proud for the fact that everthing fit perfectly. Specially when using Cardbox that it can easilly get very loosen.

I didn't have time to engrave the "title" of the box since I felt pressure to give the others time to use the machine.

Here are some pictures of the box made out of Cardbox:

{{<image src="../images/5week/cartbox_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/cartbox_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/cartbox_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/cartbox_3.jpg" alt="Size of png extantion." size="40%" >}}

I really want to make this work and I know I can! Therefore I already booked the laser cut machine for next week to finally cut it using wood. 

I think that this time I won't take this long because the wood is more precised. I know that the measurment of the kerf wont take me to long and it will be more precided for sure.

### Wood Prototype
 
It was this week that i finally finish building the model I design and I couldn't be more happy with the final result!

Like I mentioned above the kerf measurement didn’t took long and it was very precise. 

To measure the kerf I did 5 squares 20mmx20mm like usual. Once again, the different sides of the cubes gave different values and thus I made the average of them. Therefore the kerf end up being 0.3mm.

Although, jugging by the experience I acquired from the cardboard I know that I will have to consider a little bit less kerf when making the whole for the interior piece. Otherwise it will be supper tight to join them together.

Before I had a smart distance of 1.75 and now I made the hole bigger up to 1.85. The hole was very tight for a wall of 2mm thick.

{{<image src="../images/5week/before.jpg" alt="Size of png extantion." size="30%" >}}

In order to not waste too much wood i made the same model a bit smaller just to test if the joints fit perfect with the kerf I choose.

{{<image src="../images/5week/not_perfect.jpg" alt="Size of png extantion." size="40%" >}}

As you can see the kerf wasn’t the ideal pick since some of the joints fit good but that specific piece was impossible to fit. So I thought what size to pick for the kerf, if smaller or bigger. Judging by the measurements I did previously and by thinking a bit I realised I should reduce the kerf but not too much. Therefore I picked 0.25mm instead. I must say it was a very accurate shot because afterwards there was no need to change the kerf again. On top of that, everything end up fitting perfectly without forcing it. 

{{<image src="../images/5week/good.jpg" alt="Size of png extantion." size="40%" >}}

On the picture above you can see that the joints go a bit out the box and that was because the professor thought I wanted 2mm but I wanted instead 3mm. So I made the calculations counting with 3mm. This mistake was surprisingly good because I liked how it looked so I end up doing it for the final product.

Next, I cutted all the pieces of the box and i join them all together to see if it worked before engraving. 

Since all fit perfectly i start engraving. 

I felt a bit pressured by the time i had left so i didn't even thought about how the engraving had to be placed in the wood so i manage to make a ridiculous mistake. As you can see by the image bellow the words are upside down. I wish i could say that was part of the design and it was totally on purpose.

{{<image src="../images/5week/mistake.jpg" alt="Size of png extantion." size="40%" >}}

Next, I manage to make the orientation right and this is how it looks. I used medium engraving. The professor also gave a suggestion of putting tape before engraving so it doesn’t get those burn part around the words. Honestly, I really like in this case so I let it stay.

{{<image src="../images/5week/face1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/face2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/face3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/face4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/face5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/face6.jpg" alt="Size of png extantion." size="40%" >}}


Then I join the 3 interior pieces together. Like mentioned above I had to consider a small number for the kerf in order to fit perfect with no brute force and this is how it looks. 

{{<image src="../images/5week/int1.jpg" alt="Size of png extantion." size="40%" >}}

Because I tested lots of times I run out of wood so I used a leftover piece to make on of the 3 pieces and only later I realised that had some engraving there from someone else. 

I really like how it looks because it looks like the company logo was engraved there. I find such a funny coincidence and to gives a funny story to it in case someone ever wonders what does that symbol means. 

I honestly don’t know and I’m curious. 

{{<image src="../images/5week/int2.jpg" alt="Size of png extantion." size="40%" >}}

I must also mention I’m so proud of how well the interior pieces fit inside the box. It goes down so smoothly !! I’m so happy and proud.

I tried to make a video but it doesn’t have the best angle or quality. But it’s enough to show what I mean.

{{<video src="../images/5week/int_smooth.mp4">}} This is the neon blinking video.{{</video>}}

Next, I just miss glue the two top pieces together. I made some engraves on the bigger piece so I knew where to place the smaller piece. I used glue for wood and some clamps to press them together and I let it stay for more than 12 hours.

{{<image src="../images/5week/top.jpg" alt="Size of png extantion." size="40%" >}}

When I was finally testing the top on the box I realised that although it fits perfect its a bit bended. The reason why is because the wood is not thick enough and thus when cutting and engraving it bended a bit with the temperature of the lazer. 

I was a bit sad by that because everything was looking amazing and working perfectly but I completely didn’t count with the possibility of bending. 

What I will do next is repeat this two same pieces but I will use this time a thicker piece of wood for the bigger piece so it doesn’t bend. I will use maybe 5mm or 6mm.

Here is a photo that tries to show the top piece bended.

{{<image src="../images/5week/tilted.jpg" alt="Size of png extantion." size="40%" >}}

Here are more photos of the box.

{{<image src="../images/5week/box2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/box3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/box4.jpg" alt="Size of png extantion." size="40%" >}}

I went back again to the lab to redo this two bended piece. I used 5mm thickness this time and I tried to see if the wood was bended or not. It’s so hard to see it cause at the end when I cut it was still a bit bended. 

The inner piece (the small piece that has the text) come up with no tilt which was perfect, but the outside piece (the big one) it was a bit bended. 

At this point I realised that there’s no point to keep trying and waste wood so I just tried to compress the piece between heavy weights but off course it didn’t work. 

Nevertheless, I really don’t mind because, since the inner piece its 5mm think and the bending is not to much, the whole piece together will fit the box and stay attached. 

With this being said, I proceeded again by glueing the two pieces together with glue meant for wood and later I used wood varnish to make it look amazing. 

On the following images you can see how well the cover fits on the box. Even though the bigger piece was a bit bended, after gluing them togeter, the final piece was not bended since the inner one wasnt bended. Nevertheless im happy that i didnt have to cut again and that this piece fix perfectly on the box.

{{<image src="../images/5week/final_box_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/final_box_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/final_box_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<video src="../images/5week/fitting_an.mp4">}} This is the Serial Monitor pannel video.{{</video>}}

After glueing those pieces together i varnish the wood using "Wood Oil for Outdoors".

{{<image src="../images/5week/first_varnish_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/first_varnish_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/5week/first_varnish_3.jpg" alt="Size of png extantion." size="40%" >}}

### Design Explanation

Finally, I would like to explain the design behind this box.

I have this really closed friend from Finland and every time we are together we drink tea so I though about doing a tea box. 

We are also learning our languages with each other. Therefore, the engraving around the box, is remarkable words we have taught each other.

Finally, this friend is a really good poet by passion, thus I decided to engrave in the interior of the box some famous sentences from very famous Portuguese poems. 

It’s indeed a very special box to me and thus I want to make it perfect.