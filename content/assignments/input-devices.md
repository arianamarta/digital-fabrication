---
date: 2023-04-13T10:58:08-04:00
description: ""
featured_image: "/images/input.jpg"
tags: []
title: "Week 13: Input Devices"
weight: 120
---

[ [Update](#update) ]

This week we will be inserting an input device to our board. We have learned how to incorporate output devices to our board, meaning that we send signals to the output device to do something (putting it in a simple way). But now it’s the other way around. The input device will be the one sending to the board information for the board to process it and do something with it. 

I did two input devices, one of them using arduino and a bit off the scope of the course and another one where i use the XIAO board and i mill a new circuit board. In both the projects i use an accelometer as the input device.

* [Letter A](#letter-a)
* [Tap Me](#tap-me)

## Letter A

As you might know, wappu is almost there so as a fuksis we were asked to build a letter to put it in Alvari Square. Thanks to this course I acquired new skills and in general I got more handy when it comes to building. 

So we build the letter A in the fablad and we spray paint it. It looked super boring so I decided to make it more exciting. I suggested they could show their “wappu strength” by hitting with their hand on an old computer case. Their strength will be measured with an accelerometer through the vibration of the computer case that was hit by them.

Then accordingly to the strength we will light up addressable LEDs from bottom to up the letter A.

So, with this project I will have an accelerometer as an input device and the LEDs as an output device.

The way the accelerometer works is that it will measure the vibration of the case and will map those vibration to values along the x,y,z-axis. Then I will convert those values on the axis to acceleration (m/s^2) and I will print those values. By printing the values and testing different strengths we will define threshold values to light up the LEDs to different heights. Because we have addressable LEDs we can control witch LEDs to light.

The accelerometer we use is powered with a 3.3V voltage (they cant handle 5V) and the LEDs with 5V voltage (they require 5V). 

## Tap Me

For this week I was required to use the XIAO board and mill a new circuit board accordingly. As already mentioned I will use an accelerometer as the input device. The one I’m using is Adafruit MSA301 Triple Axis Accelerometer.

I first started by testing and learning how would the accelerometer work. Then I had to make a new circuit board. I was also recommended by the professor to had a battery. Therefore I designed a battery case and I 3D printed it.

With this in mind I will explain all those procedures.

#### Learning and testing accelerometer

First I look up on the internet the accelerometer I was given and found this [website](https://learn.adafruit.com/msa301-triple-axis-accelerometer?view=all) where its giving us a library called “Adafruit_MSA301”.  It also shows how the connections are made and all the information in general. This time compared to the output device week, the connections are as easy as it’s shown on the image bellow. No need to setup the pins like last time.

{{<image src="../images/13week/legenda.jpg" alt="Size of png extantion." size="50%" >}} 

In this case we use the XIAO board so I had to see the pin layout so I could know which pins were the SCL and SDA.

{{<image src="../images/13week/pins_legenda.jpg" alt="Size of png extantion." size="50%" >}} 

I followed the guide on that webpage to install all the libraries.

{{<image src="../images/13week/install_libraries.jpg" alt="Size of png extantion." size="40%" >}} 

I then try running the first example code called “acceldemo” and surprise surprise it didn’t work! Well for me was no surprise cause I seam to never be able to make the XIAO board works. There’s always tinny and stupid mistakes. 

{{<image src="../images/13week/problem.jpg" alt="Size of png extantion." size="40%" >}} 

Well, turns out that the library im using has the accelerometer MSA301 as default for the configurations. And the accelerometer im using is the 311 ! Of course I couldn’t figure out the solution by myself even though I try my best googling about it. 

I asked the professor and after some research on the internet we realised that the I2C addresses are different for MSA301 and MSA311. MSA301 has I2C address set to 0x26 whereas MSA311 has I2C address set to 0x62. 

{{<image src="../images/13week/solution_2.jpg" alt="Size of png extantion." size="70%" >}} 

The worst part is that until then I didn’t even realised that the library I installed was called MSA301 instead MSA311. 

{{<image src="../images/13week/solution_3.jpg" alt="Size of png extantion." size="60%" >}} 

We then looked at the *.h (header) file of the library to see the construction of the functions etc. On Mac to access that library we have to go to documents -> Arduino -> libraries -> Adafruit_MSA301.

And was when we realize that the default I2C address was for the MSA301 accelerometer and thus we had to change the line “Adafruit_MSA301 msa; “ to Adafruit_MSA311 msa;”.

{{<image src="../images/13week/solution_1.jpg" alt="Size of png extantion." size="45%" >}} 

{{<image src="../images/13week/solution_1_1.jpg" alt="Size of png extantion." size="45%" >}} 

When looking to the header file we see that they created a class for hardware interfacing with the MSA311 accelerometer. That class inherits from the Adafruit_MSA301 class.

{{<image src="../images/13week/I2C_address_h.jpg" alt="Size of png extantion." size="45%" >}}

{{<image src="../images/13week/311_class.jpg" alt="Size of png extantion." size="45%" >}}

The Adafruit_MSA311 class will work the same as the one it inherits from with a minor change of I2C address. 

When changing this the code starts working since now it can find the I2C address of the MSA311 address.

Here is a video showing it working:

{{<video src="../images/13week/example_1.mp4">}} acceldemo example.{{</video>}}

Later I decided to explore more examples from that library such as the “tapdemo”. In the beginning I thought it didn’t work but apparently I just had to tap it strong enough for it to sense the taping. 

This is a video showing it working:

{{<video src="../images/13week/example_2.mp4">}} tapdemo example.{{</video>}}

After learning the technic I got excited and I decided to make the single taping turn one led (RED for example) and the double tapping turn another led (GREEN for example). 

I tried to see if the code was working by using jumper cables, resistors and LEDs but one of the LEDs wasn’t working properly so the professor suggested me to just move on and mill the circuit board and then test the code using the board. He also pointed out that the jumper cables might not be working and there’s lots of margin for error so mind as well mill the board already.

#### Make the circuit board and Mill

By this time I already knew I was going to insert a battery on my device. Therefore I thought on several designs and ways to make the circuit board. I end up thinking on attach the board on the battery case and make a hole on the circuit board so the cables from the battery could be connected to the XIAO board. I would make a one sided circuit board and I would use rivets to so I could put the XIAO board and the accelerometer on the front layer along with the LEDs. 

Later on, with the help of the professor, I just decided to use a terminal to make the connection between the battery cables and the XIAO battery inputs.

With this in mind let’s go to the procedure.

The first struggle was understand which XIAO to use from the “fab” library.

{{<image src="../images/13week/xiao_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/xiao_2.jpg" alt="Size of png extantion." size="40%" >}} 

Turns out that they are the same but what it changes is the type of headers. On the first image the type of the headers are the ones im already use to it but on the second image the kind of headers we have to use are called PIN SOCKET and look as follow:

{{<image src="../images/13week/header_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/header_2.jpg" alt="Size of png extantion." size="40%" >}} 

I decided to go with the XIAO from the first image since it also displayed the battery inputs so later I could use with the terminal.

After inserting all the components this is how the “schematic editor” looks like on KiCad.

{{<image src="../images/13week/white_final.jpg" alt="Size of png extantion." size="80%" >}} 

Then I move to the “PCB editor” and this was my first idea where I had the hole for the battery cables to get in and I just had the LEDs

{{<image src="../images/13week/3D_view_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/black_1.jpg" alt="Size of png extantion." size="40%" >}} 

But latter with the idea of adding a terminal and a switch to turn on and off the battery I end up changing the design a little where I remove the hole under the place where the XIAO would have been placed.

I also made 3 more adjustments:

1. I changed the size of the diameter of the hole to 1.55mm to be able to insert the rivets 

{{<image src="../images/13week/diameter_1.jpg" alt="Size of png extantion." size="30%" >}} 

{{<image src="../images/13week/diameter_2.jpg" alt="Size of png extantion." size="60%" >}} 

I had to do this for all the holes I had since I would put rivets in all of them

Attention: Make sure to change the shape so we can had the 2.1mm diameter otherwise it doesn’t work.

{{<image src="../images/13week/diameter_3.jpg" alt="Size of png extantion." size="50%" >}} 

{{<image src="../images/13week/diameter_4.jpg" alt="Size of png extantion." size="40%" >}} 


2. Don’t forget to make space between the accelerometer and the XIAO board. I went to measure the size of the accelerometer and I had to change quite a lot so thank god I notice this.

{{<image src="../images/13week/distance.jpg" alt="Size of png extantion." size="40%" >}} 


3. Finally the professor thought me how to create a new NET. The purpose of this is to create a new one a bit wither that is capable of dealing with bigger current which is what happens with the battery connections. Although the professor said that for the sack of this board it would matter. But I got to learn it anyway which was nice. 

{{<image src="../images/13week/crate_class_net.jpg" alt="Size of png extantion." size="60%" >}} 

This is how the final design looks like. The images are from the PCB editor, the 3D view and when creating the geber file.

{{<image src="../images/13week/black_final.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/3D_view_final.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/geber_final.jpg" alt="Size of png extantion." size="40%" >}} 

I also learned that it’s okay to ignore some warnings and errors.

{{<image src="../images/13week/ignore_error.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/warnings.jpg" alt="Size of png extantion." size="50%" >}} 

Next I move on to milling where I also learn new things

I followed all the procedures as I did before but when I started milling there was this area that wasn’t being milled. My first thought was that I had screw up with the tools or something so I immediately stoped the job so I wouldn’t brake the tool. But I knew I had made everything correctly so I tried again and see what would happen and I realised that in a specific area it wouldn’t mill so I conclude it could be because the bed wasn’t level. 

I then asked the professor to help me level it and we realised that the bed was actually level but the problem could have been the tape I put at the back of the board. We tried it again and the problem maintained. It was supper strange this was happening but then we just decided to zero the Z-axis on the area that wasn’t milling so we could lower it a bit more. When doing that everything worked as expected as you can see from the image bellow.

{{<image src="../images/13week/after_mill.jpg" alt="Size of png extantion." size="40%" >}} 

This time I also learned how to sand the board and with which sand paper. I’ve never done it before as I was confused how to do it. Now I know!

#### Design and 3D print the battery case

The professor suggested me to find a design on the internet for this lithium batteries. 

{{<image src="../images/13week/battery.jpg" alt="Size of png extantion." size="40%" >}} 

We end up printing one of the designs we found on “thingiverse” but it didn’t quite suit what we wanted so as stubborn and proud person as I am I just decided to make the design myself. I took the measurements of the lithium batteries and I inspired my design on the lithium cases we have at the lab. 

I made this design along with the design on Kicad so I was sure everything would fit perfectly. However I had already printed this case before the professor suggested the option of the terminal. But it’s okay. I found a way of both the designs still work together. 

The following images will show a bit the procedure how I made this battery case:

{{<image src="../images/13week/box_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/box_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/hole_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/hole_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/small_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/small_2.jpg" alt="Size of png extantion." size="50%" >}} 

{{<image src="../images/13week/small_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/all_small.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_4.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_5.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_6.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_7.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/step_8.jpg" alt="Size of png extantion." size="40%" >}} 

This how the module looks like after printing it! 

{{<image src="../images/13week/module_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/module_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/module_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/module_4.jpg" alt="Size of png extantion." size="40%" >}} 

#### Solder everything 

It’s time to start soldering everything on the board but before all that I must make holes on the board. It’s much easier if we don’t have headers in our way. Those holes im making on the circuit board is so than I can attach it to the battery case. Since I keep having changes on the design and since Im in an hurry to finish this week I just decided to mill the board and then figure out where I want to attach the circuit board to it. It’s easy to just do holes afterwards.

To make the hole im using the same machine I used to make the vertical holes for the molding and casting week. I first marked with a pencil where I wanted to make the holes. The machine its not has precise but I manage to make the holes in the correct place. I used a 2mm tool that is used for steel.

{{<image src="../images/13week/machine_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/machine_2.jpg" alt="Size of png extantion." size="40%" >}} 

I’m attaching the battery case with the circuit board using M2 screws.

{{<image src="../images/13week/screws_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/screws_2.jpg" alt="Size of png extantion." size="40%" >}} 

Now is time to solder everything on the circuit board. I have to solder 2 headers for the XIAO and one for the accelerometer. I also have to solder the terminal and cables that go from the board to the battery tabs on the XIAO.

I first start by putting the rivets inside the holes. I have never done this before so it was nice to learn something new. It’s good practice to take some rivets out of the box and close it to prevent from letting them all fall down. Like the professor says we all are clumsy.

{{<image src="../images/13week/rivets_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/rivets_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/rivets_3.jpg" alt="Size of png extantion." size="40%" >}} 

The following image shows all the tools needed. 

{{<image src="../images/13week/tools_needed.jpg" alt="Size of png extantion." size="40%" >}} 

First we insert the rivet inside the hole with the help of a tweezer.

{{<image src="../images/13week/tweezer_1.jpg" alt="Size of png extantion." size="40%" >}} 

Then we use specific tools where we hit with the hammer to open the tweezer to the sides. 

{{<image src="../images/13week/tweezer_1_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tweezer_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tweezer_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tweezer_4.jpg" alt="Size of png extantion." size="40%" >}} 

Then we have to make sure the rivets is well connected to the circuit board. To accomplish that we have to put a bit of solder wire between the rivet and the tabs on the circuit board. For that we do the same process as always. First we apply flux and then we apply a bit of solder connecting the tab and the rivet.

{{<image src="../images/13week/flux_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/flux_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/flux_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/flux_4.jpg" alt="Size of png extantion." size="40%" >}} 

Then I measure with the multimeter if the connections were well made. If it makes a sound it means that both the extremities are connected.

{{<image src="../images/13week/extremities.jpg" alt="Size of png extantion." size="40%" >}} 

Afterwards I start soldering the headers, I put the XIAO board connected to the headers to make sure it enters perfectly after soldering. I learn it from my mistakes when I first solder headers on the XIAO board on the week 8.

{{<image src="../images/13week/solder_header_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/solder_header_2.jpg" alt="Size of png extantion." size="40%" >}} 

Once again the procedure is the same. First apply flux and then solder. I use the toxic wire since it seams to be easier melting etc.

{{<image src="../images/13week/solder_header_3.jpg" alt="Size of png extantion." size="40%" >}} 

Finally, I test if the soldering was succeeded with the multimeter. If it makes sounds it means its good.

{{<image src="../images/13week/solder_header_4.jpg" alt="Size of png extantion." size="40%" >}} 

I did the exact same thing for the headers for the accelerometer.

Finally I solder the LEDs, resistors and the switch.

Those are the most challenging ones as we all know but I feel like im becoming pro at soldering. 

First I grab the LEDs and respective resistors. For RED I use 1k Ohm and for GREEN I use 49.9 Ohm as calculated and explained on the week8.

Then apply flux in the tabs where I wanna insert the resistors and LEDs. I apply a bit of solder wire on those tabs. Once again, after trying and error I understood that the thinner wire (the toxic one) its so much easier and better for this small things as it melts and solidifies way faster then the thicker wire. 

{{<image src="../images/13week/solder_led_1.jpg" alt="Size of png extantion." size="40%" >}} 

This is how it looks after connecting all LEDs, resistors and switch. I also confirmed if everything was working with the help of the multimeter. And so far so good.

{{<image src="../images/13week/final_led_res.jpg" alt="Size of png extantion." size="40%" >}} 

Next i miss inserting the terminal. My first idea was to solder it on the back of the board so it would be hidden but off course I had to make a very noob mistake. I had all the space in the world (more less) to insert the battery case but off course I forgot to take into account the terminal and basically I didn’t leave enough space for the terminal to be on the back of the board. But luckily was space enough for the terminal pins. Of course I could have made new holes but I thought the board would look uglier than putting the terminal on the front view. Here’s some pictures showing what I just explained. This error was the first of a bunch I had. But honestly I loved that those happened so I got to learn from them and not repeat the same mistakes on future projects. 

{{<image src="../images/13week/solder_led_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/solder_led_4.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/solder_led_5.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/solder_led_6.jpg" alt="Size of png extantion." size="40%" >}} 

I end up soldering the terminal on the front part and I follow the same procedure. The professor strongly advise me to put a bit of solder connecting the rivets and the tab to make sure there was a solid connection. And that’s what I did. I start by putting flux, followed by some solder on the rivets and tabs. I once again use the thiner wire. 

{{<image src="../images/13week/terminal_sol_1_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/terminal_sol_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/terminal_sol_2.jpg" alt="Size of png extantion." size="40%" >}} 

This is how it looks like. Looks beautiful already!

{{<image src="../images/13week/terminal_sol_3.jpg" alt="Size of png extantion." size="40%" >}} 

Then I measure once again if all the connections are working using the multimeters and its when I realize that the connections from the XIAO board to the accelerometer wasn’t working. 

{{<image src="../images/13week/not_working_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/not_working_2.jpg" alt="Size of png extantion." size="40%" >}} 

For my understanding what happened is that I put the connecting paths very thin and at the end there was no path connecting the XIAO with the accelerometer. I’ve never quite understood how much should I put on the following places on the image below. 

{{<image src="../images/13week/set_contour_1.jpg" alt="Size of png extantion." size="40%" >}} 

And I think it was there I made an error. I most likely put a big number for the counter around the path and that made the path itself very thin. Next time I’ll be more careful with this parameter and I’ll make sure it will be thick enough. But for now, since I don’t have to much time and the purpose its to learn I will improvise. 

I ask the professor what could I do instead of mill another board and he suggested connecting jumper cables. And that’s what I did.

I first choose the cables from the white box cause they are more flexible and less hard but they are also harder to solder so I just use those for the first wire connection and then I change for the wires on the black box

{{<image src="../images/13week/white_box.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/black_box.jpg" alt="Size of png extantion." size="40%" >}} 

Then once again the procedure is the same. First apply flux on both the wire and the tabs. Then apply a bit of solder in both the tabs and the wire

{{<image src="../images/13week/tabs_wire_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_2.jpg" alt="Size of png extantion." size="40%" >}} 

Then apply flux again on top of the connection and melt the wire on the tab until both get connected. In my opinion the thinner wire (the toxic one) makes this procedure way easier.

{{<image src="../images/13week/tabs_wire_2.jpg" alt="Size of png extantion." size="40%" >}} 

I did the exact same procedure for the rest of the cables and I measure the connections with the multimeter.

{{<image src="../images/13week/tabs_wire_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_4.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_5.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_6.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_7.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/tabs_wire_8.jpg" alt="Size of png extantion." size="40%" >}} 

After all the connections made I measure the connections with the multimeter between the XIAO board and the accelerometer headers and I realised I hade made one of the connections wrong. Important note of this is that it’s always good practice constantly checking if what we are doing is correct cause it will facilitate debugging in case something is wrong.

{{<image src="../images/13week/tabs_wire_9.jpg" alt="Size of png extantion." size="40%" >}} 

I measure again and now it’s working properly.

{{<image src="../images/13week/working_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/working_2.jpg" alt="Size of png extantion." size="40%" >}} 

This solution end up being very nice cause as you can see with the battery case we can't even see the jumper wires on the back of the board

{{<image src="../images/13week/no_see.jpg" alt="Size of png extantion." size="40%" >}} 

Now, I just miss connection the battery case on the board. 

I follow the same procedure as the professor. I cut from an existing battery case the metal parts (the plus and minus) and I solder some wires. I did the same procedure as I’ve been discribing so far.

{{<image src="../images/13week/wires_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/wires_2.jpg" alt="Size of png extantion." size="40%" >}} 

I then insert them in the case and I push the wires through the hole I made on the case.

Next I insert the lithium battery on the case and I realize that the plus side is a bit loose and sometimes the battery wouldn’t touch the metal and those no voltage would be produced. Once again I didn’t want to change the design and 3D print again since im limited on time so I just decided to put my engineering hat and be smart to fix this issue. I end up putting some leftover rubber I had from cutting the jumper cables  between the wall of the case and the plus metal piece as you can see on the images bellow. Was a very easy and stable solution. It’s barely noticeable . I’m proud ehehe

{{<image src="../images/13week/desemerdar_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/desemerdar_2.jpg" alt="Size of png extantion." size="40%" >}} 

Now works perfect. As we say in Portugal “desemerdar”.

{{<image src="../images/13week/desemerdar_2.jpg" alt="Size of png extantion." size="40%" >}} 

Finally,  I connect the battery cables on the terminal. 

{{<image src="../images/13week/cables_in_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/cables_in_2.jpg" alt="Size of png extantion." size="40%" >}} 

Following is video showing the battery working. Uhuhhuh !!!

{{<video src="../images/13week/batery_working_v.mp4">}}{{</video>}}

Next I just miss make a connection between the circuit board and the plus/minus battery input on the XIAO board. 

It’s here where I discover once again another problem I have to overcome. This problem goes along with the same problem as I had before with the connections with the accelerometer headers. Basically the paths were too thin and the connections weren’t working. I made a video showing what was wrong. 

{{<video src="../images/13week/no_work_cables.mp4">}}{{</video>}}

As you can see on the video there was no connection going to the battery_vin. I then decided to fix it with jumper cables again. I better learn from this lesson and make thicker paths next time.

To had the jumper cables I did the same procedure as always but I must say this one was challenging. Thank good I’ve done enough soldering to have the required skills.

{{<image src="../images/13week/bat_cab_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/bat_cab_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/bat_cab_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/bat_cab_4.jpg" alt="Size of png extantion." size="40%" >}} 

As you can see I also measure if everything was well connected and it was. Following is an images showing the final setup! I’m proud! Looks beautiful !!!

{{<image src="../images/13week/beaty.jpg" alt="Size of png extantion." size="40%" >}} 

Now I just miss upload the code and see the magic happening.

#### Uploading the code to the XIAO board

I made some modifications to the “tapdemo” example code where I had the LEDs as outputs and I made it as every time we make a single tap the red LED would light up and every time we would make a double tap the green LED would light up. After making this work we can make everything we want. We can make the LEDs light up on a single tap and turn off on a double tap for example. 

Here’s the code I made and following is the video of it working.

{{< highlight go-html-template >}}
// Basic demo for tap/doubletap readings from Adafruit MSA301

#include <Adafruit_MSA301.h>

Adafruit_MSA311 msa;

int green = D2;
int red = D3;

void setup() {
  Serial.begin(115200);

  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  digitalWrite(red,LOW);
  digitalWrite(green,LOW);

  while (!Serial) { delay(10); }

  // Try to initialize!
  if (! msa.begin()) {
    Serial.println("Failed to find MSA311 chip");
    while (1) { delay(10); }
  }
  Serial.println("Found MSA311!");

  msa.setPowerMode(MSA301_NORMALMODE);
  msa.setDataRate(MSA301_DATARATE_1000_HZ);
  msa.setBandwidth(MSA301_BANDWIDTH_500_HZ);
  msa.setRange(MSA301_RANGE_2_G);
  msa.setResolution(MSA301_RESOLUTION_14 );

  msa.setClick(false, false, MSA301_TAPDUR_250_MS, 25);
  msa.enableInterrupts(true, true);  // enable single/double tap
}

void loop() {

  uint8_t motionstat = msa.getMotionInterruptStatus();
  if (motionstat) {
    Serial.print("Tap detected (0x"); Serial.print(motionstat, HEX); Serial.println(")");
    if (motionstat & (1<<5)) {
      Serial.println("\t***Single tap");
        digitalWrite(red,HIGH);
        digitalWrite(green,LOW);
        delay(1000);
    }
    if (motionstat & (1<<4)) {
      Serial.println("\t***Double tap");
      digitalWrite(red,LOW);
      digitalWrite(green,HIGH);
      delay(1000);

    }
    Serial.println("");
  }
  delay(10);
  digitalWrite(red,LOW);
  digitalWrite(green,LOW);
}
{{< /highlight >}}

And the video :

{{<video src="../images/13week/tap_v.mp4">}} tapdemo example with LEDs.{{</video>}}

Then I got excited and I decided to try more of the “Adafruit_MSA301” library examples such as the “plotdemo”.

I fist ran the code normally to understand what to expect and I realize that when I do the movement up and down the z-axis varies between 6 and 10 do I decided to program the board to led RED if its above 10 or GREEN otherwise. As you can see from the video the coding is not the best since sometimes it seams that both RED and GREEN are on. I did it in a hurry and If I would change I would do something like: LED red when above 9 or LED green when bellow 7 for example. Or insert some delay like delay(10) to make sure we are outputting the values we want after moving the accelerometer.

Here is the code following by the video of it working.

{{< highlight go-html-template >}}
// Basic demo for plotting accelerometer readings from Adafruit MSA301

#include <Wire.h>
#include <Adafruit_MSA301.h>
#include <Adafruit_Sensor.h>

Adafruit_MSA311 msa;

int green = D2;
int red = D3;

void setup(void) {
  Serial.begin(115200);

  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  digitalWrite(red,LOW);
  digitalWrite(green,LOW);

  while (!Serial) delay(10);     // will pause Zero, Leonardo, etc until serial console opens

  Serial.println("Adafruit MSA311 test!");
  
  // Try to initialize!
  if (! msa.begin()) {
    Serial.println("Failed to find MSA311 chip");
    while (1) { delay(10); }
  }
  Serial.println("MSA311 Found!");

}

void loop() {

  /* Get a new sensor event, normalized */ 
  sensors_event_t event; 
  msa.getEvent(&event);
  
  if (event.acceleration.z > 10){
    digitalWrite(red,HIGH);
    digitalWrite(green,LOW);
  } else {
    digitalWrite(red,LOW);
    digitalWrite(green,HIGH);
  }

  /* Display the results (acceleration is measured in m/s^2), with commas in between */
  Serial.print(event.acceleration.x);
  Serial.print(", "); Serial.print(event.acceleration.y); 
  Serial.print(", "); Serial.print(event.acceleration.z); 
  Serial.println();
 
  delay(10); 
}
{{< /highlight >}}

And the video :

{{<video src="../images/13week/plot_v.mp4">}} plotdemo example with LEDs.{{</video>}}

## Conclusion 

I really like this project. Even tho it took some time I feel like I learned a lot about design circuit boards and milling, 3D CAD design, solder, and overall overcome problems in a smart way. 

I got supper excited for having the code working cause I feel like right now that’s the biggest barrier I have since Arduino and XIAO board don’t seam to work with me. 


## Update

I redesign the battery box to fix the problems I mention above

{{<image src="../images/13week/add_tabs.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/extrude_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/13week/extrude_2.jpg" alt="Size of png extantion." size="40%" >}} 

