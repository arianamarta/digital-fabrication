---
date: 2023-03-29T10:58:08-04:00
description: ""
featured_image: "/images/out_device.jpg"
tags: []
title: "Week 11: Output Devices"
weight: 100
---

This week we add to incroporate an output device on our board. Like mention on the previous week i decided to go for the speaker since it will help me for my final project. 

When the professor started giving me the instructions i immediately regretted. It sounded so hard and it was so confusion everything I had to do.

Table of content:

* [Theory Explaination](#theory-explaination)
* [Design](#design)
* [Milling](#milling)
* [Connecting and Running the Program](#connecting-and-running-the-program)
* [Problems](#problems)

## Theory Explaination
I have to use the XIAO ESP32C3 board as the sound input on the speaker. We use this board since it has a pin with I2S function (I2S is a digital sound protocol that is used on circuit boards to pass audio data around. Not confuse with I2C which is used for small amounts of data)

But then I decided to just go through the documentation page for [XIAO ESP32C](https://wiki.seeedstudio.com/XIAO_ESP32C3_Getting_Started/) and [Adafruit MAX98357 I2S Amp](https://learn.adafruit.com/adafruit-max98357-i2s-class-d-mono-amp/overview). 

I like this board since it also supports wifi and bluetooth so I can use it to select the music I wanna hear.

{{<image src="../images/11week/xiao_board.jpg" alt="Size of png extantion." size="40%" >}} 

Then we use a digital audio amp breakout board MAX98357 I2S Amp. It takes standard I2S digital audio input and, not only decodes it into analog, but also amplifies it directly into a speaker.

{{<image src="../images/11week/amp.jpg" alt="Size of png extantion." size="40%" >}} 

This is a very powerful and incredibly efficient board. It’s perfect for portable and battery-powered projects. It has built in thermal and over-current protection but we can barely tell it gets hot.

The [general](https://www.adafruit.com/product/3006) page and the [Pinouts](https://learn.adafruit.com/adafruit-max98357-i2s-class-d-mono-amp/pinouts) page have really good explanation about this Amplifier. Following is the important information I took from reading them:

* The audio input is I2S standard and we can use 3.3V or 5V logic data. Three pins are used to receive audio data.

{{<image src="../images/11week/i2s_pins.jpg" alt="Size of png extantion." size="40%" >}}

* The outputs are "Bridge Tied" which means that they connect directly to the outputs and there’s no connection to ground. Summary, we can't connect the output into another amplifier, it should drive the speakers directly.

* It works as a single channel amplifier with twice the available power.

{{<image src="../images/11week/RL_channels.jpg" alt="Size of png extantion." size="40%" >}} 

* High frequencies are not heard.

* There's a Gain pin that can be manipulated to change the gain. By default, the amp will give you 9dB of gain. By connecting a pullup or pull down resistor, or wiring directly, the Gain pin can be set up to give 3dB, 6dB, 9dB, 12dB or 15dB.

{{<image src="../images/11week/gain.jpg" alt="Size of png extantion." size="40%" >}} 

* The ShutDown/Mode pin can be used to put the chip in shutdown or set up which I2S audio channel is piped to the speaker. By default, the amp will output (L+R)/2 stereo mix into mono out. By adding a resistor, you can change it to be just left or just right output. 

It works as follow:

* If we plan to use mono audio then we can leave the SD pin floating and simply send data on both Left and Right channels simultaneously.
* If we tie the pin to ground  the amplifier will be shut down.
* If we tie it to Vin the amplifier will play the left channel
* To play the right channel we have to use a pull-up resistor. An appropriate value for the resistor is 39 Kohm which should work on both 3.3V and 5V. 

{{<image src="../images/11week/SD_mode.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/SD.jpg" alt="Size of png extantion." size="40%" >}}
  
* We connect the speaker on the 3.5mm screw-terminal blocks accordingly to the input voltage and the desire output watts.

{{<image src="../images/11week/desire_IO.jpg" alt="Size of png extantion." size="40%" >}} 


Conclusions from what I read:

* About the gain. I asked the professor which one is the best one and he said that is the lowest one. I'm still trying to figure out why. Therefore, in order to achive the 3db of gain i inserted a 100k registor between GAIN and VIN.

* About the SD/mode I also have to decide what to do.
Depending on the voltage on the SD the amplifier can be either shut down or the output can be stereo average or just right or left channel. For now i decided to leave the SD pin floating which will give me a mono audio. I'm still in the process of understanding how does the right and left chanels actually work.

To controle the SD mode we can use pullup resistors on SD to balance out the 100K internal pulldown resistor.

For the breakout board, there's a 1Mohm resistor from SD to Vin which, when powering from 5V will give us the 'stereo average' output. If we want left or right channel only, or if we are powering from non-5V power, we may need to experiment with different resistors to get the desired voltage on SD.

## Design

In order to understand better how was i suppose to plug all the cables i appealed for help from our old friend youtube.

I found this useful YouTube [video](https://www.youtube.com/watch?v=At8PDQ3g7FQ). This video defiantly help me deciding which values to use for the gain and SD. Although I still don’t know the purpose of right and left channel.

Here’s how the cables should be connected:

{{<image src="../images/11week/conn_cables.jpg" alt="Size of png extantion." size="40%" >}} 

Next, I moved on to KiCad to design the board. I choose a simple design this time.

On the design I inserted an header where I will be connecting the amplifier. The amplifier has a total of 7 holes.

For this time I wanted to learn how to mill both sides of the board. So instead of using the rivets what I had to do was flip the board and the header pin to the back layer. Both the board and the headers are in blue whereas the inner connections and the register are in red (front layer). I will then connect the headers and the board from the back. Honestly the milling I’ll do on the back of the board will not influence in anything but will be good to understand the procedure for later purposes.

Following is the "Schematic Editor", "PCB Editor" and Gerber Viewer" images of this design.

{{<image src="../images/11week/S_editor.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/PCB_editor.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/GB_editor.jpg" alt="Size of png extantion." size="40%" >}} 

Next, Its time to export the Gerber files. The procedure is the same as I did on the previous week. But for this case I also had to tick Back layer and not just the Front Layer.

## Milling

After saving the files we go to CopperCam software to generate the path that the machine will be able to interpret.

The begging of the procedure is exactly the same as previous week but this time we also have to do the back layer.

I first started by uploading the front layer and I defined the outline and inline contours.

{{<image src="../images/11week/define_inside_contour_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/define_inside_contour_2.jpg" alt="Size of png extantion." size="40%" >}} 

This is how the board looks like

{{<image src="../images/11week/after_defining.jpg" alt="Size of png extantion." size="40%" >}} 

Then I added the drills and I adjust them to the respective places on the front layer

{{<image src="../images/11week/ref_pad_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/ref_pad_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/after_adjust_holes.jpg" alt="Size of png extantion." size="40%" >}} 

Afterwards I change the diameter like on the last week. I maintained the 0.8mm 

{{<image src="../images/11week/change_diameter.jpg" alt="Size of png extantion." size="40%" >}} 

And I also set the origin and the board dimensions as well as the tools. This time because I have more space I use 2mm of margin. Once again I used the caliper to measure the thickness of the board  and I had the value on the z-thickness. I used the same settings for the tools as last time.

{{<image src="../images/11week/origin.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/board_dimension.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/tools.jpg" alt="Size of png extantion." size="40%" >}}

Next it’s time to mill the board. I first start with 3 for the “Number of successive contours” and 0 for the “Extra contours around pads”. 

{{<image src="../images/11week/set_contour.jpg" alt="Size of png extantion." size="40%" >}}

I don’t know why but the engraving didn’t work to well this time. Even when changing the settings of the “Set contours” it didn’t make any difference

{{<image src="../images/11week/bad.jpg" alt="Size of png extantion." size="40%" >}}

I felt so sad cause I really wanted to engrave that text to bring more live to the board. I still need to figure out what was the problem and what can I do to fix it.
I decided then to erase the text and move on with the engraving. 

{{<image src="../images/11week/error_delete.jpg" alt="Size of png extantion." size="40%" >}}

Next, I saved the files. I saved the drills, the outline and the engraving of the front layer. 

After saving those files and naming them properly. I added the back layer to the circuit.

{{<image src="../images/11week/add_back_layer.jpg" alt="Size of png extantion." size="40%" >}}

Then I had to remove the outline and the inline of the back layer I just had because I will only need one outline since I will just cut the piece ones. Therefore I just need to focus on placing the engraving on the correct place.

{{<image src="../images/11week/delete_identical_contour.jpg" alt="Size of png extantion." size="40%" >}}

After placing the engraving in the correct place, setting the origin and the board dimensions I moved to do the milling

In this case its just engraving and its not that special so it was easy to create the paths

Here is how it looks both the back and front layer, respectively on the preview function.

{{<image src="../images/11week/back_L.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/11week/front_L.jpg" alt="Size of png extantion." size="40%" >}}

Finally, I now save just the engraving of the back layer. When saving the files for the back layer, we have to make sure we that select the proper layer (layer #2) , that we tick the mirror option and finally that we tick the “South-West corner” instead of the “white cross” as used previously. The reason why is that when we tick the mirror box we are also mirroring the white cross which means that when we will engrave the #1 and #2 layer will have different locations for the origin. I learned that by making the mistake ehehe.

{{<image src="../images/11week/mill_back.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/11week/mistake.jpg" alt="Size of png extantion." size="40%" >}} 

Then I moved to VPanel to cut the board.

I first did the engraving of the first Layer (Front Layer), then after changing the tool I cut the board. Then I used specific tools to remove the board from the bed in order to flip it and engrave the back side. I change the tool back again so I can proceed to the engraving of the second layer (Back Layer). 

{{<image src="../images/11week/cut_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/11week/cut_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/cut_3.jpg" alt="Size of png extantion." size="40%" >}}

The technique I used to engrave both sides was just by flipping the board and place it on the same place as before. Of course this is not the best procedure since its hard to actually place it where it was before and therefore there are other techniques we can do to make sure we place the board at the exact same place as before. 
For that we use the help of this small pieces where we insert then on the bed surface and they match on holes we make in our board. So then when we flip the board we can insert the board in those pieces that stayed in the same position on the bed.  

This are the pieces we can use:

{{<image src="../images/11week/rivets_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/rivets_2.jpg" alt="Size of png extantion." size="40%" >}} 

Also, as you can see from the image bellow, the first technique is not ideal because in my case it wasn’t correctly placed. I proceed with this same board because in my design it actually doesn’t matter the position. It’s just the engraving of the holes. The point of this was just to understand how to make to side boards. And the best way to learn it is by doing it. Trying and error.

{{<image src="../images/11week/bad_engraving.jpg" alt="Size of png extantion." size="40%" >}} 

I should have chosen a bigger value for the z-thickness because as you can see the inner holes weren’t completely cut through. Therefore I used this machine to make the holes myself. It’s not as precise as the machine of course but it’s just random inner holes where it doesn’t matter the size of them.

{{<image src="../images/11week/machine.jpg" alt="Size of png extantion." size="40%" >}} 

Finally, I moved on to solder the female headers where I will be inserting the XIAO board and the amplifier. I also had to solder the male hearders to the board itself and to the amplifier. 

{{<image src="../images/11week/solder_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/header_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/header_2.jpg" alt="Size of png extantion." size="40%" >}} 

When soldering the female header for the amplifier I realised I made the holes very big compared to the diameter of the pins of the header. I manage to stuck the header with tape in order to facilitate the soldering. It was challenging but I must say after this im a master in soldering. I’m really proud the way it come out. It looks really beautiful and well done.

{{<image src="../images/11week/beatifull.jpg" alt="Size of png extantion." size="40%" >}}

Next I used the multimeter to test if all the connections were well made. Both between the XIAO board and the front layer but also between the inner connections from the board to the amplifier.

{{<image src="../images/11week/mul_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/mul_2.jpg" alt="Size of png extantion." size="40%" >}} 

After all the soldering was done I moved to connecting all the cables and to run the program.

## Connecting and Running the Program

I first started by adding ESP32 board package to my Arduino IDE following the steps provided on this [website](https://wiki.seeedstudio.com/XIAO_ESP32C3_Getting_Started/).

After finishing with all the setup I first started simple by testing a small program for the speaker.

There’s this library called “Tone” that we can use to create input data for the speaker.

I first tested an easy program where I just play the tone G and latter I move on to play a song.

I had some struggle understanding with pin I could use for the speaker. Most of the examples from the Internet that use ESP32 they use the big one and thus the number of the pins are not the same. I had to make some research and testing until I found one that worked.

Here are the videos of the first program and the respective code:

{{<image src="../images/11week/code_1.jpg" alt="Size of png extantion." size="60%" >}} 

{{<video src="../images/11week/code_1_v.mp4">}}{{</video>}}

Same for the second program:

We have to make an *.h file to define the notes:

{{<image src="../images/11week/define_notes.jpg" alt="Size of png extantion." size="40%" >}} 

Code itself:

{{<image src="../images/11week/notes.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/time.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/simple_tone_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<video src="../images/11week/code_2_v.mp4">}}{{</video>}}

Finally, I want to try using the big speaker and the amplifier since that was the ideal setup for the board that I designed . 

Once again it has been hard to find help from the internet. Only after several days doing a deep research on the topic is where I found more information. Once again, all the help I found uses the big ESP32 and therefore I have to discover which pins from the XIAO are equivalent so I can make the connection correct.

I found this [GitHub page](https://github.com/pschatzmann/arduino-audio-tools) that explains really well how everything works and he provides libraries to make the code simpler and smaller.

In my case, I will use an ESP32 as an “Audio source” and an analog output (amplifier) as “Audio Sinks”. 

{{<image src="../images/11week/explain.jpg" alt="Size of png extantion." size="40%" >}} 

I downloaded the zip file from the GitHub and I started by testing the “streams-generator-csv”. Which means I don’t have to have the speaker since it will output the values on the monitor. I did this first to see if it was working.

This is the picture of the plot. It has the sign wave that we created as input data which means its working.

{{<image src="../images/11week/sine_plot.jpg" alt="Size of png extantion." size="40%" >}} 

Next, I want to test the “streams-generator-analog” which is the example code when using an analog output.

{{<image src="../images/11week/name_example.jpg" alt="Size of png extantion." size="40%" >}} 

But first I have to make the wiring and for that I have to understand which pins of the board I can connect to the amplifier. 

I look up on the internet and one of the libraries "I2SStream.h" has option to change the default config pins. But still a bit unclear about which pin am i suppose to use for what. However later i found this example code under the "Examples for XIAO_ESP32C3" and "I2S" called "Simple Tone" where i found this other library "I2S.h" where we can also change the pins and it seams a good option for "output the data using the I2S interface to a MAX08357 I2S Amp Breakout board." whixh is what i wanna do. All in all, I will first test this last library i mentioned and see what does that lead me to.

{{<image src="../images/11week/example_i2s.jpg" alt="Size of png extantion." size="40%" >}} 

So I started by making the cables connection with the Xiao, amplifier and speaker. As speaker I used the small one I used just above and that was one of my errors. It won’t work when using that speaker, I have to use another one. I still don’t know the reason behind this problem. Both the speaker are 8ohm. Maybe it’s because the small one doesn’t need an amplifier?

{{<image src="../images/11week/speaker_ohm_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/speaker_ohm_2.jpg" alt="Size of png extantion." size="40%" >}} 

For the connections I tried to look up on the internet everything I could:

{{<image src="../images/11week/connections_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/connections_2.jpg" alt="Size of png extantion." size="40%" >}} 

As expected it didn’t work for three reasons.

* The pins were wrongly setup
* The speaker was the wrong one
* The library wasn’t up to date

For two of those problem the professor help me debug it cause honestly I dint knew where to look for answers anymore. 

I first tried changing the pins. I look up on the internet and this is what I found for both the I2S.h and I2SStream.h libraries.

{{<image src="../images/11week/pins_1.jpg" alt="Size of png extantion." size="50%" >}} 

{{<image src="../images/11week/pins_2.jpg" alt="Size of png extantion." size="40%" >}} 

Even changing the pins it didn’t work. Most likely because I still have the previous speaker.

At this stage I asked help from the professor and he made the “SimpleTone” example code work by replacing the speaker and by setting up the pins in a different way.

The way to change the pins numbers its by calling the functions.

{{<image src="../images/11week/set_pins.jpg" alt="Size of png extantion." size="50%" >}} 

{{<image src="../images/11week/change_pins.jpg" alt="Size of png extantion." size="40%" >}} 

This is how the code looks like

{{<image src="../images/11week/code_simple_1.jpg" alt="Size of png extantion." size="80%" >}} 

Later on, I tried the exact same code on another laptop and another error appear! It feels like electronics it’s just joking with me cause nothing seams to work.

{{<image src="../images/11week/error_pin_SD.jpg" alt="Size of png extantion." size="90%" >}} 

I tried to uninstall and install the libraries again and that wasn’t even working. It felt like a joke. The image bellow is the error related to the library but its not to visible

{{<image src="../images/11week/error_library.jpg" alt="Size of png extantion." size="60%" >}}

At some point I manage to make the library work but still I would compile and run the code with no error but nothing would happen. I appeal for help to the internet again and I found some solutions.

{{<image src="../images/11week/try_1.jpg" alt="Size of png extantion." size="60%" >}} 

{{<image src="../images/11week/try_2.jpg" alt="Size of png extantion." size="60%" >}} 

I end up making it work by pressing the reset button while it was compiling. I don’t know what I did and why it was working that way but at least I manage to make it work. It’s so annoying that this works like this. This time I knew the code was correct so I tried everything else but the code. However if I wasn’t sure about the code I would have wasted time trying to debug the code when the problem was actually just press the reset button.  That’s why my love for electronics its a rollercoaster between loving and hating it at the same time.

This is a video of the speaker working.
{{<video src="../images/11week/worked_1_v.mp4">}}{{</video>}}

Next I try to use the breadboard I printed.
For that I had to change the pins that the professor chose in the begging to mach with the pins I selected when I printed the board.

I also realised that my design wasn’t clearly thought since I couldn’t insert the amplifier directly to the board because of the power cable that goes to the XIAO board.
I could have just unsolder and solder curved headers but I just went for a quick easy fix.

{{<image src="../images/11week/pin_header_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/pin_header_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/pin_header_3.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/11week/pin_header_4.jpg" alt="Size of png extantion." size="40%" >}} 

And this is the program working:

{{<video src="../images/11week/worked_2_v.mp4">}}{{</video>}}

All in all, the wiring and the board are well done and working properly. 
In this simple code I create a square wave that will produce a single tone that we will be able to ear through the speaker.

## Problems

Since I started the electronics I’ve been going through several annoying and simple problems that made me wanting to give up.

1. First of them all was my Arduino software stopping to work out of the sudden. 

This was the error:

{{<image src="../images/11week/arduino_problem.jpg" alt="Size of png extantion." size="80%" >}} 

I don’t know what was the problem. Most likely was the version of Arduino. The only thing I know is that after a month I made it work some how. But this problem was definitely a reason to slow down my work as I was dependent to the lab and the opening hours of the lab.

2. Second I couldn’t find the correct number of the pins

I knew I had to change the default pins since im not working with the same board as on the example code. But it was so hard to find how to do it. I found some examples on the internet but they didn’t seam to work

3. Using the wrong speaker. Still don’t know the reason why

4. Library not working. I had to uninstall and install again and even that was giving me an hard time

5. Finally I made it work by restarting the XIAO board while compiling the code.

 



