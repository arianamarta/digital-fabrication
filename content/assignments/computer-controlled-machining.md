---
date: 2023-03-16T10:58:08-04:00
description: ""
featured_image: "/images/cnc.jpg"
tags: []
title: "Week 9: Computer-Controlled Machining"
weight: 80
---

This week we learned how to use the CNC machine.

We were asked to design a simple object to start using the machine. As always i like to go big and therefore i didn't go simple. 

For this project i decided to make a support for canvas. The most challenging part was finding the correct measurements. I based my design in existing ones, but because I want to practice the finger joints I decided to change the standard designs accordingly.

I first started by seeing the videos provided on MyCourses to understand how to use the machine. 

In those videos the professor used the VCarve software but because it’s not free I could only use it at the lab and therefore I just decided to use Fusion360. For that I followed some YouTube videos on how to use Fusion 360 with CNC milling.

Table of content:

* [Design](#design)
* [Prepare to CNC](#prepare-to-cnc)
* [Cut](#cut) 
* [Cut Final Design](#cut-final-design)
* [Final Touches](#final-touches)

## Design

For the design i inspired myself on the following model I found on the internet:

{{<image src="../images/9week/canvas1.jpg" alt="Size of png extantion." size="40%" >}} 

As for the measurements I followed this model:

 {{<image src="../images/9week/canvas2.jpg" alt="Size of png extantion." size="40%" >}} 
 
Next, I started by designing my object along with the measurements I think it makes sense. Those are the initial photos. Note that I draw on my desk. Its a funny habit I have ehehe

 {{<image src="../images/9week/1sketch1.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/9week/1sketch2.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/9week/1sketch3.jpg" alt="Size of png extantion." size="40%" >}}

Because I saw the tutorials before starting designing I knew I had to do the stock with the sizes of my wood sheet. Basically make a body of the same sizes as the sheet I will work with.

 {{<image src="../images/9week/stock.jpg" alt="Size of png extantion." size="40%" >}}

Next, I started by drawing the legs. Because I wasn’t sure yet how I wanted to make the finger joints I just did the outside shape.

 {{<image src="../images/9week/leg.jpg" alt="Size of png extantion." size="40%" >}}
 
Next I did the main body. Once again without the figure joints, just the overall shape.

 {{<image src="../images/9week/main_body.jpg" alt="Size of png extantion." size="40%" >}}

Afterwards I did the big piece to support the canvas.

 {{<image src="../images/9week/big_piece.jpg" alt="Size of png extantion." size="40%" >}}

Followed by the small piece.

 {{<image src="../images/9week/small_piece.jpg" alt="Size of png extantion." size="40%" >}}

Lastly I did the back piece. The one that supports the canvas itself.

 {{<image src="../images/9week/back_piece.jpg" alt="Size of png extantion." size="40%" >}}

Like already mentioned I’m not sure about the finger joints neither the connection for the back piece and therefore I made a new sketch and ask the professors opinion.

On the following picture I asked which kind of joint to use. If the number one or two. We decided to go with number two. 

Still in this same picture I asked about the overall measurements since I’m also not sure about the stability of the canvas support. Because the material is supper thin and the object will be very tall.

 {{<image src="../images/9week/canvas_opinion.jpg" alt="Size of png extantion." size="40%" >}}

Finally I asked about the back piece

 {{<image src="../images/9week/back_opinion.jpg" alt="Size of png extantion." size="40%" >}}

The professor said that it’s not good to have the screws because it might be hard to do it in this material. Also he said that this connection its very brittle. So he suggested to make a T profile for the support to be stronger. Along with using bought or 3D printed “off-the-shelf hinges”. 

 {{<image src="../images/9week/T_profile.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/9week/hinges.jpg" alt="Size of png extantion." size="40%" >}}

This is the design i come up for the legs and for the back support to make it stronger by using the T-profile.

{{<image src="../images/9week/leg_hinges.jpg" alt="Size of png extantion." size="40%" >}}

First idea for the back piece:

{{<image src="../images/9week/back_hinges_1.jpg" alt="Size of png extantion." size="40%" >}}

Second idea and the one i will do:

{{<image src="../images/9week/back_hinges_2.jpg" alt="Size of png extantion." size="40%" >}}

## Prepare to CNC

After making all the designs on Fusion360 this is how it looks like:

{{<image src="../images/9week/final_fusion1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/final_fusion2.jpg" alt="Size of png extantion." size="40%" >}}

I thought on preparing the tool path in Fusion360 but I talk with my colleagues and TA and they strongly recommended using VCarve since it’s easier and faster.

So what I did was export the .dfx file of my sketch and open it on VCarve. 

#### Prepare CNC machine

First I switch on the back switch and I press the green button to turn the machine on:

{{<image src="../images/9week/turnOn1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/turnOn2.jpg" alt="Size of png extantion." size="40%" >}}

Following are the four steps to insert the tool on the machine:

1. Grab the foam and place it underneath the machine to prevent the tool to brake or destroy the bed surface in case it falls down.

{{<image src="../images/9week/soft_material.jpg" alt="Size of png extantion." size="40%" >}}

2. Take the tool and the corresponding collet from the drawer

{{<image src="../images/9week/tool.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/tool1.jpg" alt="Size of png extantion." size="40%" >}}

3. Insert the collet on the machines collet and screw it on the machine. Insert the tool 2 cm deep and then make sure to tight it enough.

{{<image src="../images/9week/tight.jpg" alt="Size of png extantion." size="40%" >}}

4. Insert the vacum underneath the tool using a screwdriver. 

{{<image src="../images/9week/screwdriver1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/screwdriver2.jpg" alt="Size of png extantion." size="40%" >}}

Finally we move on to test the vacum of the bed surface and if it’s not strong enough we have to insert clamps. I’ve come twice to the lab for the CNC and the vacum was never strong enough and thus I always had to use the clamps. Following are the steps:

1. Take the robber pieces of the bed so the air can go through it

1. Insert MDF board between the bed and soft wood material.

{{<image src="../images/9week/material_underneath.jpg" alt="Size of png extantion." size="40%" >}}

2. Open the vacum values. Left image is when the values are closed and the right image is when they are open. If we just use the left side of the bed there’s no need to open the right side and vice versa.

{{<image src="../images/9week/vacum_valvues_off.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/vacum_valvues_on.jpg" alt="Size of png extantion." size="40%" >}}

Before turn on the vacum put some hear protections cause its very noisy. 

{{<image src="../images/9week/vacum_btn.jpg" alt="Size of png extantion." size="40%" >}}

3. Test if the material is well sucked by the vacum. We can look to the pressure needle and if it’s on the green zone it means it’s securely attached to the bed. In my case it was never in the green zone. I tried to push the material with my best strength and the material didn’t move. But because the needle is not on the green zone I decided to put clamps anyway to make sure.

{{<image src="../images/9week/pressure.jpg" alt="Size of png extantion." size="40%" >}}

At this stage we have inserted the tool and prepared the bed. 

Next, let’s zero the X,Y,Z-axis. 

To do that we we open “Mac3“ software and we use the arrow keys to move the machine to the sides. 
I choose to place the origin on the left bottom corner. Before “Zero X” and “Zero Y” I make sure I take in consideration the campls.

After zeroing x and y its time for the z. For that we us a specific tool:

{{<image src="../images/9week/zero_tool.jpg" alt="Size of png extantion." size="40%" >}}

We place it on the bed and we try to put the needle just on top of it but without touching it. After that we go to the computer and we press the only button in Finnish “Teran mittaus”.
The needle will go down very slowly by itself until it touches the surface of the tool. After that we go to the computer and we click “Zero Z”. Remove the tool from the bed back to its place.

{{<image src="../images/9week/z_tool.jpg" alt="Size of png extantion." size="40%" >}}

Finally, we just have to make the tool path in VCarve Pro and start cutting.

#### Make the tool paths in VCarve Pro

I first create a new project where I set 1200x1200x146mm size for the “Job Size”, I measure the thickness of my board and it was 146mm and not exactly 150mm. I zero the Z position on “Material Surface” and I choose the left bottom corner for the XY position.

Next I import my .dfx design I created in Fusion360. And I align the design on the stock bed.

Then I decided to first print a test piece to see if the settings are good. For the test piece I cut the small piece I have for the canvas. Because by cutting that I test the pocket and the joints. As already explained I won’t use dog bones since I don’t want to have them in my design. Not just because it doesn’t look good but also because I want really tight connections and for that I can just make the whole small and sand it afterwards. 

Now, let’s create the tool path for the outside and pocket of the test piece:

For the outside path:

1. Go to the up right corner and choose the outline tool. 

2. Choose the “Cut Depth” value. As mentioned above the thickness of my wood is 14.6 so the professor recommended to make the Cut Depth 2mm more. I decided to go for 14.8 but after cutting the piece I realised I should make it more deep and a change it later to 15.05 as shown in the image below.

{{<image src="../images/9week/outside_1.jpg" alt="Size of png extantion." size="40%" >}}

3. Now we choose the tool. The tool is under “End Mill” and it’s the 6mm one. When clicking on it a “tool info” window will appear. It’s here where we have to make some calculus for the value of the Feed Rate(FR) and Plunge Rate(PR). The Spindle Speed(SS) is 16000 r.p.m, that can be seen on the datasheet and we always maintain the same. For the FR we have to do FR = SS * Number Flutes * Chip Load 

For the Chip Load we have to go check the value on the paper hanging on the wall:

{{<image src="../images/9week/calculus_3.jpg" alt="Size of png extantion." size="40%" >}}

Then we do FR = 16000*2*0.06 = 1920

{{<image src="../images/9week/calculus_1.jpg" alt="Size of png extantion." size="40%" >}}

For the PR we do 1/4 of FR = 480

{{<image src="../images/9week/calculus_2.jpg" alt="Size of png extantion." size="40%" >}}

Click ok to save the changes.

4. Next I edit the passes by ticking “Edit Passes …” 

{{<image src="../images/9week/edit_passes.jpg" alt="Size of png extantion." size="40%" >}}

Insert one more in the begging and end of the material.

{{<image src="../images/9week/passes_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/passes_2.jpg" alt="Size of png extantion." size="40%" >}}

5. Next we edit the tabs. For that we have to make sure we select the outline we want to cut. 

{{<image src="../images/9week/tabs.jpg" alt="Size of png extantion." size="40%" >}}

6. Change the name so it’s easy to track.

7. Press “Calculate”.

A warning will happen because the cut depth is bigger than the thickness of the material. But we are aware of that so we click “Ok”.

Finally we press “Preview Selected Toolpaths” to confirm everything looks as it should.

For the pocket path:

1. In the same location as the outline tool its the pocket tool. Click on it.

2. For the “Cut Depth” value I choose 5.05 since I wanted jut 5mm depth. Later I realizes it was very little and I should have put more.

{{<image src="../images/9week/pocket_depth.jpg" alt="Size of png extantion." size="40%" >}}

3. The tool will be automatically updated with the previous one we used so no need to change that.

4. For the “Clear Pocket…” I leave as it is: “Raster” and “Climb”

5. Finally I change the name and hit “calculate”. Make sure to select the vectores you want to make as pocket otherwise there will be a warning for that.

Once again we can preview the toolpaths and see if everything goes as it should.

Finally time to save the toolpaths to start cutting.

To save the tool path we go to the same location as the icons for the outline and pocket tool and we choose the icon “Save Toolpath”.

In this case it’s fine if the machine cuts the pocket and the outline at the same time so we can “output all visible toolpaths to one file”. However latter on I decide to save the individual paths in different files. I choose to do like this because I have so little space left on the material and like this I first cut the pockets and only later the outlines.

## Cut

After saving, we move to a different software called “Mac3”. Now that everything was previously settle we just have to load the file we saved on VCarve and press the green Botton.

This is how it looked after cutting the test piece:

{{<image src="../images/9week/test_piece_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/more_engrave.jpg" alt="Size of png extantion." size="40%" >}}

As you can see by the image bellow I should have a more deep engraving. Around 7mm for example.

Then I remove the clamps and I sand the piece.

{{<image src="../images/9week/test_piece_2.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks. Looks good.

{{<image src="../images/9week/test_piece_8.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/test_piece_7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/test_piece_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/test_piece_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/test_piece_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/test_piece_3.jpg" alt="Size of png extantion." size="40%" >}}

After this test piece I realised three things. First put a Cut Depth bigger like I already explained above. Change from 14.8 to 15.05. Second, make the pocket depth a bit bigger also. Something around 7.05mm. Thirst, the pieces fit just right but because I really want a very strong connection between the pieces I decided to make the wholes smaller so then I can just sand it until the pice fits. For that I went back to Fusion360 and I adjusted the wholes to make them approximately 5mm smaller. 

#### Clean the machine 

After cutting we have to clean the machine and leave it as we found it. 

1. Before turn in of the vacum lefts first remove the clamps
2. Turn off the vacum 
3. Take the soft material and the support material of the bed. 
4. Remove the tool the same way as we insert it but now before place it in the correct place we shall clean it first.
5. Clean the collet and the tool using the air gun

{{<image src="../images/9week/clean_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/clean_2.jpg" alt="Size of png extantion." size="40%" >}}

6. Place the tool back again to its place as well as the collet.
7. Clean the bed surface with the air gun and close the the hole with the rubber.

## Cut Final Design

If we look back again to my design, we see that the main pice has a hole all the way from the top to the end of the piece. 

{{<image src="../images/9week/design.jpg" alt="Size of png extantion." size="40%" >}}

In the beginning I made the whole 6mm width which happens to be the same size as the tool and therefore I couldn’t make it to work. So what I decide to do was change the width of the hole to 8mm. Then I tried to cut the hole using the outline toolpath but it wasn’t working because of the tabs. Later on, Fiia suggested to treat the hole as a pocket and make the cutting depth 15.05. By doing that I was able to cut the hole. 

I also made a test piece to see if this strategy would work. And it did work so I end up going with this procedure.

{{<image src="../images/9week/test_hole.jpg" alt="Size of png extantion." size="40%" >}}

The day I cut my final design was a on another day than the first test piece and therefore I had to prepare the machine again. 

I had to insert the clamps again. So, when zeroing the X and Y-axis I mesure the x and y width of my material that was left by adding the clamps. 

I use those values for the size of my stock on VCarve. I then exported the .dfx file that contain my design. Because of the clamps the design didn’t fully fit on the material so I decided to leave away two pieces that I later cut in leftover wood pieces from colleagues.

I followed the same procedure as describe above to create the toolpath for outside and inside outline and for the pockets.

When cutting the holes inside I just made sure to tick the “inside option” under inside the “2D Profile Toolpath”

{{<image src="../images/9week/inside.jpg" alt="Size of png extantion." size="40%" >}}

Then I saved the files individual so I could start by just making the engraving following by cutting the inside holes to cutting the outside profile. 

As you can see by the image I started by making the holes and engraving before cutting the outline:

{{<image src="../images/9week/holes_first.jpg" alt="Size of png extantion." size="40%" >}}

I first started by engraving the big pocket in red but I thought was very small so I decided to make it deeper by changing the cutting depth from 5.05 to almost 10. Since the material is 14.6mm this was a clear mistake because now I feel like this piece is very fragile and in the verge of braking. This piece will be the one holding the canvas so I fill like at some point I will have to replace it or make it stronger some how. Because I learned with my mistake and because I split the jobs in different files, I went back to VCarve pro and I change the cut depth of the small piece (green) to around 7mm. 

{{<image src="../images/9week/hole_mistake.jpg" alt="Size of png extantion." size="40%" >}}

This is a photo almost at the end of the job. As you can see the material is pretty much all used.

{{<image src="../images/9week/interm.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looked at the end of cutting.

{{<image src="../images/9week/end.jpg" alt="Size of png extantion." size="40%" >}}

In the picture above there's still two pieces missing. Because i didn't have space i had to use other collegues leftover wood to cut my last them.

## Final Touches 

Now before assembling everything we still have to make some final touches.

We first have to detach the pieces from the wood by removing the tabs with the help of a sort of a spatula and an hammer. Then we can smooth out the remains of the clamps with this machine:

{{<image src="../images/9week/machine_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/machine_2.jpg" alt="Size of png extantion." size="40%" >}}

Then we sand the pieces 

{{<image src="../images/9week/sand_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sand_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sand_3.jpg" alt="Size of png extantion." size="40%" >}}

I still didnt sand the holes. I will do it after varnish the pieces. I want the connections to be as tight as possible so i would have not be able to remove the conection for varnish it if i would have join them just now.

Right now you can see how tight is the holes before sanding:

{{<image src="../images/9week/tight_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/tight_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/tight_3.jpg" alt="Size of png extantion." size="40%" >}}

There is a problem when using soft wood is that when we remove the tabs most of the times there’s big pieces of wood ripping off. I have lots of big hole on the pieces because of removing the tabs. Im planning to use spackle to cover those whole and hope it doesn’t look ugly.

{{<image src="../images/9week/problem.jpg" alt="Size of png extantion." size="40%" >}}

I talk with the professor and he recommended me to not use the spackle as it will look ugly. I decided then to move on to the varnish and later i decode if i try to cover the holes some how depending how weak i feel that the piece is.

For varnishing I will do it twice. I will first apply a layer, then sand it and then apply the second layer. 

I first sand it and clean the dust that stays with a wet paper. Honestly i dont think it worked. I'll have to ask the professor how am i supposed to do afterall.

{{<image src="../images/9week/paper.jpg" alt="Size of png extantion." size="40%" >}}

Then i used "wood oil for outdors" since it was the one my collegues used. 

{{<image src="../images/9week/oil.jpg" alt="Size of png extantion." size="40%" >}}

I brush it in all sides and i let it dry.

{{<image src="../images/9week/first_dry_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/first_dry_2.jpg" alt="Size of png extantion." size="40%" >}}

After its dried, I sand it again and clean the dust so then i can start brushing the second layer. 

{{<image src="../images/9week/sand_again.jpg" alt="Size of png extantion." size="40%" >}}

After brushing it all i let it dry one more time.

{{<image src="../images/9week/second_dry_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/second_dry_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/second_dry_3.jpg" alt="Size of png extantion." size="40%" >}}

Now its time to assembly the canvas support. I will have to do some sanding to fit the pieces perfectly on the holes or i will just use the hammer. I really want the connection to be supper tight and thus im afraid to sand it to much.

Finally, I will buy this two things for the back support and for the small and big pieces that will support the canvas. I will buy 8mm screw. Later i found out we also have this screws at the lab so i just bought for nothing.

{{<image src="../images/9week/buy_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/buy_2.jpg" alt="Size of png extantion." size="40%" >}}

This is how the assembly of the canvas looks like in some of the stages:





First I started by joining the back leg pieces. There was some sanding needed like all the other pieces:

{{<image src="../images/9week/3leg_0.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/3leg_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/3leg_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/3leg_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/3leg_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/3leg_5.jpg" alt="Size of png extantion." size="40%" >}}

Than I did the front legs:

This is how I want them to be connected:

{{<image src="../images/9week/connection.jpg" alt="Size of png extantion." size="40%" >}}

After sending the pieces I try to connected them very tightly and therefore I use a wood hammer to attach them really well together. 

{{<image src="../images/9week/legs_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/legs_8.jpg" alt="Size of png extantion." size="40%" >}}

Then I made the holes on the small and big pieces that will hold the canvas:

I first started by measuring the position of the holes. Then I drill them and I confirmed that the screw can fit in the hole. {{<image src="../images/9week/holes_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/holes_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/holes_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/holes_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/holes_5.jpg" alt="Size of png extantion." size="40%" >}}

I also confirmed if the screw can fit on the big pocket that’s on the main body of the canvas.

{{<image src="../images/9week/holes_6.jpg" alt="Size of png extantion." size="40%" >}}

After drilling the holes on those pieces I can finally combine them.

I started with the small piece:

{{<image src="../images/9week/small_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/small_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/small_3.jpg" alt="Size of png extantion." size="40%" >}}

Then I moved on to the big piece and my biggest fear just happened which was sending the pieces to much so than the connections were to lose. Since this is the big piece which is the one that supports the canvas and all the weight I really had to fix this problem because the connection can be loose otherwise the canvas falls.

{{<video src="../images/9week/lose.mp4">}} Connection too lose.{{</video>}}

So, I talked with a course colleague for some opinion or recommendation for how could I fix this problem instead of mill a new piece again. 

He recommended to add some wood sticks and attach them with glue. This was actually perfect because now with the sticks and glue im sure that the connection won’t get loose with time. 

To insert the wood sticks I first had to measure their location on the pieces and make sure to drill them vertically with no bad angle. Well, guess what, I manage to not drill with the perfect angles so the pieces weren’t that well connected and therefore I had to open the hole a bit more. At the end everything fit perfect and now I have a very strong canvas support. 

I also sanded a bit the partes I was going to glue to make sure the glue sticks perfectly and efficiently.

{{<image src="../images/9week/big_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_8.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_9.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_10.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/big_11.jpg" alt="Size of png extantion." size="40%" >}}

Next I just miss sanding the attachment on the back of the canvas for the back leg.

{{<image src="../images/9week/attach_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/attach_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/sanding_6.jpg" alt="Size of png extantion." size="40%" >}}

Then i attach the hinges. The screws that came with it were too big so i had to cut them with a metal axe.

{{<image src="../images/9week/axe_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/axe_7.jpg" alt="Size of png extantion." size="40%" >}}

Next i use a 2mm tool to make a very thin hole on the wood. It will make a path and it will be easier for me later.

{{<image src="../images/9week/path_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/path_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/path_3.jpg" alt="Size of png extantion." size="40%" >}}

Then i use the pointy screw first to help open the hole a bit more:

{{<image src="../images/9week/path_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/path_5.jpg" alt="Size of png extantion." size="40%" >}}

Then i use the cut screwer and i insert it to see if it worked. It does work and the attachment is very strong and tiny which is super nice and what i wanted.

{{<image src="../images/9week/path_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/path_7.jpg" alt="Size of png extantion." size="40%" >}}

I repeat the same procedure for the leg and i also insert the hinge.

{{<image src="../images/9week/hinge_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/hinge_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/hinge_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/hinge_4.jpg" alt="Size of png extantion." size="40%" >}}

There is just a problem with the hinge. It's not the idea one cause the hinge itself its very lose so it gives a sense of instablility. However later on i tested the canvas and when its standing its not unstable at all. But here's a video about what i mean.

{{<video src="../images/9week/hinge_v.mp4">}}{{</video>}}

Finally this is how the canvas looks like after everything is combined:

{{<image src="../images/9week/final_all_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/final_all_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/9week/final_all_3.jpg" alt="Size of png extantion." size="40%" >}}

And this is a video showing how easy it is to use the wing nuts to move up and down the small and big pieces we will use to hold the canvas.

{{<video src="../images/9week/final_all_v.mp4">}}{{</video>}}







