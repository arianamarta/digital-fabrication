---
date: 2023-04-27T10:58:08-04:00
description: ""
featured_image: "/images/moldes.jpg"
tags: []
title: "Week 14: Molding and Casting"
weight: 130
---


This week we learn about molding and casting. 

We were encouraged to make a simple design so we could just focus on learning the actual important steps.

For this week I just follow the video provided by the professor for learning how to use the VCarve for 3D objects and I also followed the video of the lecture to learn how to make the liquids for the molde and final piece.

Table of content:

* [Design](#design)
* [Vcarve for fome](#vcarve-for-fome)
* [VPanel for fome](#vpanel-for-fome)
* [Mixing and Making](#mixing-and-making)

## Design 

I first started by making an easy design. I had two ideas. A ring and a piece to use on wine bottles.

{{<image src="../images/14week/object1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/object2_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/object2_2.jpg" alt="Size of png extantion." size="40%" >}} 

But I didn’t really felt the project so I change the idea and I decided to do a guitar that later I can use as a neckless and give to my brother since he’s a musician. 

So I started by going to [thingiverse](https://www.thingiverse.com/search?q=&page=1&type=things&sort=popular) and [printables](https://www.printables.com) and find a nice design for a guitar. I liked this one:

{{<image src="../images/14week/guitar_stl.jpg" alt="Size of png extantion." size="60%" >}} 

Then if I import the *.stl file to fusion360 I will have lots of problems with the meshing so I just used [tinkercad](https://www.tinkercad.com/dashboard) to make the master molde for the guitar. It was a suggestion from a classmate and honestly a time saver idea.

This program is meant for kids and thus very easy to use.

To create the master molde I just had to make 4 thick walls around the guitar and a very thin layer under the guitar as you can see from the images below:

{{<image src="../images/14week/set_up_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/set_up_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/set_up_3.jpg" alt="Size of png extantion." size="40%" >}} 

Then I group all those pieces together to make it as a whole body.

{{<image src="../images/14week/set_up_4.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/set_up_5.jpg" alt="Size of png extantion." size="40%" >}} 

Then I went to prusaSlicer to see the exact measurements of the master molde I created so I could cut the fome and the wax having those measurements into consideration.

{{<image src="../images/14week/set_up_5.jpg" alt="Size of png extantion." size="40%" >}} 


## Vcarve for fome

I first started with fome. I cut it with the size of my master molde 

{{<image src="../images/14week/cut_fome.jpg" alt="Size of png extantion." size="40%" >}} 

Then I put tape on the back of the fome to attach it on the bed of the machine MDX-40

{{<image src="../images/14week/cut_fome.jpg" alt="Size of png extantion." size="40%" >}} 

Then, because the fome is not completely flat I decided to first flat the up surface using the butterfly tool like suggested in the video.

For that I created a new project on VCarve with the size of the fome I cut.

{{<image src="../images/14week/fome_size.jpg" alt="Size of png extantion." size="40%" >}} 

Then I make a rectangle with the size of the stock 

{{<image src="../images/14week/square.jpg" alt="Size of png extantion." size="60%" >}} 

Now I want to make a pocket of that rectangle with a depth of 10mm so I can flatten the up surface.

{{<image src="../images/14week/poket_setUp.jpg" alt="Size of png extantion." size="40%" >}} 

For the tool I will use the butterfly tool since it’s faster and better for this job.

{{<image src="../images/14week/butterFly_tool.jpg" alt="Size of png extantion." size="40%" >}} 

As for the settings on the picture bellow I forgot to change the Pass Depth before taking the picture but I did change it from 1 to 2. Since im working with fome I can use 2.0 but when it comes to wax I have to use 1.0

{{<image src="../images/14week/22mm_tool.jpg" alt="Size of png extantion." size="60%" >}} 

I also had to make the calculations for the FeedRate and Plunge Rate.

The calculations are done exactly the same as with the CNC machine. So for the FR = number of flutes * Spindle Speed * Chip Load and for the PR = FR/4

For the Chip Load we have to find it on the sheet. For the wax we use the same value as the Steel. The professor also said that we can use the Steel value for the fome also. I would say that we can do that since fome is so light and softer compared to the steel that will be more than enough.

Since we have 22m we can use the double of 12mm for the calculus:

{{<image src="../images/14week/cutting_22m.jpg" alt="Size of png extantion." size="60%" >}} 

So the calculus is FR= 2*14000*(2*0.08) = 4480. 

It’s a good practice to confirm the values the FeedRate can handle and the max value is 50mm/sec which is 3000mm and therefore 4480 is bigger than the limit. So I decided to go with 2500 for the FR value as suggested by the professor. Finally the PR = FR/4 = 625.

Then I save the work and I preview the toolpath. As you can see from the image bellow, the toolpath is giving a bit of material on the corners so I decide to change the settings and had Pocket allowance with half the size of the diameter of the tool (22mm/2=11mm)

{{<image src="../images/14week/preview_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/poket_allowence.jpg" alt="Size of png extantion." size="40%" >}} 

And now the toolpath looks much better. I save the file and I move to prepare the machine to cut.

{{<image src="../images/14week/preview_2.jpg" alt="Size of png extantion." size="40%" >}} 

## VPanel for fome

As I mention above I will use the butterfly tool that fits a 6mm collet. There was two 6mm collets so I had to test which on fits the tool.

{{<image src="../images/14week/two_collets.jpg" alt="Size of png extantion." size="40%" >}} 

On the first image you can see it doesn’t enter the collet whereas on the other image the tool enters the collet and that will be the one I will use.
 
{{<image src="../images/14week/no_work.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/work.jpg" alt="Size of png extantion." size="40%" >}} 

Now I will insert the collet on the machine and tight the butterfly tool with the help of two wrench. 

{{<image src="../images/14week/collet_6mm.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/tight.jpg" alt="Size of png extantion." size="40%" >}} 

Next I will open VPanel software and I will make sure I setup everything correctly.

{{<image src="../images/14week/vpanel.jpg" alt="Size of png extantion." size="40%" >}} 

First I click “SetUp" bottom and I tick “NC Code" option.

{{<image src="../images/14week/setup.jpg" alt="Size of png extantion." size="40%" >}} 

Then I have to make sure both this options are on G54

{{<image src="../images/14week/g54_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/14week/g54_2.jpg" alt="Size of png extantion." size="40%" >}} 

Next, we have to zero the X,Y,Z-axis. 

With the help of the arrows I put the tool on the southwest most corner of my piece of fome:

{{<image src="../images/14week/XY_position_1.jpg" alt="Size of png extantion." size="40%" >}} 

Then we set XY origin and we click apply:

{{<image src="../images/14week/XY_position_2.jpg" alt="Size of png extantion." size="40%" >}}

Now for the Z axis we have to use a sensor to help zero the Z axis. The exact same thing as with the CNC machine.

{{<image src="../images/14week/Z_bfly_1.jpg" alt="Size of png extantion." size="40%" >}}

We insert the sensor above the tool and then we click “set Z origin with sensor" and we click detect

{{<image src="../images/14week/Z_bfly_2.jpg" alt="Size of png extantion." size="40%" >}}

After setting up everything there’s still one more thing left to do which is reduce the cutting speed all the way down to 50% when it’s fome and to 10% when we work with wax. The good thing about this is that we can increment the speed even when the job is working. The purpose of this is to make sure we don’t brake the tool or the belt that is rotating the tool. So it’s better to start slow and keep increasing along the way.

{{<image src="../images/14week/cutting_speed.jpg" alt="Size of png extantion." size="40%" >}}

Finally we press cut and we choose the job we want to do.

{{<image src="../images/14week/pocket_job.jpg" alt="Size of png extantion." size="60%" >}}

We press open and then output to start cutting

{{<image src="../images/14week/output_job.jpg" alt="Size of png extantion." size="40%" >}}

As you can see on the video bellow I inserted the measurements of the X and Y sizes wrong. Basically I mixed them with each other. So basically what happen is that the tool didn’t cut all the way through the x-axis and it went longer than the end of the fome on the y-axis. 

{{<video src="../images/14week/bfl_fome.mp4">}}{{</video>}}

This is how it looks after cutting. On this image it’s easier to understand what I meant above.

{{<image src="../images/14week/end_bfl.jpg" alt="Size of png extantion." size="40%" >}}

Then I just used an X-ato to cut that missing part:

{{<image src="../images/14week/x_ato.jpg" alt="Size of png extantion." size="40%" >}}

And finally I measured again the remaining piece to make sure I insert the correct values this time

{{<image src="../images/14week/measure_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/measure_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/measure_3.jpg" alt="Size of png extantion." size="40%" >}}

Now, back again to VCarve to create the tool path for the guitar.

#### VCarve for guitar in fome

I create once again a new project where I finally insert the correct measurements.

{{<image src="../images/14week/fome_size_correct.jpg" alt="Size of png extantion." size="40%" >}}

Then I import the *.stl file by going to “File" -> “import…" -> “Import Component / 3D Model…" 

{{<image src="../images/14week/import.jpg" alt="Size of png extantion." size="40%" >}}

The object wasn’t center so I just tick “Center Model"

{{<image src="../images/14week/object_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/center_model.jpg" alt="Size of png extantion." size="40%" >}}

Then I position the Zero plane just a bit above the end of my module and that was an error I realised later. But I will talk about it later.

{{<image src="../images/14week/zero_plane_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/zero_plane_2.jpg" alt="Size of png extantion." size="40%" >}}

I also made sure the Model Sizes were correct and then I hit “ok"

{{<image src="../images/14week/ok.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/after_ok.jpg" alt="Size of png extantion." size="40%" >}}

Next, I do the material Setup 

{{<image src="../images/14week/material_setup.jpg" alt="Size of png extantion." size="40%" >}}

Where we will keep the default thickness which will be the one we inserted in the beginning.

Then for the “Model Position in Material" I decided to let zero gap between the module and material. I tough on leaving some gap to fix some irregularities as shown on the image bellow. 

{{<image src="../images/14week/irregular.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/tough_1.jpg" alt="Size of png extantion." size="40%" >}}

But then I realised that this irregularity wouldn’t influence the final result and since I’ve done the first step for flat the surface theres no need for gap.

{{<image src="../images/14week/tough_2.jpg" alt="Size of png extantion." size="40%" >}}

Then, I start by creating the rough toolpath.

{{<image src="../images/14week/rough_icon.jpg" alt="Size of png extantion." size="40%" >}}

Where I choose for the milling tool the 6mm end mill (flat) tool and I had to make the calculations again.

Once again, I forgot to change the pass depth from 1 to 2 since im using fome and not wax before taking the picture. I did change it before cutting. 

I also had to make the calculation again for the Feed Rate and Plunge Rate. The math are exactly the same but this time I had to change the Chip Load to the 6mm tool.

{{<image src="../images/14week/cutting_6mm.jpg" alt="Size of png extantion." size="40%" >}}

Those are the calculus for the FR and PR respectively:

{{<image src="../images/14week/FR_6mm.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/PR_6mm.jpg" alt="Size of png extantion." size="40%" >}}

Then accordingly to the professor we have to choose the “Model Boundary" for the “Machining Limit Boundary" option. So I did that and this was the preview:

{{<image src="../images/14week/bad_preview.jpg" alt="Size of png extantion." size="40%" >}}

As you can see it’s cutting the guitar and not around it and that’s not the expected outcome. It was strange this happened and I start realising I made something wrong so went back again and I just tried to change things until I would have what I wanted. I end up changing the “Machining Limit Boundary" option and I tick “Material Boundary" . 

{{<image src="../images/14week/material_boundery.jpg" alt="Size of png extantion." size="40%" >}}

When doing that, the preview looked better but still a bit strange and not what I wanted. 

{{<image src="../images/14week/good_preview.jpg" alt="Size of png extantion." size="40%" >}}

The cutting took longer since it was cutting around the material and not just the pocket where the guitar is. I knew something was off but I didn’t have time to ask for it so I just did like this even if it would take longer. I later realize the problem when I did it for wax and I will explain it better later.

After that I just miss doing the smooth toolpath.

{{<image src="../images/14week/smooth_icon.jpg" alt="Size of png extantion." size="40%" >}}

It’s the same procedure. I first choose the tool. This time I will use the 3mm ball nose tool and once again I will put 2.0 for the pass depth and for the calculus I will look on the sheet the value for the 3mm tool.

{{<image src="../images/14week/cutting_3mm.jpg" alt="Size of png extantion." size="40%" >}}

The values for the FR and PR are shown in the images bellow respectively:

{{<image src="../images/14week/FR_3mm.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/PR_3mm.jpg" alt="Size of png extantion." size="40%" >}}

Then for the “Machining Limit Boundary" option and I tick “Material Boundary" again as I did before.

This is how the preview looks like.

{{<image src="../images/14week/smooth_preview.jpg" alt="Size of png extantion." size="40%" >}}

Now that I’ve created all the toolpath I will just saved them so I can cut them in the machine using the VPanel.

{{<image src="../images/14week/save_toolpath.jpg" alt="Size of png extantion." size="40%" >}}

#### VPanel for guitar in fome

First I removed the butterfly tool and I replace it with the 6mm flat tool. The collet stays the same since it’s a 6mm collet.

For the tool I used the smaller one as I didn’t see any problem on using it. Also I tried the bigger one and I couldn’t calculate the z-axis with the sensor since the tool was very big.

{{<image src="../images/14week/6mm.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/insert_tool.jpg" alt="Size of png extantion." size="40%" >}}

Then I zero the z-axis again (we maintain the x,y-axis) using the sensor

{{<image src="../images/14week/z-axis_6mm.jpg" alt="Size of png extantion." size="40%" >}}

Then we make sure to start with a 50% Cutting Speed before start cutting. Following we choose the job, first we do the rough path with 6mm and then we change tool to 3mm to do the smooth path.

{{<image src="../images/14week/open_toolpath.jpg" alt="Size of png extantion." size="40%" >}}

We output the job and the machine starts cutting. While was cutting I increase the speed up to 100% as I could hear and see everything was working just fine.

Here is some videos when cutting. You can hear the familiar sound that tells you everything is going okay.

{{<video src="../images/14week/fome_1.mp4">}}{{</video>}}

{{<video src="../images/14week/fome_2.mp4">}}{{</video>}}

{{<image src="../images/14week/final_6mm.jpg" alt="Size of png extantion." size="40%" >}}

Finally, we just miss doing the smooth path. 

For that we have to change the tool from 6mm to 3mm. That also implies changing the collet to a 3mm one.

{{<image src="../images/14week/3mm_collet_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/3mm_collet_1.jpg" alt="Size of png extantion." size="40%" >}}

Then I insert the 3mm tool. This time I decided to go with the big one just to see the difference and take conclusions.

{{<image src="../images/14week/3mm.jpg" alt="Size of png extantion." size="40%" >}}

Once again I had to zero the z-axis.

{{<image src="../images/14week/z-axis_3mm.jpg" alt="Size of png extantion." size="40%" >}}

And decrease the Cutting Speed up to 50% and change the job to the smooth toolpath file.

This is the machine cutting. As you can see the toolpath went also to the sides instead to just the pocket where the object is. I later found out the solution and the reason why this happened. But for now was enough.

{{<image src="../images/14week/3mm_working.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks like after this final job.

{{<image src="../images/14week/3mm_final.jpg" alt="Size of png extantion." size="40%" >}}

Then I remove the piece from the bed and I clean the machine with the vacum cleaner.

{{<image src="../images/14week/remove_fome.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/clean_fome.jpg" alt="Size of png extantion." size="40%" >}}

This is how the final piece looks like in fome. Pretty cute and with reasonable detail.

{{<image src="../images/14week/final_fome.jpg" alt="Size of png extantion." size="40%" >}}


#### VCarve for guitar in wax

Now I finally move on to cut my piece in wax. 

I follow the same procedure as for the fome.

First I cut the wax since the last piece left was the big one and for my module I just needed half of it.

For cutting the piece I asked the professor is opinion how to do it and I said to use an hand saw. He end up doing it and I forgot to take pictures but basically in order to make sure the cut was 90degrees we used an extra piece next to the block so we could use it as a guideline for the cut.

{{<image src="../images/14week/piece_half.jpg" alt="Size of png extantion." size="40%" >}}

Then I measure the piece so then I can insert those values in VCarve. 

{{<image src="../images/14week/size_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/size_2.jpg" alt="Size of png extantion." size="40%" >}}

Next I create a new project in VCarve where I insert those measurements paying attention on the values I was inserting for the x and y axis to not make the same mistake again.

{{<image src="../images/14week/size_bed_wax.jpg" alt="Size of png extantion." size="40%" >}}

Then I imported again the *.stl file that has my module.

As for the setup of the material orientation I made some changes and it was when I discover the solution for the problem I’ve been talking about along this documentation.

First I center the model as it was off the material like it happened previously. And I also confirm the values for the Module Size.

Now, for the “Zero Plane Position in Module" I made some changes. So, instead of putting the plane just a bit above the floor I made bellow the guitar, I made this offset plane a bit bellow that floor. When doing this way, the object resulting is the whole object including guitar, four walls and the floor. But the way I did previously I was excluding the floor which made my module into two objects, the four walls and the guitar. That’s why later when I used the “Model Boundary" it didn’t work properly.

{{<image src="../images/14week/size_ok.jpg" alt="Size of png extantion." size="40%" >}}

On this next images you can see on the left when the plane is a bit above the guitar (wrong setup) and on the right when the plane goes a bit bellow the guitar (correct setup)

{{<image src="../images/14week/module_wax_no.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/module_wax_ok.jpg" alt="Size of png extantion." size="40%" >}}

Next we move to the material Setup where we do the same as for the fome.

Now I will create the rough toolpath. I will use the same 6mm tool as for the fome. 

{{<image src="../images/14week/6mm_tool_wax.jpg" alt="Size of png extantion." size="40%" >}}

And now when trying the “Model Boundary" on the “Machining Limit Boundary" it will work as expected as you can see on the second image

{{<image src="../images/14week/worked_1_wax.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/worked_2_wax.jpg" alt="Size of png extantion." size="40%" >}}

Finally I generate the smooth toolpath where I do the same as explained for the fome but using “Model Boundary" on the “Machining Limit Boundary". 

{{<image src="../images/14week/worked_3_wax.jpg" alt="Size of png extantion." size="40%" >}}

Then I save the toolpath and I also checked the time it would take to do the jobs. 

{{<image src="../images/14week/time_job_wax.jpg" alt="Size of png extantion." size="40%" >}}

#### VPanel for guitar in wax

After creating the toolpath its time to cut the wax.

I grab the piece I previously cut in half and I attach it on the bed using the double sided tape.

I will first start by cutting the rough part so I insert the 6mm collet and the 6mm tool ( I used the same tool and sizes as for the fome). 

Then I zero the X,Y,Z-axis.

For the calculations for the wax I will use the same as I use for the fome. Because when I calculated for fome I used the Steel material as reference which is the one we use for wax. Therefore the calculations for the Feed Rate and Plunge Rate are the same as for the fome. 

Also I will reduce the Cutting speed to 10% like suggested by the professor and I will increase it along the cutting. 

{{<image src="../images/14week/10_cutting_wax.jpg" alt="Size of png extantion." size="40%" >}}

Here are some images of the procedure:

{{<image src="../images/14week/wax_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_8.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_9.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_10.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_11.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_11.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_12.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/wax_13.jpg" alt="Size of png extantion." size="40%" >}}

I still don’t know to well how can we edit the generated toolpath because as you can see on the image bellow the tool went all around the pocket instead of just smoothing the guitar itself. 

Although it was a good idea to smooth the area around the guitar cause that could influence the final look of the molde. I did stop the job before it ended because the guitar was already smooth and I didn’t have more time to stay in the lab.

{{<image src="../images/14week/wax_14.jpg" alt="Size of png extantion." size="40%" >}}

## Mixing and Making

Now we move to the lab to make the molde and the piece. 

I first start by making the molde and I choose to go with the simplest option as we were recommended. 

I’m gonna use the “Mold Start 15 Slow" for the molde.

{{<image src="../images/14week/mold_pack.jpg" alt="Size of png extantion." size="40%" >}}

First things first is to prepare everything I will need such as gloves, mixing cups (one for each part A and B and one mixing those parts), sticks to mix ( one for each part and one for the final mix) and a scale.

{{<image src="../images/14week/gloves.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/cups.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/sticks.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/cups_1.jpg" alt="Size of png extantion." size="40%" >}}

Then I read the datasheet so I know what I have to do.

{{<image src="../images/14week/datasheet_mold_f.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/datasheet_mold_b.jpg" alt="Size of png extantion." size="40%" >}}

So for the Mold Star 15 Slow the important info is:

* Mixing 1A:1B 
* Pot life is 50 min (so we have time to go make coffee and drink it)
* Cure time is 4 hours at room temperature
* Mix well and thoroughly both part A and B individually and when combined 
* Pour the mixture in a single spot at the lowest point of the containment field

Next we have to measure the quantity of molde we will need. We measure it with the help of a scale and water.

We pour water inside the master molde and we weight the water. Since the density of water is more less the same as molde liquid we can estimate the amount we need like this. We just have to pour a bit more then the weight of the water.

The following images show the procedure I did.

{{<image src="../images/14week/water_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/water_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/water_3.jpg" alt="Size of png extantion." size="40%" >}}

Next I use an empty glass to “tare" the scale so then I can know the exact weight of the water thats inside the cup.

{{<image src="../images/14week/water_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/water_5.jpg" alt="Size of png extantion." size="40%" >}}

Then I have to mix well and throughly both part A and B. 

{{<image src="../images/14week/mix_mold_1.jpg" alt="Size of png extantion." size="40%" >}}

Because the liquids are very viscose it’s very painful to do it for five minutes. I put headphones one and good music to motivate this step. I got so excited I even broke the stick

{{<image src="../images/14week/mix_mold_2.jpg" alt="Size of png extantion." size="40%" >}}

The weight of the water was 69.6g so I round it to 80g because there’s always some liquid on the sides of the cup remaining and also because the density of the water is not exactly the same as the molde liquid.

Since the mix ratio is 1A:1B I will pour 40g part A and 40g part B (or at least I tried to ahahah)

{{<image src="../images/14week/mixing_mold_1.jpg" alt="Size of png extantion." size="40%" >}}

Then we mix it really well. The pot life is 50 min so we have time.

{{<image src="../images/14week/mixing_mold_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/mixing_mold_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/mixing_mold_4.jpg" alt="Size of png extantion." size="40%" >}}

We clean the pots and we set them aside. We also take everything to the trash.

{{<image src="../images/14week/clean_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/clean_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/clean_3.jpg" alt="Size of png extantion." size="40%" >}}

Then we have to remove the bubbles using the air pressure machine.

We insert the pot inside the machine and we turn it on.

{{<image src="../images/14week/pot_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_3.jpg" alt="Size of png extantion." size="40%" >}}

Then we close the valve so no air can go out

{{<image src="../images/14week/pot_3.jpg" alt="Size of png extantion." size="40%" >}}

Then we have to wait for the black “needle" to go to the red “needle" and when that happens we open the air valve slowly and carefully to prevent the liquid to explode.

{{<image src="../images/14week/pot_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_6.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_7.jpg" alt="Size of png extantion." size="40%" >}}

We let the valve a bit open until the black needle reaches around 800 again.

{{<image src="../images/14week/pot_7_1.jpg" alt="Size of png extantion." size="40%" >}}

Then we close the valve again and we repeat the exact same procedure until we see that there’s no more bubbles (because the glass is so dirty its hard to see it so I just did it around 5 times)

{{<image src="../images/14week/pot_8.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_9.jpg" alt="Size of png extantion." size="40%" >}}

After all that time I open the air valve completely and I turn the machine off. I remove the liquide and I confirm if it worked

{{<image src="../images/14week/pot_10.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_11.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_12.jpg" alt="Size of png extantion." size="40%" >}}

Then I clean the master molde from the water and I pour the liquid slowly and in a single spot at the lowest point of the containment field.

The portion of mixture was just fine. 

{{<image src="../images/14week/pot_13.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_14.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_15.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/pot_16.jpg" alt="Size of png extantion." size="40%" >}}

Then I leave the molde for 4 hours to solidify.

{{<image src="../images/14week/pot_17.jpg" alt="Size of png extantion." size="40%" >}}

After all those hours the molde has solify and looks as follow.

{{<image src="../images/14week/dry_mold_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/dry_mold_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/dry_mold_3.jpg" alt="Size of png extantion." size="40%" >}}

#### Prepare the plastic liquid

Now is time to make the final object.

Once again I will use the simplest option as recommended. I will use the Smooth-Cast 305.

{{<image src="../images/14week/object_mix.jpg" alt="Size of png extantion." size="40%" >}}

The procedure is exactly the same as before.

First I prepare everything needed such as gloves, cups and sticks.

Then I read the datasheet.

{{<image src="../images/14week/datasheet_ob.jpg" alt="Size of png extantion." size="40%" >}}

So for the Smooth-Cast 305 the important info is:

* Mixing 100A:90B 
* Pot life is 7 min (so we gotta be fast, no time for coffee)
* Cure time is 30 min at room temperature
* Mix well and thoroughly both part A and B individually and when combined 
* Pour the mixture in a single spot at the lowest point of the containment field

Next step is to weight the amount of water that has the volume of the object.

{{<image src="../images/14week/water_obj_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/water_obj_2.jpg" alt="Size of png extantion." size="40%" >}}

Then we mix well both part A and B individually. This time the liquid is less viscose so it was better to miss it for long time. But it does smell more intense.

{{<image src="../images/14week/mix_object_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/mix_object_2.jpg" alt="Size of png extantion." size="40%" >}}

The weight of the water was so small so it will be hard to be precise. But I just have to make sure I maintain the 100:90 ration and it will be fine. The worst case is that I will be spoiling material.

The weight was 3.4g so I thought on do 2g part A and 1.8 part B. Off course it didn’t went exactly like this since it very hard to pour small amount of liquid that precisely.

{{<image src="../images/14week/precise_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/precise_2.jpg" alt="Size of png extantion." size="40%" >}}

Then I pour the liquid on the molde very carefully and slowly. Also maintaining a certain hight to prevent from bubbles to be created.

{{<image src="../images/14week/pour_obj.jpg" alt="Size of png extantion." size="40%" >}}

Was the first time I pour it and was so liquid that I manage to make bubbles ahaha

{{<image src="../images/14week/first_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/first_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/first_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/first_4.jpg" alt="Size of png extantion." size="40%" >}}

I had to try again to make it better with no bubbles. But this time I decided to use some colour so I could experiment with it.

To use the colours I also had to read the datasheet.

{{<image src="../images/14week/color_data.jpg" alt="Size of png extantion." size="40%" >}}

Since my mixture is so little I know I can only put a drop or something like this. On the dat sheet they also say to mix the color with the part B and only then mix part B with part A

That’s what I did and this is images of the procedure:

{{<image src="../images/14week/color1_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color1_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color1_3.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks after letting it dry. 

As you can see it also formed small bubbles so I tried it again with another color and better mixing ratio cause I feel like on the previous try it was to liquid so I might messed up with the measurements.

{{<image src="../images/14week/color1_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color1_5.jpg" alt="Size of png extantion." size="40%" >}}

I try again with purple and more carefully.

I first measure the part B then I pour the color and mix it. Then I pour the part A to that mixture and mix it again. The consistency this time looked more promising.

{{<image src="../images/14week/color2_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color2_2.jpg" alt="Size of png extantion." size="40%" >}}
 
{{<image src="../images/14week/color2_3.jpg" alt="Size of png extantion." size="40%" >}}

Then I pour the mixture to the molde with a more improve technique and I let it dry.

{{<image src="../images/14week/color2_4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color2_5.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks after drying. Looks nice and with no bubbles !!!! Next time I would pour less color to make it a less string purple.

{{<image src="../images/14week/color2_7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/color2_8.jpg" alt="Size of png extantion." size="40%" >}}

As a final touch I decided I could make this guitar as a cool necklace or piece of jewelry so I made a whole on the piece with the help of a machine that I forgot the name already. But it makes vertical holes very precise.

{{<image src="../images/14week/hole_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/hole_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/14week/hole_3.jpg" alt="Size of png extantion." size="40%" >}}

This is how the hole looks like. It’s actually really nice and will be a could necklace.

{{<image src="../images/14week/hole_4.jpg" alt="Size of png extantion." size="40%" >}}

I also though to sand it to make it more smooth at the back.