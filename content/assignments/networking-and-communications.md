---
date: 2023-05-11T10:58:08-04:00
description: ""
featured_image: "/images/network.jpg"
tags: []
title: "Week 15: Networking and Communications"
weight: 140
---

This week we learned about the functionalities of Wifi in the XIAO board. 

Again, I have a love and hate relationship with XIAO boards or Arduino in general. First was because Arduino just doesn’t work properly in my computer so I just decided to use the windows computer that’s on the lab. But guess what, at some point we will need to use the terminal and I didn’t know where to find the windows command prop. After all this problems that are holding me back I realize how much I love linux !!!!

Never the less, I’ll walk you through all the process and struggle I went through.

First I started by seeing the recorded lecture so I could do the examples at the same time.

First we tested the following simple code just to see if we could connect with the internet:

{{< highlight go-html-template >}}
#include <WiFi.h>

const char* ssid     = "Fablab";
const char* password = "Fabricationlab1";   

void setup()
{
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}  
void loop()
{
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(5000);
}
{{< /highlight >}}

So far everything worked fine.

{{<image src="../images/15week/third_example_1.jpg" alt="Size of png extantion." size="40%" >}}

Then the second example was also fine. The purpose of this exercise was to created an Access point and see how it works. It will be useful for my final project.

Here’s the code provided by the professor.

{{< highlight go-html-template >}}
#include "WiFi.h"
void setup()
{
  Serial.begin(115200);
  WiFi.softAP("CoffeePot", "CoffeeLab");
}

void loop()
{
  Serial.print("Host Name:");
  Serial.println(WiFi.softAPgetHostname());
  Serial.print("Host IP:");
  Serial.println(WiFi.softAPIP());
  Serial.print("Host SSID:");
  Serial.println(WiFi.SSID());
  Serial.print("Host Broadcast IP:");
  Serial.println(WiFi.softAPBroadcastIP());
  Serial.print("Host mac Address:");
  Serial.println(WiFi.softAPmacAddress());
  Serial.print("Number of Host Connections:");
  Serial.println(WiFi.softAPgetStationNum());
  Serial.print("Host Network ID:");
  Serial.println(WiFi.softAPNetworkID());
  Serial.print("Host Status:");
  Serial.println(WiFi.status());
  delay(1000);
}
{{< /highlight >}}

Everything work just fine:

{{<image src="../images/15week/second_example_1.jpg" alt="Size of png extantion." size="40%" >}}

But now, the third and final example the professor show on the class, was something more challenging.

First of all I needed a terminal. Going back to my computer was not an option cause it doesn’t work properly. So I just look up how could I have a terminal or windows and I found my way. However, when I thought I just had to run the professores code I was very much deceived. 

First I had the following error:

{{<image src="../images/15week/error_solution.jpg" alt="Size of png extantion." size="60%" >}}

Where I found a solution on the internet:

{{<image src="../images/15week/solution.jpg" alt="Size of png extantion." size="60%" >}}

After that, the code seamed to work reasonably. 

{{<image src="../images/15week/client_connected_1.jpg" alt="Size of png extantion." size="30%" >}}

{{<image src="../images/15week/client_connected_1_2.jpg" alt="Size of png extantion." size="60%" >}}

{{<image src="../images/15week/client_connected_2.jpg" alt="Size of png extantion." size="30%" >}}

{{<image src="../images/15week/client_connected_2_2.jpg" alt="Size of png extantion." size="60%" >}}

But, then, the professor continues the example with taking the first line followed by taking just the parameter of the footpath. Guess, what, it also doesn’t work. I don’t know why in some computers it works such as the professores and some of my colleagues, but for some of us it didn’t work. It would be stuck in the while loop as shown on the image bellow.

{{<image src="../images/15week/not_work_firstline.jpg" alt="Size of png extantion." size="60%" >}}

I was so pissed and with no head to think about another way to just remove the first line that I just decided to stop and get back to it the next day.

Because I did that I faced an error right in the begging. Perfect to boost my mood and motivation.

I then realised that I forgot to be connected to the “Fablab” network and that’s why it was giving me errors.

{{<image src="../images/15week/no_connect_server.jpg" alt="Size of png extantion." size="60%" >}}

{{<image src="../images/15week/solution_1_error.jpg" alt="Size of png extantion." size="30%" >}}

At this state I was still finishing the “input devices” week so I decided yo just finish that and get back to this later with more patience and maybe asking for the professores or colleges help.

Meanwhile the professor realised my struggle with the computers and that I was everywhere in the lab that he decided to setup one of the Linux computer to me and THANK GOD he did that cause since then my workload has been amazing and fast. Also Im in love by linux by now.

Never the less, when I finally come back to this week in the new computer I decided to start everything again from the very beginning just to be easier to debug in case of errors. 

Once again the first two examples work perfectly because they don’t involve using the terminal.

I move on to the third example once again and guess what, it doesn’t work!!!! This time wasn’t even the while loop was something else. I tried this exact some code before and it worked but now that I changed computers it doesn’t work. It connected and disconnected immediately without even printing “Thank you for connecting with us”.

{{<image src="../images/15week/error_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/15week/error_1.jpg" alt="Size of png extantion." size="50%" >}}

Before pissing the professor off I wanted to first try and debug the problem by myself.

I end up discovering that I was missing adding the “Content-lenght” on the http header. 

I found a way to had that and it worked, uhhhh !

Here’s the code followed by some pictures:

{{< highlight go-html-template >}}
#include <WiFi.h>

const char* ssid = "Fablab";
const char* password = "Fabricationlab1";

WiFiServer server(80);  //webservers usually listen on port 80

int connectionCounter = 1;
int btn = D6;

void setup() {
  pinMode(btn, INPUT_PULLUP);

  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  server.begin();  //start website
}

void loop() {

  // do we have someone connecting?
  WiFiClient client = server.available();

  if (client) {
    Serial.println("Client connected.");

    //Reading in the input
    if (client.connected()){ 
      String incoming = ""; //creating an empty string
      while (client.available()){
        char c = client.read();
        incoming += c;
      }
      
      int incoming_lenght = incoming.length();

      //Printing the input
      Serial.println(incoming);
      Serial.println(incoming_lenght);
      //client.println("Thank you for connecting with us");
      //Responding with a message
      client.println("HTTP/1.1 200 OK");
      client.println("Content-Type: text/html");
      client.println("Content-Length: ");
      client.println(incoming.length());
      client.println("Connection: close");
      client.println();
      client.println("Thank you for connecting with us");

      //give the browser time to receive the data
      delay(1);

      client.stop();
      Serial.println("Client disconnected");
    }
  }
}
{{< /highlight >}}

{{<image src="../images/15week/add_header_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/15week/add_header_2.jpg" alt="Size of png extantion." size="50%" >}}

{{<image src="../images/15week/add_header_3.jpg" alt="Size of png extantion." size="30%" >}}

Ok, finally I’m done with all the barriers until I try again the code with the while loop and again it gets stuck there.

By this time I have heard already from the professor and colleagues to avoid using this code and try the “WIFI_REST_LIB” instead. This code uses a library that handles really well http connections making it more easy to work instead of doing while loops.

So, that’s what I did, I give up on the previous example and I move on to trying this new one.

And guess what … It didn’t work !!! Of course it didn’t. And it was so strange because I had followed all the setup requirements such as what is written on the README.md file.

I found it so strange why it was working for the professor and for some of my colleges but not for me since it was a working example code. I went to look up some of my colleagues website to see which one of the examples have they followed and some used the previous example and other used the new example. I decided to take some time making the new example working before giving up and ask the professor. 
Because when we finally make something work out by ourselves the final result tastes better and makes us proud and motivated.

 I look up the errors and I tried to understand what they meant. I figure it out that there were the following typos on the code:

First it was written “request” instead of “response”.

{{<image src="../images/15week/error_2nd_ex_2.jpg" alt="Size of png extantion." size="60%" >}}

Solution:

{{<image src="../images/15week/solution_2nd_ex_2.jpg" alt="Size of png extantion." size="60%" >}}

Then there was another error. The professor wrote all the header instead of just “response” like so:

{{<image src="../images/15week/error_2nd_ex_1.jpg" alt="Size of png extantion." size="60%" >}}

Solution:

{{<image src="../images/15week/solution_2nd_ex_1.jpg" alt="Size of png extantion." size="60%" >}}

But then I realize that the purpose of this part was to send the value of the “message” string so then I first replaced “response” with “message” and off course it didn’t worked because “message” its not in the correct for to be send. So then I fix it by adding those extra components. 

Following is the error and how I fix it.

{{<image src="../images/15week/error_2nd_ex_1_3.jpg" alt="Size of png extantion." size="60%" >}}

Solution:

{{<image src="../images/15week/solution_2nd_ex_1_3.jpg" alt="Size of png extantion." size="60%" >}}

Conclusion of all this struggling debuting and fixing errors is that sometimes its nice that the professor is somehow busy so than I force myself to try to make things work by myself first and only then ask for help. If I succeed I feel so well and motivated, which is such an amazing feeling and im so great full I got to experience it this pass weeks. However if I fail to do by myself, it just makes me hate XIAO boards and Arduino even more.

Finally I reach a stable state where I feel like I won’t face any more of this unpredictable errors. 

I start playing around with the base code and I change it accordingly to the boards I have.

I will use the last board I mill, the input week one. I made two simple codes just to learn and see how the parameters and rootpath work overall.

First I made this code where I just have one parameter called color where I would turn on the light of the input color in case it exists on the board.
There’s also other functionalities such as, turn all the LEDs on or all off.

Following is the code and some videos showing it working:

{{< highlight go-html-template >}}
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char* ssid     = "Fablab";
const char* password = "Fabricationlab1"; 
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;
int GREEN = D2;
int RED = D3;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {
  pinMode(GREEN, OUTPUT);
  pinMode(RED, OUTPUT);

  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Hello world!/n");
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
  });

  // Usage IP_ADDRESS/led?state=1 where /led is our endpoint and ?state=on is a variable definition.
  // You can also chain variables like this: /led?sate=off&color=blue
  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){ 
    
    bool msg = true;
    if (request->hasParam("color")) {
      // The incoming params are Strings
      String param = request->getParam("color")->value();
      // .. so we have to interpret or cast them
      if (param == "green"){
        Serial.print("Turning green LED on.");
        digitalWrite(GREEN, HIGH);
        digitalWrite(RED, LOW);
      }else if(param == "red"){
        Serial.print("Turning red LED on.");
        digitalWrite(GREEN, LOW);
        digitalWrite(RED, HIGH);
      }else if(param == "off"){
        Serial.print("Turning LEDs off.");
        digitalWrite(GREEN, LOW);
        digitalWrite(RED, LOW);
        msg = false;
      }else if(param == "all"){
        Serial.print("Turning all LEDs on.");
        digitalWrite(GREEN, HIGH);
        digitalWrite(RED, HIGH);
      } else {
        msg = false;
      }
    } else {
      msg = false;
    }
    

    // Send back message to human
    String message = "Turning ";
    message += msg ? request->getParam("color")->value() : "LEDs ";
    message += msg ? " LED on." : "off.";
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Hello world!");
    response->addHeader("Access-Control-Allow-Origin", "*"); 
    request->send(200, "text/plain",message);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
} 

void loop() {
  // Nothing needed here at the moment
}

{{< /highlight >}}

{{<video src="../images/15week/simple_code_v.mp4">}}{{</video>}}

On this video I just show all the possible output messages depending on the input and GET request I make. This was my way of learning how does it work and how can I prevent from malicious human input.

{{<video src="../images/15week/simple_code_1_v.mp4">}}{{</video>}}

Then, for the second code, I wanted to have more than one parameter to see how it works and if it works. I then created the state and color parameters and I played around with it.
This time I also added more functionalities such as, if the ser wold insert a wrong color (a color that its not on the board) or an invalid state I wold print a message saying “invalid color” and “invalid state” respectively.

Once again, following is the code and a video of it working.

{{< highlight go-html-template >}}

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

// Some variables we will need along the way
const char* ssid     = "Fablab";
const char* password = "Fabricationlab1"; 
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;
int GREEN = D2;
int RED = D3;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  AsyncWebServerResponse *response = request->beginResponse(404, "text/plain", "Not found");
  response->addHeader("Access-Control-Allow-Origin", "*");
  request->send(response);
}

void setup() {
  pinMode(GREEN, OUTPUT);
  pinMode(RED, OUTPUT);

  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Hello world!/n");
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
  });

  // Usage IP_ADDRESS/led?state=1 where /led is our endpoint and ?state=on is a variable definition.
  // You can also chain variables like this: /led?state=off&color=blue
  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){ 
    
    String msg = "";
    if (request->hasParam("state") && request->hasParam("color")) {
      // The incoming params are Strings
      String state_param = request->getParam("state")->value();
      String color_param = request->getParam("color")->value();
      // .. so we have to interpret or cast them
      if (state_param == "on"){
        if (color_param == "green"){
          Serial.print("Turning green LED on.");
          msg += "Turning green LED on.";
          digitalWrite(GREEN, HIGH);
        } else if (color_param == "red"){
          Serial.print("Turning red LED on.");
          msg += "Turning red LED on.";
          digitalWrite(RED, HIGH);
        } else {
          Serial.print("No valid color.");
          msg += "No valid color.";
        }
      }else if(state_param == "off"){
        if (color_param == "green"){
          Serial.print("Turning green LED off.");
          msg += "Turning green LED off.";
          digitalWrite(GREEN, LOW);
        } else if (color_param == "red"){
          Serial.print("Turning red LED off.");
          msg += "Turning red LED off.";
          digitalWrite(RED, LOW);
        } else {
          Serial.print("No valid color.");
          msg += "No valid color.";
        }
      }else {
        Serial.print("No valid state.");
        msg += "No valid state.";
      }
    } else if (request->hasParam("state")){
      String state_param = request->getParam("state")->value();
      
      if (state_param == "on"){
        Serial.print("Turning LEDs on.");
        msg += "Turning LEDs on.";
        digitalWrite(GREEN, HIGH);
        digitalWrite(RED, HIGH);
      }else if(state_param == "off"){
        Serial.print("Turning LEDs off.");
        msg += "Turning LEDs off.";
        digitalWrite(GREEN, LOW);
        digitalWrite(RED, LOW);
      }else {
        Serial.print("No valid state.");
        msg += "No valid state.";
      }
    } else {
      Serial.print("No valid parameters.");
      msg += "No valid parameters.";
    }
    
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Hello world!");
    response->addHeader("Access-Control-Allow-Origin", "*"); 
    request->send(200,"text/plain",msg);
  });

  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();
} 
{{< /highlight >}}

{{<video src="../images/15week/complex_code_v.mp4">}}{{</video>}}

On the video you can see how nice it is to have the battery because I can just upload the code on the XIAO board and walk around with the device and it will work without needing to be plug on the computer. In this case was even nicer because then we could see both the terminal and the XIAO board reacting at the same time!

Finally i just want to mention that there was one more problem i had to face where i just come up with a temporary solution. 

When I first tried to import code from the Arduino to the XIAO board this error appear. 

{{<image src="../images/15week/another_error.jpg" alt="Size of png extantion." size="50%" >}}

Back then I couldn’t ask kris for help so I found this temporary solution:

{{<image src="../images/15week/solution_another_error.jpg" alt="Size of png extantion." size="60%" >}}

This solution is just temporary. It’s annoying to always have to type this every time I connect the USB cable.

Update: There was one day I had the error again and the solution wasn’t working. After trying everything I could imagine such as boot the board or unplug and plug the board several times I just decided to ask the professor to help me debug it.

We both struggled a lot to fix this problem. Apparently It had something to do with the bootloader and we had to install something on the board. Lets hope the problem doesn’t appear again.