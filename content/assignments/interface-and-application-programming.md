---
date: 2023-05-18T10:58:08-04:00
description: ""
featured_image: "/images/human.jpg"
tags: []
title: "Week 16: Interface and Application Programming"
weight: 150
---

For this week we were though how to use [Bootstrap](https://getbootstrap.com) and [jQuery](https://jquery.com) to help style a webpage. We use both those libraries to insert buttons, bars, colours, etc with the help of visual studio code.

Once again I first start by following the lecture. I understood everything that was done and it looked so interesting what we can build with it.

As you can see on the images bellow I succeeded on following the professor’s guide. I created an index.html file and I added the lib folder with the bootstrap and jQuery libraries on it. 

{{<image src="../images/16week/create_index_file.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/add_library.jpg" alt="Size of png extantion." size="40%" >}}

Following are some screenshots I took while following the lecture.

{{<image src="../images/16week/steps_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/steps_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/steps_3.jpg" alt="Size of png extantion." size="40%" >}}

Now it’s time to make my own main interface. I still don’t know what to do, I wanted to do something interesting but on the other hand I’m thinking to use this on the final project so I might just save this for later.

I was thinking to display like a graph or a bar of the percentage of the words/moods selected by the users of the exhibition.

I’m finally back here after finishing the code of my final project where the basic work flow is the user clicks a button corresponding to a word/mood/feeling and the brain will react different accordingly to the user input. Now I’m ready to finish this week. I will use the code I have and show some graphs of the average of the words selected.

I first made sure I include both bootstrap and jQuery libraries on the same rootpath as the new index.html file I created for the final project. I basically just copy paste the same lib I created when following the professor’s class.

{{<image src="../images/16week/create_new_index_file.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/insert_lib_in_new_folder.jpg" alt="Size of png extantion." size="40%" >}}

Then I copy paste the html code from the class to the new index.html file I created and change it along the way. 

I change the title and header title.

{{<image src="../images/16week/change_title.jpg" alt="Size of png extantion." size="40%" >}}

At this point I think I will make this happen in the input box. So I start by changing the input box Arduino code which is the client side. I first change the wifi connection. Normally the input box is connected to an Access point (AP) created on the server, the brain. Since I just wanna make the page and style it I will change the WiFi for the ‘FabLab’ network so I can work on it easier. Otherwise I would have to have the other board plugged so it could create the AP and every time I would want to make changes on the code I would have to change network. Its not ideal.

{{<image src="../images/16week/change_wifi_for_debug.jpg" alt="Size of png extantion." size="40%" >}}

Then I see what is the IP address and I change the rootpath accordingly to both the Arduino code and the index.html file.

{{<image src="../images/16week/change_ip_address_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/change_ip_address_2.jpg" alt="Size of png extantion." size="40%" >}}

When I was at this state I realise it will be easier to have it on the server side. At least I tried using the input box code and it didn’t work because I don’t even do the “server.on(…)” functions. I honestly don’t know if it would even work and I don’t have time to try to make it work since I can just do it on the server side.

I will then first simulate how will it look like and then I’ll change some lines of the code to make it work with the AP and with the input box.

So I created a simple Arduino code where I basically copy paste the code from the lecture and I will make the changes starting with the base code.

{{<image src="../images/16week/didnt_work_create_new_ino.jpg" alt="Size of png extantion." size="40%" >}}

Now that I tried using the code from the lecture it works. This code is basically the same idea as the code I have for the server so it should work. When I tried with the input box it didn’t work even tho I changed the ip address. I’m guessing it’s because I don’t even use an “AsyncWebServer” object on the code. 

Then, on the index.html file I cleaned it a bit so I can start from scratch. On the foto bellow you can see it working (it says “connected”) and you can see I just change the footer to say “PikkuLintu”.

{{<image src="../images/16week/now_works_before_was_disconnected.jpg" alt="Size of png extantion." size="40%" >}}

Then I start adding things to the index.html file as I follow the lecture again. 

I had once again the title.

{{<image src="../images/16week/beggining_of_project_work.jpg" alt="Size of png extantion." size="40%" >}}

Then I know I want to have some graphs. Ideally I would like to have vertical bar graph showing the number of people that pressed which word.

I know that bootstrap can provide us with so many things so I decided to start by looking to their website and see if I can find nice graphs. 

I found this colourful ones but they are horizontal though.

{{<image src="../images/16week/bootstrap_bar.jpg" alt="Size of png extantion." size="40%" >}}

I copy the code anyway just to see how it looks like. And this is how it turned out. I really don’t like it. It missed some stylish.

{{<image src="../images/16week/ugly_miss_change.jpg" alt="Size of png extantion." size="40%" >}}

Because I don’t have the energy neither time to look through all the documentation of bootstrap or jQuery I just decided to ask on the internet for HTML code to make vertical bars. 

Once again I’m very bad at goggling and I can lose to much time on it which is not nice. I end up learning about this “Chart.js” where we can download the code and use it to make nice charts. I tried to insert this and use some example code I found on the Internet but any of them worked. 

I was almost running out of options and I was about to ask the professor for help when I remember that we are lucky to be in 2023 where something called chatGPT was invented. 

I’m still not used to it. I always tend to google solutions but I’m so bad at it and most of the times doesn’t work. Never the less I tried asking chatGPT if he could give me the html code to make a coloured vertical bar chart. I’m still very new at this so I didn’t guide it the best way so I always had to add more info until I got a working good from it. So exciting.

{{<image src="../images/16week/asking_color_bar.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/16week/result_from_color_bar.jpg" alt="Size of png extantion." size="40%" >}}

I also like the fact that chatGPT also explains how the code works and what are things for. So nice. It’s like a professor in a way. The picture bellow is an example of this explanation I mean.

{{<image src="../images/16week/explanaition_from_chat.jpg" alt="Size of png extantion." size="40%" >}}

Then, like I already mentioned, I found about this “Chart.js” where they apparently have a website showing all the graphs they have as well all some part of the code. They also show all kinds of things we can do with the parameters they provide. I basically found the documentation website. Nevertheless I found this colour vertical bar that looks so nice so I just went there and copy the RGB values and added the “borderColor” parameter as well to the code chatGPT made.

{{<image src="../images/16week/update_colors.jpg" alt="Size of png extantion." size="40%" >}}

So this is how it looks like. The graph is very big and it occupies the whole screen. I have to had some kind of grid or create a class container. Nevertheless I will first make sure the graph as all the things I need and later I adjust this.

{{<image src="../images/16week/final_look_big.jpg" alt="Size of png extantion." size="40%" >}}

I need to had 9 bars since I have 9 buttons. I didn’t know what colours to attribute to the words so I just decided to get some inspirations. Since its pride month I look up the colours of the flags.

{{<image src="../images/16week/color_flag.jpg" alt="Size of png extantion." size="40%" >}}

Then I look up the RGBA value of the missing colours such as pink and violet.

{{<image src="../images/16week/add_new_colors.jpg" alt="Size of png extantion." size="40%" >}}

Then I also changed the label array with all the words and I also insert a paragraph where I’ll explain briefly the purpose of the exhibition.

{{<image src="../images/16week/add_nine_buttons.jpg" alt="Size of png extantion." size="40%" >}}

I realised its a bit hard to tell the exact number when using a chart with 5 intervals so I ask chatGPT to give code so I can put the number on top of the bars and this is the result.

{{<image src="../images/16week/add_nine_buttons.jpg" alt="Size of png extantion." size="40%" >}}

Then I wanted to insert another graph showing the percentages. I look up the Chart.js documentation and I found this doughnut shaped chart that I found really nice.

Since I looked through the code chatGPT gave and I took some time understanding it, I challenge myself to make this graph by myself just by looking to the Chart.js documentation. And I did succeeded. 

{{<image src="../images/16week/add_doughnut.jpg" alt="Size of png extantion." size="40%" >}}

Then, as you can see by the pictures, both graphs are too big and I wanna change that. I ask chatGPT and he gave me the code. I basically have to create this class container where I can style it as I want using the “style” tag. Which basically simulate the CSS file.

{{<image src="../images/16week/make_it_smaller.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/they_are_smaller.jpg" alt="Size of png extantion." size="40%" >}}

However it’s still not perfect. The footer is getting on top of the graphs. I have to find a way to fix it.

{{<image src="../images/16week/footer_on_top.jpg" alt="Size of png extantion." size="40%" >}}

I ask chaptGPT and once again I have to create a class container so then I can style it. I think I’m start getting the recipe for this. I latter changed the height of the bar cause its currently very big and I also change the padding-top so it adjusts well with the height reduction.

{{<image src="../images/16week/ask_about_footer.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/footer_proper_place.jpg" alt="Size of png extantion." size="40%" >}}

Another adjustment I had to do was making this application work in all screens. So whenever I would crop the screen the second graph should go bellow the first and if I increase the screen the second graph should go next to the side of the first graph.

The solution is using this “@media” method and change the chart container when the screen reaches a smaller size. I asked help for chatGPT and he explained really well what to do and change.

{{<image src="../images/16week/make_it_scallable.jpg" alt="Size of png extantion." size="40%" >}}

This time I wasn’t following to much the lecture but I had to go back there to see something when I come across with the creation of the main tag and the explanation of the container parameters from bootstrap. I decided to make all my body inside this main tag and I also added this margin-bottom so the footer doesn’t cut the doughnut graph. I also change the footer colour where I went to the internet to look for the RGBA values of a colour I choose.

{{<image src="../images/16week/add_evryth_in_main.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/change_footer_color.jpg" alt="Size of png extantion." size="40%" >}}

Finally, I’m happy with the final result. Next step is to move this code to the server code and make it work with the input box. This will be the actual challenge. 

I first thought how was I gonna read the values and change the data array dynamically. I thought on using JSON objects since the professor did the same in his example.

I go to the Arduino code of my server and I start changing it. I started by creating a new server.on() function to listen to GET requests from the rootpath “/wordCount”. I then decided to send the array of the number of button pressed as a JSON object. 

If you remember, at this point my server only receives from the client information if the button pressed is a good or a bad word. There’s no information on which of the words was selected. So I gotta send that information to the server as well. I will use the same /wordCount rootpath for that purpose.

On the client code I create this function where I send the words position in the array. Which means that if the button pressed is the number 1, the position on the array is the number 0, and so on. I call this function inside the turnLEDon() where I send the value of the position accordingly to the button pressed. I also print the message received to see how does the JSON object looks like.

Following is an image of that part of the code I created for better understanding.

{{<image src="../images/16week/client_side_off_code.jpg" alt="Size of png extantion." size="40%" >}}

I move one once again to the server code. At this point I have the client sending the position of the button pressed to the rootpath /wordCount. Now the server has to listen to that rootpath and do something with the position received. 

With that in mind I first create a wordCount array with zeros and a totalAmountUsers set to zero. 

{{<image src="../images/16week/creat_array.jpg" alt="Size of png extantion." size="40%" >}}

Then, just to see if I know how to send JSON objects and if the html file can read them properly I go to the other Arduino code I used to test and I create a example array with random values inside as well as a random number of total users. Then I try to send the JSON object being inspired by the professors code but it didn’t work. I had and error compiling the Arduino code because I was trying to send an array as a JSON object but in a wrong way. I then ask chatGPT how could I send an array as a JSON object and it told me I have to install this “ArduinoJSON” library. I did so and I also copy the code it gave me. Once again thank god it gives some explanations about the code cause I was so confused with the “doc” element.

{{<image src="../images/16week/install_JSON_library.jpg" alt="Size of png extantion." size="40%" >}}

Now I just miss changing my html file to change the data array dynamically accordingly to what I will receive from the /wordCount rootpath. I really had no idea how to do it but it turns out not to be that hard. I basically have to change the data array to a variable and create that variable globally. To find the working solution I first asked chatGPT. This time was hard to guide it to the solution I wanted. But as more I asked the more I understood what I had to do. He basically told me I should insert all the code I had to create both charts inside the “$(document).ready()” function and also declare the variables globally.

I compile the Arduino code and I see the console to look for errors.

{{<image src="../images/16week/error_with_data.jpg" alt="Size of png extantion." size="40%" >}}

As you can see from the console I was having an error with the data1 saying that it was not declare in this scope. And its when I remember that chatGPT did told me about declaring the variable globally but I kinda ignore it cause I didn’t know how to do it. I then just try putting it above all the functions and it worked. 

{{<image src="../images/16week/data_globally.jpg" alt="Size of png extantion." size="40%" >}}
 
Then I continue having some other console errors I had to debug such as the following one.

{{<image src="../images/16week/error_with_totalamount.jpg" alt="Size of png extantion." size="40%" >}}

After all the debugging it finally worked uhuhuhu. As you can see on the image bellow I also print the percentage array that by this stage its not exactly correct but later I fix it using the map() function.

{{<image src="../images/16week/error_with_totalamount.jpg" alt="Size of png extantion." size="40%" >}}

Right now I have a working solution that constantly listen (1s in 1s) to the rootpath /wordCount and I also have an example Arduino code that sends those arrays as JSON. Now I have to go back to the server Arduino code and copy the important things from this example code such us how to convert the wordCount array to JSON object.

On the image bellow you can see the server code on the left side and the example code on the right side. I will copy past the code to convert to a JSON object and I won’t even need to change anything since I gave the same names.

On the server code you can also see me reading the parameter sent by the client and add +1 to the wordCount array of that position received. I also increment the total amount of user by one so I can do the average. 

{{<image src="../images/16week/transfere_code.jpg" alt="Size of png extantion." size="40%" >}}

Finally I just miss making it work with the Access Point and with the input box. I create the AP I have to have the server running.

I first start by changing the ip address to the one the server is creating. 

{{<image src="../images/16week/change_ip.jpg" alt="Size of png extantion." size="40%" >}}

I then turn the client on and I press the buttons. I refresh the page and I have some errors on the console about the API and the API status is printing “Disconnected”. 
So the first thing I thought was that maybe I just have to put the ip address without the http before. I move on to change the code.

{{<image src="../images/16week/change_ip_add_1.jpg" alt="Size of png extantion." size="40%" >}}

I refresh the page again and still doesn’t work. It’s when I realise that I should be connected to the “Mentally” network (the one being created by the server). 

{{<image src="../images/16week/connect_to_mentally.jpg" alt="Size of png extantion." size="40%" >}}

I do that but it still didn’t work. So I change the ip address again to have the http in front and it finally worked!!!!

{{<image src="../images/16week/change_ip_add_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/16week/code_working_1.jpg" alt="Size of png extantion." size="40%" >}}

As you can see on the bellow above I am printing the number of connections. In this case are two which is the desktop computer and the input_box.

{{<image src="../images/16week/code_working_2.jpg" alt="Size of png extantion." size="40%" >}}

Following is the serial monitor on the client side the I also print the JSON object to confirm everything is fine.

{{<image src="../images/16week/message_cliente.jpg" alt="Size of png extantion." size="40%" >}}

Finally here’s a video if me showing this working.

{{<video src="../images/16week/web_working_v.mp4">}} Website working.{{</video>}}

I also made two more videos of me understanding the if I have to send any info on the main rootpath. 

The first video shows the things working even when it says disconnected and that’s because I’m not sending any request to the main rootpath. If you see the code I check the API status when there’s a request to the main rootpath. Since I don’t receive any response (because im not sending any), the checkAPIStatus function will do the fail part.

On the second video I send the response and it works normally. So, conclusion is that I have to send the response, not exactly for the thing to work but more to have it displayed “Connected”. 

{{<video src="../images/16week/video_1_v.mp4">}} Website working.{{</video>}}

{{<video src="../images/16week/video_2_v.mp4">}} Website working.{{</video>}}

Following is my index.html code.

{{< highlight go-html-template >}}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mentally Confused - Final Project</title>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    
    <style>
        .chart-container {
            width: 100%;
            height: 300px;
            margin-bottom: 50px;
            
        }
        .charts-wrapper {
            display: flex;
            flex-wrap: wrap;
            margin-left: 20px;
        }

        @media (min-width: 768px) {
            .chart-container {
                width: 50%;
                margin-bottom: 0;
            }
        }

        .footer {
            position: fixed;
            right: 0;
            bottom: 0;
            left: 0;
            height: 20px;
            text-align: center;
            padding-bottom: 1px;
        }
    </style>
</head>
<body>
    
    <header class = "container mt-3">
        <div id="status-alert" class="alert alert-light" role="alert">
            API status: <span id="status-api">Undefined</span>
        </div>
    </header>

    <main class="container mb-5">
        
        <h1>Mentally Confused</h1>
        
        <p> Add some text here explaining the purpose of the final project.</p>
       

        <div class="charts-wrapper">
            <div class="chart-container">
                <h2>Number o selections by the Users</h2>
                <canvas id="barChart"></canvas>
            </div>
            <div class="chart-container">
                <h2>Percentage of selections by the Users</h2>
                <canvas id="doughnutChart"></canvas>
            </div>
        </div>

        <div class="footer">
            &copy; PikkuLintu
        </div>
    </main>
    

    <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="lib/jquery/jquery-3.7.0.min.js"></script>
    <script>

        var data1; 
        var data2; 
        var chart1;
        var chart2;

        function checkAPIStatus(){
            $.ajax({
                url: "http://192.168.4.1",
                timeout: 5000
            })
            .done(function(){
                $("#status-api").text("Connected");
                if ($("#status-alert").hasClass("alert-light")){
                    $("#status-alert").removeClass("alert-light");
                }
                $("#status-alert").addClass("alert-success");
            })
            .fail(function(){
                $("#status-api").text("Disconnected");
            });
        }

        function getButtonValues(){
            $.ajax({
                url: "http://192.168.4.1/wordCount",
                timeout: 5000
            })
            .done(function(response){
                // response is a JSON object
                const responseJSON = JSON.parse(response);
                console.log("responseJSON: ");
                console.log(responseJSON);

                var receivedData = responseJSON.nameCount;
                var amountUsers = responseJSON.totalAmountUsers;

                console.log("nameCount: ");
                console.log(receivedData);

                console.log("totalAmountUsers: ");
                console.log(amountUsers);

                // Update the data arrays with the received values
                data1.datasets[0].data = receivedData;
                data2.datasets[0].data = receivedData.map((value) => (value / amountUsers) * 100);
                
                console.log("percentage: ");
                console.log(receivedData.map((value) => (value / amountUsers) * 100));

                // Update the charts with the new data
                chart1.update();
                chart2.update();
            })
            .fail(function(){
                // Handle API request failure
                console.log("Failed to fetch data from the API.");
            });
        }
        

        $(document).ready(function(){
            console.log("Document has loaded!");
            
            // first chart
            var ctx = document.getElementById('barChart').getContext('2d');

            data1 = {
            labels: ['Happy', 'Stressed', 'Motivated', 'Proud', 'Sad', 'Confident', 'Anxious', 'Thrilled', 'Scared'],
            datasets: [{
                label: '# selected by users',
                data: [10, 20, 30, 15, 25, 5, 17, 22, 13],
                backgroundColor: [
                'rgba(255, 192, 203, 0.3)', //pink
                'rgba(255, 99, 132, 0.2)',  //red
                'rgba(255, 159, 64, 0.2)',  //orange
                'rgba(255, 205, 86, 0.2)',  //yellow
                'rgba(75, 192, 192, 0.2)',  //green
                'rgba(54, 162, 235, 0.2)',  //blue
                'rgba(153, 102, 255, 0.2)',   //purple
                'rgba(238, 130, 238, 0.2)', //violet
                'rgba(201, 203, 207, 0.2)' //gray
                ],
                borderColor: [
                'rgba(255, 192, 203)', 
                'rgb(255, 99, 132)',
                'rgb(255, 159, 64)',
                'rgb(255, 205, 86)',
                'rgb(75, 192, 192)',
                'rgb(54, 162, 235)',
                'rgb(153, 102, 255)',
                'rgba(238, 130, 238)',
                'rgb(201, 203, 207)'
                ],
                borderWidth: 1
            }]
            };
        
            var options = {
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                plugins: {
                },
                animation: {
                    onComplete: function () {
                    var chart = this;
                    var ctx = chart.ctx;
                    ctx.font = Chart.helpers.fontString(Chart.defaults.font.family, 'normal', Chart.defaults.font.size);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    chart.data.datasets.forEach(function (dataset, datasetIndex) {
                        var meta = chart.getDatasetMeta(datasetIndex);
                        meta.data.forEach(function (bar, index) {
                        var data = dataset.data[index];
                        var position = bar.tooltipPosition();
                        ctx.fillText(data, position.x, position.y);
                        });
                    });
                    }
                }

            };
    
            chart1 = new Chart(ctx, {
                type: 'bar',
                data: data1,
                options: options
            });

            // second chart
            var ctx = document.getElementById('doughnutChart').getContext('2d');
            data2 = {
            labels: ['Happy', 'Stressed', 'Motivated', 'Proud', 'Sad', 'Confident', 'Anxious', 'Thrilled', 'Scared'],
            datasets: [{
                label: '% selected by users',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0],
                backgroundColor: [
                'rgba(255, 192, 203)', //pink
                'rgba(255, 99, 132)',  //red
                'rgba(255, 159, 64)',  //orange
                'rgba(255, 205, 86)',  //yellow
                'rgba(47, 210, 123)',  //green
                'rgba(90, 220, 235)',  //blue
                'rgba(153, 102, 255)',   //purple
                'rgba(238, 130, 238)', //violet
                'rgba(201, 203, 207)' //gray
                ],
                hoverOffset: 4
            }]
            };

            var options = {
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            };

            chart2 = new Chart(ctx, {
                type: 'doughnut',
                data: data2,
                options: options
            });

            // do this fuction in loop
            // it will check the status every two seconds

            setInterval(checkAPIStatus, 2000);
            setInterval(getButtonValues, 1000);

            $(".footer").css("background", 'rgba(100, 193, 131, 0.5)');
            $(".footer").css("color", "grey");
            
        });
        
    </script>

</body>
</html>

{{< /highlight >}}

Next, what I could do is also show the amount of users that have been in the exhibition as well as write the description of the project.

I was also li+ooking to the Bootstrap documentation and i come across with the "Popovers". It would be nice i could have memes for both good and bad word and whenever i trigger them they would pop up for a bit. Maybe ill do this during summer. 

This documentation is not the best. It’s a bit messy and all over the place like my thoughts were. I was also more focus on debugging and finding solutions that I forgot to take more pictures.  

