---
date: 2023-03-01T10:58:08-04:00
description: ""
featured_image: "/images/3D.jpg"
tags: []
title: "Week 7: 3D Scanning and Printing"
weight: 60
---

This week was about 3D printing and learn the constraints of the machine. We had an introduction to the machine a few weeks ago and this week was finally the time to put in practice what we learned and test our own designs.

Table of content:

* [Design Process and Study](#design-process-and-study)
* [Print a test model](#print-a-test-model)
* [Print my Design](#print-my-design)
* [Adjust Design and PLA Print](adjust-design-and-pla-print)
* [SLA Print](#sla-print)


## Design Process and Study

I got so excited about printing my own design that i went straight for it. We had to design something small so we wouldn't use the machine for a long time and thus prevent the other students from using it. Therefore I immidiatly thought about doing a earing or a ring with a nice and hollow design. I did several invistigations but nothing seemed to be exactly what i was looking for. 

I start thinking about something I like and it was when i come up with the shape of a mushroom. I just love mushrooms !! I love the taste of them and picking them in the nature. I'm vegan and mushrooms are a really important source of protein. They are so versatile. Depending on the species they can change in texture and taste. Plus we can use them to replace non-vegan foods to vegan, for example, [vegan ribs](https://www.youtube.com/results?search_query=vegan+ribs+with+mushroom), [vegan polled pork](https://www.youtube.com/results?search_query=vegan+pulled+pork+mushroom+), [vegan scallops](https://www.youtube.com/results?search_query=avant+garde+vegan+scallops+), etc. Sooo excitingggg !!!

The task was to do an hollow object that can not be made using subtractive manufacturing. I made some research and i imagine some cool shapes for achiving what we were asked. This is how it looks like. 

{{<image src="../images/7week/my_all.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/7week/my_int.jpg" alt="Size of png extantion." size="40%" >}}

Here are some images and explanations of how I achieved the final design.

For the base of the mushroom i design a simple curve and then i "revolve". No problems here so far.

{{<image src="../images/7week/scketch_base.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/7week/first_revolve.jpg" alt="Size of png extantion." size="40%" >}}

Next, it was one the challenge started! I wanted to make a nice pattern around the surface of the base. I try to find some examples on the internet but nothing very conclusive. However, because i consumed a bunch of tutorials i end up making my way to achieve the desired output by combining all the tools i learned throughout those tutorial. 

So what i first did was creating on single part of the pattern in a new sketch and then do "Rectangular Pattern" to multiply the single one along the x-y axis. 

By doing that the final sketch looked like this:

For the next step we have to do "Emboss". I encounter again a problem because when we try to emboss the sketch on the curved shape it wont work.

{{<image src="../images/7week/final_scketch.jpg" alt="Size of png extantion." size="40%" >}}

For the next step we have to do "Emboss". I encounter again a problem because when we try to emboss the sketch on the curved shape it wont work.

{{<image src="../images/7week/try_emboss.jpg" alt="Size of png extantion." size="40%" >}}

Soooo, what i had to do was create a cylinder with the same diameter as the small diameter of my curved shape:

{{<image src="../images/7week/same_diameter.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/7week/cilinder.jpg" alt="Size of png extantion." size="40%" >}}

And only now i can emboss my patter around the cylinder. To do that i just click the emboss tool, select the whole pattern and the cylinder and click ok. For this part i have to make some adjustments. I usually look around the cylinder and i see if the pattern fits perfectly around the cylinder. Otherwise i should either change the distance between the single pieces or the amount of pieces i put on the x-axis. 

After im done with that, this is how it looks like:

{{<image src="../images/7week/emboss.jpg" alt="Size of png extantion." size="40%" >}}

We are not close to be done here. The next step is to remove the cylinder to just have the pieces left.

{{<image src="../images/7week/remove_cilinder.jpg" alt="Size of png extantion." size="40%" >}}

Finally, i join those pieces with the curved shape:

{{<image src="../images/7week/join.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/join2.jpg" alt="Size of png extantion." size="40%" >}}

As you can see the pieces have different sizes and they don’t follow the shape as i want. Therefore, the solution i discovered by accident just by playing around with the tools of this software was the following:

In a new sketch i did the same shape but a bit bigger so goes around the original shape with a 1.8mm distance from each other:

{{<image src="../images/7week/new_shape.jpg" alt="Size of png extantion." size="30%" >}}

Then i clicked the revolve tool but i used intersection instead. I select all the pieces and the curved shape (you can see on the image bellow) and then i intersect them in order to have the desire output:

{{<image src="../images/7week/intersect.jpg" alt="Size of png extantion." size="55%" >}}

{{<image src="../images/7week/intersect2.jpg" alt="Size of png extantion." size="30%" >}}

This was definitely the most challenging part and the one that consumed me more time.

Now, moving on to the upper part of the mushroom. This one was easier since i got the flow of the work.

I made a sketch of how i wanted to look like followed by a "revolve".

{{<image src="../images/7week/up_shape.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/up_shape_ext.jpg" alt="Size of png extantion." size="40%" >}}

Then i wanted to make some cool design for the interior to kinda mimic the same idea as the interior of the mushrooms.

I thought on doing so with the same strategy as before which is the intersection. So with that in mind i first made the shape i wanted to have as final result in a new sketch followed by an "revolve" of 360 degrees.

{{<image src="../images/7week/desired_shape.jpg" alt="Size of png extantion." size="40%" >}}

Now, because i know im going to do intersect i made this simple sketch where i used "circular pattern" to repeat the shape around the circle.

{{<image src="../images/7week/circular_pattern.jpg" alt="Size of png extantion." size="40%" >}}

And this is how it looks like after the intersect:

{{<image src="../images/7week/intersect3.jpg" alt="Size of png extantion." size="40%" >}}

In order to unity the base of the mushroom with the top i made a simple connection by extruding a cylinder from the bottom to the top.

{{<image src="../images/7week/union1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/union2.jpg" alt="Size of png extantion." size="40%" >}}

Finally, because i want to make this a hearing or a keychain i made a ring on the top of the mushroom.

{{<image src="../images/7week/ring.jpg" alt="Size of png extantion." size="35%" >}}

{{<image src="../images/7week/ring2.jpg" alt="Size of png extantion." size="45%" >}}

Ufff, what a long journey!! One that i definitely loved.

This is the first time I'm using Fusion 360 and thus I'm so proud with the outcome result! Making those patterns around the base of the mushroom was definitely a challenge and time consuming.


## Print a Test Model

Another task assigned to us was printing an already made design so we could learn the machine constraints and capabilities. 

I choose to print the ["Iron Throne Benchy"](https://www.thingiverse.com/thing:2404462) suggested by the professor, that can be found in [Thingiverse](https://www.thingiverse.com) website. I printed on the Ultimaker 2+ Connect.

I started good by making a mistake at the very beggining. I choose to use support because I tought we were supposed to, but, apperently the goal is to try to avoid using supports. The professor even mentioned that he perfers to abdicate of the design just to not use supports. 

Weeelllll, the momment he said that, I realised that maybe my beatifull design had to have some changes. That makes me so sad because i really liked the idea of the interior of the mushroom, but in order to make that possible I would have to use a lot of support and it would probably not work or be a pain to 3D print.

In conclusion, the first think to have into account when designing is trying to avoid using support even if we have to abdicate doing something really cool.

Following are some photos of the printing using the support. The red lines on the images shows the failures of the pritting plus areas where there should have printed but it didn't. 

{{<image src="../images/7week/support1.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/7week/support2.jpg" alt="Size of png extantion." size="40%" >}}

Ans this is without support. It worked perfectly and smoothly. It was interesting to see how well it printed even with no support specially on the window. That was exactly the purpuse of this week. Is understanding the limites of the machine.

{{<image src="../images/7week/no_support1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/no_support2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/no_support3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/no_support4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/no_support5.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/no_support6.jpg" alt="Size of png extantion." size="40%" >}}

Finally, this is how it looked like at the end of the printing.

{{<image src="../images/7week/final1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/final2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/final3.jpg" alt="Size of png extantion." size="40%" >}}

It worked so well even with no support.

Another thing i want to point for later use is that we should use "Skirt" for the Build Plate Adhesion" setting in "Cura" software. We shouldn't use "Brim" beacuse it's to much plastic wasted to make the base and there's no need, but also we shouldn't use "None" beacuse we should make sure that when we start building our design the needle is relising plastic. "Skirt" works as a safety method.


## Print my Design

Now, going back to my design, I know i will have to change it because it wont be possible to 3D print it without support as you can see by the following images:

{{<image src="../images/7week/mine_no_support1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/mine_no_support2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/mine_no_support3.jpg" alt="Size of png extantion." size="40%" >}}

And on the otherhand, if I build using support it will be a pain to remove them all aftewards, or it wont even work.

Here are some pictures to show what i mean.

{{<image src="../images/7week/mine_support1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/mine_support2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/mine_support3.jpg" alt="Size of png extantion." size="40%" >}}

Therefore, I first started by making the interior of the mushroom with more feeling than what it had before. It looks cool the design i did for the interior but nobody will be able to see it anyway so mind as well avoid using the supports.

This is how it looks after introducing more feeling.

{{<image src="../images/7week/my_all_update.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_int_update.jpg" alt="Size of png extantion." size="40%" >}}

What i did was just simply change the scketch by removing the dark blue line (image on the left) in order to reach the desired outcome (image on the right). 

{{<image src="../images/7week/change1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/change2.jpg" alt="Size of png extantion." size="40%" >}}

Then i just "revolve" the surface in dark blue on the image on the right and this is what i got:

{{<image src="../images/7week/final_revolve.jpg" alt="Size of png extantion." size="40%" >}}

Next, the challenge was how to make it possibel to print with the less amount of supports. 

After hearing some feedback from the professor and some collegues i decides to flip my design in ["Cura"](https://ultimaker.com/software/ultimaker-cura?gclid=CjwKCAiAr4GgBhBFEiwAgwORre8IPS68lZRAMb4pnO3j3qfBEbQzmwA885YfNQjsU3am9h-OJMNOQBoC1icQAvD_BwE) and see how would improve.

First i flip it 90 degrees and the amount of supports increased a lot as you can see by the images bellow:

{{<image src="../images/7week/try1_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/try1_2.jpg" alt="Size of png extantion." size="40%" >}}

And the amount of time to print it also reduced from 3h21m to 2h13m

{{<image src="../images/7week/try1_3.jpg" alt="Size of png extantion." size="40%" >}}

But later on i realized i could flip the mushroom upside down and get rid of much more supports as you can see:

{{<image src="../images/7week/try2_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/try2_2.jpg" alt="Size of png extantion." size="40%" >}}

Once again the time decrease to 1h17m which is very significative:

{{<image src="../images/7week/try2_3.jpg" alt="Size of png extantion." size="40%" >}}

Next, I used a tool called "Support Blocker" suggested by a college to get rid of unnecessary supporst such as the ones floating on the image bellow:

{{<image src="../images/7week/delete_sup1.jpg" alt="Size of png extantion." size="40%" >}}

And this is how i got rid of them:

{{<image src="../images/7week/delete_sup2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/delete_sup3.jpg" alt="Size of png extantion." size="40%" >}}

The settings look good so then i decided to print it and see how it goes. This time i printed on the Ultimaker S3 for two reasons.

First reason its because the plastic is yellow which happens to be my favourite colour and secondly because from what I understood from the other colleges this printer prints better than the other machines.

In the beggining of the printing i was a bit concerned because it looked like it wasn't printing correctly the support. I even paused to see if i could remove but i saw i was making it even worse so i just decided to continue printing and see the flow.

I realized I really like to be there standing at it printing. It's so relaxing and satisfying. Unfortunatly I can't afoard using 3 hours of my time just to look at it printing.  

Following are some photos of the procedure and the final result.

This two first pictures discribe the rugh beggining i mentioned above. I thought on repeating cause the material was lifting and not staying on the pate. But then i continuing watching it working and it seamed it was going to work anyway. It was scary tho.

{{<image src="../images/7week/my_final7.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final9.jpg" alt="Size of png extantion." size="40%" >}}

As you can see by the following image it had some struggles building the supports, but the final product worked well no matter what so its fine.

{{<image src="../images/7week/my_final10.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final8.jpg" alt="Size of png extantion." size="40%" >}}

Then it was time to clean the suports, its was fun and relaxing. Good to relise stress. 

Here are some images of the procedure and how much I was having fun.

{{<image src="../images/7week/my_final1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/me_happy_cleaning.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final4.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/7week/my_final5.jpg" alt="Size of png extantion." size="40%" >}}

Another thing to be proud is that it even stads up on its own.

{{<image src="../images/7week/my_final5.jpg" alt="Size of png extantion." size="40%" >}}

One thing we can see by the image above is the fact that the ring for the key chain didn't quite work. Two things that made that happen is the fact that it wasn't thick enough and thus by removing the support around it broke. Also, the fact it was the first layer in contact with the plate it made it a bit more fragile. 

What i would do to fix it is make it thicker both to the sides and hight so i have some room for error. I will also avoid having supports inside the ring itself but just around it.

Also I like how the sun and light runs trough the interior of the object. So beatiful the colours it gets.

{{<image src="../images/7week/my_final3.jpg" alt="Size of png extantion." size="40%" >}}

Besides the first fight it end up preaty well. I will try to fix the problem with the ring for the next print.

{{<image src="../images/7week/me_happy.jpg" alt="Me happy with the design i made." size="40%" >}}

Next, my goal is to 3D print using SLA printers and maybe do that one as earing and this one on the PLA printers ill make it as a key chain.

Also, I'm excited to learn how to scan an object and 3D print it. We will be learning that next week.


## Adjust Design and PLA Print

As mentioned above, when i printed on the PLA machine, the ring for the keychain broke because it was two thin and short. Therefore i change the design to make it thicker and a bit taller.

I move one to print it again and this is what happened:

{{<image src="../images/7week/failure1.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/failure2.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/failure3.jpg" alt="Me happy with the design i made." size="40%" >}}

So what happen here is that I used “Skirt” for the “Build Plate Adhesion” which in this case its not enough. I have to put a stinger support so I then change it to “Brim”.

{{<image src="../images/7week/brim1.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/brim2.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/brim3.jpg" alt="Me happy with the design i made." size="40%" >}}

By doing that I manage to print it successfully

{{<image src="../images/7week/sucess1.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/sucess2.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/sucess3.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/sucess4.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/sucess5.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/sucess6.jpg" alt="Me happy with the design i made." size="40%" >}}

For this one I scale it so it’s just 35mm high and the purpose of it is to make it as a key chain.

## SLA Print

Finally I moved on to the PLA machine. 

I asked Solomon which of them to use, if the “KnowingPrairie” number 2 or the “MatureWrasse” number 3. 

He explained me that the number 3 has a stronger resign which can be useful if we want to use it for supporting heavy forces. Whereas the number 2, even tho the material its not as strong it has the positive thing of making the layers thinner up to 0.025mm. 

In conclusion, I used the machine number 2 because I want my object to be as precise as possible. Since I will make earrings they don’t need to be built out of a strong material. 

So, I first uploaded the design to the “PreForm” software and I rotated the mushroom upside down like I did when I printed on the SLA machine to make sure I don’t need to many supports.

{{<image src="../images/7week/rotate.jpg" alt="Me happy with the design i made." size="40%" >}}

Next I change the size of it to make it small since i’ll make it as earrings. I scale the z-axis to 35.88mm and the x and y-axis scaled accordingly. 

{{<image src="../images/7week/size.jpg" alt="Me happy with the design i made." size="40%" >}}

Finally, I move on to generate the supports by clicking “Auto-Generate Selected”. 

{{<image src="../images/7week/auto.jpg" alt="Me happy with the design i made." size="40%" >}}

As you can see by the image bellow, it auto generate a lot of supports I don’t feel its needed.
So I clicked “edit” and I start removing the supports by mousing over the white bolls and click on them to remove them. We can also click somewhere in the object to add supports. 

{{<image src="../images/7week/support.jpg" alt="Me happy with the design i made." size="40%" >}}

When we mouse over the white bolls, the boll turns red. Then we click on it to remove it.

{{<image src="../images/7week/red.jpg" alt="Me happy with the design i made." size="40%" >}}

If we try to remove certain areas that are crucial, the object will turn red in the area that needs support. Therefore we have to maintain the support in that area.

{{<image src="../images/7week/white.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/red_area.jpg" alt="Me happy with the design i made." size="40%" >}}

Next, I move on to the cap of the mushroom. I decided to change the supports accordingly to the pattern I want to have at the end. I do this because, after taking all the supports from the object , there’s still some footprint left in the cap. For some other designs this could be a problem but in my case having those small remaining dotes from the support gives a good pattern to the final prototype. 

{{<image src="../images/7week/cap.jpg" alt="Me happy with the design i made." size="40%" >}}

This is how the final supports look like.

{{<image src="../images/7week/final_supports.jpg" alt="Me happy with the design i made." size="40%" >}}

Finally I make sure I select the correct machine, colour and thickness of the layers followed by hitting on the “apply” button.

{{<image src="../images/7week/edit.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/machine.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/colour.jpg" alt="Me happy with the design i made." size="40%" >}} 

{{<image src="../images/7week/apply.jpg" alt="Me happy with the design i made." size="40%" >}} 

Finally, we can look to the right side of the software and confirm that everything is checked.  As you can see on the image bellow there’s only green thumbs up with means we are good to send the job to the printer.

 {{<image src="../images/7week/send.jpg" alt="Me happy with the design i made." size="40%" >}} 
 
After sending the job to the printer we just have to follow the instructions. It’s written there that we have to open the air valve so the tank can fill itself with resign and we also have to make sure the plate is clean. Then we hit print and we wait until it’s done.

 {{<image src="../images/7week/done.jpg" alt="Me happy with the design i made." size="40%" >}} 

For the following steps we have to grab gloves and use them all the time since the resign is very toxic.

After putting the gloves on, we grab the plate with the object still attached to it and we transfere it to the washing machine. Because im using soft black material I just have to put it for 30 min. It written on the sheets there.

After being washed we remove the object from the plate with specific tools for that and we transfere it to the drying machine where it stays for 10 min at 60degrees. Once again we can see the specific time and temperature for the material on the sheets that are at the lab.

While it’s drying we can start cleaning the plate and the tools using paper and alcohol. We have to make sure we clean the plate really well and that we don’t leave any remaining of resign on it to not compromise the following printings.

After we are done with the drying we remove the supports using the clamps and we enjoy our lovely design.

{{<image src="../images/7week/enjoy1.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/enjoy2.jpg" alt="Me happy with the design i made." size="40%" >}}

{{<image src="../images/7week/enjoy3.jpg" alt="Me happy with the design i made." size="40%" >}}

On the following image you can see what I meant with the dots that the supports leave after removing them. It gives a nice unintentional design to the mushroom. 

{{<image src="../images/7week/enjoy4.jpg" alt="Me happy with the design i made." size="40%" >}}

Now I just miss by the earring support and they will be ready to use.
 