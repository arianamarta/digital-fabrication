---
date: 2023-01-26T10:58:08-04:00
description: ""
featured_image: "/images/hugo.jpg"
tags: ["scene"]
title: "Week 2: Principles and Practices"
weight: 10
---

On this week we finish learning about Git and all its important commands and thus we moved over to learn about [Hugo](https://gohugo.io). 

On the second class of the week we learned how to install and run Hugo server. 

Besides that we learned how to work with Hugo. We will basically use Markdown structure instead of plain HTML. 

In the beggining Hugo can be a bit difficult to understand with all the folders and the specific names we have to give to the files. However, Hugo is a really friendly and powerfull tool to use for web development.

Here it goes a meme that defines Git pain.

{{<image src="../images/git.jpg" alt="Meme that defines Git pain." size="50%" >}}
