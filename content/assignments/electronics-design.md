---
date: 2023-03-09T10:58:08-04:00
description: ""
featured_image: "/images/plc.jpg"
tags: []
title: "Week 8: Electronics Design"
weight: 70
---

This week we learn how to design our own PCB using [KiCad](https://www.kicad.org).

The idea is to design around the XIAO board in order to get familier with the software and his tools.

Therefore we were asked to include in the board at least a couple of LEDs and Buttons as weel as design the board with the whatever shape we want. We were told we can even export the design from another software such as illustrator and later use it as the design of the board.

After following the same steps as the professors I decided to move on to build my own design. 

As usually I didn't fully understood what we were supposed to do so i end up doing three different designs:

* [LED RGB](#led-rgb)
* [FIXE](#fixe)
* [COW](#cow)
* [All together](#all-together)

## LED RGB

I first started by creating a new project:

{{<image src="../images/8week/start_new_project.jpg" alt="Size of png extantion." size="60%" >}} 

Then I added the XIAO board:

{{<image src="../images/8week/xiao_board.jpg" alt="Size of png extantion." size="60%" >}} 

Follow by adding the Ground and Power:

{{<image src="../images/8week/power.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/8week/power_3v3.jpg" alt="Size of png extantion." size="40%" >}} 

Next, I started to look to the components provided on the libraries and I came across with the following component that made me curious:

{{<image src="../images/8week/strange_led.jpg" alt="Size of png extantion." size="60%" >}} 

I didn’t quite get how that led worked. I was hoping to ask the professor but he was on vacation this week so I decided to just improvise and then ask later if it’s correct or not.

I looked to it’s datasheet that can be found by going to the [Fab Academy](https://fabacademy.org) website -> “content” -> “2023” -> “Schedule” -> “Mar 01: electronics design”  -“Electronics Design”.  Afterwards look for the component you want. In my case is the LED RGB. With the respective [datasheet ](https://assets.cree-led.com/a/ds/h/HB-CLV1A-FKB.pdf) .

{{<image src="../images/8week/LED_RGB_sheet.jpg" alt="Size of png extantion." size="60%" >}} 

When looking though the datasheet I come across with the values for the Forward Voltage and current for all the 3 RGB colours. 

{{<image src="../images/8week/f_current.jpg" alt="Size of png extantion." size="100%" >}} 

{{<image src="../images/8week/f_voltage.jpg" alt="Size of png extantion." size="100%" >}} 

This is the values of the current flowing through the circuit and LED:

{{<image src="../images/8week/current.jpg" alt="Size of png extantion." size="40%" >}} 

For a current of 20mA and by knowing the value of the voltage that a LED will have we can calculate the voltage the resistance has to have by doing total amount of voltage (V_total) minus the voltage on the LED (V_LED) : V_Res = V_Total - V_LED

{{<image src="../images/8week/voltage.jpg" alt="Size of png extantion." size="50%" >}} 

Finally, we can calculate the value of the resistor we have to have in order to not explode the LED. It’s done with the mathematical equation R=V/I :

{{<image src="../images/8week/resist.jpg" alt="Size of png extantion." size="50%" >}} 

Once again, I go to the previous link and look for the Resistors components and find the one suitable for the values calculated. In my case I will need the 4.99OHM and 49.9OHM.

{{<image src="../images/8week/which_registor.jpg" alt="Size of png extantion." size="70%" >}} 

After that I also added three buttons where I used the “Label” tool to make it less confusing with the lines.

{{<image src="../images/8week/create_lables.jpg" alt="Size of png extantion." size="70%" >}} 

Although I’m not quite sure if this is how I should use the LED, I do think it makes sense. This is how everything looks together:

{{<image src="../images/8week/strange.jpg" alt="Size of png extantion." size="40%" >}} 

Finally, we press “Rules Check” button to check for possible errors and warnings. All looked good and therefore it's time to move on to the “board editor” where I will upload this same file to the “board editor”.

Because, i wasn't sure if this is the correct set up for this led i decided to not proceed with the design of the board and move one to another project with different components.

## FIXE 

This was the next design i come up with. On this one i decided to go with familiar components.

First, I started a new project where I added the XIAO board and connected the ground and power to it. 

Afterwords I decided to put two LEDs and two Buttons. On of the LEDs will be the blue LED and the other RED. The idea of the buttons is that they can be programmed to turn on the LEDs.

{{<image src="../images/8week/fixe_design.jpg" alt="Size of png extantion." size="60%" >}} 

As you can see on the image above I did exactly the same procedure as described above for calculating the Resistors. 

For the calculous of the registores I follow the same steps as above. I went to the same link and I look to the LED componentes:

 {{<image src="../images/8week/Find_RB_LED.jpg" alt="Size of png extantion." size="60%" >}} 

Those were the values I found on the datasheet:

For the Red LED:

 {{<image src="../images/8week/Vf_red.jpg" alt="Size of png extantion." size="40%" >}} 

 {{<image src="../images/8week/If_red.jpg" alt="Size of png extantion." size="40%" >}} 

For the Blue LED:

 {{<image src="../images/8week/Vf_Blue.jpg" alt="Size of png extantion." size="40%" >}} 

 {{<image src="../images/8week/If_Blue.jpg" alt="Size of png extantion." size="40%" >}} 

Finally, I run the “rule checker” and I come up with this error:

 {{<image src="../images/8week/switchingname_violation.jpg" alt="Size of png extantion." size="60%" >}} 

It is strange but I just switch the name back again to the original one and it worked:

 {{<image src="../images/8week/originalname_noviolation.jpg" alt="Size of png extantion." size="60%" >}} 

Next, I tranfer this file to the "board editor" design tool to do the design.

I start making a shape I find interesting. The idea for this week is also experiment all the tools and designs we want.

 {{<image src="../images/8week/do_shape.jpg" alt="Size of png extantion." size="40%" >}} 

By looking to the shape above I immediately thought about a fish. I also added a circle to be like a keychain but it also looks like the eye of the fish.

Before drawing I changed the grid to 1.0000mm.

{{<image src="../images/8week/grid.jpg" alt="Size of png extantion." size="40%" >}} 

And this is how it looked:

{{<image src="../images/8week/fish_shape.jpg" alt="Size of png extantion." size="40%" >}} 

Then I realised I forgot to do something. I forgot to change the layer for the outside and inside drawings. Those lines should be on the “Edge.Cuts” layer.

{{<image src="../images/8week/change_layer.jpg" alt="Size of png extantion." size="40%" >}} 

We don’t have to draw everything again. We can simple change the layer on the “Properties”.

{{<image src="../images/8week/final_fish_shape.jpg" alt="Size of png extantion." size="60%" >}} 

Next I draw the red lines you can see on the image above. However, before that I had to change the “Net Classes” under “edit Pre-defined Sizes…” and flip the board upside down. We can confirm if it worked because the pins turned blue (colour for the bottom face).

Lastly, I decided to include some text. I wrote “Fixe” which is a way to say “cool” in Portugal and it’s also pronounced the same way as “fish”.

{{<image src="../images/8week/fixe.jpg" alt="Size of png extantion." size="60%" >}} 

Finally we have to test if everything is working. We can do that the same way as the previous software by clicking on the “rule checker”.

In my case I discover that the teeth weren’t made properly so I change the design to look like this.

{{<image src="../images/8week/change_theeth.jpg" alt="Size of png extantion." size="40%" >}} 

When everything works we can see the design in a 3D view. Here I’m showing two different designs for the text. Couldn’t decide the one I liked the most.

{{<image src="../images/8week/3D_view_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/8week/3D_view_2.jpg" alt="Size of png extantion." size="40%" >}} 

## COW

The idea behing this design is beacuse we also had to include headers to our board. 

I took so long to understand what are hearders! I had to make some research and i come up realizing what it is and what is used for.

There are two types of headers, male and female, respectively:

{{<image src="../images/8week/male_header.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/8week/female_header.jpg" alt="Size of png extantion." size="40%" >}} 

And this is an example of both of them placed in a board:

{{<image src="../images/8week/m_f_headers.jpg" alt="Size of png extantion." size="50%" >}} 

After seeing this image I finally understood how they look like and what they are used for.

It's actually pretty useful and we have used already a male header in the cables on the week 6.

{{<image src="../images/8week/cable.jpg" alt="Size of png extantion." size="40%" >}} 

Once again i started a new project, inserted the XIAO board, ground and power.

Next I go to the “fab” library and I try to look for headers. There were so many options and all of them hard to understand what they actually were. 

{{<image src="../images/8week/look_for_header.jpg" alt="Size of png extantion." size="60%" >}}

I decided to just go to the respective datasheets and figure them out. 

{{<image src="../images/8week/info_headers.jpg" alt="Size of png extantion." size="60%" >}}

Never the less I decided to choose a simple 2 pin male header and I inserted it to the board as follow.

{{<image src="../images/8week/xiao_header.jpg" alt="Size of png extantion." size="60%" >}} 

Next I run the “rule checker” and everything looks fine. Therefore I move on to the “board editor” and I upload the design I just made. 

This is the first design and the respective 3D view that I come up with.

{{<image src="../images/8week/cow_header.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/8week/3D_cow_header.jpg" alt="Size of png extantion." size="40%" >}}  

Later on I decided I wanted to have more elements so I decided to put a female header. I’m not quite sure if this one is a female. Was a bit hard to guess since the names are not that much suggestive.

This is the final look for the caw design.

{{<image src="../images/8week/final_cow_header.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/8week/final_3D_cow_header.jpg" alt="Size of png extantion." size="40%" >}}  

## All together


I realised that we were supposed to do everything I just did but all together in the same project. Meaning that I would have to try to add buttons, lLEDs, headers, etc in the same PCB board. 

Therefore, I decide to make a last design where I add all the components together.

However, when I was just about to start this project an error appear to me.

{{<image src="../images/8week/error.jpg" alt="Size of png extantion." size="70%" >}} 

I tried to remove and add the library back again. I changed the directory of the library just in case. I reinstalled KiCad but still the same error keep appearing. I still don’t know what happened and im waiting for the professor to recover from flue to see if he managed to understand this error.

Never the less I tried to move on and ignore the error, but unfortunately I couldn’t even proceed further since the “board editor” icon doesn’t even appear in the tool bar anymore.

{{<image src="../images/8week/no_appear.jpg" alt="Size of png extantion." size="70%" >}} 

I couldn’t keep waiting for the professor, so I just decided to go to the lab and use the KiCad software from there. The work flow was so much better and I’m glad I did it.

Once again I started a new project and I added all the elements as already explained deeply above.

{{<image src="../images/8week/all_white.jpg" alt="Size of png extantion." size="70%" >}} 

Next, after doing the “rules check” I move on to the “board editor” where I make the same “fixe” design I did above.

{{<image src="../images/8week/noShape_black.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/8week/shape_black.jpg" alt="Size of png extantion." size="40%" >}} 

Then I put the inner cables. Where I have to make sure that first I change the parameters of the “Net Classes” like explained above.

{{<image src="../images/8week/inside_cables.jpg" alt="Size of png extantion." size="60%" >}} 

Almost at the end of the design I heard the feedback from my colleagues that I should flip the board unless I engrave both sides. 

Thus I move on to flip the board again so it’s facing the front face. In order for the design to work and to prevent for it to not be messy I change the  position of my elements on the board.

{{<image src="../images/8week/flip_white.jpg" alt="Size of png extantion." size="60%" >}} 

{{<image src="../images/8week/flip_black.jpg" alt="Size of png extantion." size="60%" >}} 

{{<image src="../images/8week/fixe_3D.jpg" alt="Size of png extantion." size="60%" >}} 

Meanwhile I checked the “rule checker” to see if everything was good. 

Lastly, I decided to create a “Filled Zone” where I would choose the ground to be the one.

To do that we go to the right side and we press the “Filled Zone” tool. 

{{<image src="../images/8week/filled_zone.jpg" alt="Size of png extantion." size="40%" >}} 

We press somewhere on the design and a window will pop up.

{{<image src="../images/8week/settings.jpg" alt="Size of png extantion." size="60%" >}} 

We then select “PWR_GND” and hit “OK”.

Afterwards we finish drawing around the design lastly we press “b”. 

This is what it looks like after doing this action.

{{<image src="../images/8week/press_b.jpg" alt="Size of png extantion." size="60%" >}} 

Next, I saw how it looked on the “3D view”.

{{<image src="../images/8week/with_ground.jpg" alt="Size of png extantion." size="60%" >}} 

This is the final design for this week where I tried to incorporate all possible elements as possible.
