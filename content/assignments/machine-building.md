---
date: 2023-04-06T10:58:08-04:00
description: ""
featured_image: "/images/assembly.jpg"
tags: []
title: "Week 12: Machine Building"
weight: 110
---

For this week we had to build a machine in group. The machine we had to build was the Original Prusa MINI+. 

Here is the [group documentation page](https://aaltofablab.gitlab.io/machine-building-2023-group-b/) where we documented everything.

The building process itself was very straightforward and with no difficulty. The calibration part was the most challeging one since we had to do several test and take conclutions.

We all were present for the building and calibration and we all had our roles ranging from finding the correct screws and screwing them, to a translator of technical English words.

My contribution for the group was on both the building and calibration as im a very curious person and i would like to build my own prusa some day so mind as well learn how to do it at every stage right now.

I was present all the time for the building and i was the one reading the guide/instructions and giving to my collegues the correct screws and nuts as well what to do. I wasn't the only eye on the guide as we all can make mistakes so mind as well have double check all the time from my collegues.

Beacuse im a very handy and manual person i also did some screwing (which is the most exiting part !!!).

Here's a picture of the machine on the last stage of the building.

{{<image src="../images/12week/final_5_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/12week/final_5_2.jpg" alt="Size of png extantion." size="40%" >}} 

I was also present on the calibration part. Was a bit more challenge when it came to finding the correct position for the z-axis. We did several test and we took conclutions. I helped discussing the tests and making conclutions in group.

Here are some photos of crucial moments of dicussion.

{{<image src="../images/12week/analyse.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/12week/tilted.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/12week/bad.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/12week/two.jpg" alt="Size of png extantion." size="40%" >}} 

As you can see the second time we printed the cube it worked and there were good presision on the X,Y,Z-axis:

{{<image src="../images/12week/dimention_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/12week/dimention_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/12week/dimention_3.jpg" alt="Size of png extantion." size="40%" >}} 

Conclusion, Like i mentioned im a very manual and handy person and therefore i really loved to build this machine where i finally put my hands on the work. 

Now, im ready to build my own printer and pay less money for it uhuhuh.
