---
date: 2023-02-02T10:58:08-04:00
description: ""
featured_image: "/images/git_root.jpg"
tags: []
title: "Week 3: Project Managment"
weight: 20
---

<!---use JPEG for the fotos !!!!!
 for video is H.264 !!!! -  Handbrake - Imagemagick - audio less then 1 M specially

 create a copy folder of the images if we want to resize them all at the same time 

 For video if we want as a gif we remove the "controle" tag on the video.html
--->

<!---
Add a video:

{{<video src="video.mp4">}} This is the encoded video{{</video>}}
--->

We have reached the third week of assignment and final week of learning how to work with Git and Hugo. 

At this stage we have created a website and style it either with bootstrap, Hugo templates or even plain CSS for those who have courage.

## How did I created my website?

My first website attempt was just plain html with no style. Was the first things we learned and the following steps describe how i did it:

1. [Learn Git](#learn-git)
2. [Create GitLab account](#create-gitlab-account)
3. [Create your first webpage and publish it on GitLab](#create-your-first-webpage-and-publish-it-on-gitlab)
4. [Create a GitLab page](#create-a-gitlab-page)

However, later in the course we were introduced with [Boostrap](https://getbootstrap.com) and [Hugo](https://gohugo.io) to not just make our lives easier, since we can code in markdown and not plain html, but also to style our webpage with lower effort. 
I decided to use an [Hugo template](https://themes.gohugo.io) and this is how I did it:

- [Using Hugo Template](#using-hugo-template)

Last but not least, we were also thought how to capture and optimaze images and videos in order to make them less heavy and therefore easier to load the page. 

This is crucial for those that unfortunatly don't have access to good internet connections like we do here in Finland.

- [Image and Video optimization](#image-and-video-optimization)

### Learn Git 

We first learn the basics before jumping into something more complex. Those basics we have to acquire are the simple Git commands we will be using along the course.

Since I’m an engineer student, I’ve already worked with git and those I didn’t learn anything new. 

The simple commands we have to master are the following:

```
$ git pull
$ git status
$ git add .
$ git commit -m “(something)“
$ git push
```

We’ve also learn other commands I don’t personally use to much such as:

```
$ git diff
```

### Create GitLab account 

On this step I bumped into more problems since I was trying to login in into Aalto GitLab and not the official GitLab. 

Unfortunately the first option doesn’t work for creating a GitLab page and those I move one to the second option. 

It can be a bit terrifying since they ask for the bank account but the professor said they won’t charge money.

After being logged in we had to create a new project to which I called “digital-fabrication”.

For the next step we have to clone the repository of that project. To do so we just have to copy the SSH or HTTPS links. You can find those by pressing the “clone” button on the home page of the “digital-fabrication” project.

{{<image src="../images/clone.jpg" alt="Clone the ssh or https links." size="50%" >}}

### Create your first webpage and publish it on GitLab

Finally it was time to see some action going on. 

We first created a folder for our website.

I did it by typing on the Mac terminal:

```
$ cd Desktop
$ mkdir website
```

Inside that folder we created a file called “index.html”. This is the first file the browser search for, meaning this will be our home page of the website.

To create a file using the command line:

```
$ cd website
$ touch index.html
```

You can confirm if it worked by typing:

```
$ ls
```

**Remember about that SSH key or HTTPS link you copied on the previous step?** Well, now its time to use it!

Still inside the “website” folder, I typed the following:

```
$ git clone https://gitlab.com/arianamarta/digital-fabrication.git
```

By doing so, I have cloned the “digital-fabrication” repository inside that folder. Meaning that with the use of git commands I can pull and push content to the repository that is on my GitLab account.

You can confirm that it worked by typing:

```
$ ls -a
```

If you see a “.git” file, like shown on the figure bellow, it means it worked. 

{{<image src="../images/git_file.jpg" alt="Git file present" >}}


- **Now is time to edit the HTML file** so we can see some content on the browser.

Inside the website folder I wrote the following command to open Visual Studio Code:

```
$ code .
```

We wrote a simple code just for the home page. 

We also learned a bit of HTML. We learned about the most used tags such as head, body, lists, links, etc.

We played around with those tags and we even created other HTML files such as “final_project.html” so we could call those on the “index.html”. Those other HTML files can be seen as other web pages and we can access them by providing a link to those on the home page “index.html”.

For example, inside “index.html” we could reference to “final_project.html” file by typing:

```
<a href=“/final_project.html”>Final Project</a>
```

Finally, time to publish it on the GitLab.

To do so we have to put in practice all the Git commands we leared the previous weeks.
    
First lets see the status of our repository:
    
```
$ git status
```

We will only see in red the creation of the "index.html" file, since that was the only thing we did. 
    
We want to add the changes to our repository and we can do so by just simple type:
    
```
$ git add index.html
```

Or in case we have several changes and we want to add them all we can do that by typing:
    
```
$ git add .
```

Next we have to commit the changes. This is good to keep track of what we changed. This is even more important when more than one person is working on the same repository/project.
    
```
$ git commit -m "Creation of index.html file."
```

Feel free to type again "git status" and you will see that the main branch is up to date.
    
Finally, it's time to push the changes to the repository by typing:
    
```
$ git push
```

**Note:** You might be asked for a password. That will only happen if you had cloned your repository using https.If you used SSH you have generated a key pair and thus no need to type the password everytime you push to the repository.
    
### Create a GitLab page
    
In order to create a GitLab page we have to create an .gitlab-ci.yml file. 

To do so go to the main page of your project on GitLab and look for a plus simbole like shown on the figure bellow:

{{<image src="../images/plus.jpg" alt="Plus icon." >}}

Press "new file" and you will be directed to another page where you will write ".gitlab-ci.yml" on the "file name" space. 
    
By doing so, a template will appear and you have to choose "html".
    
You can see the steps i just discribed on the figure bellow.
    
{{<image src="../images/template.jpg" alt="Template file." >}}
    
After that you can scroll down the page and press "commit changes".
    
Now, you have just created a pipeline. You can see what you just created by going to "CI/CD" tab on the left and click on "Pipelines".
    
You can also see the status of it by going to "digital-fabrication" home page. The pipeline will be running until the blue icon with a timimng its still appering.
    
{{<image src="../images/blue.jpg" alt="Blue icon." >}}
    
And the pipeline will be ready when you see a green tick like so:

{{<image src="../images/green.jpg" alt="Green icon."  >}}
    
Finally, go to "Settings" -> "Pages" and you will find the link to your website.
    
{{<image src="../images/link.jpg" alt="Link to website." size="60%" >}}
    
Copy the link and paste it in another browser tab and voilá! Magic!

    
## Using Hugo Template
    
* The first step is definetly install hugo in our computer.
    
Since I'm a Mac user i just ran the following on my terminal:
    
```
$ brew install hugo
```

To check if the installation succeded and where it's located you can type on the terminal as shown on the image bellow:
    
{{<image src="../images/version.jpg" alt="Check version and location of hugo.">}} 
    
* Next step is to create a folder where we want to start a new hugo site.
    
I created the folder like i did last time by typing this time:
    
```
$ cd Desktop
$ mkdir digital-fabrication
$ cd digital-fabrication
$ hugo new site
```

Now, I've created a new hugo site inside "digital-fabrication".
    
* Now its time to choose the template I will be work with.
    
I went trough a several of options and i decided to pick the ["Ananke" theme](https://themes.gohugo.io/themes/gohugo-theme-ananke/).
    
* Next i initiated git:
    
```
git init
```

And again the same ".git" folder was created.
    
* Now I have to Clone the Ananke theme into the themes directory, adding it to my project as a Git submodule.
    
(Fist i clone it without behing a submodule but latter when i tryed to git push to my repository i had an error saying i had two repositories overlapping. Thus, in order to prevent the error to happen i cloned the "Ananke" theme repository as a submodule.)
    
```
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke themes/ananke
```

* Next, i changed the "config.toml" file
    
The theme name on the line ' theme = "ananke" ' has to exaclty match the name of the folder inside the themes folder. In my case its "ananke".
    
I've also changed the baseURL with the link where i have my website: "https://arianamarta.gitlab.io/digital-fabrication/"
    
* finally i started hugo server to see if it worked.
    
```
$ hugo server    
```

And guess what?? It's up and running !!! :partying_face:
    
* Now that i have the theme installed, it's time to edit with the contant i want.
    
For that i created three folders under content folder called "about", "assignemnts" and "final project".
    
Under assignemnts i will have a markdown file for each week assignment. 
    
Besides that i will also have an images folder to where ill store all images for this assignment section.

### Conclution
    
We learned the basics of how does Hugo function in the lecture. Basic things such as what each folder mean (content, layouts, shortcode, static, etc). We've also leaned about to important markdwon files where we have to be carefull with their naming. The files are "_index.md" and "single.md".

The first is the one responsible for the home page and the last is the template for all other markdown files linked to other pages.

I'm learning more about hugo as I go. 
    
Everytime I want to do something more complex I either ask the professor or I go to the [Hugo shortcodes]("https://gohugo.io/content-management/shortcodes/") page. 
    
We can pretty much do everything we want throught the shortcodes. So I recomend reading through that page.
    

## Image and Video optimization
    
Fisrt we were thought how to capture both images and videos.
    
To do a screenshot on Mac we just have to press (Shift+Command+3). If we want just a specific area of the page we can press (Shift+Command+4). The last is pretty handy and the one i use the most.
    
There's another command that besides doing what i just explain, it also allows for screen recording which can be done by pressing (Shift+Command+5).

There's one more thing we have to do. We have to install [Magick]("https://imagemagick.org") to optimaze images and [Ffmpeg]("https://ffmpeg.org") for the videos.

Once again, as a Mac user i just install it through homebrew:

```
$ brew install magick
$ brew install ffmpeg
```

Confirm the installation by typing:

```
$ which magick
$ which ffmpeg
```
    
* ## How to optimize the images? 

1. #### Make sure you use jpeg over png

Every time I take a screenshot, the default extation is always png. But we want to aim for jpeg since its less heavy (less MB) as you can see by the following images.

{{<image src="../images/size_png.jpg" alt="Size of png extantion." size="50%" >}}

{{<image src="../images/size_jpeg.jpg" alt="Size of jpeg extantion." size="50%" >}}

A way to convert it to jpeg can be done with “magick” by typing the following:

```
$ magick size_png.png size_png.jpg
```

2. #### Reduce the size of the image 
    
Another trick to reduce the MB of the images is reducing their size.
    
An optimal value for the longest edge is 1200px. Exceeding 1920px along that edges is not recomended.
    
Note that if the image is already small theres no need to make it 1200px otherwise that change will increase the amount of Bytes of the image. And goal is to reduce it and not make it more heavy.
    
I went to this [website]("https://tiny-img.com/blog/best-image-size-for-website/") to see the correct measurements for the image sizes.
    
Once again a way to do it is by using magick:

```
$ magick convert canvas.jpg -resize 1200x630 canvas_1200x630.jpg
```
As you can see by the images bellow, the original image was 460KB and the resized version only 174KB

{{<image src="../images/bits_resize.jpg" alt="Bits of the resized image.">}}
 
And also you can see the size of the previous images and how short it became after the resize.
    
{{<image src="../images/size_resize.jpg" alt="Size of the resized image.">}}
    

3. #### Reduce the quality of the image

Finally, there's another commun strategy to make the image less heavy wich is reducing the quality of the image.
    
In general 65% quality is enough for most images and 75% for those who need to look a bit better.
    
We can do it using -quality tag for example:
    
```
$ magick convert canvas_1200x630.jpg -quality 65 canvas_1200x630_65.jpg
```

As you can see by the image bellow the Bytes reduced significantly:
    
{{<image src="../images/bits_quality.jpg" alt="Bytes of the quality image.">}}
    
We can wrap all this in just one line of code:
    
```
$ magick convert canvas.jpg -resize 1200x630 -quality 65 canvas_1200x630_65.jpg
```

Later i found out we can just use one line of command line to convert all the images that have .jpg as extantion. Knowing this changed my life as it made it much easier and quicker.

``` 
$ magick mogrify -resize 1200x1200 -quality 65 *.jpg 
``` 

I also discovered we can do the same thing to convert all *.png to *.jpg. Also a life changing.

``` 
magick mogrify -format jpg *.png
``` 

* ## How to optimize the videos? 

What we normally do is going to this [homepage](https://trac.ffmpeg.org/wiki/Encode/H.264) and see its documentation.
    
1. #### Choosing CRF valeu

We can start by choosing a CRF value which has basically to do with the quallity of the video. The smallest the number the higher the quality. The default valeu is 23: "-crf 35"

The "-c:a copy" means we want to copy the sound.

```
$ ffmpeg -i 2button.mp4 -c:v libx264 -crf 35 -c:a copy 2button_crf_35.mp4
```

2. #### Choose a Preset

It defines how fast the encoding works. The faster the encodes the bigger the file size.

Lets try the CRF along the slow preset and compare the file sizes: "-preset slow"

```
$ ffmpeg -i 2button.mp4 -c:v libx264 -preset slow -crf 35 -c:a copy 2button_crf_35_slow.mp4
```

3. #### Scale the video

Another thing we can do its scale the video down. Intuitively the smaller the video the smaller the file size.

You can look to this [page](https://trac.ffmpeg.org/wiki/Scaling) for more detailed information. 

Lets add that to the previous line. Don't forget to define the pixel format: "-vf scale=720:-1 -pix_fmt yuv422p"

```
$ ffmpeg -i 2button.mp4 -vf scale=720:-1 -pix_fmt yuv422p -c:v libx264 -preset slow -crf 35 -c:a copy 2button_crf_35_slow_s720.mp4
```

4. #### Audio 

We can also specify the audio. We can look at this [page](https://trac.ffmpeg.org/wiki/Encode/AAC) for more functionalities.

We can set the audio codec and the bit rate: "-c:a libfdk_aac -b:a 128k".

```
$ ffmpeg -i 2button.mp4 -vf scale=720:-1 -pix_fmt yuv422p -c:v libx264 -preset slow -crf 35 -c:a libfdk_aac -b:a 128k 2button_crf_35_slow_s720_aAACb128.mp4
```

There's also an option to show the video without audio and in that case we can do: "-an" which stands for "audio none".

```
$ ffmpeg -i 2button.mp4 -vf scale=720:-1 -pix_fmt yuv422p -c:v libx264 -preset slow -crf 35 -an 2button_crf_35_slow_s720_an.mp4
```

As you can see on the image bellow, this option (3th one of the image) reduces a lot more the size of the file by hundreds of bytes.

The bellow image shows the size of the original video (2button.mp4) on the left, of the 2button_crf_35_slow_s720.mp4 in the middle and of the video with no sound on the right.

{{<image src="../images/size_video.jpg" alt="Sizes of the different stages of optimazing the video.">}}


