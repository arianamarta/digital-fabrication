---
date: 2023-05-25T10:58:08-04:00
description: ""
featured_image: "/images/amp_imp.jpg"
tags: []
title: "Week 18: Applications and Implications"
weight: 170
---

We were asked to respond to some questions to create the **BOM** (Build Of Material)
 
**What will it do?**

The project can be seen as an exhibition about “Mental health”. 

There will be an input box with 9 buttons where each button correspond to a word/mood/feeling and there will be an output brain that will react differently depending on the buttons selected by the user.

If the user is feeling good, and thus presses a good button, the brain will light up some LEDs inside of it with an happy and insightful animations.

However if the participant is feeling bad, and thus presses a bad button, the brain will deflate. Exactly the same as we feel when we are sad or tired. It feels like our brain is heavy and smooshed.  

Finally I will display the overall mood of the participants of Aalto. There will be two graphs. One showing the number of people that picked the feeling and another one showing the percentage of how many times a felling was selected by the participants.

I find this website very interesting and useful to understand how the overall students are feeling. This could be good as an interactive study about mental health for schools and universities. 
 
**Who has done what beforehand?**

I didn’t search for any inspirations from anywhere. I come up with the whole idea by myself. So I don’t know if there is any kind of similar project as mine.

We can say though, that I thought about making my project as an exhibition about something important for the society, after I saw the professore’s exhibition about (for my understanding) climate change and the human influence on the nature and ecosystem.

**What will you design?**

I will have a lot of design to do.

For the input box: 
- I will have to design the shape of the PCB
- Design the mold for the silicone keys as well for the individual keys for testing
- Design the Box itself which is made out of three pieces.

For the output brain:
- I had to design the brain itself
- The mold box for the brain
- The support for the brain
- The small attachment pieces to support the brain from falling/tilting
- The silicone mold for the base of the support 
- The small attachment for the pump and air tubes
- The small box that will hold the components (its made out of two pieces)
- The big plywood box that will be hiding all of those and supporting the brain

**What materials and components will be used?**

My project is very big and has so many things happening and therefore a lot of materials and components:

For the input box:

1. Components:

	- 1 PCB
	- 9 buttons
	- 9 NEON LEDS
	- 1 Voltage Converter
	- 1 switch
	- 1 mux 1:16
	- 1 display
	- 1 XIAO board
	- 2 lithium batteries and the case

2. Materials:

	- PLA for the box
	- 40g of dragon skin (silicone)
	- 4 M2 screws

For the output box:

1. Components:

	- 1 PCB
	- 1 micropressure sensore
	- 2 diodes
	- 1 Voltage Converter
	- 2 Mosfets
	- 1 pump (12V)
	- 1 valve
	- 1 XIAO board
	- 1 NEON LED strip with 6 LED in total 
	- 250V to 12V transformer and cable

2. Materials:

	- PLA for the box
	- 1 plywood 4mm
	- 1 packs of dragon skin (silicone)
	- 9 zip ties
	- 2* 4 M5 screws and nuts
	- 4 M2.5 screws and nuts
	- 4g of Epoxy


**Where will they come from?**

All the materials are at the lab and most of them were bought from [DigiKey](https://www.digikey.fi/en).
The LED strip and the Epoxy it was bought by me. The LEDs where bought in clas ohlson and the Epoxy from K-ruoka.

**How much will they cost?**

The diodes and mux its an estimation since I couldn’t find the exact same I used.

DigiKey:
- 2 [XIAO boards](https://www.digikey.fi/en/products/detail/seeed-technology-co-ltd/113991054/16652880) = 2* 4.62€
- 9 [buttons](https://www.digikey.fi/en/products/detail/cui-devices/TS04-66-50-BK-100-SMT/15634347?utm_adgroup=&utm_source=google&utm_medium=cpc&utm_campaign=PMAX%20Shopping_Supplier_Focus%20Supplier&utm_term=&productid=15634347&gclid=CjwKCAjw-vmkBhBMEiwAlrMeF6PUkiBIcgDUNM-z_Ex9QX1-ll-uqP-GzIC15n9SymRNAcr2NByLrxoClIwQAvD_BwE) = 9* 0.21€
- 9 [addressable LED](https://www.digikey.fi/en/products/detail/adafruit-industries-llc/2758/6134706) = 9* 0.55€
- 1 [mux](https://www.digikey.fi/en/products/filter/signal-switches-multiplexers-decoders/743?s=N4IgTCBcDaILYFcAeIC6BfIA) = 0.49€
- 1 [display](https://www.digikey.fi/en/products/detail/adafruit-industries-llc/326/5353680) = 16.19€
- 2* [Converter](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwi6ve-GqOv_AhVK-lEKHbbcCjwYABAaGgJ3cw&ohost=www.google.com&cid=CAESauD2X337dfimS-Mvl7sfqcWlxv6bl8svxxAVa1zwLyokzBL2w04EyYMSpKtDA6O1lWK-671X6ZD0YE6PXHzcFdm0MOkPv7ZoCrbVfb9xbn0hofFV2v6D5JEWRmGLiNdCk-jPXCraUxIQnaQ&sig=AOD64_0vjgOx3JXGl0rwoYSxh2UcsO67eQ&ctype=5&q=&ved=2ahUKEwj33eaGqOv_AhUzSvEDHaN0A9AQ9aACKAB6BQgGEIIB&nis=2&adurl=) = 2* 2.72€
- 2* [Switch](https://www.digikey.fi/en/products/detail/e-switch/R1966ABLKBLKFF/210519) = 2* 1.66€
- 2* [diodes](https://www.digikey.fi/en/products/filter/single-zener-diodes/287?s=N4IgTCBcDaICYEsD2cCmBnEBdAvkA) = 2* 0.20€
- 2* [Mosfets](https://www.digikey.fi/en/products/result?s=N4IgTCBcDaIHYFoC2B7AzgMwKYBcQF0BfIA) = 2* 0.59€
- 1 [valve](https://www.digikey.fi/en/products/detail/adafruit-industries-llc/4663/13170960?s=N4IgTCBcDaIGwDUAEBBAlgJyQghgGwDcBTEAXQF8g) = 2.73€



De-smec:
- 2* [lithium batteries](https://fi.rsdelivers.com/product/samsung/icr18650-26f/samsung-icr18650-26f-37v-18650-lithium-ion-battery/7887261?cm_mmc=FI-PLA-DS3A-_-google-_-CSS_FI_EN_Batteries_%26_Chargers_Whoop-_-(FI:Whoop!)+Speciality+Size+Rechargeable+Batteries-_-PRODUCT_GROUP&matchtype=&pla-336186248382&s_kwcid=AL!14853!3!663751521337!!!g!336186248382!&gclid=CjwKCAjw-vmkBhBMEiwAlrMeF3ZBAsWzIb_AuIKwP8gmrRgLNqI_DB47bfSGXio5RGc09AVj2c_bjhoCp_gQAvD_BwE&gclsrc=aw.ds) = 2* 17.70€

Elfa Distrelec Suomi:
- 1 [micro pressure sensor](https://www.elfadistrelec.fi/fi/paineanturikortti-qwiic-micropressure-sparkfun-electronics-sen-16476/p/30216195?cq_src=google_ads&cq_cmp=17726976400&cq_con=&cq_term=&cq_med=pla&cq_plac=&cq_net=x&cq_pos=&cq_plt=gp&gclid=CjwKCAjw-vmkBhBMEiwAlrMeF3PZwDpamZ5TtHNuI0Z2D-KgMzISzzOiC6QAB1uj1Y4IamvBsU-bvxoCinYQAvD_BwE&gclsrc=aw.ds) = 18,20€
- 1 [pump](https://www.elfadistrelec.fi/fi/alipainepumppu-12v-sparkfun-electronics-d2028b/p/30145494?cq_src=google_ads&cq_cmp=17726976400&cq_con=&cq_term=&cq_med=pla&cq_plac=&cq_net=x&cq_pos=&cq_plt=gp&gclid=CjwKCAjw-vmkBhBMEiwAlrMeF0dY5dopUbv4Un5CtQCMOz7RIsO_Qe-wMqP-tDD7VMYByNxv8EgxARoCp-gQAvD_BwE&gclsrc=aw.ds) = 25.53€

K-ruoka:
- Epoxy = 12.99€

Clas ohlson:
	- 3m LED strip = 50€ -> I just use around 4€ of that.

FabLab: 
- 1 plywood 4mm = 5€ max ( I don’t know the exact value)

Other places:
- 1 pack [Dragan skin silicone](https://www.kaupo.de/shop/en/SILICONE-RUBBER-Platinum-Cure/DRAGON-SKIN-SERIES/Dragon-Skin-30/Dragon-Skin-30-1-Silicone-Rubber.html) = 50€ to 100€ (depending where we buy)
- 9 [zip-ties](https://www.partyking.fi/nippuside-musta-83516.html?gad=1&gclid=CjwKCAjw-vmkBhBMEiwAlrMeF9MA9cmgnxZJKxudq6GBaabYzQaV95Y9PT7c8iVnPXg134X3__JrfBoCWYMQAvD_BwE) = 9* 0.29€
- 1 small pack full of screws and nuts in k-ruoka costs around 2.50€

**What parts and systems will be made?**

For the input box: 
- I have to make the circuit board and mill using the MDX-40
- I will have to solder all the eletronics
- I will have to make the desin of the box in Fusion 360 and print it in a Prusa printer in PLA
- I will have to design and print in PLA in Prusa printers the mold box for the keys 
- I will have to cast the keys with dragon skin silicone.

For the output brain:
- The design of the PCB and mill it in the MDX-40
- Solder all the electronics 
- I will have to make the design of the brain and print it in PLA in Prusa printers
- The design of the mold box for the brain and print it in PLA this time in the Ultimaker +2 Connected
- The design of the brain support and print in PLA in Prusa printers
- The design of the mold for the brain support and print in PLA in Prusa printers 
- The design of the small inner box and print in PLA in Prusa printers
- The design of the outside box and cut it in the laser cut machine
