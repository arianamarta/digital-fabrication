---
date: 2023-03-23T10:58:08-04:00
description: ""
featured_image: "/images/pcb_milling.jpg"
tags: []
title: "Week 10: Electronics Production"
weight: 90
---

This week we learned how to mill the circuit boards we design on the week 8 "Electronics Design" using Roland SRM-20 machine.

I first started by following the lecture video to learn how to save the design we made in the correct file extension. So then we could upload those files in CopperCam software.

Weeeell I saved the files only on the computer so when i arrived to the lab the next day to start milling the computer has vanished.  

Thefore i decided to move on to the next week assignment and create a easy design for a board that incorporates and output device. I decided to go for a speaker. 

When the computer finally returns i'll mill the first design and go back to the speaker.

Table of content:

* [Generate Files](#generate-files)
* [CopperCam](#CopperCam)
* [VPanel](#vpanel)
* [Solder](#solder)
* [Testing the PCB](#testing-the-pcb)
* [Note](#note)

## Generate Files 

So, this is the design I made on the week 8.

{{<image src="../images/10week/before_1.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/10week/before_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/10week/before_3.jpg" alt="Size of png extantion." size="40%" >}} 

However, before start milling I was taught that I should flip the XIAO board otherwise I would have to put rivets to conduct the electricity from the back side of the board to the front side where I placed all the components. If ui put the board on the front side, I will be soldering it on the back side therefore I have to have the rivets to conduct the electricity from the back to the front side. However, if I flip the XIAO board I will have to insert it from the back and thus I will be soldering the pins on the front. Therefore there is no need for rivets.

With this being said this is how the design looks like after flipping the board.

{{<image src="../images/10week/after_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/after_2.jpg" alt="Size of png extantion." size="40%" >}} 

{{<image src="../images/10week/after_1.jpg" alt="Size of png extantion." size="40%" >}} 

Now, in order to export the Geber files, we go to the PCB editor and we press the “Plot” icon.

{{<image src="../images/10week/plot_icon.jpg" alt="Size of png extantion." size="40%" >}}

A window will pop up and we will have to make some changes. I followed the same flow as explained by the professor in the class.

We maintain the “plot format” as Geber. Then for the “Include Layers” we will just tick the front layer (F.Cu) since I don’t have any important information on the back layer. As for the “Plot on All Layers” we will just tick the “Edge.Cuts” layer with is the one that we will use to cut the outline. I also made sure I selected the desire folder on the “Output directory” section. I pretty much followed the guide and this is how it looks after setting all up.

{{<image src="../images/10week/plot_setup.jpg" alt="Size of png extantion." size="70%" >}}

After that, we hit “plot” and a file will be generated:

{{<image src="../images/10week/file_gen_1.jpg" alt="Size of png extantion." size="40%" >}}

Next we hit “Generate Drill Files” and this is how the setup should look like.

{{<image src="../images/10week/drill_setup.jpg" alt="Size of png extantion." size="60%" >}}

Then we hit “Generate Drill File” and it will generate 2 files. One PTH and one NPTH. The only file we will need os the PTH because for some reason CopperCam cant read NPTH.

{{<image src="../images/10week/file_gen_2.jpg" alt="Size of png extantion." size="50%" >}}

Finally we can see the Weber file we just produced by opening the “Geber Viewer” option on KiCad.

{{<image src="../images/10week/geber_viewer.jpg" alt="Size of png extantion." size="50%" >}}

By opening the front copper layer geber file we just saved this is what appears.

{{<image src="../images/10week/fixe_geber.jpg" alt="Size of png extantion." size="40%" >}}

Then we can basically see the brushes each part of the board have by right clicking on a hole for example and pressing “highlight”

{{<image src="../images/10week/highlight.jpg" alt="Size of png extantion." size="40%" >}}

We can also see the list of all the brushes.

{{<image src="../images/10week/where_list.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/brush_list.jpg" alt="Size of png extantion." size="40%" >}}

We can also add the drip files and we will be able to see all the layers and the tools decided on those specific layers.

Finally, this two files we generated, geber and PTH files, we have to open them on CopperCam to generate the toolpath.

## CopperCam

In CopperCam we start by opening a “New Circuit”

{{<image src="../images/10week/new_circuit.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/file_T1.jpg" alt="Size of png extantion." size="70%" >}}

We open the front copper layer file that has “-F_Cu.gbr” as extension.

{{<image src="../images/10week/fixe_1.jpg" alt="Size of png extantion." size="40%" >}}

Has you can see by the image bellow, the contour wasn’t defined so we have to do it. Click in one of the lines that will define the contour and right click on it. Then tick “Define as card contour”. Do the same for the eye that will be the inner contour (like a hole).

{{<image src="../images/10week/define_contour.jpg" alt="Size of png extantion." size="80%" >}}

Then we want to had the drills and for that we open “Drills”.

{{<image src="../images/10week/open_drill.jpg" alt="Size of png extantion." size="40%" >}}

And we choose the PTH file that we generated previously. 

{{<image src="../images/10week/PTH_file.jpg" alt="Size of png extantion." size="40%" >}}

The drills will be placed in the canvas but they won’t be properly aligned. 

{{<image src="../images/10week/no_position.jpg" alt="Size of png extantion." size="40%" >}}

The way to align them is by dong the following:

Make sure to have the “Layer 1” selected and then right click on the up-left hoop. Click on the “Set as reference pad”

{{<image src="../images/10week/ref_pad_1.jpg" alt="Size of png extantion." size="40%" >}}

Then, change layer to “Layer 5” which is the layer of the “PTH” file we just added. Right click on the up-left circule and “Adjust to reference pad #1”.

{{<image src="../images/10week/pad_1.jpg" alt="Size of png extantion." size="40%" >}}

Do the exact same for the bottom-right drill (don’t forget to change the layers accordingly):

{{<image src="../images/10week/ref_pad_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/pad_2.jpg" alt="Size of png extantion." size="40%" >}}

After this procedure it will be aligned perfectly.

{{<image src="../images/10week/yes_position.jpg" alt="Size of png extantion." size="40%" >}}

Next, we have to change the diameter of the holes accordingly to the size that we want the holes to be and also accordingly to the tool we have.

{{<image src="../images/10week/change_diameter.jpg" alt="Size of png extantion." size="40%" >}}

In my case I will choose 0.8mm for the holes since it’s the size of the tool I will use and also the perfect size for the pins I want to insert there

{{<image src="../images/10week/diameter.jpg" alt="Size of png extantion." size="50%" >}}

Then we have to set the board dimensions. For the margin I just set 1 since the board I will use doesn’t have that much place left. For the Z-thickness I put 0.2 mm more than the thickness of the board. I measure the thickness of the board using the caliper witch gave me a result of 1.6mm.

{{<image src="../images/10week/board_dimention.jpg" alt="Size of png extantion." size="40%" >}}

Afterwards we have to set the origin to zero on the X and Y-axis.

{{<image src="../images/10week/origin.jpg" alt="Size of png extantion." size="40%" >}}

Now, we have to select the tools just before starting saving the tool-path files.

{{<image src="../images/10week/select_tools_1.jpg" alt="Size of png extantion." size="40%" >}}

The default options is pretty much what I will use. Contrary to previous years, this time we will use just one tool for cutting and drilling. I also change the “Drilling depth” to 1.9mm with is 0.1mm more then the Z-thickness to make sure it drills all the way to the material. The 0.1mm is to account for the tape that we will put on the material.

{{<image src="../images/10week/select_tools_2.jpg" alt="Size of png extantion." size="70%" >}}

Finally, we have to calculate the contours by clicking on the “Calculate contours” tool.

{{<image src="../images/10week/calculate_contour.jpg" alt="Size of png extantion." size="40%" >}}

I first started by setting 3 units to both “Number of successive contours” and “Extra contours around pads like I saw in the video, but then the teaching assistance recommended to try first with less and only then increment it if needed.

So I change from 3 3 to 3 1 and I finally decided to stay with 3 0.

{{<image src="../images/10week/set_contour_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/set_contour_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/set_contour_3.jpg" alt="Size of png extantion." size="40%" >}}

This is the preview. I don’t exactly know what is the meaning of the red lines and no-one could actually explain me. They just said that it’s okay and that will work.

{{<image src="../images/10week/contour_fixe.jpg" alt="Size of png extantion." size="40%" >}}

Finally, we can start exporting the tool paths. For that we click on the “Mill” button.

{{<image src="../images/10week/contour_fixe.jpg" alt="Size of png extantion." size="40%" >}}

Since we are just doing one side milling we can define all the jobs we will do right now. Which consist on the engraving, cutting and drilling. For the cutting and drilling we will use the same tool. If we would mill the back layer also, we would have to first export the front layer and then go back to the milling tool and select “Engraving Layer #2”. The number one layer is the front layer and the number 2 is the back layer.

{{<image src="../images/10week/save_path.jpg" alt="Size of png extantion." size="80%" >}}

Also, make sure to use “White cross” as the “XY-zero point” and “Circuit surface” as the “Z-zero point”. It “OK” and choose the path were you want the files to be saved.

Now, is time to open another software “VPanel for SRM-20” that will manipulate the machine.


## VPanel

This are the tools we will need for this part:

{{<image src="../images/10week/tools_needed.jpg" alt="Size of png extantion." size="40%" >}}

When opening the software we have first set up before cutting.

First we click on “set-up” and we have to make sure we choose “RML-1” for the “Command Set” and “Millimetres” as the “units”

{{<image src="../images/10week/save_path.jpg" alt="Size of png extantion." size="80%" >}}

Then, we have to make sure we use the “User Coordinate System”. Which will be in accordance with the “Set Origin Point”.

{{<image src="../images/10week/user_cord.jpg" alt="Size of png extantion." size="70%" >}}

Along the years the TAs have tested and decided that it’s actually better to have the “Spindle Speed” with a percentage in the middle and not exactly to 100%.

Next, we have to attach the material we are going to cut. We were asked to try to use as much of the left over pieces as possible. Before attaching the material we have to put the double sided tape on the back of the material and brush the bed surface to remove potential dust.

{{<image src="../images/10week/brush.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/putting.jpg" alt="Size of png extantion." size="40%" >}}

The TA helping us made some tests on the bed and she said that the up-right corner was better and because I hade to use leftover pieces I had to cut the board so I could use that corner.

{{<image src="../images/10week/cutting.jpg" alt="Size of png extantion." size="40%" >}}

The next step is to zero the X,Y,Z-axis. For that I have to insert the tool I will be cutting.

I will first do the engraving and only then the drilling and cutting. For the engraving I will use the T1 tool that has an angle of 60 deg. We can see the color and the list of the tools in a sheet close to the machine so we are sure to not make a mistake and brake the tool because of that.

{{<image src="../images/10week/sheet.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/tool_color.jpg" alt="Size of png extantion." size="40%" >}}

Then, I use the arrows on the VPanel software to move the X,Y,Z-axis. I will choose the location where I want my origin to be and I will press the X/Y button to zero those axis.

{{<image src="../images/10week/zeroXYZ.jpg" alt="Size of png extantion." size="40%" >}}

Now, for the Z-axis we will first put the tool in the middle of where our PCB will be placed and we will lower the tool until we let a gap of moreless the size of the green line shown on the picture bellow.

{{<image src="../images/10week/zero_Z.jpg" alt="Size of png extantion." size="40%" >}}

Then we use the tool to unscrew the tool until it falls down to the material and we screw it back again. We click the Z button to zero the z-axis. 

Use the arrows to bring the tool up a bit before cutting. 

{{<image src="../images/10week/setup_XYZ.jpg" alt="Size of png extantion." size="60%" >}}

Now, we have setup everything and we have zero the axis we can finally start cutting.

For that we have to tick the “Cut” button and choose the correct file. Like I mentioned above the first thing to do is the engraving. The file generated for the engraving has the “-T1” extension. 

{{<image src="../images/10week/file_T1.jpg" alt="Size of png extantion." size="80%" >}}

Press output so we send the job to the machine.

{{<image src="../images/10week/output.jpg" alt="Size of png extantion." size="50%" >}}

A error will appear but we will just ignore it by pressing resume.

{{<image src="../images/10week/error.jpg" alt="Size of png extantion." size="50%" >}}

After the engraving is done we have to clean the remaining using a vacum.

{{<image src="../images/10week/clean_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/10week/clean_2.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks after cleaning.

{{<image src="../images/10week/engrave.jpg" alt="Size of png extantion." size="40%" >}}

Next, it’s time for cutting and drilling. We do the exact same procedure.

We first remove the engraving tool to the cutting tool. It’s the one that has 0.8 written on it. Then we maintain the same position for the origin but we have to zero the z-axis again.

After that we choose the “-T2” file which is the one generated for the cutting and drilling. 

This is the machine cutting.

 {{<image src="../images/10week/cut.jpg" alt="Size of png extantion." size="40%" >}}

Finally, I use some kind of spatula to remove it from the bed.

 {{<image src="../images/10week/remove_1.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/remove_2.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/remove_3.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/remove_4.jpg" alt="Size of png extantion." size="40%" >}}


## Solder

There’s only one more thing missing before uploading the program to the XIAO board and see if the PCB works which is solder the LEDs, Buttons and resistors.

I must say that this was a challenge because all those components are very small but I loved the challenge and I must say it went quite well. 

Like already explained above I have to solder the board from the back of the board. So I first started by adding some female headers so I can later insert the XIAO board on them.

 {{<image src="../images/10week/solder_1.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/solder_2.jpg" alt="Size of png extantion." size="40%" >}}

Then with the help of clamps I solder the little components.

 {{<image src="../images/10week/clamps.jpg" alt="Size of png extantion." size="40%" >}}

With the help of a multimeters I checked if the soldering and the connections were working properly. Judging buy the sound output they were.

 {{<image src="../images/10week/check_1.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/check_2.jpg" alt="Size of png extantion." size="40%" >}}

This is how it looks after soldering.

 {{<image src="../images/10week/final_solder_1.jpg" alt="Size of png extantion." size="40%" >}}

 {{<image src="../images/10week/final_solder_2.jpg" alt="Size of png extantion." size="40%" >}}

Next is time to test the board by uploading some program to the XIAO board.

## Testing the PCB

For testing the PCB I reused the same programs I wrote on the week 6 were I have two buttons and I used them to light the LEDs.

I just had to change the pins number on the code. Now I don’t use the LEDs from the XIAO board but the LEDs I solder on my board. So for that I change the code where I say which pins I placed the LEDs as well which ones I placed the Buttons. 

I made the button on the top light the led from the top and so on. 

Here is the code:

{{<image src="../images/10week/code.jpg" alt="Size of png extantion." size="40%" >}}

I uploaded the program to the board and this is what happened.

{{<video src="../images/10week/code_running.mp4">}}Code running.{{</video>}}

So happy that everything worked perfectly and that the board itself looks good. I’m proud with the fact I manage to solder the leds, buttons and resistors without braking them or destroy them.

## Note

As you can see on the board there is this strange engraving on the up part. 

{{<image src="../images/10week/strange_eng.jpg" alt="Size of png extantion." size="40%" >}}

That was suppose to be female headers but I clearly didn’t quite understand which now was which when I design it on KiCad so now I end up not having headers. Bit its fine, I learned with my mistake and now I succeeded on the week11. 














