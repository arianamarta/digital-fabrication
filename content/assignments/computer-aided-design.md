---
date: 2023-02-08T10:58:08-04:00
description: ""
featured_image: "/images/cad.jpg"
tags: []
title: "Week 4: Computer-Aided Design"
weight: 30
---
This week we learned how to work with a simple, easy and free source CAD software called [SolveSpace](https://solvespace.com/index.pl).

Personally I would have loved to use [Fusion 360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription). I've always wanted to learn it and this could finally be a good excuse for that. It sounds like a more professional and diverse sofware. However I didn't manage to make it work with the students free access so I just decided to go with the suggested option mention above. 

For what we have to design, SolveSpace works more than fine. 

Table of content:

* [SolveSpace](#solvespace)
* [My projects Design](#my-projects-design)
* [Comparation between SolveSpace and Fusion 360](#comparation-between-solvespace-and-fusion-360)
* [Conclusion](#conclusion)

# SolveSpace

We started by [downloading](https://github.com/solvespace/solvespace/releases/tag/v3.1) the software. 

Next, are the steps I found important to mention for later use and also to show my progress.

1. ### Select line and press H or V to make it Horizontal or Vertical.

{{<image src="../images/4week/same_h.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/4week/final_h.jpg" alt="Size of png extantion." size="40%" >}}
    
{{<image src="../images/4week/select_h.jpg" alt="Size of png extantion." size="40%" >}}

2. ### Make lines with the same lenght

{{<image src="../images/4week/equal_length.jpg" alt="Size of png extantion." size="50%" >}}


3. ### Create Toggle Contruction
    
{{<image src="../images/4week/toggle_construction.jpg" alt="Size of png extantion." size="50%" >}}

4. ### We want to bring the Degrees of Freedom down to zero
    
    {{<image src="../images/4week/6DoF.jpg" alt="Size of png extantion." size="50%" >}}

* We have to do something in order to not be possible to move any of the elements of the model. 

    1. Point Snapped to the center --> DoF reduced to 4!!
    
    {{<image src="../images/4week/point.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/4DoF.jpg" alt="Size of png extantion." size="40%" >}}
    
    2. Constraint and add Distance between two points
    
    {{<image src="../images/4week/distance.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/3DoF.jpg" alt="Size of png extantion." size="40%" >}}
    
    * I used contraints in 4 different lines of the bench. After doing that we reach 0 DoF.

     {{<image src="../images/4week/0DoF.jpg" alt="Size of png extantion." size="50%" >}}
    
5. ### Make it thick 
    * same procedure as 2D. We select the line and we press the shortcut key "D" and we type in this case "10" mm.
    
    {{<image src="../images/4week/mm.jpg" alt="Size of png extantion." size="50%" >}}

6. ### Change to two-side to make it simetrical 
    
    {{<image src="../images/4week/2side.jpg" alt="Size of png extantion." size="50%" >}}
    
7. ### Mirror to repeat the same part.
    
8. ### Add finger joints so we can snapp things together with that surfice we are creating
    
    * create scketchy plane (a work plane) by selecting a point and two lines connected to that point.
    
    {{<image src="../images/4week/grouping.jpg" alt="Size of png extantion." size="50%" >}}
    
    * creat a rectangule with 40 mm big size and with a 20 mm ofset from the center.
    
    {{<image src="../images/4week/rect.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/rect_final.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/rect_sizes.jpg" alt="Size of png extantion." size="50%" >}}
    
    * Click "extruding" option 
    
    {{<image src="../images/4week/extruding_option.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/extruding_final.jpg" alt="Size of png extantion." size="40%" >}}
    
    
    * make the sizes with equal lenght
    
    {{<image src="../images/4week/sizes_eq_lenght.jpg" alt="Size of png extantion." size="50%" >}}
    
    * mirror it again
    
9. ### Save the file
    
    
10. ### We want the rectangle to be symmetrical along the line and those are the following steps to do it.
    
    * create point online --> select point and line --> constrain/ on point
    
    The line will snapp to the point, next we select again the point and the line and we click "At midpoint" under constrain. It will keep the point always in the middle.
    
    {{<image src="../images/4week/p_L-onpoint.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/midpoint.jpg" alt="Size of png extantion." size="40%" >}}
    
    
    
    * When we want the figure to end in a specific place (in this case in the middle) we use a construction line like already explained bellow.
    
    
    * Then we reach a point where we wanna cut through the object and that can be donr using "difference" option under "solid model as"
    
     {{<image src="../images/4week/difference.jpg" alt="Size of png extantion." size="50%" >}}
    
    
    
Now, we are going to do the circle for top of the chair.

For that we open a new file.

1. Draw a circle
    
    {{<image src="../images/4week/circle.jpg" alt="Size of png extantion." size="50%" >}}
    
2. Choose correct diameter : 300mm
    
    {{<image src="../images/4week/diameter.jpg" alt="Size of png extantion." size="50%" >}}
    
3. Extrude 
    
    {{<image src="../images/4week/extruding.jpg" alt="Size of png extantion." size="50%" >}}
    
4. 10 mm thickness
    
    {{<image src="../images/4week/thickness.jpg" alt="Size of png extantion." size="50%" >}}
    
5. create some cuts
    
    * do the work plain again using point and the normal of the plain.
    
    Follow the same procedures of the retangle, extruding and difference.
    
    {{<image src="../images/4week/rect_circle.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/circle_middle_pro.jpg" alt="Size of png extantion." size="40%" >}}
    
    We will choose a thickness of 10 mm and we want to repeat it 4 times.
    
    This is how it looks after this steps:
    
    {{<image src="../images/4week/circle_final.jpg" alt="Size of png extantion." size="50%" >}}
    
    
11. ### Lets finally assembly
    
* Create a new file called "assembly" 

* Import the leg_bottom.slvs
    
    {{<image src="../images/4week/button.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/import_button.jpg" alt="Size of png extantion." size="40%" >}}
    
    
    * Place the origin of the axiz on the middle of the object as shown on the image bellow. We have to select those two red points and do "contrains" --> "On Point".
    
    {{<image src="../images/4week/origin.jpg" alt="Size of png extantion." size="50%" >}}
    
    * align the orientation of the axiz correctly. Click on the up normal and press the y-axiz (same axis of the normal)
    
    {{<image src="../images/4week/axiz.jpg" alt="Size of png extantion." size="50%" >}}
    
    
* Import the leg_top.slvs
    
    * Do the same idea as for the previous file. Pick the a normal and choose the axiz you want the normal to be paralell with in order to rotate so it can be prependicular with the previous object. See the image bellow for better understanding.
    
    {{<image src="../images/4week/axiz_2.jpg" alt="Size of png extantion." size="50%" >}}
    
    * Afterwards click "contrain"--> "Same Orientation". And this is what will happen.
    
    {{<image src="../images/4week/axiz2_final.jpg" alt="Size of png extantion." size="50%" >}}

    
* Finally, snap the middle point of each of the objects with eachother. 
    
    * do it by clicking on both points and "constrain" --> "On Point"
    
    {{<image src="../images/4week/snap.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/snaped.jpg" alt="Size of png extantion." size="40%" >}}


* Lastly we have also to import the surface.
    
    * Its exaclty the same thing.
    
    * First we pick the normal and we choose the axiz we want the normal to be paralell with following by "contrain"--> "Same Orientation"
    
    {{<image src="../images/4week/axiz_3.jpg" alt="Size of png extantion." size="40%" >}}
    
    {{<image src="../images/4week/rotated.jpg" alt="Size of png extantion." size="40%" >}}
    
    * Next we have to snap the middle point of the objects together.
    Once again we do that by clicking the two red points and "constrain" --> "On Point".
    
    {{<image src="../images/4week/points_3.jpg" alt="Size of png extantion." size="50%" >}}
    
    By doing that we finally reach the final product as shown on the image bellow.
    
    {{<image src="../images/4week/final_object.jpg" alt="Size of png extantion." size="50%" >}}
    
    
12. ### How to export:
    
If we try to export all the elements it will trow an error. 
    
The smarts option will be to export them individually as follows:
    
* Open to one of the files for example "leg_top.slvs"
    
* Export as "Triengle Mash" and save.
    
13. ### Other important information:
    
We might need this drawing for the laser cutting machine for example. And for that we have to export it as a 2 Dimentional drawing.
    
We can do so by doing the following steps:
    
* We choose the surface that is going to represent the outline of it.     
 Make sure the following button is enable
    
{{<image src="../images/4week/enable.jpg" alt="Size of png extantion." size="50%" >}}
    
* Go to "File" --> "Export 2d Section".
    
* Safe as pdf.

## My projects Design

After following the professor's tutorial I decided to explore more the software by starting designing my own project ideas.

The project ideas I liked the most were the measuring cup and speaker. 

- Measured cup:

{{<image src="../images/4week/mea_cup.jpg" alt="Size of png extantion." size="50%" >}}

- Speaker:

{{<image src="../images/4week/speaker_1.jpg" alt="Size of png extantion." size="50%" >}}


## Comparation between SolveSpace and Fusion 360

I started this page by saying that I really wanted to learn [Fusion 360](https://www.autodesk.com/products/fusion-360/overview?term=1-YEAR&tab=subscription) but I couldn't manage to have the student access and thus I decided to go with SolveSpace.
    
Weeeell, after seen lots of my colleagues actually using Fusion 360 and managing to have the student right I decided to not give up and fight for what i wanted in the first place.
    
Therefore I decided to use Fusion 360 as the comparation software for SolveSpace.
    
I'm still using the 30 day free trail but I hope to have this problem solved by the end of the trail.
    
In order to learn how to use the software I watch a [youtube tutorial](https://www.youtube.com/watch?v=mK60ROb2RKI) (classic). I tried to choose an object that had feauture I will need latter for my project.
    
Before moving to the comparation part I would like to show a bit of what I learned from this software.
    
1. ### Before 3D we have to do 2D
    
It's importante to understad that we have to do a good 2D design before moving to the extruding part.
    
In order to enter the scketch mode we just have to press the following button or the shortkey "l".
    
{{<image src="../images/4week/scketch_button.jpg" alt="Size of png extantion." size="30%" >}}
    
To exit the scketch mode press the following button or press the sortkey "q".
    
{{<image src="../images/4week/out_scketch.jpg" alt="Size of png extantion." size="30%" >}}

Here's an example of a scketch:
    
{{<image src="../images/4week/scketch.jpg" alt="Size of png extantion." size="40%" >}}
    
### 2. Make sure you get rid off all constraints (DoF).

You will know when you're good to go when the lines change from blue to black:
    
{{<image src="../images/4week/constraints.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/4week/no_constraints.jpg" alt="Size of png extantion." size="40%" >}}

    
### 3. Several tools I learned:
    
* **Chamber**

    Removes or adds matterial to the edges
    
{{<image src="../images/4week/chamber.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/4week/cilinder_chamber.jpg" alt="Size of png extantion." size="40%" >}}
    
* **Fillet**
    
Same idea as before but it rounds the edges instead of mantaining them sharp.
    
{{<image src="../images/4week/fillet.jpg" alt="Size of png extantion." size="40%" >}}
    
    
* **Extrude**
    
Extrude functions the same as in SolveSpace or any CAD software and it does what the name implies which is extruding.

{{<image src="../images/4week/extrude.jpg" alt="Size of png extantion." size="40%" >}}

* **Inspect**
    
Under inspect select "Section Analysis". By doing that we can cut the object over a choosen plane.
    
{{<image src="../images/4week/inspect.jpg" alt="Size of png extantion." size="50%" >}}
    
As you can see by the image we have material in the interior and a way to remove it is by using the Modify tool.
    
    
* **Modify**
    
We can think of modify as the opposite of extrude. It will basically extract the interior.
    
{{<image src="../images/4week/modify.jpg" alt="Size of png extantion." size="50%" >}}
    
{{<image src="../images/4week/modify_result.jpg" alt="Size of png extantion." size="40%" >}}
    
    
* **Offset plane**
    
It can be very useful to use offset planes. In this case we used it to add a new component on top of that plane.
    
{{<image src="../images/4week/offset_plane.jpg" alt="Size of png extantion." size="50%" >}}
    
On the following image you can see where the plane went:
     
{{<image src="../images/4week/plane.jpg" alt="Size of png extantion." size="50%" >}}
    
After which we would snapp the new created component on it.
    
* **Create new components**
    
We can create a new component by doing the following:
    
{{<image src="../images/4week/new_component.jpg" alt="Size of png extantion." size="40%" >}}
    
When creating a new component we enter a new timeline as you can see by the end of the following image:

{{<image src="../images/4week/timeline.jpg" alt="Size of png extantion." size="30%" >}}
    
* **Align**
    
Finally, I learn how to align two different objects. Which is so usefull.
    
{{<image src="../images/4week/align_tool.jpg" alt="Size of png extantion." size="40%" >}}
 
Then we choose the lines we want them to be snapped into and hit "ok".
    
As you can see by the image bellow, the filter is perfectly snapped on top of the coffe cup.
    
{{<image src="../images/4week/fits_perfect.jpg" alt="Size of png extantion." size="40%" >}}
 
    
## Comparation
    
After spending some time learning Fusion 360 i fill like is much more easy going and intuitive than SolveSpace. It has easy shortkeys and overall a more friendly layout. The fact that it has all the layers on the left side and a timeline on the bottom makes it more easy to manage.
    
The only "con" i can think about when using Fusion 360 is that its not free and open source and therefore that's the only "pro" i can think for SolveSpace.
    
SolveSpace it's a bit messy, confusing to read the timeline and overall doesn't have an appealing and friendly look.
    
## Conclusion
    
I will most likely pick Fusion 360 to design my CAD models. 

I really got allong well with it and i feel like I already learned 70% of what i will need to scketch my final project. For example, the extruding, the chamber and filter to smoth out edges, and the "Thread" tool.
    
Later on the week I'll edit this page again with the CAD model of my project made in Fusion 360.
    
My goal will also be solving my student access rights, which has been a real challenge.


