---
date: 2023-05-18T10:58:08-04:00
description: ""
featured_image: "/images/wildcard_1.jpg"
tags: []
title: "Week 17: WildCard Week"
weight: 160
---

This week we could pick whichever machine we wanted from a list provided to us.

Me and some other colleagues suggested doing the “Ceramics and Glass” workshop. The professor accepted and since it’s the first time we do this workshop he had to inform himself about it.

- **Ceramics**
I had some friends from bachelors design doing that workshop so I asked to teach me how to do ceramics.

I made time-lapse of me doing the ceramics. 

The most important things I’ve been taught is the following.

* The clay has to be fairly wet and in order to not let the big block dry we have to wrap it really well in a plastic bad
* We have to massage the clay or just trow it hard to the surface to make sure we remove all the bubbles and prevent from cracking later when dry.
* When combining pieces together, we have to cut the parts we are going to connect. You can see that in the video better.
* Also, if we are making a tall piece we should let the bottom piece dry a little bit so then, when inserting the up piece, the bottom doesn’t get smooshed. (I didn’t hade time to wait for it to dry so I had to work with it the best I could)
* Finally, we should create a pocket at the end of the piece so it’s not to thick. Otherwise it takes longer to dry. 
* I was also recomemded to make some holes in those thicker areas if I don’t mind to have them in the design.

I made two pieces. My initial idea was to make a 10*10*10 bowl so then I could scan it and 3D print it in natural PLA so then I could make the same object out of glass. 

However, on my way to do it, I realise that if I do those sizes the bowl will be big enough for a mouse to drink milk from it. Therefore I just decide to do a different design for the glass and just continue with the clay object with better sizes. 

So I first do a flower pot where I use one kind of technique and then I finally do the bowl I wanted with another technique. The first one we just connect the two pieces vertically (I dont know the name of the technique) but as for the second one, it’s called sausage technique because we build the mug higher and higher by adding sausages shaped pieces of clay on top of each other.

This second technique makes the piece more wobbly which I like, whereas the first one makes the pieces look more straight and firm.

Following is the videos of the time lapse I did.

{{< youtube px4mHeQCm6s  >}} 

{{< youtube sCN9Jz2HDVA  >}} 

{{< youtube ITPizrGFVgY  >}} 

Here are some photos I took.

I received this block of clay.

{{<image src="../images/17week/block_clay.jpg" alt="Size of png extantion." size="40%" >}}

And those are the tools I need to make the pieces. There’s more options but I went for those.

{{<image src="../images/17week/tools.jpg" alt="Size of png extantion." size="40%" >}}

Like I mentioned above we have to wrap the clay we are not working on with a plastic bag to prevent it to dry.

{{<image src="../images/17week/tno_dry.jpg" alt="Size of png extantion." size="40%" >}}

Following is a video showing the throwing we have to do to remove the bubbles.

{{<video src="../images/17week/trowing_v.mp4">}}{{</video>}}

Finally, this is how the first piece turned out to be after the first day I work with it. I didn’t took a picture of the second piece since I forgot. But its in the time lapse.

{{<image src="../images/17week/1st_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_4.jpg" alt="Size of png extantion." size="40%" >}}

Then this is how the pieces look like after the second day, where I also draw on the outside.

{{<image src="../images/17week/2nd.jpg" alt="Size of png extantion." size="40%" >}}

{{<video src="../images/17week/1st_2nd_1_v.mp4">}}{{</video>}}

{{<video src="../images/17week/1st_2nd_2_v.mp4">}}{{</video>}}

{{<video src="../images/17week/1st_2nd_3_v.mp4">}}{{</video>}}

Then, ive been taught that if I wanna apply glaze on the pieces I shouldn’t kiln them in full max time and temperature but do it in two times. 

So I first have to put the pieces to dry in the kiln a first time. Then apply the glaze and finally kiln them again for the last time. Where the oven will be hotter. 

After the second kiln they will be ready.

So I first put them to the kiln the first time.

{{<image src="../images/17week/dry_kiln.jpg" alt="Size of png extantion." size="40%" >}}

And this is the final result when they get out of the kiln. They have a more orange colour now.

{{<image src="../images/17week/1st_kiln_1.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_kiln_2.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_kiln_3.jpg" alt="Size of png extantion." size="40%" >}}

{{<image src="../images/17week/1st_kiln_4.jpg" alt="Size of png extantion." size="40%" >}}

Now, I should glaze and put them in the kiln for the last time. 

However because the school already ended and the workshops are closed they suggested me to come back in September to glaze the pieces and kiln them for the last time. 

So that’s what I have left to do.

About the glazes I will use the default ones they have or ill make my own. It’s not hard, I just have to follow the recipes that already exist and then I have to mix the powder with water.

- **Glass**

About the glass we had an introduction class about it where we were taught how to do it and the procedure we have to take.

First we have to do a design in a CAD software where we have to be cautious with the measurements we pick. 

The object cant be bigger than 10*10*10 and it can be thinner than 6mm minimum. The reason is that the glass won’t go through thiner pockets than 6mm. 

Finally we have to make a cone where we will be letting the glass melt through it. 

We have to 3D print in natural PLA the object we want to have in glass at the end. After that we have to put plaster all around it considering the cone.

Then we let the plaster set. 

After seeing we melt the PLA form the plaster, so we have to consider making a hole on the design.

Next we have to know how much glass our object will need. For that we have to know the volume of our object. We can see it easily in any CAD software. I personally use Fusion. 

After that the ratio is 1:1. Which means that we will have to make a cone with the same volume as the object we made. And that’s how we know how much glass we need.

For example, let’s imagine my object has a volume of 10cm^3, that means I have to make a cone with the volume of 10cm^3. So then, later I will pour enough glass on the cone until it gets full.

Finally, we let the glass melt through the cone and to the plaster mold. 

If we make the measurements correct, meaning if we have a minimum thickness of 6mm and a cone with the same volume as the object, everything should work at the end.

This process of the glass melting might take up to 1-3 days depending on the size of the object.

What I have made until now was design something in Fusion.

As I already mentioned I have some friends doing “ceramics and glass” workshop and I show their glass pieces and I got so in love by the jewellery that’s I thought on doing the same.

I thought on making this dog footprints and this is how it turned out.

I first made the sketch.

{{<image src="../images/17week/first_sketch_of_animal.jpg" alt="Size of png extantion." size="40%" >}}

Then I extrude more than 6mm. I did 8mm.

{{<image src="../images/17week/engrave_1mm.jpg" alt="Size of png extantion." size="40%" >}}

Then I made some fillets so it’s easy to remove. 

{{<image src="../images/17week/fillets_so_easy_to_remove.jpg" alt="Size of png extantion." size="40%" >}}

Then I make two to have the pair.

{{<image src="../images/17week/make_two_hearings.jpg" alt="Size of png extantion." size="40%" >}}

Then just before printing the pieces I talked with my friends and they said that since I can melt the PLA out from the plaster I should make harder shapes. 

I thought about that and that’s true. I should make harder and interesting shapes, but right now, it’s the end of the course and I’m so exhausted to think about any nice shape I wanna have from glass. 

So, since we cant put the plaster neither use the glass workshop I decided to make a new design later during summer and print in PLA when I cone back to Finland in august. 

I already described what I have to do and the procedure and those are the rules I will follow when making the new design.



