---
date: 2023-05-25T10:58:08-04:00
description: ""
featured_image: "/images/video_1.jpg"
tags: []
title: "Week 19: Invention, IP and Income"
weight: 180
---

As for the final assignment we were asked to discuss our future plans for the final project as well as pick an open source licence for our website and project.

### My future plans for the final project

As already mentioned at the end of my project I still have some final adjustments I wanna do to the project.

1. I need to make a cut on the plywood output box so it’s easier to open the box again after closing it.
2. I gotta improve the small support for the brain to prevent from it to tilt to the side. Also substitute the zip tie for a PLA printed piece
3. Write on the website the amount of participants through the day as well write the description of the project
4. Finally, make the paper where I show the words to the corresponding buttons.

This is pretty much what I have left to do for the final project and exhibition.

However, if I’m allowed to dream big, I could see this kind of prototype as a study or game that the schools and universities could have as an experiment to see and measure the overall mental health of the students. 

It could either be one time exercise or a fixed piece places in a specific place (like vare building) where the people could go there every day and press how are they feeling. 

Of course more improvement had to be done if this were to be part of a study. Since it’s about mental health people might be seeking for help and therefore the website could also display some words or help contacts in case a bad feeling is selected.

This is just me throwing ideas here but who knows. We gotta think big. 

### Comparing 3 open source licenses

We were asked to compare 3 open source licenses. I found this website on the internet that gives a brief explanation of the most used licenses out there as well as the “Premissions”, “conditions” and “limitations”. 

I also decided to ask chatGPT about it and it suggested me the “MIT License”, the “GNU General Public License (GPL)” and the “Apache License 2.0”.

As we all know by now chatGPT also provide us with good explanations. So now I know that “MIT License” allows people to use, modify, and distribute the source code of my website for both commercially and non-commercially purposes. It’s also known for its simplicity and flexibility. Its indeed not a bad option.

About the “GPL” license its state that is a copyleft license which means that the software and any modifications stays open source for whoever wants to see it. In case someone proceeds with the project or does any modification, the same as to be made under the same license. That is kinda restricted. Not suitable for me for sure.

Finally, “Apache License 2.0” its another open source license that allows people to freely use it, modify it and distribute it including for commercial purposes. This license also provides some patent protections which is suitable if I wanna patent my final project. That sounds like a good option.

Nevertheless I also asked chatGPT if there was any license that would have copy rights. Meaning that every time somebody would use my content they would have to mention the author (me)

Turns out that “Creative Commons Attribution (CC-BY)” license does exactly that. 

CC-BY is also an open license that allows people to share, change and use my content for whatever purpose such us commercial purposes as long as they attribute the work to the original author (me). That sounds nice. 

It’s a license that allows people to use my things as starting point to grow further or even just as an experiment about mental health like I mentioned already. That would make me really happy and proud if that would happen so yha I wanna be able to share the work with the others. And off course being names for the works its nice feeling of consideration and therefore I’m gonna go for this license as my final pick.

**More information about CC-BY license**

If I use CC-BY license anyone using my content has to give me the credits by providing my name and user name and indicating that I was the creator of the content. They must also include a link to the original work if it’s available online. Which in my case its this website.

An important think to know about this licence is that it doesn’t apply for the whole website but just to specific content I choose to license. That’s not ideal but I’ll stick with this one anyway.

So, I might drop out of this idea and find a new license.

I saw this one CERN Open Hardware License v2 Permissive that has what I need. It has basically the same things as CC-BY but I can just apply it to my whole website. Although cern specifically create it for open-source hardware projects. 

Another good thing about this license is that does not include copyleft provisions. Which means that derivation or modifications of the original work dont have to be under the same license. Its the opposite of GPL for example.

I wasn’t sure what was the procedure to integrate a license in the website, if I had to fill a form or something so I asked chatGPT and it said “When you include the license information on your website, as mentioned in the previous response, it serves as a notice to visitors that your hardware design is licensed under the CERN OHL v2. By including the license text and providing a link to the full license on the CERN website, you fulfill the requirement of clearly indicating the licensing terms to users.”

Which means I literally just have to mention I have the license and provide a link for more information about it. Sounds amazing.

I was suggested to show the license I’m using in the README.md file. I put it both there and in the front page of my website in the “_index.md” file.

Finally we were also asked to make a 1 minute video about the final project as well as a poster.

Click [here](https://arianamarta.gitlab.io/digital-fabrication/presentation.mp4) for the video and [here](https://arianamarta.gitlab.io/digital-fabrication/presentation.png) for the poster.





